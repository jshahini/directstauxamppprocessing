#!/bin/bash

# Script for retrying all partially 'finished' or 'exhausted' PanDA jobs using pandamonium.
# Can be configured to repeat resubmission of jobs every few hours.
# Usage: $> source retryPanDAJobs.sh


# ------------------------ CONFIGURATION ----------------------------
# Production tag used for the initial submission of the jobs:
PRODUCTIONTAG="XAMPP.HadHad.DexDel3v2_"
# Look up jobs submitted within the past N days:
LOOKUP_DAYS=40
# Repeat resubmission of finished jobs N times (1 = do it only once):
NITERATIONS=1
# Wait N hours between repetitions:
STEPTIME_HOURS=2
# -------------------------------------------------------------------

# TWiki:  https://twiki.cern.ch/twiki/bin/view/PanDA/PandaJEDI
# GitHub: https://github.com/dguest/pandamonium

if [[ -z "$SINGULARITY_INIT" ]]; then
    printf "ERROR : Please enter a SLC6 environemnt first\n"
    return 1
fi

DSALOOP_PYTHONDIR="$(echo $PYTHONPATH | tr ':' '\n' | grep "DSAnalysisLoop/python")"
if [[ -z "$DSALOOP_PYTHONDIR" ]]; then
    printf "ERROR : Please set up python paths (lsetup python && source init_python.sh)\n"
    return 1
fi

if [[ -z "$PANDA_SYS" ]]; then
    printf "ERROR : Please setup the PanDA client first (lsetup panda)\n"
    return 1
fi

STARTTIME=$SECONDS
STEP=0
while [ $(($SECONDS-$STARTTIME)) -lt $(($NITERATIONS*$STEPTIME_HOURS*60*60)) ]; do
    printf "INFO : Updating status of PanDA jobs...\n"
    for ID in $(pandamon "user.$RUCIO_ACCOUNT*$PRODUCTIONTAG*" -d $LOOKUP_DAYS --clean | grep -E "finished|exhausted" | awk '{print $2}'); do
        python $DSALOOP_PYTHONDIR/tools/pandamonium/panda-resub-taskid $ID
        printf "INFO : Resubmission of JobID=${ID} completed\n"
        sleep 1
    done
    STEP=$((STEP+1))
    printf "INFO : $STEP/$NITERATIONS iterations completed\n"
    if [ $((STEP)) -eq $((NITERATIONS)) ]; then
        break
    fi
    sleep $(($STEPTIME_HOURS*60*60))
done

printf "INFO : Exiting gracefully\n"
return 0
