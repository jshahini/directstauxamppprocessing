#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import re
import sys
import glob
import json
import itertools
import collections
import ConfigParser

from tools.logger import logger
from tools.IOManager import IOManager
from tools.NtupleParser import NtupleParser
from tools.LumiCalculator import CalculateLumi
from tools.sample import Sample, GuessProcess, GuessGenerator

codepath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
logger.debug("DSAnalysisLoop root path: {}".format(codepath))

def makeDir(path):
    if not os.path.exists(path):
        os.makedirs(path)

class ProjectHandler(object):
    def __init__(self, **options):
        self.DAODs = collections.defaultdict(set) # {category: daod}
        self.NTUPLEs = collections.defaultdict(dict) # {category: {ntuple: {<parsed info>}}}
        self.daodntupledict = {}
        self.options = options
        self.periodsdict = {}
        self.foundsamiddaods = False
        self.config = ConfigParser.ConfigParser()
        config_file = self.options.get("config")
        self.config.read(config_file)
        self.prodname = self.config.get("PATHS", "ProductionName")
        self.proddir = os.path.join(codepath, "productions", self.prodname)
        self.NtupleParser = NtupleParser(self.proddir)
        self.version = "V{}".format(str(self.options.get("version")).rjust(2, '0'))
        self.projectdir = os.path.join(self.config.get("PATHS", "OutputPath"), self.prodname, self.version)
        logger.info("The current production is: {}".format(self.prodname))
        if os.path.exists(self.projectdir):
            logger.error("Project {} already exists! Overwrite?".format(self.version))
            if not raw_input("  (Y/N) : ") in ["y", "Y", "yes", "Yes", "YES"]:
                logger.info("Exiting...")
                sys.exit(0)
        try:
            os.remove(os.path.join(codepath, "CWD"))
        except OSError:
            pass
        if self.config.get("CONFIG", "OverrideMetadata"):
            self.overridemetadata = os.path.join(self.proddir, "override_metadata.txt")
            if not os.path.isfile(self.overridemetadata):
                logger.error("'{}' does not exist!".format(self.overridemetadata))
                sys.exit(0)
            logger.warning("Metadata will be overwritten for DSIDs {}!".format( \
                ", ".join([l.split()[0] for l in open(self.overridemetadata, "r").readlines() if not l.startswith("#")])))
        logger.info("Creating symbolic link {}".format(codepath, "CWD"))
        os.symlink(self.projectdir, os.path.join(codepath, "CWD"))
        self._createProjectDirs()

    def RetrieveNTUPLEs(self):
        """Get a list of all available NTUPLEs, parse and sort them with respect to their category (DATA/MC16a/...).
           Only those containerLists specified in the 'InputLists' section of the config.ini are considered."""
        logger.info("Starting to parse NTUPLEs...")
        for x in json.loads(self.config.get('FILES', 'InputLists')):
            print(x)
        for containerList in json.loads(self.config.get('FILES', 'InputLists')):
            niter = sum(1 for line in open(os.path.join(self.proddir, "containerLists", containerList), "r").read().splitlines())
            for i, ntuple in enumerate(open(os.path.join(self.proddir, "containerLists", containerList), "r").read().splitlines()):
                print("Getting ntuple info")
                ntupleinfo = self.NtupleParser.Parse(ntuple)

                print("Got ntuple info")
                self.NTUPLEs[ntupleinfo["Category"]][ntuple] = ntupleinfo
                sys.stdout.write("\r" + "{}: {}/{} NTUPLEs parsed".format(containerList,i+1,niter))
                sys.stdout.flush()
            sys.stdout.write("\n")

    def RetrieveDAODs(self):
        """Get a list of all DAODs used for the current production and sort them with respect to their category (DATA/MC16a/...)."""
        periodsdict = {}
        try:
            pdict = json.load(open(os.path.join(self.proddir, "periodsdict.json"), "r"))
            for periodcontainer, runlist in pdict.iteritems():
                periodsdict[periodcontainer] = set(runlist)
            logger.info("Found {} period containers for DATA, corresponding to {} runs in total.".format(
                len(periodsdict.keys()),
                len(list(itertools.chain(*periodsdict.values())))))
        except IOError:
            logger.info("No periods dict found.")
        logger.info("Looking up DAODs...")
        for daodlist in [d for d in RecurseDir(os.path.join(self.proddir, "DAODs")) if d.endswith(".txt")]:
            if daodlist.endswith("unavailable.txt"):
                continue
            for daod in open(daodlist, "r").read().splitlines():
                if not daod or daod.startswith("#"):
                    continue
                if daod.startswith("data") and periodsdict:
                    self.DAODs[Sample(daod).Category()] |= periodsdict[daod]
                else:
                    self.DAODs[Sample(daod).Category()].add(daod)

    def CompletenessCheck(self):
        """Check whether an NTUPLE exits for every initial DAOD. Multiple DAODs may get merged into one NTUPLE container."""
        for category, initial_daods in self.DAODs.iteritems():
            available_daods = [] # list of DAODs that were used as input for all ntuples of the curent category
            for ntuple, info in self.NTUPLEs[category].iteritems():
                available_daods += info["DAODs"]
            missing_daods = list(set(initial_daods)-set(available_daods))
            if len(missing_daods) == 0:
                logger.info("All requested {} datasets are available ({}/{}).".format(category, len(available_daods), len(initial_daods)))
            else:
                logger.warning("Not all requested {} datasets are available ({}/{}). Update your containerLists (and rootdPaths.txt)!".format(
                    category, len(available_daods), len(initial_daods)))
                logger.warning("**NO** NTUPLEs have been found for the the following {} DAODs:".format(category))
                for daod in sorted(missing_daods):
                    print "\033[1;31;40m{}\033[0m".format(daod.split(":")[1])

    def CalculateLuminosity(self):
        # MC subcategory to data taking period dictionary
        # see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC16
        MCSCDTPdict = {"MC16a": ["data15", "data16"], # matches 2015+2016 dataset + mu profile
                       "MC16b": None,                 # uses higher mu (30-70) for trigger and CP studies for 2017 data
                       "MC16c": ["data17"],           # uses expected 2017 mu profile, geometry and trigger
                       "MC16d": ["data17"],           # uses true 2017 mu profile
                       "MC16e": ["data18"],           # uses expected 2018 mu profile and trigger
                       "MC16f": ["data18"]}           # uses true 2018 mu profile if required
        LumiCalc = json.loads(self.config.get('FILES', 'LumiCalc'))
        logger.info("Starting calculation of integrated luminosity...")
        # Get runNumbers from NTUPLEs and LumiCalc files
        data_nutple_paths = []
        for line in open(os.path.join(self.proddir, "rootdPaths.txt"), "r").read().splitlines():
            ntuple, path = line.split()
            if ntuple in self.NTUPLEs["DATA"].keys():
                data_nutple_paths.append(path)
        TotalNTUPLERunNumbers = IOManager.LoopTree(*data_nutple_paths,
            tree="MetaDataTree",
            branches=["runNumber"])["runNumber"] # all in the NTUPLEs available runNumbers (of all data taking periods)
        LumiCalcRunNumbers = {} # runNumber per data taking period that are available in the respective LumiCalc file
        for period, lumiCalcFile in LumiCalc.iteritems():
            LumiCalcRunNumbers[period] = IOManager.LoopTree(lumiCalcFile,
                tree="LumiMetaData",
                branches=["RunNbr"])["RunNbr"]
        # Compute Luminosity for each MC subcampaign
        self.Luminosity = {}
        for category in [c for c in self.NTUPLEs.keys() if c.startswith("MC")]:
            RunNumbers = list(set(TotalNTUPLERunNumbers) - \
                set([r for period, runNumbers in LumiCalcRunNumbers.iteritems() if not period in MCSCDTPdict[category] \
                for r in runNumbers]))
            if RunNumbers:
                logger.info("{} RunNumbers found for {}. Calculating luminosity...".format(len(RunNumbers), "+".join(MCSCDTPdict[category])))
                self.Luminosity[category] = CalculateLumi(LumiCalc, RunNumbers)
                logger.info("Calculated a luminosity of {:6.2f} fb^-1 for {}. All {} samples will be reweighted to this value.".format(self.Luminosity[category], "+".join(MCSCDTPdict[category]), category))
                if self.options["luminosity"]:
                    logger.debug("Found RunNumbers! Will ignore the manually set luminosity value!")
            elif self.options["luminosity"]:
                self.Luminosity[category] = self.options["luminosity"]
                logger.info("Luminosity was manually set to {} fb^-1! All {} samples will be reweighted to this value.".format(self.Luminosity[category], category))
            else:
                logger.error("No RunNumbers found for {}! This would result in a luminosity of 0 fb^-1. If no Data is processed, you need to set up the luminosity manually with the -l option!".format("+".join(MCSCDTPdict[category])))
                sys.exit(0)
        logger.info("The total luminosity is {:6.2f} fb^-1.".format(sum(self.Luminosity.values())))
        self.Luminosity["DATA"] = 1.0
        self.Luminosity.setdefault("MC16a", 0.0)
        self.Luminosity.setdefault("MC16d", 0.0)
        self.Luminosity.setdefault("MC16e", 0.0)

    def WriteJobOptions(self):
        self.joboptionspath = os.path.join(self.projectdir, "joboptions.json")
        save = {}
        save["ProductionName"] = self.config.get("PATHS", "ProductionName")
        save["Version"] = self.version
        save["NominalTree"] = self.options.get("nominalTree")
        save["MetadataTree"] = self.options.get("metadataTree")
        save["Luminosity"] = self.Luminosity # dict
        fakefactorsfile = None
        if self.options.get("applyFakeFactors"):
            fakefactorsfile = self.config.get("FILES", "FakeFactors")
        save["ApplyFakeFactors"] = fakefactorsfile
        save["FFMTargetSelection"] = self.options.get("FFMTargetSelection")
        save["OverrideMetadata"] = self.overridemetadata
        # save["availableNTUPLEs"] = self.availableNTUPLEs # dict
        # Add new options (that you have specified in python/tools/ArgParser.py::__init__) here:
        save["NoTrigger"] = self.options.get("noTrigger")
        save["MuonSelection"] = self.options.get("MuonSelection")
        save["SignalTauID"] = self.options.get("signalTauID")
        save["SelectAntiTaus"] = self.options.get("selectAntiTaus")
        save["ttbarMETSlicing"] = self.options.get("ttbarMETSlicing")
        logger.info("Writing {}".format(self.joboptionspath))
        with open(self.joboptionspath, "w") as joboptions:
            json.dump(save, joboptions, indent=2)

    def WriteDatasetDict(self):
        self.datasetdictpath = os.path.join(self.projectdir, "datasetdict.json")
        logger.info("Writing {}".format(self.datasetdictpath))
        d = {}
        for v in self.NTUPLEs.values():
            d.update(v)
        with open(self.datasetdictpath, "w") as datasetdict:
            json.dump(d, datasetdict, indent=2)

    def _createProjectDirs(self):
        logger.info("Creating project directories at {}".format(self.projectdir))
        makeDir(self.projectdir)
        for dstype in ["Data", "Background", "Signal"]:
            makeDir(os.path.join(self.projectdir, dstype))



def RecurseDir(dirname):
    paths = []
    for root, subdirs, files in os.walk(dirname):
        for f in files:
            paths.append(os.path.join(root, f))
        for subdir in subdirs:
            RecurseDir(os.path.join(root, subdir))
    return paths


def main(**options):

    PH = ProjectHandler(**options)

    print("Retrieving ntuples...")
    PH.RetrieveNTUPLEs()

    print("Retrieving DAODs...")
    PH.RetrieveDAODs()
    
    print("Doing completeness check...")
    PH.CompletenessCheck()
    
    print("Doing calculating luminosity...")
    PH.CalculateLuminosity()
    
    print("Writing dataset dict...")
    PH.WriteDatasetDict()
    
    print("Writing Job options...")
    PH.WriteJobOptions()


if __name__ == '__main__':

    from tools.ArgParser import ArgParser

    parser = ArgParser(description='Create new DSAnalysisLoop project')
    parser.add_argument('-v', '--version',
        required=True,
        type=int,
        help='supply version')
    parser.add_argument('-c', '--config',
        default=os.path.join(codepath, "config.ini"),
        type=str,
        help='explicitly specify config')
    parser.add_argument("--applyFakeFactors",
        default=False,
        action="store_true",
        help="produce output trees with a data-driven estimate for the QCD background (default: %(default)s)")
    options = parser.retrieve_args()

    main(**options)
