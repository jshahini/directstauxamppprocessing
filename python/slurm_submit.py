#!/usr/bin/env python3

import os
import sys
import json
import argparse
import collections

from tools.logger import logger
from tools.sample import Sample
from tools.Chunkinator import Chunkinator
from slurmy import Slurm, JobHandler, Theme

argparser = argparse.ArgumentParser("slurm job handler")
argparser.add_argument("-c", "--chunk", type=int, metavar="N", default=None, help="split data containers into chunks of N input files each (default: %(default)s)")
slurmoptions = vars(argparser.parse_args())

codepath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
logger.debug("DSAnalysisLoop root path: {}".format(codepath))

# Load joboptions and dataset info from current project:
joboptions = json.load(open(os.path.join(codepath, "CWD", "joboptions.json"), "r"))
datasetdict = json.load(open(os.path.join(codepath, "CWD", "datasetdict.json"), "r"))

prodname = joboptions["ProductionName"]

tauWPDict = dict(default=-1, loose=1, medium=2, tight=3)

# Load paths to ntuples
tmp_pathdict = collections.defaultdict(list)
for line in open(os.path.join(codepath, "productions", prodname, "rootdPaths.txt"), "r").read().splitlines():
    container, path = line.split()
    tmp_pathdict[container].append(path)

# Split pathdict into chunks for faster processing of large datasets (data only!)
pathdict = {}
for container, paths in tmp_pathdict.items():
    if not container in datasetdict.keys():
        continue
    pathdict[container] = {}
    nchunks = chunksize = None
    if datasetdict[container]["Type"] == "Data":
        chunksize = slurmoptions["chunk"]
    else:
        nchunks = 1
    for i, chunk in Chunkinator(paths, chunksize=chunksize, nchunks=nchunks).items():
        pathdict[container][i] = chunk
if slurmoptions["chunk"] is not None:
    ndatacontainers = len([cont for cont in pathdict.keys() if "data" in cont])
    ndatachunks = len([chunk for cont in pathdict.keys() if "data" in cont for chunk in pathdict[cont].keys()])
    logger.info("Data containers are split into chunks of {} input files each. "
    "This will lead to {} additional jobs.".format(slurmoptions["chunk"], ndatachunks - ndatacontainers))

# Group same ID MC datasets into one job (MC subcampaigns stay separate)
redundant_containers = [] # delete these containers from the pathdict after merging
if prodname in ["AlkalineAzarole", "ArchaicAmalgam", "BetazoidBeetle", "CrispyCyanide"]: # prods. with old format
    for container in pathdict.keys():
        if datasetdict[container]["Type"] == "Data":
            continue
        IDs = datasetdict[container]["IDs"]
        category = datasetdict[container]["Category"]
        if len(IDs) > 1:
            logger.error("Found NTUPLE with multiple input DSIDs ({}): {}".format(", ".join(IDs), container))
        sameID_containers = [c for c in pathdict.keys() if \
            datasetdict[c]["IDs"][0] == IDs[0] \
            and datasetdict[c]["Category"] == category \
            and c != container]
        if len(sameID_containers) > 0:
            logger.info("Found {} {} datasets with the same ID: {}. Inputfiles will be merged into one job!".format(len(sameID_containers)+1, category, IDs[0]))
            for sameID_container in sameID_containers:
                pathdict[container][1] += list(pathdict[sameID_container][1])
            redundant_containers += sameID_containers
for deleteme in list(set(redundant_containers)):
    del pathdict[deleteme]

# Define run script to be executed by each worker:
run_script = '''
cd {cwd}
module load root/6.10.08
{cmd}
exit $?'''

# Configure individual SLURMY jobs:
scope = "DSALoop"
nchunkdict = {}
jh = JobHandler(name=scope, theme=Theme.Boring, backend=Slurm(partition='lsschaile'), work_dir=os.path.join(os.readlink("CWD"), "slurmy"))
for i, (ntuple, info) in enumerate(datasetdict.items(), start=1):

    if ntuple in redundant_containers:
        continue
    elif ntuple not in pathdict.keys():
        logger.error("No paths found for '{}'! Please update your rootdPaths.txt".format(ntuple))
        sys.exit(1)

    dstype       = info["Type"]
    category     = info["Category"]
    IDs          = info["IDs"]
    daods        = info["DAODs"]
    processes    = info["Processes"]
    subprocesses = info["SubProcesses"]
    derivation   = info["Derivation"]
    luminosity   = joboptions["Luminosity"]
    nominaltree  = joboptions["NominalTree"]
    metadatatree = joboptions["MetadataTree"]

    # TODO: Find a better solution for this?
    if len(processes) > 1:
        logger.warning("{} seems to contain multiple processes!".format(ntuple))
        process = "_".join(processes)
    else:
        process = processes[0]
    if len(subprocesses) > 1:
        logger.warning("{} seems to contain multiple subprocesses!".format(ntuple))
        subprocess = "merged"
    else:
        subprocess = subprocesses[0]
    if len(daods) == 1:
        ID = Sample(daods[0]).ID()
    else:
        ID = "{}_{}".format(min(IDs), max(IDs))

    # Only consider available datasets:
    if ntuple is not None:
        if dstype == "Data":
            nchunkdict[ID] = len(pathdict[ntuple].values())
        for ichunk, paths in pathdict[ntuple].items():
            inputfilepaths = paths

            # Make new unique identifier:
            IDC = "{}_chunk{}".format(ID, ichunk) if len(pathdict[ntuple].keys()) > 1 else ID

            # Build output file names:
            if dstype == "Data":
                outputfile = "{}.{}.{}.root".format(category, IDC, derivation)
            elif dstype in ["Background", "Signal"]:
                outputfile = "{}.{}.{}.{}.{}.root".format(category, IDC, process, subprocess, derivation)
            else:
                logger.warning("Found dataset of unknown type. Skipping...")
                continue
            outputfilepath = os.path.join(codepath, os.readlink("CWD"), dstype, outputfile)

            # Command invoking DSAnalysisLoop:
            cmd = ["./build/DSAnalysisLoop.x"]

            # Append DSAnalysisLoop CL arguments to cmd:
            ## Basic options:
            cmd.append("-ifiles='{}'".format(" ".join(inputfilepaths)))
            cmd.append("-ofile={}".format(outputfilepath))
            cmd.append("-ntree={}".format(nominaltree))
            cmd.append("-mtree={}".format(metadatatree))
            cmd.append("-lumiMC16a={}".format(luminosity["MC16a"]))
            cmd.append("-lumiMC16d={}".format(luminosity["MC16d"]))
            cmd.append("-lumiMC16e={}".format(luminosity["MC16e"]))

            ## Append new options (that you have specified in python/newProject.py::writeJobOptions and in python/tools/ArgParser.py::__init__) here:
            if joboptions["NoTrigger"]: cmd.append("--noTrigger")
            if joboptions["MuonSelection"]: cmd.append("--MuonSelection")
            if joboptions["ApplyFakeFactors"]: cmd.append("--applyFakeFactors={}".format(joboptions["ApplyFakeFactors"]))
            if joboptions["FFMTargetSelection"]: cmd.append("--FFMTargetSelection={}".format(joboptions["FFMTargetSelection"]))
            if joboptions["OverrideMetadata"]: cmd.append("--overrideMetadata={}".format(joboptions["OverrideMetadata"]))
            if joboptions["SignalTauID"]: cmd.append("--signalTauID={}".format(tauWPDict[joboptions["SignalTauID"]]))
            if joboptions["SelectAntiTaus"]: cmd.append("--selectAntiTaus")
            if joboptions["ttbarMETSlicing"]: cmd.append("--ttbarMETSlicing")

            mem = "4000mb" if (process == "Wlnu" or process == "Zll") else "2000mb"

            logger.debug("Will execute the following command:\n{}".format(" ".join(cmd)))
            jh.add_job(backend=Slurm(mem=mem), run_script=run_script.format(cwd=codepath, cmd=" ".join(cmd)), name="DSALoop_{}_{}_{}".format(category, IDC, process))

# Write logfile tracking number of chunks per ID (for data only; IDs are unique)
with open(os.path.join(codepath, os.readlink("CWD"), "slurmchunkslog.json"), "w") as slurmchunkslog:
    json.dump(nchunkdict, slurmchunkslog, indent=2)

# Write logfile holding the now redundant (same-ID) container which have been merged
with open(os.path.join(codepath, os.readlink("CWD"), "mergedcontainers.json"), "w") as mergedcontainers:
    json.dump(redundant_containers, mergedcontainers, indent=2)

# Submit jobs:
jh.run_jobs(interval=2)
