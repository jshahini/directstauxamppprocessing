#!/usr/bin/env python2.7

import logging
logging.basicConfig(format='%(levelname)-8s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

databaseDirectory = "/dev/shm/rucioUtils/"

import os
import sys
import re
import itertools

from tools.sample import Sample

try:
    from rucio import client
    from rucio.client import Client
    from rucio.common.exception import DatabaseException
except ImportError:
    logger.error("Note: To use rucio functions you have to setup rucio within an SLC6 environment!")
    raise
c = Client()

# ptags dict (keep this up to date! can't do it automatically since twiki pages are protected...)
### https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYxAODderivationsr20 (rel 20 & 21.0)
### https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYxAODderivationsr21 (rel 21.2 +)
ptags = {
    "MC": {         # rel 21.0           # rel 21.2
        "skim":     [3212, 3238, 3260] + [3371, 3387, 3401],
        "noskim":   [3215, 3241, 3263] + [3374, 3390, 3404],
    },
    "Data": {       # rel 21.0           # rel 21.2
        "skim":     [3213, 3239, 3261] + [3372, 3388, 3402],
        "noskim":   []
    }
}

class DAODFinder(object):
    """Find DAODs via rucio with a given list of AODs"""

    def __init__(self, txtfile, **kwargs):
        self.options = kwargs
        self.infile = txtfile
        self.process = re.sub(".txt", "", os.path.basename(txtfile))
        self.available = []
        self.unavailable = []

    def AODtoDAOD(self, DID):
        """Get list of candidate DAODs (with skim) from a given AOD.
           Sorted w.r.t. to ptag in descending order."""
        outDID, count = re.subn('AOD', 'DAOD_' + self.options["derivation"], DID)
        if count != 1:
            logger.warning("{} couldn't be replaced to DAOD_{}".format(DID, self.options["derivation"]))
            raise ExtractionException
        outDID = re.sub('merge', 'deriv', outDID)
        sample = Sample(outDID)
        daods = []
        for candidate in [f for f in c.list_dids(sample.scope(), {'name': sample.DID() + "*"})]:
            if re.search("p[0-9]{4}$", candidate):
                thisptag = int(candidate[-4:])
                if thisptag in ptags[sample.datasetType]["skim"]:
                    logger.debug("Found skimmed {} DAOD for DSID {} (ptag: p{}).".format(self.options["derivation"], sample.ID(), thisptag))
                    daods.append(candidate)
                elif thisptag in ptags[sample.datasetType]["noskim"]:
                    logger.debug("Found unskimmed {} DAOD for DSID {} (ptag: p{}).".format(self.options["derivation"], sample.ID(), thisptag))
                else:
                    logger.error("Found unknown ptag p{} for {} DAOD with DSID {}. Please update the ptags dict this script!".format(thisptag, self.options["derivation"], sample.ID()))
        daods.sort(key=lambda x: int(x[-4:]), reverse=True)
        return daods

    def createDAODList(self):
        infilecontent = open(self.infile, "r").read().splitlines()
        for line in infilecontent:
            line = line.lstrip() # remove leading whitespace
            if line.startswith("#"):
                self.unavailable.append(line)
            else:
                aodsample = Sample(line)
                candidates = self.AODtoDAOD(line)
                if len(candidates) < 1:
                    self.unavailable.append(line)
                elif len(candidates) == 1:
                    self.available.append(candidates[0])
                else:
                    logger.debug("Found {} possible DAOD candidates for DSID {}! Will be using the one with later ptag...".format(len(candidates), aodsample.ID()))
                    self.available.append(candidates[0])
        if len(self.available) + len(self.unavailable) != len(infilecontent):
            logger.error("Getting slightly paranoid here...")
        logger.info("{} : {} / {} DAODs are available".format(self.process, len(self.available), len(infilecontent)))
        if not self.options["dryRun"]:
            self.writeOutputFile()
        else:
            for daod in self.available:
                print daod
            if len(self.unavailable) > 0:
                print "# UNAVAILABLE DAODS:"
                for unav in self.unavailable:
                    print "# " + unav
            print

    def writeOutputFile(self):
        if not os.path.exists(self.options["outputFolder"]):
            os.mkdir(self.options["outputFolder"])
        with open(os.path.join(self.options["outputFolder"], "{}.txt".format(self.process)), "w") as outfile:
            for daod in self.available:
                outfile.write(daod + "\n")
            if len(self.unavailable) > 0 and not self.options["truncateUnavailable"]:
                outfile.write("# UNAVAILABLE DAODS:\n")
                for unav in self.unavailable:
                    outfile.write("# " + unav + "\n")
        if options["collectUnavailable"] and len(self.unavailable) > 0:
            with open(os.path.join(self.options["outputFolder"], "unavailable.txt"), "a") as unavoutfile:
                unavoutfile.write(self.process + "\n")
                for unav in self.unavailable:
                    unavoutfile.write(unav + "\n")
                unavoutfile.write("\n")





if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("aodlist", metavar="aodlist.txt", type=str, nargs="+", help="list of AODs to be converted to list of DAODs")
    parser.add_argument("--derivation", type=str, default="SUSY3", help="specify derivation (default: %(default)s)")
    parser.add_argument("--dryRun", default=False, action="store_true", help="don't write output file but print to stdout only (default: %(default)s)")
    parser.add_argument("--outputFolder", type=str, default=os.path.join(os.getcwd(), "DAODs"), help="DAOD lists are outputted to this folder (default: '%(default)s')")
    parser.add_argument("--truncateUnavailable", default=False, action="store_true", help="don't write unavailable DAODs to output file (default: %(default)s)")
    parser.add_argument("--collectUnavailable", default=False, action="store_true", help="collect all unavailable DAODs in a single output file (default: %(default)s)")
    options = vars(parser.parse_args())

    logger.info("Will search rucio database for ptags {}".format(", ".join([str(p) for p in sorted(ptags["MC"]["skim"] + ptags["Data"]["skim"])])))
    logger.info("Please find the latest ptags for derivation caches in release 21.2 (and later) here:\n" + " "*10 + "https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYxAODderivationsr21")
    logger.info("The following options have been specified:")
    for key, value in sorted(options.iteritems()):
        logger.info("  * {} = {}".format(key, value))

    # Make it an absolute path
    if not options["outputFolder"].startswith(("/", "$", "~")):
        options["outputFolder"] = os.path.join(os.getcwd(), options["outputFolder"])

    unavoutfile = os.path.join(options["outputFolder"], "unavailable.txt")
    if options["collectUnavailable"]:
        # Reset old file (as DAODFinder appends to it)
        with open(unavoutfile, "w") as unavoutfile:
            pass
    elif os.path.exists(unavoutfile):
        logger.warning("Found '{}' in output folder, which might be outdated now!".format(os.path.basename(unavoutfile)))

    # Here we go
    finder = {}
    logger.info("Starting rucio query...")
    for infile in options["aodlist"]:
        finder[infile] = DAODFinder(infile, **options)
        finder[infile].createDAODList()
