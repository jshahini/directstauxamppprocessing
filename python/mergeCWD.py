#!/usr/bin/env python

from __future__ import print_function

__author__ = "Ferdinand Krieter"
__email__  = "ferdinand.krieter@cern.ch"

"""Merge all slurm output files into containers."""

# TODO: * Make slurm_submit.py output file naming scheme importable(?)
#       * Add possibility to conveniently reprocess missing files
#       * Implement merging for QCD trees from FFM

import os
import sys
import json
import subprocess

from os.path import dirname, abspath
from collections import defaultdict as defaultdict

from tools.logger import logger
from tools.sample import Sample

codepath = dirname(dirname(abspath(__file__)))

class Merger(object):

    def __init__(self, projectdir, **options):
        self.projectdir      = projectdir
        self.datafiledir     = os.path.join(self.projectdir, "Data")
        self.filedir         = {t:os.path.join(self.projectdir, t) for t in ["Data", "Background", "Signal", "Merged"]}
        self.options         = options
        self.joboptions      = None
        self.datasetdict     = None
        self.chunksdict      = None
        self.filesmissing    = False
        self.sourcefilegroup = defaultdict(list)

    def LoadBookkeepingFiles(self):
        self.joboptions = json.load(open(os.path.join(self.projectdir, "joboptions.json"), "r"))
        self.datasetdict = json.load(open(os.path.join(self.projectdir, "datasetdict.json"), "r"))
        try:
            self.chunksdict = json.load(open(os.path.join(self.projectdir, "slurmchunkslog.json"), "r"))
        except IOError:
            pass

    def CheckExistence(self, path, **info):
        """After checking the file path it is added to the corresponding group of source files to be merged together."""
        dstype     = info["Type"]
        category   = info["Category"]
        process    = info["Process"]
        subprocess = info["SubProcess"]
        if not os.path.exists(path):
            logger.error("{} ({}): {} does not exist!".format(dstype, category, os.path.basename(path)))
            self.filesmissing = True
        else:
            if dstype in ["Background", "Signal"] and self.options["group"] == "SubProcess":
                # groupname = "{}.{}".format(process, subprocess)
                logger.error("Sorry this functionality is currently not suppported.")
                raise NotImplementedError
            else:
                groupname = process if dstype != "Signal" else "{}.{}".format(process, subprocess)
            self.sourcefilegroup[(dstype, groupname)].append(path)

    def CompletenessCheck(self):
        """Check weather all the expected outputfile exist.
           (See slurm_submit.py for naming scheme)"""
        self.LoadBookkeepingFiles()
        self._retrieveMergedContainersList()
        for ntuple, info in self.datasetdict.iteritems():
            if ntuple in self.redundant_containers:
                continue
            dstype       = info["Type"]
            category     = info["Category"]
            IDs          = info["IDs"]
            daods        = info["DAODs"]
            processes    = info["Processes"]
            subprocesses = info["SubProcesses"]
            derivation   = info["Derivation"]
            # TODO: Find a better solution for this?
            process = "-".join(processes) if len(processes) > 1 else processes[0]
            subprocess = "merged" if len(subprocesses) > 1 else subprocesses[0]
            info["Process"] = process
            info["SubProcess"] = subprocess
            ID = Sample(daods[0]).ID() if len(daods) == 1 else "{}_{}".format(min(IDs), max(IDs))
            if dstype == "Data":
                if self.chunksdict is not None:
                    for ichunk in range(1, self.chunksdict[ID]+1, 1):
                        IDC = "{}_chunk{}".format(ID, ichunk) if self.chunksdict[ID] > 1 else ID
                        outputfile = "{}.{}.{}.root".format(category, IDC, derivation)
                        self.CheckExistence(os.path.join(self.filedir[dstype], outputfile), **info)
                else:
                    outputfile = "{}.{}.{}.root".format(category, ID, derivation)
                    self.CheckExistence(os.path.join(self.filedir[dstype], outputfile), **info)
            else:
                outputfile = "{}.{}.{}.{}.{}.root".format(category, ID, process, subprocess, derivation)
                self.CheckExistence(os.path.join(self.filedir[dstype], outputfile), **info)

    def Merge(self, targetpath, sourcefilelist):
        cmd = ["hadd"]
        cmd.append(targetpath)
        for sourcefile in sourcefilelist:
            cmd.append(sourcefile)
        logger.info("Merging files into {}...".format(os.path.basename(targetpath)))
        logger.debug("Will execute the following command:\n{}".format(" ".join(cmd)))
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, bufsize=1, universal_newlines=True)
        if self.options["verbose"]:
            for line in process.stdout:
                print(line, end='')
        process.wait()

    def Run(self):
        self.CompletenessCheck()
        if self.filesmissing:
            logger.warning("Some files seem to be missing! Proceed with caution!")
        else:
            logger.info("All files are available!")
        if self.options["dryRun"]: return
        if not os.path.exists(self.filedir["Merged"]):
            os.mkdir(self.filedir["Merged"])
        elif len(os.listdir(self.filedir["Merged"])) != 0:
            logger.error("{} is not empty! Please clean up!".format(self.filedir["Merged"]))
            sys.exit(1)
        if self.joboptions["ApplyFakeFactors"]:
            targetpath = os.path.join(self.filedir["Merged"], "QCD.root")
            qcd_sourcefilelist = []
            for key, sourcefilelist in self.sourcefilegroup.items():
                dstype, group = key
                if dstype != "Signal":
                    qcd_sourcefilelist += sourcefilelist
            self.Merge(targetpath, qcd_sourcefilelist)
        else:
            supergroup = defaultdict(list) # total data / total bkg group
            for key, sourcefilelist in self.sourcefilegroup.items():
                dstype, group  = key
                targetpath     = os.path.join(self.filedir["Merged"], "{}.root".format(group))
                supergroup[dstype].append(targetpath)
                self.Merge(targetpath, sourcefilelist)
            for dstype in ["Data", "Background"]:
                if not self.options["totalBkg"] and dstype == "Background": continue
                self.Merge(os.path.join(self.filedir["Merged"], "{}.root".format(dstype)), supergroup[dstype])

    def _retrieveMergedContainersList(self):
        try:
            self.redundant_containers = json.load(open(os.path.join(self.projectdir, "mergedcontainers.json"), "r"))
        except IOError:
            self.redundant_containers = []


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser("Merge all the files!")
    parser.add_argument("--cwd",
        default=os.path.join(codepath, "CWD"),
        type=str,
        help="path to project (default: %(default)s)")
    parser.add_argument("-v", "--verbose",
        default=False,
        action="store_true",
        help="get on with the spam... (default: %(default)s)")
    parser.add_argument("--dryRun",
        default=False,
        action="store_true",
        help="just check if files are missing (default: %(default)s)")
    parser.add_argument("-g", "--group",
        default="Process",
        choices=["Process", "SubProcess"],
        type=str,
        help="merge into files grouped by either by Process or SubProcess (default: %(default)s)")
    parser.add_argument("--totalBkg",
        default=False,
        action="store_true",
        help="in addition, merge all background samples into one (default: %(default)s)")

    args = vars(parser.parse_args())

    myMerger = Merger( args["cwd"], **args )
    myMerger.Run()
