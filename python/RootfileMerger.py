#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import ROOT
import os
import re
import json
import configparser
from tools.logger import logger
import shutil
import uuid
from subprocess import call


def FileMerging(paths, inputlist, outputfile):
  
  if len(inputlist) < 1:
    logger.error("No files to merge!")
    
  elif len(inputlist) == 1:
    logger.info("Only one file - copy it to 'Merge' directory.")
    shutil.copy2(inputlist[0], paths+outputfile)
    
  elif len(inputlist) > 1:
    logger.info("Begin merging {} files.".format(len(inputlist)))
    command = call(['hadd'] + ['-f'] + ['-O'] + [paths + outputfile] + inputlist)
    if command != 0:
      logger.error("Failed to run hadd: {}".format(command))
    logger.info("merging {} to {} completed".format(inputlist, outputfile)) 
    

if __name__ == '__main__':
  
# Define all needed paths:
  
  codepath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
  
  chunksdict = json.load(open(os.path.join(codepath, "CWD", "slurmchunkslog.json"), "r"))
  
  datasetdict = json.load(open(os.path.join(codepath, "CWD", "datasetdict.json"), "r"))
  
  bgdirectory = "CWD/Background/"
  sgdirectory = "CWD/Signal/"
  dadirectory = "CWD/Data/"
  
  filelist = [bgdirectory + f for f in os.listdir(bgdirectory)]
  filelist.extend(sgdirectory + f for f in os.listdir(sgdirectory))
  #filelist.extend(dadirectory + f for f in os.listdir(dadirectory))
  
  datalist = [dadirectory + f for f in os.listdir(dadirectory)]
  
  if not os.path.isdir(os.path.join(os.path.join(codepath,"CWD"), "Merged")):
    os.mkdir(os.path.join(os.path.join(codepath,"CWD"), "Merged"))
  
  destination = os.path.join(os.path.join(codepath,"CWD"), "Merged/")
  
  if not os.path.isdir(os.path.join(os.path.join(codepath,"CWD"), "Data_Merged")):
    os.mkdir(os.path.join(os.path.join(codepath,"CWD"), "Data_Merged"))
  
  data_dir = os.path.join(os.path.join(codepath,"CWD"), "Data_Merged/")
  
  
# Check if all rootfiles listed in json file exist:
  
  control = False
  
  countchunks = {}
  
  mergeyears = ["data15", "data16", "data17"]
  mergedata = ["periodA", "periodB", "periodC", "periodD", "periodE", "periodF", "periodG", "periodH", "periodI", "periodJ", "periodK", "periodL"]
  
  for y in mergeyears:
    countchunks[y] = {}
  
  for y in mergeyears:
    for period in mergedata:
      countchunks[y][period] = 0

  
  for sample in filelist:
    for daod in datasetdict:  
      if datasetdict[daod]["Type"]!="Data":
        if datasetdict[daod]["ID"] in sample and datasetdict[daod]["Category"] in sample and datasetdict[daod]["Version"] in sample:      
          control = True
          break
        else:
          continue
    if not control:
        logger.warning("Missing file for ID {}, production {}, tags {}".format(datasetdict[daod]["ID"], datasetdict[daod]["Category"], datasetdict[daod]["Version"]))
    control = False
      
  for sample in datalist:
    for daod in datasetdict:
      if datasetdict[daod]["ID"] in sample:
          for year in mergeyears:
            for period in mergedata:
              if year in datasetdict[daod]["ID"] and period in datasetdict[daod]["ID"]:
                countchunks[year][period] += 1
  
  for daod in datasetdict:
        for year in mergeyears:
          for period in mergedata:
            if year in datasetdict[daod]["ID"] and period in datasetdict[daod]["ID"]:
              for chunk in chunksdict:
                if year in chunk and period in chunk:
                  if countchunks[year][period] == chunksdict[chunk]:
                    logger.info("Found all chunks for {}".format(chunk))
                  else:
                    logger.warning("{} missing chunks for {}".format(chunksdict[chunk]-countchunks[year][period],chunk))
        
          
      
# Define which samples should be merged. Add the "Process" entry of the dataset dictonary to the "mergeprocesses" list, "Type" to "mergetypes" and "SubProcess" to mergesubprocesses.
  
  
  mergeprocesses = ["singletop", "ttbar", "ttV", "multitop", "Zll", "Wlnu", "Diboson", "DY", "data15", "data16", "data17"]
  mergetypes = ["Background", "Data"]
  mergesubprocesses = ["200p0_1p0"]
  mergecategories = ["MC16a", "MC16d"]
  
  perioddict = {}
  for y in mergeyears:
    perioddict[y] = {}

  
  prodict = {}
  typedict = {}
  subprodict = {}
  categorydict = {}
  
  for y in mergeyears:
    for name in mergedata:
      perioddict[y][name] = []
  
  for name in mergeprocesses:
    prodict[name] = []
    
  for name in mergetypes:
    typedict[name] = []
  
  for name in mergesubprocesses:
    subprodict[name] = []
  
  
  for sample in datalist:
    for year in mergeyears:
      for per in mergedata:
        if per in sample and year in sample:
          perioddict[year][per].append(sample)
  
  for year in mergeyears:
   for name in mergedata:
     FileMerging(data_dir, perioddict[year][name], "DATA." + year + "_" + "13TeV." + name + ".SUSY3.grp" + year.strip("data") + "_v01_p3372.root")
                 
  filelist.extend(data_dir + f for f in os.listdir(data_dir))
  
# Fill merging lists with the corresponding sample names:  

  for daod in datasetdict:
    for sample in filelist:
      if datasetdict[daod]["ID"] in sample and datasetdict[daod]["Category"] in sample and datasetdict[daod]["Version"] in sample:
        #if datasetdict[daod]["Category"] == "MC16a":
          if datasetdict[daod]["Process"] in prodict.keys():
            prodict[datasetdict[daod]["Process"]].append(sample)
          if datasetdict[daod]["Type"] in typedict.keys():
            typedict[datasetdict[daod]["Type"]].append(sample)
          if datasetdict[daod]["SubProcess"] in subprodict.keys():
            subprodict[datasetdict[daod]["SubProcess"]].append(sample)
          #if datasetdict[daod]["Category"] in categorydict.keys():
          #if datasetdict[daod]["Process"] in categorydict.keys():
            #categorydict[datasetdict[daod]["Category"]][datasetdict[daod]["Process"]].append(sample)

	  
# Call merging function:

  for name in mergeprocesses:
    FileMerging(destination, prodict[name], name + ".root")
  
  for name in mergetypes:
    FileMerging(destination, typedict[name], name + ".root")
    
  for name in mergesubprocesses:
    FileMerging(destination, subprodict[name], name + ".root")
    
  #for name in mergecategories:
    #for thing in mergeprocesses:
      #FileMerging(destination, categorydict[name][thing], name + "_" + thing + ".root")
	    
	   
# Merge files more or less independent of Process, Type or SubProcess, e.g. top background:
  
  tops = []
  
  for daod in datasetdict:
    for sample in filelist:
      if datasetdict[daod]["ID"] in sample and datasetdict[daod]["Category"] in sample and datasetdict[daod]["Version"] in sample:
        if datasetdict[daod]["Type"] == "Background":
          if datasetdict[daod]["Process"] == "singletop":
            tops.append(sample)
          if datasetdict[daod]["Process"] == "ttbar":
            tops.append(sample)
          if datasetdict[daod]["Process"] == "ttV":
            tops.append(sample)
          if datasetdict[daod]["Process"] == "multitop":
            tops.append(sample)
         
        
  FileMerging(destination, tops, "top.root")