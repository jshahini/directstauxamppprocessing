#!/usr/bin/env python2.7

from __future__ import print_function

import math

from tools.logger import logger

class Chunkinator(dict):

    def __init__(self, alonglist, **options):
        self.initiallist = alonglist
        self.options = options
        if self.checkOption("nchunks") and not self.checkOption("chunksize"):
            self.nchunks = options.pop("nchunks")
            self.chunksize = int(math.ceil(float(len(self.initiallist))/self.nchunks))
        elif not self.checkOption("nchunks") and self.checkOption("chunksize"):
            self.chunksize = options.pop("chunksize")
            self.nchunks = int(math.ceil(float(len(self.initiallist))/self.chunksize))
        elif not self.checkOption("nchunks") and not self.checkOption("chunksize"):
            self.chunksize = 4
            self.nchunks = int(math.ceil(float(len(self.initiallist))/self.chunksize))
        else:
            logger.error("Can't take 'nchunks' and 'chunksize' option at the same time.")
            raise AssertionError
        self.chunkinate()

    def checkOption(self, option):
        if option in self.options and self.options[option] is not None:
            return True
        return False

    def chunkinate(self):
        ichunk = 0
        for i, entry in enumerate(sorted(self.initiallist), start=1):
            if (i-1) % self.chunksize == 0 or i == 1:
                ichunk += 1
                self[ichunk] = []
            self[ichunk].append(entry)

def main():
    print(Chunkinator([i for i in range(1, 20, 1)], nchunks=None, chunksize=4))

if __name__ == '__main__':
    main()
