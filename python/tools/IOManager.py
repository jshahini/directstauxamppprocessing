#!/usr/bin/env python

import ROOT

import os
import sys
import re
import math
import collections
import shutil
import glob
import uuid
import atexit
import tempfile

import logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

import ROOT

from tools.meme import meme
from array import array

generatedClasses = []
databaseDirectory = "/dev/shm/IOManager/"

def getMergedHist(hists):
    """returns a merged histogram"""
    if len(hists) == 1:
        return hists[0]
    comb = hists[0].Clone('comb{}'.format(next(tempNames)))
    comb.Reset()
    l = ROOT.TList()
    for hist in hists:
        l.Add(hist)
    comb.Merge(l)
    return comb

@meme.cache("_cache", useJSON=True)
def getYields(cutsDict, weight, path, treeName):
    drawer = HistoFactory(path, treeName)
    yieldHists = {}
    for region, cut in cutsDict.items():
        logger.debug("Adding hist for cut region {} with cut {}".format(region, cut))
        yieldHists[region] = drawer.addHist(cut, weight)
    logger.debug("Running drawer")
    #drawer.run(compile=True)
    drawer.run(compile=False)
    regionYields = {}
    for region, hist in yieldHists.items():
        logger.debug("Got a hist: {}".format(hist))
        logger.debug("Integral: {}".format(hist.Integral()))
        err = ROOT.Double()
        yie = hist.IntegralAndError(0, hist.GetNbinsX(), err)
        n = hist.GetEntries()
        regionYields[region] = (n, yie, float(err))
    return regionYields

@meme.cache("_cache", useJSON=True)
def getHists(histsConfDict, path, treeName, compile=True):
    """
    Wrapper to get all hists for a like dict::

        {
          "hist1" : {nbins=nbins1, xmin=xmin1, xmax=xmax1, varexp=var1, cut=cut1, weight=weight1},
          "hist2" : {nbins=nbins2, xmin=xmin2, xmax=xmax2, varexp=var2, cut=cut2, weight=weight2},
          ...
        }

    Returns a dict with histograms
    """
    drawer = HistoFactory(path, treeName)
    histsDict = {}
    for histname, kwargs in histsConfDict.items():
        logger.debug("Adding hist {} with kwargs {}".format(histname, kwargs))
        if ":" in kwargs.get("varexp"):
            histsDict[histname] = drawer.add2DHist(**kwargs)
        else:
            histsDict[histname] = drawer.addHist(**kwargs)
    logger.debug("Running drawer")
    drawer.run(compile=compile)
    return histsDict


def getHistsPaths(histConfDict, treeName, *paths, **kwargs):
    "Call getHists and merges the returned histsDicts for all given paths."
    compile = kwargs.pop("compile", True)
    if kwargs:
        raise KeyError("Got Unexpected kwargs: {}".format(kwargs))
    histListDict = {}
    for path in paths:
        histsDict = getHists(histConfDict, path, treeName, compile=compile)
        for histname, hist in histsDict.items():
            if not histname in histListDict:
                histListDict[histname] = []
            histListDict[histname].append(hist)
    mergedHistDict = {}
    for histname, histlist in histListDict.items():
        mergedHistDict[histname] = getMergedHist(histlist)
    return mergedHistDict

@meme.cache()
def ProjectTree(filename, **kwargs):
    hname = kwargs.get("name")
    varexp = kwargs.get("varexp")
    title = kwargs.get("title", "")
    xbinning = array("d", [float(b) for b in  kwargs.get("xbinning").split(",")])
    rootfile = ROOT.TFile.Open(filename, "read")
    if rootfile == None:
        logging.error("File {} not found!".format(rootfile))
        raise IOError
    tree = rootfile.Get(kwargs.get("tree", "DirectStau"))
    if tree == None or not issubclass(type(tree), ROOT.TTree):
        logging.error("Tree {} not found!".format(tree))
        raise IOError
    if isinstance(kwargs.get("cut", ["1"]) , list):
        cutString = "&&".join(kwargs.get("cut", ["1"]))
    elif isinstance(kwargs.get("cut", ["1"]) , str):
        cutString = kwargs.get("cut", "1")
    if ":" in varexp:
        ybinning = array("d", [float(b) for b in  kwargs.get("ybinning").split(",")])
        tmph = ROOT.TH2D(hname, title, len(xbinning)-1, xbinning, len(ybinning)-1, ybinning)
    else:
        tmph = ROOT.TH1D(hname, title, len(xbinning)-1, xbinning)
    tmph.Sumw2()
    nEvts = tree.Project(hname, kwargs.get("varexp", "MET"), "({})*({})".format(kwargs.get("weight", "1"), cutString))
    tmph.SetDirectory(0)
    rootfile.Close()
    return tmph

def GetListOfBinEdges(histo, **kwargs):
    axis = {"x": histo.GetXaxis(), "y": histo.GetYaxis()}.get(kwargs.get("axis", "x"))
    return [axis.GetBinLowEdge(bn) for bn in range(1, axis.GetNbins()+2, 1)]

def ConvertColor(color):
    if isinstance(color, str):
        return ROOT.TColor.GetColor(color)
    elif isinstance(color, ROOT.TColor) or isinstance(color, int):
        return color
    else:
        raise TypeError

def getRunOption(path, **kwargs):
    import json
    jsonfile = json.load(open(path))
    return jsonfile[kwargs.get("option")]

def HasVarBinning(histo, **kwargs):
    axis = {"x": histo.GetXaxis(), "y": histo.GetYaxis()}.get(kwargs.get("axis", "x"))
    binwidths = [axis.GetBinLowEdge(bn+1) - axis.GetBinLowEdge(bn) for bn in range(1, axis.GetNbins()+2, 1)]
    for i in range(len(binwidths)-1):
        if not binwidths[i] % binwidths[i+1] == 0:
            return True
    return False

def getBranchNames(tree):
    branches = []
    for branch in tree.GetListOfBranches():
        branches.append( branch.GetName())
    return branches

def getTreeNames(tFile, getAll=False):
    if not getAll:
        logger.warning("Only fetching 10 tree names")
    trees = []
    if getAll:
        for key in tFile.GetListOfKeys():
            if issubclass(type(tFile.Get(key.GetName())), ROOT.TTree):
                trees.append(key.GetName())
    else:
        for i, key in zip(range(10), tFile.GetListOfKeys()):
            if issubclass(type(tFile.Get(key.GetName())), ROOT.TTree):
                trees.append(key.GetName())
    return trees


class workInTempDir:
    """Context manager for changing to a temporary directory that will be
    deleted afterwards. The given dir will be the base directory in
    which the tempdir is created. If not given, the system tmp
    directory will be used. If *skipCleanup* is given the directory is
    not deleted afterwards. Use *prefix* to control the naming format.
    If *cleanAtExit* is set the tmp directory is cleaned at exit of the script,
    not when exiting the context.
    """
    def __init__(self, baseDir=None, skipCleanup=True, prefix="tmp_", cleanAtExit=False):
        self.baseDir = baseDir
        self.tempDir = tempfile.mkdtemp(dir=baseDir, prefix=prefix)
        self.skipCleanup = skipCleanup
        self.cleanAtExit = cleanAtExit

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.tempDir)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)
        if self.skipCleanup:
            return
        if not self.cleanAtExit:
            self.cleanup()
        else:
            atexit.register(self.cleanup)

    def cleanup(self):
        shutil.rmtree(self.tempDir)


class HistoFactory:

    classdir = None
    """Directory to work in for the temporary classes. If None, the
    systems tmp directory is used"""

    skipCleanup = False
    """for debug purposes - set true to keep generated c files"""

    def __init__(self, path, treeName):
        self.path = path
        self.treeName = treeName
        self.rootfile = ROOT.TFile.Open(path)
        if not self.rootfile:
            logger.error("'{}' does not exist!".format(path))
            raise IOError
        self.tree = self.rootfile.Get(self.treeName)
        if not self.tree:
            raise KeyError("{} not found in file {}, try: {}".format(treeName, path, getTreeNames(self.rootfile)))
        self.hists = []
        self.classnumber = uuid.uuid4().hex ## hehe
        self.classname = "_tmpClass_{}".format(self.classnumber)
        generatedClasses.append(self.classname)

    def addHist(self, cut, weight, **kwargs):
        "Add a histogram to be filled later. Returns the histogram (still unfilled)."
        varexp = kwargs.get("varexp", "1")
        xmin = kwargs.get("xmin", 0.5)
        xmax = kwargs.get("xmax", 1.5)
        nbins = kwargs.get("nbinsx", 1)
        binLowEdges = kwargs.get("xBinLowEdges", None)
        name = "hist_"+uuid.uuid4().hex
        if not binLowEdges:
            roothist = ROOT.TH1D(name, "", nbins, xmin, xmax)
        else:
            xbins = array('d', binLowEdges)
            roothist = ROOT.TH1D(name, "", len(binLowEdges)-1, xbins)
        roothist.Sumw2()
        roothist.GetXaxis().SetTitle(varexp)

        # merge cut and weight
        cut = "({})*({})".format(cut, weight)

        # support for simple vector branch expressions
        varexp = re.sub("\[([0-9]+)\]", "->at(\\1)", varexp)
        cut = re.sub("\[([0-9]+)\]", "->at(\\1)", cut)

        hist = {"name" : name, "roothist" : roothist, "varexp" : varexp, "cut" : cut}
        self.hists.append(hist)
        hist["roothist"].SetDirectory(0) # seems important
        return hist["roothist"]

    def add2DHist(self, cut, weight, **kwargs):
        "Add a histogram to be filled later. Returns the histogram (still unfilled)."
        varexp = kwargs.get("varexp", "1:1") # y:x
        yvar, xvar = varexp.split(":")
        xmin = kwargs.get("xmin", 0.5)
        xmax = kwargs.get("xmax", 1.5)
        ymin = kwargs.get("ymin", 0.5)
        ymax = kwargs.get("ymax", 1.5)
        nbinsx = kwargs.get("nbinsx", 1)
        nbinsy = kwargs.get("nbinsy", 1)
        xBinLowEdges = kwargs.get("xBinLowEdges", None)
        yBinLowEdges = kwargs.get("yBinLowEdges", None)
        name = "hist_"+uuid.uuid4().hex
        if not xBinLowEdges and not yBinLowEdges:
            roothist = ROOT.TH2D(name, "", nbinsx, xmin, xmax, nbinsy, ymin, ymax)
        elif not xBinLowEdges and yBinLowEdges:
            roothist = ROOT.TH2D(name, "", nbinsx, xmin, xmax, len(yBinLowEdges)-1, array('d', yBinLowEdges))
        elif xBinLowEdges and not yBinLowEdges:
            roothist = ROOT.TH2D(name, "", len(xBinLowEdges)-1, array('d', xBinLowEdges), nbinsy, ymin, ymax)
        else:
            roothist = ROOT.TH2D(name, "", len(xBinLowEdges)-1, array('d', xBinLowEdges), len(yBinLowEdges)-1, array('d', yBinLowEdges))
        roothist.Sumw2()
        roothist.GetXaxis().SetTitle(xvar)
        roothist.GetYaxis().SetTitle(yvar)

        # merge cut and weight
        cut = "({})*({})".format(cut, weight)

        # support for simple vector branch expressions
        varexp = re.sub("\[([0-9]+)\]", "->at(\\1)", varexp)
        cut = re.sub("\[([0-9]+)\]", "->at(\\1)", cut)

        hist = {"name" : name, "roothist" : roothist, "varexp" : varexp, "cut" : cut}
        self.hists.append(hist)
        hist["roothist"].SetDirectory(0) # seems important
        return hist["roothist"]

    def getOverallOR(self):
        "Return an expression that evaluates if any of the used selections passed"
        return "||".join(["({})".format(_hist["cut"]) for _hist in self.hists])

    def generate(self):
        "Generate the c++ code"
        self.tree.MakeClass(self.classname)
        headerlines = None
        with open("{}.h".format(self.classname)) as f:
            headerlines = f.readlines()

        with open("{}.h".format(self.classname), "w") as of:
            for line in headerlines:
                # if "Loop();" in line:
                #     of.write("   virtual TH1D*     Loop();\n")
                if "class {} {{".format(self.classname) in line:
                    of.write("#include \"TH1.h\"\n#include \"TH2.h\"\n")
                    of.write(line)
                    continue
                if "public" in line:
                    of.write(line)
                    for hist in self.hists:
                        if hist["roothist"].ClassName() == "TH1D":
                            of.write("TH1D* {};\n".format(hist["name"]))
                        elif hist["roothist"].ClassName() == "TH2D":
                            of.write("TH2D* {};\n".format(hist["name"]))
                        else:
                            raise NotImplementedError
                    continue
                of.write(line)

        with open("{}.C".format(self.classname), "w") as of:
            of.write("""#define {classname}_cxx
        #include "{classname}.h"
        #include <TH2.h>
        #include <TStyle.h>
        #include <TCanvas.h>

        #include <iostream>

        void {classname}::Loop()
        {{   if (fChain == 0) return;


           Long64_t nentries = fChain->GetEntriesFast();


           fChain->SetBranchStatus(\"*\", 0);
""".format(classname=self.classname))
            for branchname in [_b.GetName() for _b in self.tree.GetListOfBranches()]:
                if self.branchUsed(branchname):
                    logger.debug("Activating branch {}".format(branchname))
                    of.write("{:>10}fChain->SetBranchStatus(\"{}\", 1);\n".format("", branchname))
            of.write("""


           Long64_t nbytes = 0, nb = 0;
           for (Long64_t jentry=0; jentry<nentries;jentry++) {
              Long64_t ientry = LoadTree(jentry);
              if (ientry < 0) break;
              nb = fChain->GetEntry(jentry);   nbytes += nb;
""")
            # of.write("{:>14}if (!({})) continue;\n".format("", self.getOverallOR())) // default
            of.write("try {\n" + "{:>14}if (!({})) continue;\n".format("", self.getOverallOR()) + "} catch (...) {" + "}")
            for hist in self.hists:
                # of.write("{:>14}if ({cut}) {name}->Fill({varexp}, {cut});\n".format("", **dict(hist))) // default
                if ":" in hist.get("varexp"):
                    y, x = hist.get("varexp").split(":")
                    of.write("try {\n" + "{:>14}if ({cut}) {name}->Fill({}, {}, {cut});\n".format("", x, y, **dict(hist)) + "} catch (...) {" + "}")
                else:
                    of.write("try {\n" + "{:>14}if ({cut}) {name}->Fill({varexp}, {cut});\n".format("", **dict(hist)) + "} catch (...) {" + "}")
            of.write("""
           }
        }
""")


    def branchUsed(self, branchName):
        """Determine wether a branch is used for any cut or
        varexp. This is not perfect - it might activate too many
        branches. But it shouldn't miss any (i hope)"""
        for hist in self.hists:
            if branchName in hist["varexp"]:
                return True
            if branchName in hist["cut"]:
                return True
        return False


    def run(self, compile=False):
        "Finally generate and run the code which will fill the hists."
        with workInTempDir(baseDir=self.classdir,
                              skipCleanup=self.skipCleanup,
                              prefix="mhd_",
                              cleanAtExit=True):
            logger.debug("Current directory: {}".format(os.getcwd()))
            self.generate()
            logger.debug("Loading generated c++ file")
            if compile:
                ROOT.gROOT.ProcessLine(".L {}.C++".format(self.classname))
            else:
                ROOT.gROOT.ProcessLine(".L {}.C".format(self.classname))
            logger.debug("Loading done")
            looper = ROOT.__getattr__(self.classname)(self.tree)
            for hist in self.hists:
                looper.__setattr__(hist["name"], hist["roothist"])
            logger.info("Loop over tree {} in file {}".format(self.treeName, self.path))
            looper.Loop()
        for hist in self.hists:
            if hist["roothist"].GetEntries() == 0:
                logger.warning("no events extracted for tree {} in file {} (varexp=\"{}\", cut=\"{}\")"
                               .format(self.treeName, self.path, hist["varexp"], hist["cut"]))



class IOManager(object):
    tdirdict = collections.OrderedDict()
    objtitledict = {}
    outfile = "outfile path"

    @staticmethod
    def addPlot(histo, **kwargs):
        if kwargs.get("folder") != None:
            tobjarray = kwargs.get("folder")
            objtitle = kwargs.get("title", histo.GetName())
            if not tobjarray in IOManager.tdirdict.keys():
                IOManager.tdirdict[tobjarray] = ROOT.TObjArray(0)
            if not kwargs.get("customOptions", False):
                if histo.ClassName() == "TH1D":
                    histo.SetOption("HIST")
                    histo.GetXaxis().SetTitle(objtitle)
                elif histo.ClassName() == "TH2D":
                    histo.SetOption("COLZ text")
                    # histo.GetYaxis().SetTitle(objtitle.split(":")[0])
                    # histo.GetXaxis().SetTitle(objtitle.split(":")[1])
            else:
                if histo.ClassName() == "TH1D":
                    histo.SetOption(kwargs.get("drawoption"))
                    histo.GetXaxis().SetTitle(kwargs.get("xtitle"))
                elif histo.ClassName() == "TH2D":
                    histo.SetOption(kwargs.get("drawoption"))
                    histo.GetXaxis().SetTitle(kwargs.get("xtitle"))
                    histo.GetYaxis().SetTitle(kwargs.get("ytitle"))
            IOManager.tdirdict[tobjarray].Add(histo)
            IOManager.objtitledict[histo.GetName()] = objtitle
            logging.info("Added {} '{}' to TObjArray '{}'.".format(histo.ClassName(), histo.GetName(), tobjarray))

    @staticmethod
    def addTObjArray(tobjarray, **kwargs):
        if kwargs.get("folder") != None:
            if not kwargs.get("customOptions", False):
                for histo in tobjarray:
                    if histo.ClassName() == "TH1D":
                        histo.SetOption("HIST")
                        histo.GetXaxis().SetTitle(objtitle)
                    elif histo.ClassName() == "TH2D":
                        histo.SetOption("COLZ text")
                        histo.GetYaxis().SetTitle(objtitle.split(":")[0])
                        histo.GetXaxis().SetTitle(objtitle.split(":")[1])
                    else:
                        raise NotImplementedError
            IOManager.tdirdict[kwargs.get("folder")] = ROOT.TObjArray(0)

    @staticmethod
    def initOutputDir(path):
        if path.startswith("."):
            path = os.path.join(os.getcwd(), path.lstrip(".").lstrip("/"))
        if not os.path.exists(path):
            os.mkdir(path)
        return path

    @staticmethod
    def getTObjFromFile(filepath, tobjpath, **kwargs):
        rootfile = ROOT.TFile(filepath)
        if "/" in tobjpath:
            tobjdir = os.path.dirname(tobjpath)
            rootfile.cd(tobjdir)
        tobj = os.path.basename(tobjpath)
        htmp = ROOT.gDirectory.Get(tobj)
        htmp.SetDirectory(0)
        rootfile.Close()
        if kwargs.get("name") is not None:
            htmp.SetName(kwargs.get("name"))
        return htmp

    @staticmethod
    def writeOutputFile(path, **kwargs):
        mode = kwargs.get("mode", "RECREATE")
        IOManager.initOutputDir(os.path.dirname(path))
        IOManager.outfile = ROOT.TFile(path, mode)
        IOManager.outfile.cd();
        for tobjarray in IOManager.tdirdict.keys():
            folder = IOManager.outfile.GetDirectory(tobjarray)
            if not folder:
                folder = IOManager.outfile.mkdir(tobjarray)
            folder.cd("{}:{}".format(path,tobjarray))
            for obj in IOManager.tdirdict[tobjarray]:
                obj.SetName(IOManager.objtitledict[obj.GetName()])
            IOManager.tdirdict[tobjarray].Write()
            logging.info("TObjArray '{}' written to file.".format(tobjarray))
            del IOManager.tdirdict[tobjarray]
        IOManager.outfile.Write()
        logging.info("TFile written: {}".format(os.path.abspath(path)))

    @staticmethod
    def projectTree(histo, filename, **kwargs):
        """Wrapper for standalone ProjectTree (meme requires hashable function parameters)"""
        kwargs["name"] = histo.GetName()
        kwargs["xbinning"] =  ",".join([str(histo.GetXaxis().GetBinLowEdge(bn)) for bn in range(1, histo.GetNbinsX()+2, 1)])
        kwargs["title"] = histo.GetTitle()
        if isinstance(kwargs.get("cut", ["1"]) , list):
            kwargs["cut"] = "&&".join(kwargs.get("cut", ["1"]))
        elif isinstance(kwargs.get("cut", ["1"]) , str):
            kwargs["cut"] = kwargs.get("cut", "1")
        if ":" in kwargs.get("varexp"):
            kwargs["ybinning"] = ",".join([str(histo.GetYaxis().GetBinLowEdge(bn)) for bn in range(1, histo.GetNbinsY()+2, 1)])
        tmph = ProjectTree(filename, **kwargs)
        if kwargs.get("add", False):
            histo.Add(tmph.Clone())
        else:
            tmph.Copy(histo)
        del tmph

    @staticmethod
    @meme.cache(cacheDir=databaseDirectory, useJSON=True, ttl=172800)
    def LoopTree(*infiles, **options):
        """Loop over a tree of one or more input files and return dictionary containing a list
           of entries for each requested branch."""
        branches = options.get("branches")
        values = collections.defaultdict(set)
        for infile in infiles:
            print("reading {}".format(infile))
            print("looking for tree named {}".format(options.get("tree")))
            rootfile = ROOT.TFile.Open(infile, "read")
            try:
                tree = rootfile.Get(options.get("tree"))
                print("found tree and file!")
            except ReferenceError:
                logger.error("'{}' has no tree called '{}'!".format(infile, options.get("tree")))
                return {}
            for event in tree:
                print("another event") 
                for branch in branches:
                    try:
                        if tree.GetBranch(branch).GetClassName(): # it's a vector branch!
                            values[branch].add([float(value) for value in getattr(event, branch)])
                        else:
                            values[branch].add(getattr(event, branch))
                    except ReferenceError:
                        values[branch].add(None)
            rootfile.Close()
        return {k:list(v) for k, v in values.iteritems()}

    class Factory(object):
        """Draw multiple histograms with one loop."""

        def __init__(self, infilepath, treename):
            self.vecbranchdict = {} # vector branches
            self.histoconf = {}
            self.histodict = {}
            self.path = infilepath
            self.treename = treename
            tfile =  ROOT.TFile.Open(self.path, "read")
            for branch in tfile.Get(self.treename).GetListOfBranches():
                if branch.GetClassName(): # returns None ifs of non user class type
                    self.vecbranchdict[branch.GetName()] = branch.GetClassName()
            del tfile

        def registerHisto(self, histo, **kwargs):
            if HasVarBinning(histo):
                kwargs["xBinLowEdges"] = GetListOfBinEdges(histo)
            else:
                kwargs["nbinsx"] = histo.GetXaxis().GetNbins()
                kwargs["xmin"] = histo.GetXaxis().GetBinLowEdge(1)
                kwargs["xmax"] = histo.GetXaxis().GetBinLowEdge(histo.GetXaxis().GetNbins() + 1)
            if histo.ClassName().startswith("TH2"):
                if HasVarBinning(histo):
                    kwargs["yBinLowEdges"] = GetListOfBinEdges(histo, axis="y")
                else:
                    kwargs["nbinsy"] = histo.GetYaxis().GetNbins()
                    kwargs["ymin"] = histo.GetYaxis().GetBinLowEdge(1)
                    kwargs["ymax"] = histo.GetYaxis().GetBinLowEdge(histo.GetYaxis().GetNbins() + 1)
            if isinstance(kwargs.get("cut", ["1"]) , list):
                kwargs["cut"] = "&&".join(kwargs.get("cut", ["1"]))
            elif isinstance(kwargs.get("cut", ["1"]) , str):
                kwargs["cut"] = kwargs.get("cut", "1")
            for var in self.vecbranchdict.keys():
                if var in kwargs.get("varexp"):
                    logger.warning("branch '{}' is a of type '{}' (only the first entry will be considered)".format(var, self.vecbranchdict[var]))
                    kwargs["varexp"] = re.sub(var, var + "[0]", kwargs["varexp"])
                if var in kwargs["cut"]:
                    logger.warning("branch '{}' is a of type '{}' (only the first entry will be considered)".format(var, self.vecbranchdict[var]))
                    kwargs["cut"] = re.sub(var, var + "[0]", kwargs["cut"])
                if var in kwargs.get("weight"):
                    logger.warning("branch '{}' is a of type '{}' (only the first entry will be considered)".format(var, self.vecbranchdict[var]))
                    kwargs["weight"] = re.sub(var, var + "[0]", kwargs["weight"])
            hname = histo.GetName()
            self.histoconf[hname] = kwargs

        def fillAll(self):
            tmpdict = getHists(self.histoconf, self.path, self.treename)
            for hname in self.histoconf.keys():
                tmpdict[hname].SetName(hname)
                tmpdict[hname].Copy(ROOT.gROOT.FindObject(hname))


def main():
    fileDirectory = "/project/etp1/fkrieter/Analysis/DirectStau/GornGrasshopper_skim01/V01/Merged"
    h = ROOT.TH1D("blubb", "", 10, 0., 500.)
    IOManager.projectTree(h, os.path.join(fileDirectory, "Data.root"), tree="DirectStau", varexp="MET", cut=["MET>50", "tau1Pt>40", "tau2Pt>30"], weight="evt_weight")

    print h
    print h.Integral()


if __name__ == '__main__':
    main()
