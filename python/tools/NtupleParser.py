#!/usr/bin/env python2.7

__author__ = "Ferdinand Krieter"
__email__  = "ferdinand.krieter@cern.ch"

import os
import sys
import json
import glob
import collections

import ROOT

from tools.logger import logger
from tools.meme import cache
from tools.IOManager import IOManager
from tools.sample import Sample, GuessProcess, GuessGenerator


databaseDirectory = "/dev/shm/NtupleParser/"

class NtupleParser(object):
    """Classify a XAMPPstau ntuple container based on the files it contains.
         * Category
         * Derivation **must be unique**
         * IDs
         * DAODs
         * Generators
         * Processes
         * Subprocesses
         * Type **must be unique**
    """

    def __init__(self, proddir):
        self.proddir = proddir # where the underlying production is stored
        self.pathdict = collections.defaultdict(list)
        for line in open(os.path.join(self.proddir, "rootdPaths.txt"), "r").read().splitlines():
            ntuple, path = line.split()
            self.pathdict[ntuple].append(path)
        self.expecteddaods = set() # all DAODs that are stored in proddir/DAODs/
        self._retrieveExpectedDAODs()
        self._retrieveIODSDicts()

    def Parse(self, ntuple):
        print("looping tree")
        metadata = IOManager.LoopTree(*self.pathdict[ntuple],
            tree="MetaDataTree",
            branches=["isData", "mcChannelNumber", "runNumber"])
        #print("finished looping tree")
        assert len(metadata["isData"]) == 1
        isData = True if metadata["isData"][0] == 1 else False
        if not isData:
            assert len(metadata["runNumber"]) == 1
            category = {
                284500: "MC16a",
                300000: "MC16d",
                310000: "MC16e",
            }.get(metadata["runNumber"][0], "UNKNOWN")
            ids = metadata["mcChannelNumber"]
        else:
            category = "DATA"
            ids = metadata["runNumber"]
        daods        = set()
        derivations  = set()
        generators   = set()
        processes    = set()
        subprocesses = set()
        dstypes      = set()
        for daod in self.expecteddaods:
            sample = Sample(daod)
            daod_id = int(sample.ID())
            if category != sample.Category():
                continue
            if not daod_id in ids:
                continue
            derivation = sample._dataType.lstrip("DAOD_")
            process, subprocess = GuessProcess(daod)
            if sample._project.startswith("mc"):
                dstype = "Signal" if any(s in sample._physicsShort for s in ["StauStau", "TT_stau"]) else "Background"
            elif sample._project.startswith("data"):
                dstype = "Data"
            else:
                dstype = "Unknown"
            try:
                if self.daodntupledict[daod] == ntuple:
                    daods.add(daod)
            except KeyError:
                daods.add(daod)
            derivations.add(derivation)
            generators.add(GuessGenerator(daod))
            processes.add(process)
            subprocesses.add(subprocess)
            dstypes.add(dstype)
        derivations = list(derivations)
        dstypes = list(dstypes)
        if derivations == []:
            print ntuple
            print "IDs={}, Category={}, Type={}, DAODs={}".format(ids, category, dstypes, daods)
        assert len(derivations) == 1
        assert len(dstypes) == 1
        return dict(
            IDs=ids,
            Category=category,
            Derivation=derivations[0],
            Type=dstypes[0],
            Generators=list(generators),
            Processes=list(processes),
            SubProcesses=list(subprocesses),
            DAODs=list(daods))

    def GetExpectedDAODs(self):
        return self.expecteddaods

    def _retrieveExpectedDAODs(self):
        self._retrievePeriodsDict()
        for daodlist in RecurseDir(os.path.join(self.proddir, "DAODs")):
            if not daodlist.endswith(".txt") or daodlist.endswith("unavailable.txt"):
                continue
            for daod in open(daodlist, "r").read().splitlines():
                if not daod or daod.startswith("#"):
                    continue
                if "period" in daod:
                    try:
                        self.expecteddaods |= set(self.periodsdict[daod])
                    except KeyError:
                        logger.error("{} not found in periodsdict.json!".format(daod))
                else:
                    self.expecteddaods.add(daod)

    def _retrievePeriodsDict(self):
        self.periodsdict = {}
        try:
            pdict = json.load(open(os.path.join(self.proddir, "periodsdict.json"), "r"))
            for periodcontainer, runlist in pdict.iteritems():
                self.periodsdict[periodcontainer] = runlist
        except IOError:
            logger.debug("No periods dict found in {}".format(self.proddir))

    def _retrieveIODSDicts(self):
        self.daodntupledict = {}
        iodsdictpaths = glob.glob(os.path.join(self.proddir, "*IODSdict*.txt")) # should be of the form "DAOD"<space>"NTUPLE"
        if not iodsdictpaths:
            logger.debug("No input-output dataset dict found. Assuming that multiple input DAODs get merged into one NTUPLE, otherwise I need help associating same-ID DAODs to available NTUPLEs!")
        else:
            logger.info("Using {} to associate same-ID DAODs with available NTUPLEs.".format(", ".join([os.path.relpath(p) for p in iodsdictpaths])))
        for d in iodsdictpaths:
            for line in open(d, "r").read().splitlines():
                DAOD = line.split()[0]
                try:
                    ntuple = line.split()[1]
                except IndexError:
                    ntuple = None
                self.daodntupledict[DAOD] = ntuple

def RecurseDir(dirname):
    paths = []
    for root, subdirs, files in os.walk(dirname):
        for f in files:
            paths.append(os.path.join(root, f))
        for subdir in subdirs:
            RecurseDir(os.path.join(root, subdir))
    return paths

# Example:
if __name__ == '__main__':

    from os.path import dirname
    from pprint import pprint

    basepath = dirname(dirname(dirname(os.path.abspath(__file__))))

    ntupparser = NtupleParser(os.path.join(basepath, "productions", "CrispyCyanide"))
    pprint(ntupparser.Parse("user.liuya:user.liuya.mc16_13TeV.364188.S3.DS_Yang_V05a.e5340_s3126_r9364_r9315_p3401_XAMPP"))
    pprint(ntupparser.Parse("user.liuya:user.liuya.data17_13TeV.periodB.S3.DS_Yang_V05a.grp17_v01_p3372_XAMPP"))

    ntupparser2 = NtupleParser(os.path.join(basepath, "productions", "DextralDeltoid"))
    pprint(ntupparser2.Parse("user.fkrieter:user.fkrieter.XAMPP.HadHad.DD3_mc16a.Sherpa222_VVV_XAMPP"))
