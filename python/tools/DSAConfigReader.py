import os
import json

try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser

codepath = os.getenv("DSALOOP_DIR")

class DSAConfig(dict):
    """Read a DSA config file."""

    def __init__(self, configfile=os.path.join(codepath, "config.ini")):
        super(DSAConfig, self).__init__()
        self.Read(configfile)

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def Read(self, configfile):
        if not os.path.isfile(configfile):
            raise IOError("{} does not exist!")
        parser = ConfigParser.ConfigParser()
        parser.read(configfile)
        # PATHS:
        self["OutputPath"] = parser.get("PATHS", "OutputPath")
        self["ProductionName"] = parser.get("PATHS", "ProductionName")
        # FILES:
        self["InputLists"] = json.loads(parser.get("FILES", "InputLists"))
        self["LumiCalc"] = json.loads(parser.get("FILES", "LumiCalc"))
        self["FakeFactors"] = parser.get("FILES", "FakeFactors")

    def Print(self):
        print(json.dumps(self, indent=4))
