#!/usr/bin/env python2.7
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

import re

class ExtractionException(Exception):
    pass

class Sample(object):
    """ class providing a unique interface from sample names """
    def __init__(self, name):
        self.initialExpression = name
        self.datasetType = None
        self._project = None
        self._subProject = None
        self._category = None # DATA / MC16a / MC16d / MC16e
        self._ID = "00"
        self._physicsShort = None
        self._streamName = None
        self._prodStep = None
        self._dataType = None
        self._version = None

        self._DID = None
        scopeSplit = name.split(":")
        if len(scopeSplit) == 1:
            self._DID = name
        elif len(scopeSplit) == 2:
            self._DID = scopeSplit[1]
            self._scope = scopeSplit[0]
        else:
            logger.error("DID no assigned")
            raise ExtractionException
        if self._DID.endswith('/'):
            self._DID = self._DID[:-1]
        self._parseSample(self._DID)

    def _parseSample(self, DID):
        """ parsing the sample and identifying the type (https://cds.cern.ch/record/1070318/files/gen-int-2007-001.pdf) """
        if DID[:5] == 'user.':
            self.datasetType = 'user'

        elif DID[:6] == 'group.':
            self.datasetType = 'group'
        elif 'PhysCont' in DID:
            # self.datasetType = 'container'
            self.datasetType = 'Data'
            split = DID.split(".")
            assert len(split) == 6
            self._project, self._runRange, self._streamName, self._prodStep, self._dataType, self._version = split
            self.subProject = self._project.split("_")[1]
            self._ID = "{}.{}".format(self._project, self._runRange)
        elif re.search('data\d{2}_calib.', DID) is not None:
            self.datasetType = 'calibration'
        elif re.search('data\d{2}_', DID) is not None:
            self.datasetType = 'Data'
            split = DID.split(".")
            assert len(split) == 6
            self._project, self._ID, self._streamName, self._prodStep, self._dataType, self._version = split
            self.subProject = self._project.split("_")[1]
        elif re.search('mc\d{2}_', DID) is not None:
            self.datasetType = 'MC'
            split = DID.split(".")
            assert len(split) == 6
            self._project, self._ID, self._physicsShort, self._prodStep, self._dataType, self._version = split
            self.subProject = self._project.split("_")[1]
            self._category = {
                "r9364":  "MC16a",
                "r9781":  "MC16c",
                "r10201": "MC16d",
                "r10724": "MC16e",
            }.get(re.search("r\d+_", DID).group(0).strip("_"), None)
        else:
            raise ExtractionException("could not find datasetType for {}".format(DID))
        if self.datasetType == "Data":
            self._category = "DATA"

    def ID(self):
        return self._ID

    def Version(self):
        return self._version

    def Category(self):
        return self._category

    def DID(self):
        """ extract DID from name should not have "scope:*", such that it can be used in rucio and ami """
        return self._DID

    def AOD(self):
        """ extract AOD from name """
        if self._dataType == "AOD":
            return self.DID()
        return DAODtoAOD(self._DID)

    def scope(self):
        """ returns the scope of the sample, e.g. mc15_13TeV or None """
        try:
            return self._scope
        except AttributeError:
            DID = self.DID()
            if DID.startswith('user') or DID.startswith('group'):
                return ".".join(DID.split('.')[0:2])
            else:
                return DID.split('.')[0]

    def __repr__(self):
        lines = []
        lines.append("Sample ({}):".format(self.datasetType))
        lines.append(" - original name: {}".format(self.initialExpression))
        lines.append(" - project: {}".format(self._project))
        lines.append(" - ID: {}".format(self._ID))
        lines.append(" - physics short: {}".format(self._physicsShort))
        lines.append(" - stream: {}".format(self._streamName))
        lines.append(" - production step: {}".format(self._prodStep))
        lines.append(" - data type: {}".format(self._dataType))
        lines.append(" - version: {}".format(self._version))
        return '\n'.join(lines)

# functions that might be interesting standalone
def DAODtoAOD(DID):
    """ get AOD from DAOD """
    outDID, count = re.subn('DAOD_\w{4}\d+', 'AOD', DID)
    outDID = re.sub("deriv", "merge", outDID)
    if count != 1:
        logger.warning("{} couldn't be replaced to AOD".format(DID))
        raise ExtractionException
    # usually we want to strip one pTag (reprocessed data)
    outDID, pTag = outDID[:-6], outDID[-5:]
    if re.search('p\d{4}', pTag) is None:
        logger.warning("pTag removal failed {}, {}".format(DID, pTag))
        raise ExtractionException
    return outDID

def GuessProcess(name):
    ProcessDefs = {"Wlnu": ["Wenu", "Wmunu", "Wtaunu", "Wplusenu", "Wplusmunu", "Wplustaunu", "Wminusenu",
                            "Wminusmunu", "Wminustaunu"],
                   "ttV": ["ttW", "ttZ", "ttbarWW", "ttbarWll", "ttee", "ttmumu", "tttautau", "ttll", "tta"],
                   "DY": ["Zmumu_Mll10to40", "Ztautau_Mll10to40", "Zee_Mll10to40"],
                   "Zll": ["Zee", "Zmumu", "Ztautau", "Znunu"],
                   "Diboson": ["WplvWmqq", "WpqqWmlv", "WlvZqq", "WqqZll", "WqqZvv", "ZqqZll", "ZqqZvv", "_llll",
                               "_lllv", "_llvv", "WWlvlv", "WZlvll", "WZlvvv", "ZZllll", "ZZvvll", "ZZvvvv", "WWlvqq",
                               "WZqqll","WZqqvv", "WZlvqq", "ZZqqll", "ZZvvqq",
                               'ggllll', '_ggllvv', '_lvvv', '_vvvv', '6l0v', '5l1v', '4l2v', '3l3v', '2l4v',
                               'ggZvvZqq', 'ggWmlvWpqq', 'ggZvvZqq', 'ggWmlvWpqq'],
                   "Triboson": ["_ZZZ_","_WWW_","_WWZ_","_WZZ_"],
                   # "ttH": ["ttH125_dil", "ttH125_semilep", "ttH125_allhad"],
                   "Higgs": ["ttH125_dil", "ttH125_semilep", "ttH125_allhad", "_ggH125_", "_WH125_", "_ZH125_", "_VBFH125_"],
                   "ttbar": ["SingleLepton", "DiLepton", "AllHadron", "_allhad", "_nonallhad"],
                   "singletop": ["singletop_tchan", "Wt_inclusive", "Wt_dilepton", "SingleTopSchan", "Wt_DS_dilepton", "Wt_DS_inclusive", "Wt_DR_inclusive", "tWZDR", "tZ_4fl_tchan"],
                   "multitop": ["3top", "4top"],
                   "dijet": ["jetjet_JZ"],
                   "data15": ["data15"],
                   "data16": ["data16"],
                   "data17": ["data17"],
                   "data18": ["data18"],
                   "Signal": ["TT_stau", "C1N2_Wh", "StauStau"]}
    if "ttZnunu" in name:
        return ("ttV", "ttZnunu")
    # if "Zee_Mll10_40" in name:
    #     return ("Zll", "Zee")
    if "Zmm_Mll10_40" in name:
        return ("Zll", "Zmumu")
    if "Ztt_Mll10_40" in name:
        return ("Zll", "Ztautau")

    for process in sorted(ProcessDefs.keys()):
        for p in ProcessDefs[process]:
            if p in name:
                if process == 'Signal':
                    if "TT_stau" in name:
                        search = re.search("TT_stau_(?P<mstop>\d{2,4})_(?P<mstau>\d{2,4})", name)
                        if search is None:
                            print ("searching {} for masses fails".format(name))
                        masses = "TT_{}_{}".format(search.group('mstop'), search.group('mstau'))
                        return (process, masses)
                    elif "C1N2_Wh_hall" in name:
                        search = re.search("C1N2_Wh_hall_(?P<mC1N2>\d{1,4}p\d{1})_(?P<mN0>\d{1,4}p\d{1})", name)
                        masses = "C1N2_Wh_hall_{}_{}".format(search.group('mC1N2'), search.group('mN0'))
                        if search is None:
                            print ("searching {} for masses fails".format(name))
                        return (process, masses)
                    elif "StauStau_direct" in name:
                        search = re.search("StauStau_direct_(?P<mC1N2>\d{1,4}p\d{1})_(?P<mN0>\d{1,4}p\d{1})", name)
                        process = "StauStau_direct"
                        masses = "{}_{}".format(search.group('mC1N2'), search.group('mN0'))
                        if search is None:
                            print ("searching {} for masses fails".format(name))
                        return (process, masses)
                    elif "StauStau" in name:
                        search = re.search("StauStau_(?P<mC1N2>\d{1,4}p\d{1})_(?P<mN0>\d{1,4}p\d{1})_TFilt", name)
                        if "_mmix_" in name:
                            process = "StauStauMMIX"
                        else:
                            process = "StauStau"
                        masses = "{}_{}".format(search.group('mC1N2'), search.group('mN0'))
                        if search is None:
                            print ("searching {} for masses fails".format(name))
                        return (process, masses)
                else:
                    return (process, p)

        if process in name:
            return (process, process)
    print ("Problems with guessing Process out of {}. Help me!".format(name))
    return ("Unkown!", "More Unkown")

def GuessGenerator(name):
    if "data" in name:
        return "ATLAS"
    elif "Sherpa_NNPDF30NNLO" in name:
        return "Sherpa2.2"
    elif "Sherpa_222" in name:
        return "Sherpa2.2.2"
    elif "Sherpa_221" in name:
        return "Sherpa2.2.1"
    elif "Sherpa" in name:
        return "Sherpa2.1"
    elif "TT_stau" in name:
        return "StopStau"
    elif "C1N2_Wh_hall" in name:
        return "C1N2_Wh_hall"
    elif "Stau_direct" in name:
        return "DirectStau"

    GeneratorDefs = ["Powheg", "MGPy8EG","aMcAtNloHerwigppEvtGen", 'MadGraphPythia8EvtGen', 'MadGraphPythiaEvtGen', "Pythia8EvtGen", "PhHppEG", "PhPyEG", "PhPy8EG"]
    for gen in GeneratorDefs:
        if gen in name:
            return gen
    print ("Problems with guessing Generator out of {}. Help me!".format(name))
    return "Unknown Gen!"


if __name__ == '__main__':
    # some test cases for this class:
    # for name in ['mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470',
    #              'mc15_13TeV:mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470',
    #              'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470/',
    #              'mc15_13TeV:mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470/']:
    #     ttbar = Sample(name)
    #     print(ttbar)
    #     assert ttbar.scope() == 'mc15_13TeV', "{}".format(ttbar.scope())
    #     # assert ttbar._project == 'mc15_13TeV', ttbar._project
    #     assert ttbar.DID() == 'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470'
    #     assert ttbar.AOD() == 'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7267_r6282'
    #     assert ttbar.ID() == '410000'
    #
    # data15 = Sample("data15_13TeV.00276511.physics_Main.merge.DAOD_SUSY5.r7562_p2521_p2667")
    # print(data15)
    # assert data15.scope() == 'data15_13TeV'
    # assert data15.ID() == '00276511', data15.ID()
    # assert data15.DID() == "data15_13TeV.00276511.physics_Main.merge.DAOD_SUSY5.r7562_p2521_p2667"
    #
    # for name in ['user.bschacht.0001.v020-87Data_FF.SUSYTauAnalysisCycle.data15_00276262.f620_m1480_p2425_SUSYTauAnalysis.root']:
    #     cont = Sample(name)
    #     print(cont)
    #     assert cont.scope() == 'user.bschacht', "{}".format(ttbar.scope())
    #     assert cont.DID() == 'user.bschacht.0001.v020-87Data_FF.SUSYTauAnalysisCycle.data15_00276262.f620_m1480_p2425_SUSYTauAnalysis.root'
    #     try:
    #         cont.AOD()
    #         assert False, cont.AOD()
    #     except ExtractionException:
    #         pass
    #     # assert cont.ID() == '00276262', cont.ID()

    for name in [
      "mc16_13TeV:mc16_13TeV.345121.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulm15hp20.deriv.DAOD_SUSY3.e5814_s3126_r10724_p3652",
      "mc16_13TeV:mc16_13TeV.345122.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulp15hm20.deriv.DAOD_SUSY3.e5814_s3126_r10724_p3652",
      "mc16_13TeV:mc16_13TeV.342284.Pythia8EvtGen_A14NNPDF23LO_WH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r10724_p3652",
      "mc16_13TeV:mc16_13TeV.342285.Pythia8EvtGen_A14NNPDF23LO_ZH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r10724_p3652",
      "mc16_13TeV:mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_SUSY3.e6337_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY3.e6337_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.410081.MadGraphPythia8EvtGen_A14NNPDF23_ttbarWW.deriv.DAOD_SUSY3.e4111_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.407321.MadGraphPythia8EvtGen_A14NNPDF23LO_ttbarWll.deriv.DAOD_SUSY3.e5536_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.343366.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_semilep.deriv.DAOD_SUSY3.e4706_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.342285.Pythia8EvtGen_A14NNPDF23LO_ZH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.342284.Pythia8EvtGen_A14NNPDF23LO_WH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.343367.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_allhad.deriv.DAOD_SUSY3.e4706_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.345122.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulp15hm20.deriv.DAOD_SUSY3.e5814_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.345121.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulm15hp20.deriv.DAOD_SUSY3.e5814_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.345123.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautauh30h20.deriv.DAOD_SUSY3.e5814_s3126_r10201_p3529",
      "mc16_13TeV:mc16_13TeV.345121.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulm15hp20.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3529",
      "mc16_13TeV:mc16_13TeV.345122.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulp15hm20.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3529",
      "mc16_13TeV:mc16_13TeV.342285.Pythia8EvtGen_A14NNPDF23LO_ZH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r9364_p3529",
      "mc16_13TeV:mc16_13TeV.345123.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautauh30h20.deriv.DAOD_SUSY3.e5814_s3126_r9364_p3529",
      "mc16_13TeV:mc16_13TeV.342284.Pythia8EvtGen_A14NNPDF23LO_WH125_inc.deriv.DAOD_SUSY3.e4246_s3126_r9364_p3529",
      "mc16_13TeV:mc16_13TeV.343367.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_allhad.deriv.DAOD_SUSY3.e4706_s3126_r9364_p3529",
      "mc16_13TeV:mc16_13TeV.343366.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_semilep.deriv.DAOD_SUSY3.e4706_s3126_r9364_p3529"
    ]:
        smpl = (Sample(name))
        prcs, subprcs = GuessProcess(name)
        print("{name}\n  * Process: {process}\n  * Subprocess: {subprocess}\n".format(
            name=smpl.initialExpression,
            process=prcs,
            subprocess=subprcs))


    print("passed all tests!")
