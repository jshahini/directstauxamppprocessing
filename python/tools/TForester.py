#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Class for skimming & slimming of ntuples."""

__author__ = "Ferdinand Krieter"
__email__  = "ferdinand.krieter@cern.ch"

import os
import re
import sys

import ROOT

from collections import OrderedDict

from logger import logger

class TForester(object):

    required_kwargs = ["tree", "outfilepath"]
    optional_kwargs = ["macrodir", "classname", "skimming"]

    def __init__(self, infilepath, **kwargs):
        for kwarg in kwargs.keys():
            if not kwarg in list(TForester.required_kwargs + TForester.optional_kwargs):
                logger.error("Unknown keyword argument '{}'!".format(kwarg))
                raise KeyError
        for required_kwarg in TForester.required_kwargs:
            if not kwargs.get(required_kwarg):
                logger.error("{}() is missing required keyword argument '{}'!".format(
                    self.__class__.__name__, required_kwarg))
                raise KeyError
        self._infilepath  = infilepath
        self._treename    = kwargs.get("tree")
        self._outfilepath = kwargs.get("outfilepath")
        self._classname   = kwargs.get("classname", self._treename)
        self._macrodir    = kwargs.get("macrodir", "") # default = here
        if not self._macrodir.startswith("/"):
            self._macrodir = os.path.join(os.getcwd(), self._macrodir)
        if not self._outfilepath.startswith("/"):
            self._outfilepath = os.path.join(os.getcwd(), self._outfilepath)
        self._treeloop_C = None
        self._treeloop_h = None
        self._skimming   = kwargs.get("skimming", [])
        self._slimming   = kwargs.get("slimming", [])
        try:
            logger.info("Opening ROOT file '{}'...".format(self._infilepath))
            self.infile = ROOT.TFile.Open(self._infilepath, "READ")
        except IOError:
            logger.error("{} does not exist!".format(self._infilepath))
            raise IOError
        self._intree = self.infile.Get(self._treename)
        if not self._intree:
            logger.error("{} has no tree named {}!".format(self._infilepath, self._treename))
            raise KeyError
        self._BuildMacro()

    def _GetFileContentDict(self):
        """Structure: key=tuple(tkeyname, tkeyclass), value=list of tuples in subfolder if key is a tdir else []"""
        def _recurseTDir(tdirectory, folder):
            if not isinstance(tdirectory, ROOT.TDirectory):
                return
            for tkey in tdirectory.GetListOfKeys():
                tkeyname = tkey.GetName()
                tkeyclass = re.search(r'"([^"]*)"', str(tdirectory.Get(tkeyname).Class())).group().strip('\"')
                entry = (tkeyname, tkeyclass)
                if tkey.IsFolder():
                    subfolder = {entry: []}
                    folder.append(subfolder)
                    _recurseTDir(tdirectory.Get(tkeyname), subfolder[entry])
                else:
                    folder.append(entry)
        tfilecontent = []
        _recurseTDir(self.infile, tfilecontent)
        return tfilecontent

    def _BuildMacro(self):
        with CWD(self._macrodir):
            logger.info("Generating loop algorithm at {}...".format(self._macrodir))
            self._intree.MakeClass(self._classname)
            self._treeloop_h = "{}.h".format(self._classname)
            self._treeloop_C = "{}.C".format(self._classname)
            headerlines = open(self._treeloop_h, "r").readlines()
            cxxlines = open(self._treeloop_C, "r").readlines()
            branchvar_dict = OrderedDict()
            for line in headerlines:
                if "fChain->SetBranchAddress" in line:
                    branchname = re.search(r'"([^"]*)"', line).group().strip('\"')
                    varname = re.sub("[{}]".format("".join([r"\{}".format(s) for s in "!@#$%^&*()[];:,./<>?\|`~-=_+"])), \
                        "_", branchname)
                    branchvar_dict[branchname] = varname

            # H file:
            with open(self._treeloop_h, "w") as h:
                for line in headerlines:
                    # Insert before current line:
                    if """class {} {{""".format(self._classname) in line:
                        h.write("#include <iostream>\n#include <string>\n")
                    # Insert current line. Make sure to substitute ill formatted varibales:
                    ## Fix, since by default special characters in branch names yield variables with the same name:
                    if "fChain->SetBranchAddress" in line:
                        p1, p2, p3 = line.split()
                        branchname = re.search( r'"([^"]*)"' , p1).group().strip('\"')
                        line = " "*3 + " ".join([p1, re.sub(branchname, branchvar_dict[branchname], p2), \
                            re.sub(branchname, branchvar_dict[branchname], p3)]) + "\n"
                    else:
                        for original, substitute in branchvar_dict.iteritems():
                            if original in line:
                                line = re.sub(original, substitute, line)
                    ## The input file is a member variable:
                    for original, substitute in {
                        "TFile *f = (TFile*)gROOT": "InFile = (TFile*)gROOT",
                        "(!f || !f->IsOpen())": "(!InFile || !InFile->IsOpen())",
                        "   f = new TFile": "   InFile = new TFile",
                        "   f->GetObject": "   InFile->GetObject"}.iteritems():
                        if original in line:
                            line = line.replace(original, substitute)
                            break
                    h.write(line)
                    # Insert after current line:
                    ## Header:
                    if "#include <TFile.h>" in line:
                        h.write("""
#include <TKey.h>
#include <TH1D.h>\n""")
                    ## Declaration of new member variables:
                    if "Int_t           fCurrent;" in line:
                        h.write("""
   TFile          *InFile;
   TTree          *OutTree;
   TFile          *OutFile;
   int nevts_initial;
{cutflowvar_decls}\n""".format(cutflowvar_decls="\n".join(["   int nevts_cut{};".format(i) for i in range(len(self._skimming))])))
                    if "virtual ~{}();".format(self._classname) in line:
                        h.write("""
   virtual void     InitOutputTree();
   virtual bool     PassesSkimming();
   virtual void     CopyDir(TDirectory* source, TDirectory*);
   virtual void     CopyDir(TFile* source, TDirectory*);
   virtual void     WriteCutflow();
   virtual void     WriteTree();\n""")
                    ## Implementation of new classes:
                    if line.startswith("#ifdef {}_cxx".format(self._classname)):
                        h.write("""
void {classname}::InitOutputTree()
{{
{branch_decls}
}}
bool {classname}::PassesSkimming()
{{
   nevts_initial ++;
{cut_ifs}
   return true;
}}
void {classname}::CopyDir(TFile* source, TDirectory* target)
{{
   CopyDir(dynamic_cast<TDirectory*>(source), target);
}}
void {classname}::CopyDir(TDirectory* source, TDirectory* target)
{{
   TDirectory *initdir = gDirectory;
   source->cd();
   TKey *key;
   TIter nextkey(source->GetListOfKeys());
   while ((key = (TKey*)nextkey())) {{
      if(strncmp(key->GetName(), "Staus_TAUS", 10) == 0) continue;
      else if(strncmp(key->GetName(), "Staus_MUON", 10) == 0) continue;
      else if(strncmp(key->GetName(), "Staus_JET", 9) == 0) continue;
      else if(strncmp(key->GetName(), "Staus_MET", 9) == 0) continue;
      else if(strncmp(key->GetName(), "Staus_EG", 8) == 0) continue;
      const char *classname = key->GetClassName();
      TClass *cl = gROOT->GetClass(classname);
      if (!cl) continue;
      if (cl->InheritsFrom(TDirectory::Class())) {{
         source->cd(key->GetName());
         TDirectory *source_subdir = gDirectory;
         target->cd();
         TDirectory *target_subdir = gDirectory->mkdir(source_subdir->GetName());
         std::cout << cl->GetName() << " '" << target_subdir->GetName() << "' has been created within '" << target->GetName() << "'" << std::endl;
         CopyDir(source_subdir, target_subdir);
         source->cd();
      }} else {{
         if (cl->InheritsFrom(TTree::Class())) {{
            if (key->Compare(fChain) == 0) continue;
            TTree *T = (TTree*)source->Get(key->GetName());
            target->cd();
            TTree *newT = T->CloneTree(-1, "fast");
            newT->Write();
         }} else {{
            TObject *obj = key->ReadObj();
            target->cd();
            obj->Write();
            delete obj;
         }}
         std::cout << cl->GetName() << " '" << key->GetName() << "' has been written to '" << gDirectory->GetName() << "'" << std::endl;
      }}
   }}
   target->SaveSelf(kTRUE);
   initdir->cd();
}}
void {classname}::WriteCutflow()
{{
   TH1D *cfh = new TH1D("{cutflow_histname}", "Cutflow histogram for {treename}", {cutflow_nbins}, 0, {cutflow_nbins});
   cfh->Sumw2();
   cfh->SetBinContent(1, nevts_initial);
   cfh->GetXaxis()->SetBinLabel(1, "initial");
{cutflow_fill}
   OutFile->cd();
   cfh->Write();
   std::cout << "TH1D '" << cfh->GetName() << "' has been written to '" << gDirectory->GetName() << "'" << std::endl;
}}
void {classname}::WriteTree()
{{
   OutFile->cd();
   OutTree->Write(0, TObject::kOverwrite);
   std::cout << "TTree '" << OutTree->GetName() << "' has been written to '" << gDirectory->GetName() << "'" << std::endl;
}}\n""".format(classname=self._classname,
    treename=self._treename,
    branch_decls="\n".join(['   OutTree->Branch("{}", &{});'.format(branchname, variable) for branchname, variable in branchvar_dict.iteritems()]),
    cut_ifs="\n".join(["""   if (!({boolean})) return false;\n   nevts_cut{index} ++;""".format(boolean=cuttpl[1], index=i) for i, cuttpl in enumerate(self._skimming)]),
    cutflow_histname="{prefix}.{treename}.Cutflow".format(prefix=self.__class__.__name__,
        treename=self._treename),
    cutflow_nbins=len(self._skimming)+1,
    cutflow_fill="\n".join(["""   cfh->SetBinContent({binindex}, nevts_cut{cutindex});\n   cfh->GetXaxis()->SetBinLabel({binindex}, "{cutname}");""".format(binindex=i+2, cutindex=i, cutname=cuttpl[0]) for i, cuttpl in enumerate(self._skimming)])))
                    ## Create outfile and tree:
                    if "fChain->SetMakeClass(1);" in line:
                        h.write("""
   OutFile = new TFile("{outfilepath}", "RECREATE");
   OutTree = new TTree("{treename}", fChain->GetTitle());
   nevts_initial = 0;
{cutflowvar_inits}\n""".format(outfilepath=self._outfilepath,
    treename=self._treename,
    cutflowvar_inits="\n".join(["   nevts_cut{} = 0;".format(i) for i in range(len(self._skimming))])))

            # CXX file:
            with open(self._treeloop_C, "w") as cxx:
                for line in cxxlines:
                    # Insert before the current line:
                    if "Long64_t nbytes = 0, nb = 0;" in line:
                        cxx.write("""   std::cout << "Start looping over '" << fChain->GetName() << "'..." << std::endl;\n""")
                    # Insert current line:
                    cxx.write(line)
                    # Insert after current line:
                    if "if (fChain == 0) return" in line:
                        cxx.write("""
   InitOutputTree();
   CopyDir(InFile, OutFile);\n""")
                    if "nb = fChain->GetEntry(jentry);" in line:
                        cxx.write("""
      if ((jentry+1) % 10000 == 0 || (jentry+1) == nentries) {{
         std::cout << "Processed " << jentry+1 << " / " << nentries << " events" << std::endl;
      }}
      if (!PassesSkimming()) continue;
      OutTree->Fill();\n""")
                    if "   }" in line:
                        cxx.write("""
   WriteCutflow();
   WriteTree();
   OutFile->Close();\n""")
        logger.info("Finished creating C++ macro")

    def Run(self, **kwargs):
        compile_macro = kwargs.get("compile", True)
        with CWD(self._macrodir):
            if compile:
                logger.info("Compiling macro '{}.C'...".format(self._classname))
                ROOT.gROOT.ProcessLine(".L {}.C++".format(self._classname))
            else:
                ROOT.gROOT.ProcessLine(".L {}.C".format(self._classname))
            logger.info("Initializing '{}' class...".format(self._classname))
            looper = ROOT.__getattr__(self._classname)()
            logger.info("Executing '{}' loop algorithm...".format(self._classname))
            looper.Loop()
        if os.path.isfile(self._outfilepath):
            logger.info("Sucess! Exiting...")
            return 0
        logger.error("Something went wrong! X_X")
        return 1


class CWD:
    """Context manager for changing the current working directory (CWD)."""

    def __init__(self, path):
        self.cwd = path # current working directory
        self.pwd = None # previous working directory
        self.cwd_basedir = os.path.dirname(self.cwd)
        if not os.path.exists(self.cwd_basedir):
            logger.error("'{}' does not exist!".format(self.cwd_basedir))
            raise OSError
        if not os.path.exists(self.cwd):
            os.mkdir(self.cwd)

    def __enter__(self):
        self.pwd = os.getcwd()
        os.chdir(self.cwd)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.pwd)


if __name__ == '__main__':

    import json
    import argparse

    argparser = argparse.ArgumentParser("The TForester: He cuts down TLeafs and TBranches and TTrees...")
    argparser.add_argument('infilepath',
        type=str,
        metavar="INFILEPATH",
        help='input file path')
    argparser.add_argument('-t', '--tree',
        required=True,
        type=str,
        help='name of input tree to be skimmed')
    argparser.add_argument('-o', '--outfilepath',
        required=True,
        type=str,
        help='output file path')
    argparser.add_argument('--macrodir',
        type=str,
        default="./",
        help='directory holding generated C++ macros, default: %(default)s')
    argparser.add_argument('--classname',
        type=str,
        default="TREE",
        help='name of the macro files (and their internal class), default: %(default)s')
    argparser.add_argument('--skimming',
        type=str,
        default="'[]'",
        help="list of single item dicts of the form {cutname: cutexpr} in JSON format (e.g. use json.dumps(mylist)). cutexpr must be a string of a C++ boolean (e.g. 'branch>=value'), default: %(default)s = no cuts)")

    options = vars(argparser.parse_args())

    options.setdefault("classname", options.get("tree"))
    skimming_tuples = []
    for d in json.loads(options.get("skimming")):
        if len(d) != 1:
            raise TypeError("Get some --help")
        skimming_tuples.append( (d.keys()[0], d.values()[0]) )
    options["skimming"] = skimming_tuples
    infilepath = options.pop("infilepath")

    loop = TForester(infilepath, **options)
    exitcode = loop.Run()
    sys.exit(exitcode)
