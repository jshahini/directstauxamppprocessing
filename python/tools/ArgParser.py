#!/usr/bin/env python3

from __future__ import print_function

import json
import argparse

class ArgParser(argparse.ArgumentParser):
    """Extends default ArgumentParser with some default options"""
    def __init__(self, **kwargs):
        """Initialize default options"""
        super(ArgParser, self).__init__(**kwargs) # Note: allow_abbrev is available from Python 3.5 upwards
        # Set default options here:
        self.add_argument("--nameTree",
                          default="Nominal",
                          type=str,
                          help="name of nominal input tree (default: %(default)s)")
        self.add_argument("--metadataTree",
                          default="MetaDataTree",
                          type=str,
                          help="name of metadata input tree (default: %(default)s)")
        self.add_argument("--noTrigger",
                          default=False,
                          action="store_true",
                          help="turn of trigger (dec+match+plateau) in preselection step (default: %(default)s)")
        self.add_argument("--MuonSelection",
                          default=False,
                          action="store_true",
                          help="apply preselection and trigger selection for W+Jets CR/VR (default: %(default)s)")
        self.add_argument("--signalTauID",
                          default=False,
                          type=str,
                          choices=["default", "loose", "medium", "tight"],
                          help="choose identification working point for signal taus (default: XAMPP definition)")
        self.add_argument("--selectAntiTaus",
                          default=False,
                          action="store_true",
                          help="select anti taus for fake factor method (default: %(default)s)")
        self.add_argument("--ttbarMETSlicing",
                          default=False,
                          action="store_true",
                          help="remove overlap between ttbar datasets (default: %(default)s)")
        self.add_argument("-l",
                          "--luminosity",
                          default=1.0,
                          type=json.loads,
                          help="set the global luminosity scaling in fb^-1 (default: %(default)s). individual luminosities" \
                          " for the MC16a, MC16d and MC16e subcampaign can be set by passing a dictionary of the form " \
                          "'{\"MC16a\": <LUMI(data15+16)>, \"MC16d\": <LUMI(data17)>, \"MC16e\": <LUMI(data18)>}' instead.")
        self.add_argument("--FFMTargetSelection",
                          default="Default",
                          type=str,
                          help="target selection for QCD estimate, 'Default' = at least two signal (medium) taus, " \
                          "'AtLeastOneTightTau' = 'Default' && tau1 or tau2 is tight, 'AtLeastTwoTightTaus' = tau1 and tau2 is tight (default: %(default)s)")

    def retrieve_args(self):
        """Returns options dictionary"""
        return vars(self.parse_args())


if __name__ == '__main__':
    myParser = ArgParser(description="This is a test")
    myParser.add_argument("--printMeep",
                          default=False,
                          action="store_true",
                          help="print meep, for no apparent reason (default: %(default)s)")
    options = myParser.retrieve_args()

    for arg, value in options.items():
        print(arg.ljust(max([len(key) for key in options.keys()])), "=", value)

    if options["printMeep"]:
        print("\n...MEEP!")
