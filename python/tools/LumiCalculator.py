import ROOT
import os, re

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

def CalculateLumi(LumiCalcDict, runnumbers):
    '''
    Takes a dictionary of ilumicalc files with keys data15 and data16 and a list of runnubers
    Returns the integrated luminosity of these runs in inverse femtobarn
    '''

    from ROOT import TFile

    # get a dictionary to save the lumi per run
    runLumiDict = {}
    for runnumber in runnumbers:
        runLumiDict[str(int(runnumber))] = -1.0 # the lumical files do not use the leading zeros

    for period, ilumicalcFile in LumiCalcDict.items():
        if not os.path.isfile(ilumicalcFile):
            logger.error("trying to access ilumicalcfile {} for period {}, but is not a file!".format(ilumicalcFile, period))
            continue

        logger.debug("Calculating Luminosity from file: {0}".format(os.path.basename(ilumicalcFile)))
        lumicalc = TFile(ilumicalcFile, 'READ')

        content = lumicalc.GetListOfKeys()
        for entry in content:
            search = re.search("run(\d{6})_intlumi", entry.GetName())
            if search is None:
                continue
            else:
                ID = search.group(1)
            if ID is not None and ID in runLumiDict:
                if runLumiDict[ID] >= 0.0:
                    logger.warning("Skipping run {} in period {}, because Lumi is determined from other file".format(ID, period))
                    continue

                try:
                    runLumiDict[ID] = lumicalc.Get("run{}_intlumi".format(ID)).GetMaximum()
                    # print ID, runLumiDict[ID]
                except AttributeError:
                    logger.error('Run {0} is not contained in the LumiCalc file!'.format(ID))

        lumicalc.Close()

    L = 0.0
    for runnumber, lumi in runLumiDict.items():
        if lumi < 0.0:
            logger.warning("no luminosity extracted for run {}".format(runnumber))
            continue
        L += lumi

    # To get inverse fb
    L /= 1.0e9
    # logger.info("Luminosity: {0:6.2f} inverse fb".format(L))
    return L

if __name__ == '__main__':
    # little test one of the runs is not in the files ;)
    print CalculateLumi({'data15': '/project/etp3/bschacht/Code/binaries/20160628/PRW/ilumicalc_histograms_None_276262-284484.root', 'data16': '/project/etp3/bschacht/Code/binaries/20160628/PRW/ilumicalc_histograms_None_297730-301973.root'}, ['276262', '301973', '000000'])
