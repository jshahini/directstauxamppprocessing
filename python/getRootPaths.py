#!/usr/bin/env python2.7

"""
    Simple script for getting paths to ntuples stored at LRZ-LMU_LOCALGROUPDISK.
    Use it like this:
    Enter a SLC6 environemnt
    lsetup python rucio
    source init.sh  # now all scripts can be executed globally
    getRootPaths.py container1 container2 ...
      or
    getRootPaths.py containerlist1.txt containerlist2.txt ... # must end with .txt
      or
    rucio list-dids --short "user.fkrieter:*<ProductionTag>_XAMPP" | xargs getRootPaths.py
      or
    getRootPaths.py /folder/to/lists/*.txt
"""

import os
import sys
from tools.rucioUtil import rootPaths

def listDIDS(*args):
    dids = []
    for arg in args:
        if arg.endswith(".txt"):
            for line in open(arg, "r").read().splitlines():
                dids.append(line)
        else:
            dids.append(arg)
    return dids

def main():
  for dataset in listDIDS(*sys.argv[1:]):
      for path in rootPaths(dataset):
          print '{} {}'.format(dataset, path)

if __name__ == '__main__':
  main()
