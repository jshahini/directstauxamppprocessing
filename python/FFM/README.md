# Fake factor method

The following describes the how to compute fake factors from a  given project and how to apply them in order to create ntuples to be used for estimating multijet background. Currently it is designed to give a multijet estimate for **target regions** with **at least two signal taus** (default: **medium**) _and_ **one or more** fulfilling **tight** identification.


### Measuring fake factors

Create a new project (which includes data and MC samples)
```
newproject.py -v1 --selectAntiTaus
```
After all jobs finished successfully and the outputs have been merged, configure the calculator via the ```fakefactors_config.ini``` file (if you don't have one yet, copy the default one):
* ***ProjectPath*** (path to your previously created project)
* ***OutputPath*** (folder in which the results are saved)
* ***InputDir*** (folder within your project holding the input files)
* ***InputLists*** (list of data and MC input files, respectively)
* ***Cuts*** (measurement region cuts, same-sign requirement is implicit)
* ***Weights*** (weight expressions for data and MC, respectively)
* ***BinningPt*** (list bin edges for tau1/2Pt)
* ***BinningEta*** (list bin edges for tau1/2Eta)

The fake factor histograms can then be created via
```
python python/FFM/FakeFactorCalculator.py
```
Systematic uncertainties are automatically computed and include the following sources:
* Variation between FFs measured for the leading and subleading tau
* Variation between the same-sign measurement and the opposite-sign application region
* Statistical uncertainty of the FF combined measurements


### Creating QCD samples

The multijet background is estimated using the full set of data and MC samples.
For this a new project is created in which the set of fake factors defined by the **FakeFactors** property in  ```config.ini``` is included into the computation of the **ff_weight** variable:
```
newproject.py -v2 --applyFakeFactors
```
Moreover a target region can be specified via the ```--FFMTargetSelection``` flag.
Consider also requiring ```--signalTauID tight``` for a *AtLeastTwoTightTaus* target region to increase the available anti-ID tau statistics by including also medium taus this way.

The resulting output files must be merged, e.g. via ```mergedCWD.py```, as only the combined sample should be used for comparisons with data.


### Plotting

To get the correct histograms from the QCD trees apply both the **evt_weight** and the **ff_weight**.
Make sure to use the same preselection and triggers as for the corresponding measurment.


### Further remarks

* As all taus in a QCD sample have anti-ID, the easiest way to plot this background along with data and/or other MC backgrounds in a medium-tight or tigh-tight selection  is to use the **atLeastOneTightTau** or **atLeastTwoTightTaus** variable.
* When using a project with **SelectAntiTaus** turned on in the job options to create plots for some target region, don't forget to require that both taus are of signal ID.
* All taus in QCD samples are defined to have signal ID. Note that this is only valid if the **evt_weight** and **ff_weight** are applied.
* Due to the offline threshold it is not viable to compute FFs and a QCD estimate for a selection requiring only the di-tau+MET trigger.
