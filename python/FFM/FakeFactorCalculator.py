#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fake factor calculator running on ntuples written by DSAnalysisLoop.

   Use fakefactor_config.ini to define inputs and setting etc.
   Tipp: For best performance choose few (large) ntuples as input."""

__author__ = "Ferdinand Krieter"
__email__  = "ferdinand.krieter@cern.ch"

# TODO: 1.) Add debug plots to output
#       2.) Make combination of FFs configurable

import os
import sys
import glob
import json
import math
import itertools
import collections
import ConfigParser

import ROOT
from ROOT import TH2D

from array import array
from operator import xor as XOR

from tools.logger import logger
from tools.IOManager import IOManager

codepath = os.path.dirname(os.path.abspath(__file__))

class FakeFactorCalculator(object):
    """Calculates the individual and combined fake factors.
       Fake factors are binned w.r.t. to pT, eta and prongness of the leading or
       subleading tau.

       Example:
        tau1Quality
        |________________
        |       |        |
        |   3   |    4   | ID
        |_______|________|
        |       |        |
        |   1   |    2   | Anti-ID
        |_______|________|___ tau2Quality
         Anti-ID    ID

        Fake factors are calculated for each transition between neighboring regions,
        e.g. 1->2, 1->3, ... and are then combined into a mean weighted by their errors.
    """

    def __init__(self, chargecomb, **kwargs):
        assert(chargecomb in ["SS", "OS"])
        self.chargecomb         = chargecomb
        self.project            = None # path to project holding ntuples with antiID taus
        self.outdir             = None # output directory
        self.outfilename        = None # output ROOT file name
        self.histo              = {}   # stores all required histograms
        self.inputdir           = None # directory containing input files (Merged/ or ./)
        self.infiles            = {}   # paths to input ntuples for Data & MC
        self.mr_cuts            = kwargs.get("cuts", [])   # global cuts defining the measurement region
        self.cuts               = {}   # cuts for each region (e.g. tau1/2-quality combination)
        self.weight             = {}   # data (jetTrig-prescales for SUSY11!) & MC weights
        self.binning            = {}   # FF binning for Pt & Eta **as list of bin-low edges**
        self.tauQualityDict     = dict(VL=0, L=1, M=2, T=3)
        self.nbins              = {} # number of bins for Pt and Eta
        self.ReadConfig()
        self.CheckProject()
        self.MakeCutDict()
        self.fakefactor = {} # final fake factor histograms for each transition
        # Create empty histograms to be filled later:
        self.histo = {} # Data & MC tau1/2-histograms for each selection and decay mode
        for dstype in self.infiles.keys():
            self.histo[dstype] = {}
            for region in self.cuts.keys():
                self.histo[dstype][region] = {}
                for tauindex in [1,2]:
                    varexp = "fabs(tau{index}Eta):tau{index}Pt".format(index=tauindex)
                    self.histo[dstype][region][varexp] = {}
                    for decaymode in [1, 3]:
                        self.histo[dstype][region][varexp][decaymode] = ROOT.TH2D(
                            "{}_{}_{}_{}prong_{}".format(dstype, self.chargecomb, region, decaymode, varexp),
                            "",
                            self.nbins["Pt"],    # X
                            self.binning["Pt"],
                            self.nbins["Eta"],   # Y
                            self.binning["Eta"])
                        self.histo[dstype][region][varexp][decaymode].Sumw2()

    def ReadConfig(self):
        config = ConfigParser.ConfigParser()
        config.read(os.path.join(codepath, "fakefactor_config.ini"))
        self.project            = config.get('PATHS', 'ProjectPath')
        self.outdir             = config.get('PATHS', 'OutputPath')
        self.outfilename        = config.get('PATHS', 'OutputFileName')
        self.inputdir           = os.path.join(self.project, config.get('FILES', 'InputDir'))
        self.mr_cuts            += json.loads(config.get('DEFINITIONS', 'Cuts'))
        self.weight             = json.loads(config.get('DEFINITIONS', 'Weights'))
        self.binning            = {"Pt":  array("d", json.loads(config.get('DEFINITIONS', 'BinningPt'))),
                                   "Eta": array("d", json.loads(config.get('DEFINITIONS', 'BinningEta')))}
        self.nbins              = dict(Pt=len(self.binning["Pt"])-1, Eta=len(self.binning["Eta"])-1)
        for dstype, infiles in json.loads(config.get('FILES', 'InputLists')).iteritems():
            self.infiles[dstype] = []
            for infilepattern in infiles: # supports wildcards!
                if not os.path.exists(self.inputdir):
                    logger.error("Input directory '{}' does not exists."
                                 "Exiting...".format(self.inputdir))
                    sys.exit(1)
                for infile in glob.glob(os.path.join(self.inputdir, infilepattern)):
                    self.infiles[dstype].append(infile)

    def CheckProject(self):
        config  = json.load(open(os.path.join(self.project, "joboptions.json"), "r"))
        if not config["SelectAntiTaus"]:
            logger.error("No anti-ID taus have been selected for project '{}'. "
                         "Exiting...".format(self.project))
            sys.exit(1)
        for infile in list(itertools.chain(*self.infiles.values())):
            if not os.path.exists(infile):
                logger.error("Requested input file '{}' does not exist. "
                             "Exiting...".format(infile))
                sys.exit(1)
        logger.info("Project '{}' has been successfully initialized!".format(self.project))

    def _tauQualitiesToCutExpr(self, tauclasses, tauindex):
        if not isinstance(tauclasses, tuple):
            logger.error("Argument 'tauclasses' ({}) is not of type 'tuple'.".format(tauclasses))
            return None
        if not tauindex in [1, 2]:
            logger.error("Argument 'tauindex' ({}) must be 1 or 2.".format(tauindex))
            return None
        return "({})".format("||".join(["tau{}Quality=={}".format(tauindex, self.tauQualityDict.get(q)) for q in tauclasses]))

    def _getListOfTransitions(self, regionlist):
        transitionslist = []
        for A in regionlist:
            for B in regionlist:
                if A[0] != B[0]:
                    if A[1] != B[1]:
                        continue
                    if max([self.tauQualityDict[q] for q in A[0]]) >= min([self.tauQualityDict[q] for q in B[0]]):
                        continue
                elif A[1] != B[1]:
                    if max([self.tauQualityDict[q] for q in A[1]]) >= min([self.tauQualityDict[q] for q in B[1]]):
                        continue
                else:
                    continue
                transitionslist.append((A, B))
        return transitionslist

    def MakeCutDict(self):
        tauQualitiesList = [
            ('VL', 'L'), ('VL', 'L', 'M'),  # anti-ID taus
            ("M",), ("T",), ('M', 'T')      # ID taus
        ]
        for region in list(itertools.product(tauQualitiesList, tauQualitiesList)):
            self.cuts[region] = [self._tauQualitiesToCutExpr(region[0], 1), self._tauQualitiesToCutExpr(region[1], 2)]
            self.cuts[region] += self.mr_cuts
            self.cuts[region] += dict(SS=["OS==0"], OS=["OS==1"]).get(self.chargecomb)

    def FillHistograms(self, **kwargs):
        """Fill temporary histograms and merge them into the corresponding Data and MC histograms."""
        htmp = {}
        for dstype, infiles in self.infiles.iteritems():
            for infile in infiles:
                filename = os.path.basename(infile)[:-5]
                tree = "DirectStau"
                htmp[filename] = {}
                factory = IOManager.Factory(infile, tree)
                logger.info("IOManager.Factory initialized for {} (tree: {})".format(
                    os.path.basename(infile), tree))
                for region, cuts in self.cuts.iteritems():
                    htmp[filename][region] = {}
                    for tauindex in [1, 2]:
                        varexp = "fabs(tau{index}Eta):tau{index}Pt".format(index=tauindex)
                        htmp[filename][region][varexp] = {}
                        for decaymode in [1, 3]:
                            tauprongcut = "tau{}NProng=={}".format(tauindex, decaymode)
                            htmp[filename][region][varexp][decaymode] = ROOT.TH2D(
                                "{}_{}_{}_{}_{}prong".format(filename, self.chargecomb, region, varexp, decaymode), "",
                                self.nbins["Pt"],    # X
                                self.binning["Pt"],
                                self.nbins["Eta"],   # Y
                                self.binning["Eta"])
                            htmp[filename][region][varexp][decaymode].Sumw2()
                            factory.registerHisto(
                                htmp[filename][region][varexp][decaymode],
                                varexp=varexp,
                                cut=list(cuts + [tauprongcut]),
                                weight=self.weight[dstype])
                factory.fillAll()
                for region in htmp[filename].keys():
                    for varexp in htmp[filename][region].keys():
                        for decaymode in [1, 3]:
                            self.histo[dstype][region][varexp][decaymode].Add(
                                htmp[filename][region][varexp][decaymode])
        if kwargs.get("IncludeUnderOverflowBins", True):
            self.IncludeUnderOverflowBins()

    def IncludeUnderOverflowBins(self):
        logger.info("Under- and overflow bins will be included to the first and last bin"
            ", respectivley, for each histogram.")
        for histo in list(deep_values(self.histo)):
            # xaxis = histo.GetXaxis() # Pt
            # yaxis = histo.GetYaxis() # Eta
            # Vertical folding:
            for i in range(0, self.nbins["Pt"]+2, 1):
                # Fold underflow "up":
                merge_bin = histo.GetBin(i, 1)
                uflow_bin = histo.GetBin(i, 0)
                histo.SetBinContent(merge_bin, histo.GetBinContent(merge_bin) + histo.GetBinContent(uflow_bin))
                histo.SetBinError(merge_bin, math.sqrt(histo.GetBinError(merge_bin)**2 + histo.GetBinError(uflow_bin)**2))
                # Fold overflow "down":
                merge_bin = histo.GetBin(i, self.nbins["Eta"])
                oflow_bin = histo.GetBin(i, self.nbins["Eta"]+1)
                histo.SetBinContent(merge_bin, histo.GetBinContent(merge_bin) + histo.GetBinContent(oflow_bin))
                histo.SetBinError(merge_bin, math.sqrt(histo.GetBinError(merge_bin)**2 + histo.GetBinError(oflow_bin)**2))
            # Horizontal folding:
            for i in range(1, self.nbins["Eta"]+1, 1): # mind the range! don't count edges twice!
                # Fold underflow "to the right":
                merge_bin = histo.GetBin(1, i)
                uflow_bin = histo.GetBin(0, i)
                histo.SetBinContent(merge_bin, histo.GetBinContent(merge_bin) + histo.GetBinContent(uflow_bin))
                histo.SetBinError(merge_bin, math.sqrt(histo.GetBinError(merge_bin)**2 + histo.GetBinError(uflow_bin)**2))
                # Fold overflow "to the left":
                merge_bin = histo.GetBin(self.nbins["Pt"], i)
                oflow_bin = histo.GetBin(self.nbins["Pt"]+1, i)
                histo.SetBinContent(merge_bin, histo.GetBinContent(merge_bin) + histo.GetBinContent(oflow_bin))
                histo.SetBinError(merge_bin, math.sqrt(histo.GetBinError(merge_bin)**2 + histo.GetBinError(oflow_bin)**2))

    def ComputeFakeFactors(self):
        """Calculate the fake factor for each transition A -> B."""
        self.FillHistograms(IncludeUnderOverflowBins=True)
        for pair in self._getListOfTransitions(self.cuts.keys()):
            self.fakefactor[pair] = {}
            tauindex = self._getIndexOfPromotedTau(pair)
            varexp = "fabs(tau{index}Eta):tau{index}Pt".format(index=tauindex)
            A, B = pair
            transition_str = "({},{})".format("+".join(A[0]), "+".join(A[1])) \
                             + "=>({},{})".format("+".join(B[0]), "+".join(B[1]))
            logger.info("Calculating fake factors for {}...".format(transition_str))
            for decaymode in [1, 3]:
                # FF = (N_Data(B) - N_MC(B)) / (N_Data(A) - N_MC(A))
                # Denominator:
                denom = self.histo["Data"][A][varexp][decaymode].Clone(
                    "FFdenom_{}_{}_{}prong_{}".format(self.chargecomb, transition_str, decaymode, varexp))
                denom.Add(self.histo["MC"][A][varexp][decaymode], -1.0)
                # Numerator:
                num = self.histo["Data"][B][varexp][decaymode].Clone(
                    "FFnum_{}_{}_{}prong_{}".format(self.chargecomb, transition_str, decaymode, varexp))
                num.Add(self.histo["MC"][B][varexp][decaymode], -1.0)
                # Fake factor:
                self.fakefactor[pair][decaymode] = num.Clone("FF_{}_{}_{}prong_{}_Nominal".format(self.chargecomb, transition_str, decaymode, varexp))
                self.fakefactor[pair][decaymode].Divide(denom)
            #     for i in range(1, self.nbins["Pt"]+1, 1):
            #         for j in range(1, self.nbins["Eta"]+1, 1):
            #             bin = self.fakefactor[pair][decaymode].GetBin(i,j)
            #             print "({},{}) >> tau{} ({}-prong) : ( {} - {} ) / ( {} - {} ) = {}  +/- {}".format(i, j,
            #                 tauindex,
            #                 decaymode,
            #                 self.histo["Data"][B][varexp][decaymode].GetBinContent(bin),
            #                 self.histo["MC"][B][varexp][decaymode].GetBinContent(bin),
            #                 self.histo["Data"][A][varexp][decaymode].GetBinContent(bin),
            #                 self.histo["MC"][A][varexp][decaymode].GetBinContent(bin),
            #                 self.fakefactor[pair][decaymode].GetBinContent(bin),
            #                 self.fakefactor[pair][decaymode].GetBinError(bin))
            # print
        self.GetCombinedFakeFactors()

    def GetCombinedFakeFactors(self):
        """Binwise combination of all fake factors (separate for each decay mode).
           The individual fake factors are weighted by their relative errors.
           (see: https://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Dealing_with_variance)"""
        self.combinedFFs = {}
        for decaymode in [1, 3]:
            self.combinedFFs[decaymode] = {}
            for i in range(1, self.nbins["Pt"]+1, 1):
                for j in range(1, self.nbins["Eta"]+1, 1):
                    mean  = collections.defaultdict(float) # sum_i(x_i*(1/dx_i^2)) / norm
                    norm  = collections.defaultdict(float) # sum_i(1/dx_i^2)
                    error = collections.defaultdict(float) # sqrt(1/norm)
                    for pair in self._getListOfTransitions(self.cuts.keys()):
                        promstep = self._getPromotionStep(pair)
                        unpromtauqual = self._getUnpromotedTauQualities(pair)
                        if not (promstep, unpromtauqual) in self.combinedFFs[decaymode]:
                            self.combinedFFs[decaymode][promstep, unpromtauqual] = ROOT.TH2D(
                                "FF_{}_{}_[{}]_{}prong_combined_Nominal".format(self.chargecomb, promstep, unpromtauqual, decaymode),
                                "",
                                self.nbins["Pt"],    # X
                                self.binning["Pt"],
                                self.nbins["Eta"],   # Y
                                self.binning["Eta"])
                        bin = self.combinedFFs[decaymode][promstep, unpromtauqual].GetBin(i,j)
                        try:
                            mean[promstep, unpromtauqual] += self.fakefactor[pair][decaymode].GetBinContent(bin) \
                                *(1./self.fakefactor[pair][decaymode].GetBinError(bin)**2)
                            norm[promstep, unpromtauqual] += (1./self.fakefactor[pair][decaymode].GetBinError(bin))**2
                        except ZeroDivisionError:
                            pass
                    for promstep, unpromtauqual in self.combinedFFs[decaymode].keys():
                        try:
                            mean[promstep, unpromtauqual] = mean[promstep, unpromtauqual]/norm[promstep, unpromtauqual]
                        except ZeroDivisionError:
                            if j != 3:
                                logger.error("Combining the fake factors for {} with {} failed in bin "
                                    "(pT={},eta={}) for {}-prong taus!".format(promstep, unpromtauqual, i, j, decaymode))
                            mean[promstep, unpromtauqual] = 0.0
                        if mean[promstep, unpromtauqual] < 0:
                            logger.warning("Negative fake factor {} for {} with [{}] failed in bin "
                                "(pT={},eta={}) for {}-prong taus!".format(round(mean[promstep, unpromtauqual], 4), promstep, unpromtauqual, i, j, decaymode))
                        self.combinedFFs[decaymode][promstep, unpromtauqual].SetBinContent(bin, mean[promstep, unpromtauqual])
                        try:
                            self.combinedFFs[decaymode][promstep, unpromtauqual].SetBinError(bin, math.sqrt(1./norm[promstep, unpromtauqual]))
                        except ZeroDivisionError:
                            pass
        logger.info("Finished calculating the combined fake factor.")

    def _getIndexOfPromotedTau(self, transitiontuple):
        A, B = transitiontuple
        for i in range(2):
            if A[i] != B[i]: return i + 1

    def _getPromotionStep(self, transitiontuple):
        A, B = transitiontuple
        i = self._getIndexOfPromotedTau(transitiontuple) -1
        return "{}=>{}".format("+".join(A[i]), "+".join(B[i]))

    def _getUnpromotedTauQualities(self, transitiontuple):
        A, B = transitiontuple
        i = abs(self._getIndexOfPromotedTau(transitiontuple) - 2)
        return "+".join(A[i])

    def SaveResults(self):
        for decaymode in [1, 3]:
            for key in self.fakefactor.keys():
                ytitle, xtitle = self.fakefactor[key][decaymode].GetName().split("_")[4].split(":")
                IOManager.addPlot(self.fakefactor[key][decaymode],
                    folder="FF_Nominal/{}/{}/{}-prong".format(
                        self._getPromotionStep(key),
                        self._getUnpromotedTauQualities(key),
                        decaymode),
                    customOptions=True,
                    drawoption="COLZ TEXT",
                    xtitle=xtitle,
                    ytitle=ytitle,
                    title=self.fakefactor[key][decaymode].GetName())
            for promstep, unpromtauqual in self.combinedFFs[decaymode].keys():
                IOManager.addPlot(self.combinedFFs[decaymode][promstep, unpromtauqual],
                    folder="FF_Nominal/{}/{}/{}-prong".format(
                        promstep,
                        unpromtauqual,
                        decaymode),
                    customOptions=True,
                    drawoption="COLZ TEXT",
                    xtitle="tauPt",
                    ytitle="tauEta",
                    title=self.combinedFFs[decaymode][promstep, unpromtauqual].GetName())
        if not os.path.exists(self.outdir):
            os.mkdir(self.outdir)
        IOManager.writeOutputFile(os.path.join(self.outdir, self.outfilename))



# Santa's little helpers:

def deep_values(d):
    for v in d.values():
        if not isinstance(v, dict):
            yield v
        else:
            for y in deep_values(v):
                yield y



if __name__ == '__main__':

    from FFSystCalculator import FFSystCalculator

    # Nominal FFs:
    FFC_SS = FakeFactorCalculator("SS")
    FFC_SS.ComputeFakeFactors()
    FFC_SS.SaveResults()

    # OS SysVar FFs:
    FFC_OS = FakeFactorCalculator("OS", cuts=["MET<50"]) # increase QCD purity
    FFC_OS.ComputeFakeFactors()

    FFSystCalc = FFSystCalculator(FFC_SS, FFC_OS)
    FFSystCalc.ComputeSysVar()
    FFSystCalc.SaveResults()
