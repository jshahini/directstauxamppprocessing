#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Ferdinand Krieter"
__email__  = "ferdinand.krieter@cern.ch"


import os
import sys
import math

import ROOT

from collections import defaultdict

from tools.logger import logger
from tools.IOManager import IOManager

codepath = os.path.dirname(os.path.abspath(__file__))

class FFSystCalculator(object):

    def __init__(self, nominalFFC, sysvarFFC):
        self.nominalFFC = nominalFFC
        self.sysvarFFC  = sysvarFFC
        self.ffsysvardiff = defaultdict(lambda: defaultdict( lambda: {}))
        self.fftotalsys = {
            "UP":   defaultdict(lambda: {}),
            "DOWN": defaultdict(lambda: {})
        }

    def _computeTauPtSysVarDiff(self):
        """Computes the difference of FF measured for leading and subleading tau to the
           combined FF."""
        for decaymode, combffsdict in self.nominalFFC.combinedFFs.items():
            for (transition, nonpromqual), histo in combffsdict.items():
                before, after = [tuple(s.split("+")) for s in transition.split("=>")]
                const = tuple(nonpromqual.split("+"))
                for idx in [1, 2]:
                    key = [[const, const], [const, const]]
                    key[0][idx-1] = before
                    key[1][idx-1] = after
                    key = tuplefy(key)
                    self.ffsysvardiff["Tau{}Pt".format(idx)][decaymode][(transition, nonpromqual)] = histo.Clone("{}_SysVarTau{}Pt".format(histo.GetName(), idx))
                    self.ffsysvardiff["Tau{}Pt".format(idx)][decaymode][(transition, nonpromqual)].Add(self.nominalFFC.fakefactor[key][decaymode], -1.0)
                    self.ffsysvardiff["Tau{}Pt".format(idx)][decaymode][(transition, nonpromqual)].Scale(-1.0)
                    for xbin in range(1, histo.GetNbinsX() +1, 1):
                        for ybin in range(1, histo.GetNbinsY() +1, 1):
                            if self.nominalFFC.fakefactor[key][decaymode].GetBinContent(xbin, ybin) == 0:
                                self.ffsysvardiff["Tau{}Pt".format(idx)][decaymode][(transition, nonpromqual)].SetBinContent(xbin, ybin, 0.0)
                                self.ffsysvardiff["Tau{}Pt".format(idx)][decaymode][(transition, nonpromqual)].SetBinContent(xbin, ybin, 0.0)

    def _computeOSSysVarDiff(self):
        """Computes the difference of combined FFs measured in the nominal (SS) selection
           vs. the ones measured in the OS selection."""
        for decaymode, combffsdict in self.nominalFFC.combinedFFs.items():
            for (transition, nonpromqual), histo in combffsdict.items():
                self.ffsysvardiff["OS"][decaymode][(transition, nonpromqual)] = histo.Clone("{}_SysVarOS".format(histo.GetName()))
                self.ffsysvardiff["OS"][decaymode][(transition, nonpromqual)].Add(self.sysvarFFC.combinedFFs[decaymode][(transition, nonpromqual)], -1.0)
                self.ffsysvardiff["OS"][decaymode][(transition, nonpromqual)].Scale(-1.0)

    def _computeStatSysVarDiff(self):
        """Retrieve stat. uncertainty of the combined FFs measured in the nominal (SS) selection."""
        for decaymode, combffsdict in self.nominalFFC.combinedFFs.items():
            for (transition, nonpromqual), histo in combffsdict.items():
                self.ffsysvardiff["StatUP"][decaymode][(transition, nonpromqual)] = histo.Clone("{}_SysVarStatUP".format(histo.GetName()))
                self.ffsysvardiff["StatDOWN"][decaymode][(transition, nonpromqual)] = histo.Clone("{}_SysVarStatDOWN".format(histo.GetName()))
                for xbin in range(1, histo.GetNbinsX() +1, 1):
                    for ybin in range(1, histo.GetNbinsY() +1, 1):
                        lbn = histo.GetBin(xbin, ybin) # linearized bin number
                        self.ffsysvardiff["StatUP"][decaymode][(transition, nonpromqual)].SetBinContent(lbn, histo.GetBinError(lbn))
                        self.ffsysvardiff["StatDOWN"][decaymode][(transition, nonpromqual)].SetBinContent(lbn, (-1.0)*histo.GetBinError(lbn))

    def _getTotalFFSys(self):
        for decaymode, combffsdict in self.nominalFFC.combinedFFs.items():
            for (transition, nonpromqual), histo in combffsdict.items():
                self.fftotalsys["UP"][decaymode][(transition, nonpromqual)]   = histo.Clone("{}_SysUP".format(histo.GetName().rstrip("_Nominal")))
                self.fftotalsys["DOWN"][decaymode][(transition, nonpromqual)] = histo.Clone("{}_SysDOWN".format(histo.GetName().rstrip("_Nominal")))
                for xbin in range(1, histo.GetNbinsX() +1, 1):
                    for ybin in range(1, histo.GetNbinsY() +1, 1):
                        uperr, downerr = 0., 0.
                        for source, ffsysvardiffdict in self.ffsysvardiff.items():
                            err = ffsysvardiffdict[decaymode][(transition, nonpromqual)].GetBinContent(xbin, ybin)
                            if err >= 0:
                                uperr = math.sqrt(uperr**2 + err**2)
                            else:
                                downerr = math.sqrt(downerr**2 + err**2)
                        lbn = histo.GetBin(xbin, ybin) # linearized bin number
                        self.fftotalsys["UP"][decaymode][(transition, nonpromqual)].AddBinContent(lbn, uperr)
                        self.fftotalsys["DOWN"][decaymode][(transition, nonpromqual)].AddBinContent(lbn, (-1.0)*downerr)
                        if self.fftotalsys["DOWN"][decaymode][(transition, nonpromqual)].GetBinContent(lbn) < 0:
                            self.fftotalsys["DOWN"][decaymode][(transition, nonpromqual)].SetBinContent(lbn, 0.0)

    def ComputeSysVar(self):
        self._computeStatSysVarDiff()
        self._computeTauPtSysVarDiff()
        self._computeOSSysVarDiff()
        self._getTotalFFSys()

    def SaveResults(self):
        for sysdirection, fftotalsysdict in self.fftotalsys.items():
            for decaymode in [1, 3]:
                for (transition, nonpromqual), histo in fftotalsysdict[decaymode].items():
                    IOManager.addPlot(histo,
                        folder="FF_Sys{}/{}/{}/{}-prong".format(
                            sysdirection,
                            transition,
                            nonpromqual,
                            decaymode),
                        customOptions=True,
                        drawoption="COLZ TEXT",
                        xtitle="tauPt",
                        ytitle="tauEta",
                        title=histo.GetName())
        for sysvar, ffsysvardiffdict in self.ffsysvardiff.items():
            for decaymode in [1, 3]:
                for (transition, nonpromqual), histo in ffsysvardiffdict[decaymode].items():
                    IOManager.addPlot(histo,
                        folder="SysVarDiffHistos/{}/{}/{}/{}-prong".format(
                            sysvar,
                            transition,
                            nonpromqual,
                            decaymode),
                        customOptions=True,
                        drawoption="COLZ TEXT",
                        xtitle="tauPt",
                        ytitle="tauEta",
                        title=histo.GetName())
        IOManager.writeOutputFile(os.path.join(self.nominalFFC.outdir, self.nominalFFC.outfilename), mode="UPDATE")







# Hopefully helpful helpers help me:
def depth(d):
    if isinstance(d, dict):
        return 1 + (max(map(depth, d.values())) if d else 0)
    return 0

def tuplefy(l):
    for i, subl in enumerate(list(l)):
        if not isinstance(subl, list):
            break
        tuplefy(subl)
        l[i] = tuple(subl)
    l = tuple(l)
    return l
