if [[ ! "$PATH" =~ (^|:)"$(pwd)"(:|$) ]]; then
    echo "adding $(pwd) to PATH..."
    export PATH=$(readlink -f .):$PATH
else
    echo "PATH already contains $(pwd)"
fi

if [[ ! "$PYTHONPATH" =~ (^|:)"$(pwd)"(:|$) ]]; then
    echo "adding $(pwd) to PYTHONPATH..."
    export PYTHONPATH=$(readlink -f .):$PYTHONPATH
else
    echo "PYTHONPATH already contains $(pwd)"
fi
