#!/usr/bin/env python3

import os
import re
import sys
import json
import shutil
import argparse

from tools.logger import logger
from tools.DSAConfigReader import DSAConfig
from tools.Chunkinator import Chunkinator
from slurmy import Slurm, JobHandler, Theme

codepath = os.getenv("DSALOOP_DIR")

### SKIMMING CONFIG: ###
tree = "Staus_Nominal"
outputdir = "/project/etp1/fkrieter/NTUPLEs/DirectStau"
skimtag = "skim01b-ext1"
triggerlist = [
    # MET trigger:
    "HLT_xe70_mht",
    "HLT_xe90_mht_L1XE50",
    "HLT_xe110_mht_L1XE50",
    "HLT_xe110_pufit_L1XE55",
    "HLT_xe110_pufit_xe70_L1XE50",
    # Ditau+MET trigger:
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
    "HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
    "HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
    "HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
    # Asym. ditau trigger:
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
    "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
    "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
]
triggerlist = [
    t.replace("-", "_") for t in triggerlist
]  # sometimes: variable names != branch names
skimming = [
    {"trigs": "||".join(["Trig{} == 1".format(trig) for trig in triggerlist])},
    {"eVeto": "electrons_pt->size() == 0"},
    {"muVeto": "muons_pt->size() == 0"}
    # {"bVeto":  "std::count_if(jets_bjet->begin(), jets_bjet->end(), [&](const int btag) { return btag == 1; }) == 0"}
]
########################

argparser = argparse.ArgumentParser("slurm job handler")
argparser.add_argument(
    "-c",
    "--chunk",
    type=int,
    metavar="N",
    default=None,
    help="combine N processes of TForester into on job (will be executed in succession,"
    " default: %(default)s)",
)
slurmoptions = vars(argparser.parse_args())

config = DSAConfig()

outputdir = os.path.join(
    outputdir,
    "{production}_{skimtag}".format(
        production=config["ProductionName"], skimtag=skimtag
    ),
)
macrodir = os.path.join(outputdir, "macros")
outputfiledir = os.path.join(outputdir, "files")

if not os.path.exists(outputdir):
    os.makedirs(outputdir)
    logger.info("Created output directory '{}'".format(outputdir))
else:
    logger.error("Output directory '{}' already exists! Overwrite?".format(outputdir))
    if not input("  (Y/N) : ") in ["y", "Y", "yes", "Yes", "YES"]:
        logger.info("Exiting...")
        sys.exit(0)
    else:
        shutil.rmtree(outputdir)

if not os.path.exists(outputfiledir):
    os.makedirs(outputfiledir)

rootdPaths = open(
    os.path.join(codepath, "productions", config["ProductionName"], "rootdPaths.txt"),
    "r",
).readlines()
infiles = [line.split()[1] for line in rootdPaths]

# Define run script to be executed by each worker:
run_script = """
cd {cwd}
module load root/6.10.08
{cmd}
exit $?"""

iodsdict = {}
jobidsdict = {}
scope = "TForester"
jh = JobHandler(
    name=scope,
    theme=Theme.Boring,
    backend=Slurm(partition="lsschaile"),
    work_dir=os.path.join(outputdir, "slurmy"),
)

# Configure individual SLURMY jobs:
logger.info("Start configuring jobs...")
infile_chunks = Chunkinator(infiles, chunksize=slurmoptions["chunk"])
for i, infile_chunk in infile_chunks.items():

    cmds = []
    jobname = "SkimJob_{}".format(str(i).rjust(4, "0"))
    jobidsdict[jobname] = infile_chunk

    for infile in infile_chunk:
        outfilename = re.sub(
            "XAMPP", "XAMPP_{}".format(skimtag), os.path.basename(infile)
        )
        outfilepath = os.path.join(outputfiledir, outfilename)
        jobnumber = "{}{}".format(outfilename.split(".")[2], outfilename.split(".")[4])
        classname = "{}_{}".format(tree, jobnumber)
        iodsdict[infile] = outfilepath

        # Command invoking TForester
        cmd = [
            "python",
            "python/tools/TForester.py",
            "-t",
            tree,
            "-o",
            outfilepath.strip("\n"),
            "--macrodir",
            macrodir,
            "--classname",
            classname,
            "--skimming",
            "'{}'".format(json.dumps(skimming)),
            infile.strip("\n"),
        ]
        cmds.append(" ".join(cmd))

    logger.debug("Will execute the following command:\n{}\n".format(" && ".join(cmds)))
    jh.add_job(
        backend=Slurm(mem="2000mb"),
        run_script=run_script.format(cwd=codepath, cmd=" && \\\n".join(cmds)),
        name=jobname,
    )
    sys.stdout.write("\r" + "{}/{} jobs configured".format(i, len(infile_chunks)))
    sys.stdout.flush()
sys.stdout.write("\n")

with open(os.path.join(outputdir, "iodsdict.json"), "w") as iodsjson:
    json.dump(iodsdict, iodsjson, indent=2)

with open(os.path.join(outputdir, "jobidsdict.json"), "w") as jobidsjson:
    json.dump(jobidsdict, jobidsjson, indent=2)

# Submit jobs:
jh.run_jobs(interval=2)
