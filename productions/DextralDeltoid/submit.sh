### DextralDeltoid production grid submission script
### launched on Jun 19, 2018 by user.fkrieter
### usage: $> source submit.sh | tee subission_log.txt

export RUCIO_RSE="LRZ-LMU_LOCALGROUPDISK"

### DATA #####################################################

printf "\n\n\nSubmitting DD3_data15: data15...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/data15.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_data15

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_data16: data16...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/data16.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_data16

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_data15: data17...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/data17.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_data17

printf "\n\nDone"


### MC16a #####################################################

printf "\n\n\nSubmitting DD3_mc16a: diboson...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16a/diboson.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16a

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16a: ttbar...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16a/ttbar.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16a

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16a: wlnu...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16a/wlnu.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16a

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16a: zll...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16a/zll.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16a

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16a: directstau...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16a/directstau.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16a

printf "\n\nDone"


### MC16d #####################################################

printf "\n\n\nSubmitting DD3_mc16d: diboson...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16d/diboson.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16d

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16d: ttbar...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16d/ttbar.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16d

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16d: wlnu...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16d/wlnu.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16d

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16d: zll...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16d/zll.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16d

printf "\n\nDone"

printf "\n\n\nSubmitting DD3_mc16d: directstau...\n\n"

python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList /project/etp1/fkrieter/Code/DSAnalysisLoop/productions/DextralDeltoid/DAODs/MC16d/directstau.txt \
       --stream HadHad \
       --noSyst \
       --production DD3_mc16d

printf "\n\nDone"
