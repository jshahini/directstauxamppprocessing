#DextralDeltoid 

* AthAnalysis,21.2.34
* SUSY3 DxAODs
* data15+16+17, MC16a, MC16d
* Preselection: trig. match, b-veto, e/mu-veto, >=2 baseline taus
* signal tau ID: tight
* Started on 2018-06-19, 16:25

