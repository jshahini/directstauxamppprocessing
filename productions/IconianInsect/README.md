#  IconianInsect

(produced by Huajie Cheng)

* AthAnalysis,21.2.49
* SUSY3
* data15+16+17+18 , MC16a+MC16d+MC16e (18v02.2)
* Part of extended signal grid included
* Started on 2018-11-04 19:26:08

##### XAMPPstau:
Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git

Branch: chengj
Tag: HadHad.18v02b

Commit: [d151706db4f9f387800b64f16f1a75624863f6e6]

##### XAMPPbase:

Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPbase.git

Commit: [497e91f5b4452ee7c09f5224ff02c2e8c9e1ee86]
