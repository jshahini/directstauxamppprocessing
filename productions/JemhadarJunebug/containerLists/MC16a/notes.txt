# Original samples
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.32.Sherpa221_Wtaunu_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.aMCatNLOPy8_ttV_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Higgs_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.MG5Py8_4t_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.MG5Py8_tW_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.MG5Py8_tZ_noAllHad_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.PowHegPy8_ttbar_incl_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.PowhegPy_top_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_VVV_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_VV_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_Wenu_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_Wmunu_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_Zee_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_Zmumu_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_Znunu_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa221_Ztautau_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa222_VVV_XAMPP # <<-- BUGGY
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.3.Sherpa222_VV_XAMPP

# Updated samples:
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.33.Sherpa222_VV_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.33.Sherpa221_Ztautau_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.33.Sherpa221_Wenu_XAMPP
group.phys-susy:group.phys-susy.XAMPP.HadHad.18mc16av02.33.Sherpa221_VV_XAMPP
