#  JemhadarJunebug

(produced by Huajie Cheng)

* AthAnalysis,21.2.49
* SUSY3
* data15+16+17 (18v02.2), data18,MC16a+MC16d+MC16e (18v02.3)
* extended signal grid included
* systematic uncertainties included
* Muon and Tau trigger SFs included
* Added data18 periods O and Q
* Updated lepton isolation WPs
* Started on 2018-12-13 01:47:52

##### XAMPPstau:
Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git

Branch: chengj
Tag: HadHad.18v02b

Commit: [d151706db4f9f387800b64f16f1a75624863f6e6]

##### XAMPPbase:

Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPbase.git

Commit: [497e91f5b4452ee7c09f5224ff02c2e8c9e1ee86]
