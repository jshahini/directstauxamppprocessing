#  HirogenHoneybee

(produced by Huajie Cheng)

* AthAnalysis,21.2.42
* SUSY3
* data15+16+17+18 , MC16a+MC16d (18v01.4)
* Started on 2018-10-12, 10:28:35
* diff to GG production: updated trigger menu for 2018 data

##### XAMPPstau:
Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git

Branch: chengj

Commit: [c2d1aba0a95fe8098493657db58da31fc9ab3742]

##### XAMPPbase:

Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPbase.git

Commit: [399a3223ead40971c131a120640897b6622caefe]
