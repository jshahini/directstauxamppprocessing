# Created on 2018-10-18
# Fix for erratic xsec on AMI for ggH samples.
# See https://its.cern.ch/jira/browse/CENTRPAGE-57, https://its.cern.ch/jira/browse/ATLHLEPTON-269
# DSID          Physics short                                                 xsec                  k-fac          eff                 rel. unc.
345120          PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaul13l7                 3.0469                1.0             5.3756E-02         0.0
345121          PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulm15hp20              3.0469                1.0             3.7749E-02         0.0
345122          PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulp15hm20              3.0469                1.0             3.7826E-02         0.0
345123          PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautauh30h20                3.0469                1.0            18.8900E-02         0.0
