#  IconianInsect\_skim02

(Created on 2018-11-22)

Skimmed version of [IconianInsect](https://gitlab.cern.ch/fkrieter/DSAnalysisLoop/tree/master/productions/IconianInsect):
* Trigger list skimming (see below)
* Baseline electron veto
* Baseline muon veto
* No b-tagged baseline jets

#### Trigger list:

* **MET trigger:**
HLT_xe70_mht
HLT_xe90_mht_L1XE50
HLT_xe110_mht_L1XE50
HLT_xe110_pufit_L1XE55
HLT_xe110_pufit_xe70_L1XE50
* **Ditau+MET trigger:**
HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50
HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50
HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50
HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50
* **Asym. ditau trigger:**
HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12
HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40
HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40
HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40
