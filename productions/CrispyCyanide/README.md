# CrispyCyanide

(produced by Yang Liu)

* AthAnalysis,21.2.23
* SUSY3
* data15+16+17, MC16a, MC16d
* Started on 2018-04-27, 16:24

##### XAMPPStau:
Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git

Branch: liuya

Commit: [78b91b3ce099d5602a32f8f2fe8f00855f3a3bc0](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau/tree/78b91b3ce099d5602a32f8f2fe8f00855f3a3bc0)
