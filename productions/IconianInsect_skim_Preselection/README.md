#  IconianInsect\_skim\_Preselection

(Created on 2018-11-15)

Skimmed version of [IconianInsect](https://gitlab.cern.ch/fkrieter/DSAnalysisLoop/tree/master/productions/IconianInsect):
* Baseline electron veto
* Baseline muon veto
* No b-tagged baseline jets
