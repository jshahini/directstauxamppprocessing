#  GornGrasshopper\_skim01

(Created on 2018-10-15)

Skimmed version of [GornGrasshopper](https://gitlab.cern.ch/fkrieter/DSAnalysisLoop/tree/master/productions/GornGrasshopper):
* Baseline electron veto
* Baseline muon veto
* No b-tagged baseline jets
