#  GornGrasshopper

(produced by Huajie Cheng)

* AthAnalysis,21.2.42
* SUSY3
* data15+16+17+18 , MC16a+MC16d (18v01.2)
* Started on 2018-09-17, 18:15

##### XAMPPstau:
Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git

Branch: chengj

Commit: [911110582a689cb4bfc1446aca751a595818e4da]

##### XAMPPbase:

Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPbase.git

Commit: [de54292c54e38fe6c6e612fa2ca378a44aaf8dd0]
