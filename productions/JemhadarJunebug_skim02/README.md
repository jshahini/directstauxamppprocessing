#  JemhadarJunebug\_skim02

(Created on 2019-01-28)

Skimmed version of [JemhadarJunebug](https://gitlab.cern.ch/fkrieter/DSAnalysisLoop/tree/master/productions/JemhadarJunebug):
* Trigger list skimming (see below)
* Exactly one signal muon (pT>50 GeV)
* Exactly one signal tau (pT>50 GeV)
* OS(tau,mu)
* MET>60 GeV
* b-veto

#### Trigger list:

* **single mu trigger:**
HLT_mu26_ivarmedium
