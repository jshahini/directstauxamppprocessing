while read -r line; do
  INFILE=$(echo $line)
  FILE=$(echo $line | cut -d ':' -f 2)
  rucio list-rules-history $INFILE | grep LRZ-LMU_LOCALGROUPDISK
  if (($?==0)); then
     continue
  fi
  rucio add-rule $FILE --grouping DATASET 1 "LRZ-LMU_LOCALGROUPDISK"
  echo "this works"
  echo $FILE
done < $1