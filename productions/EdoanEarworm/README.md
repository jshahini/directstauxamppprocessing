#  EdoanEarworm

(produced by Yang Liu)

* AthAnalysis,21.2.31
* SUSY3
* data15+16+17 (v13a), MC16a+MC16d (v15a)
* v13a and v15a only differ by the correct application of tau trigger SFs in v15a
* Started on 2018-06-21, 14:19

##### XAMPPstau:
Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git

Branch: liuya

Commit: [1cea99fd98ad4c75d1f0b2s4cca69332a3ba1063]

##### XAMPPbase:

Remote: ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPbase.git

Commit: [dfa4c9127fde795a35d8a52a6c08df82ba432247]