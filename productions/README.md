# DSAnalysisLoop

Simple loop for [XAMPPstau](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau) n-tuples.

## Create A New Production Folder

Production folders are used to hold all the information of a XAMPPStau n-tuple production, e.g. a list of input DAODs, paths to the output files stored on the local group disk and more.
Continuing with an old tradition, productions names are chosen to consist of two words starting with the same letter. The letter is chosen such that the alphabetical order of productions matches the chronological one.

#### Example

First enter a [SLC6](https://wiki.physik.uni-muenchen.de/etp/index.php/Scientific_Linux_from_CVMFS_with_Singularity) environment.

```
lsetup python rucio
voms-proxy-init -voms atlas
pushd ../python; source init_python.sh; popd
mkdir AlkalineAzarole; cd AlkalineAzarole/
mkdir DAODs containerLists
rucio list-dids --short 'user.fkrieter:*S11.AA_DATA15_XAMPP' > containerLists/data15.txt'
rucio list-dids --short 'user.fkrieter:*S11.AA_DATA16_XAMPP' > containerLists/data16.txt'
...
```

Also copy all input lists, holding the names of the DAODs used for the XAMPPStau n-tuple production, into the ```DAODs/``` folder.

**Note:** Input and output lists of MC samples must be stored in subfolders named after their respective MC subcampaign, e.g.```DAODs/MC16x/``` and ```containerLists/MC16x/```.

Then retrieve the paths to all XAMPPStau n-tuples stored on the local group disk and save it to a file called  ```rootdPaths.txt```:
```
find containerLists -name \*.txt -print | xargs -L1 getRootPaths.py | tee rootdPaths.txt
```
Note that files, which are still being transfered to the destination storage element or are associated with failed or only partially finished grid jobs, are not included.

Also consider creating a ```README.md```, holding additional information like associated AthAnalysis release, XAMPPStau git-tag etc.

#### Copying Ntuples to a Local Group Disk

If you want to copy some files on the grid to your local group disk, do

```
source CopyToLocalGroupDisk.sh /path/to/a/list/of/the/files.txt
```
You may have to change the LGD name in the sh-file.
