export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS -2
lsetup "root 6.10.06-x86_64-slc6-gcc62-opt"
cd python
#source init_python.sh
function SourcePython() { source ./init_python.sh ; } ; SourcePython
cd ../
export DSALOOP_DIR=$(pwd)
