# DSAnalysisLoop

Simple loop for [XAMPPstau](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau) n-tuples.
Copy of code from this repo, which may now be inaccessible (https://gitlab.cern.ch/fkrieter/DSAnalysisLoop)

## Setup

Clone and compile the code
```
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/dojones/directstauxamppprocessing.git
cd directstauxamppprocessing
mkdir build
cd build
cmake3 ../
make -j4
cd ../
```

For subsequent compiles, just change to build directory and do make


To enable **Recursive Jigsaw Reconstruction** install [RestFrames](http://restframes.com/) in your favorite directory as described [here](http://restframes.com/downloads/). Do this either beforehand or recompile the code afterwards using the last four commands above.

Every time you invoke a new shell execute this command:
```
source init.sh
```

## Execute

#### Locally

To run the code **locally**, e.g. for testing
```
dsaloop [-o output.root --nominalTree NomTree --metadataTree MetaTree] input1.root [input2.root input3.root]
```
Additional arguments can be viewed with the ```-h``` option.

#### Via Batch

For processing multiple inputs **in parallel**, jobs are submitted to the [batch system](https://wiki.physik.uni-muenchen.de/etp/index.php/Slurm_Batch_on_ETP).
The necessary configuartions and final outputs are stored in a corresponding project folder.

The base parameters of a project are controlled by the ```config.ini``` file (if you don't have one yet, copy the default one).
Edit it and define a value for each of the parameters:
* ***OutputPath*** (where your project folder is created)
* ***ProductionName*** (the name of the XAMPPStau n-tuple production you will use as input)
* ***InputLists*** (specify which lists of n-tuples shall be used as input)
* ***LumiCalc*** (the files required to calculate the integrated luminosity of your data)

A new project is then created via
```
newProject.py -v1
```
The ```-v1``` argument creates a subdirectory of the project, labelled ```V01/```. More arguments are available and can be viewed with the ```--help``` option.

Jobs are then submitted using [SLURMY](https://github.com/Thomas-Maier/slurmy) via
```
slurm_submit.py
```
The current project directory can be easily accessed via the ```CWD``` link.

For convenience the resulting output files can be merged according their dataset type and process. This is done via
```
mergeCWD.py
```
The merged ntuples are saved in the ```CWD/Merged``` folder.

## More

* **Fake factor method:** Instructions on how to create ntuples for multijet estimation are given [here](https://gitlab.cern.ch/fkrieter/DSAnalysisLoop/tree/master/python/FFM).

