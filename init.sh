#!/usr/bin/env bash

echo "setting up DSAnalysisLoop environment..."
export DSALOOP_DIR=$(pwd)

# determine platform
PLATFORM=""
case $(hostname) in
    gar-ws-etp*) PLATFORM="etp";;
    cip-ws-*) PLATFORM="etp";;
    lxe*) PLATFORM="lxe";;
    hlcgr*) PLATFORM="lxe";;
esac

if [[ -z $PLATFORM ]]; then
    echo "unkown platform!"
    return
fi

# path to shared libraries for root
# required to make main exe run from any dir
mkdir -p $DSALOOP_DIR/build
if [[ ! "$LD_LIBRARY_PATH" =~ (^|:)"$DSALOOP_DIR/build"(:|$) ]]; then
    echo "adding $DSALOOP_DIR/build to LD_LIBRARY_PATH..."
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DSALOOP_DIR/build
else
    echo "LD_LIBRARY_PATH already contains $DSALOOP_DIR/build"
fi

# if last build was compiled with RestFrames support try to reload the package
if [ -e "build/CMakeCache.txt" ]; then
    # extract RestFrames library path from cmake cache
    RESTFRAMES_LIB=$(grep "RestFrames" build/CMakeCache.txt | awk '{split($0,a,"="); print a[2]}')
    if [[ $RESTFRAMES_LIB != *"NOTFOUND"* ]]; then
        echo "previous build was compiled with RestFrames support"
        if [[ ! $RESTFRAMESSYS ]]; then
            RESTFRAMES_DIR=$(dirname $RESTFRAMES_LIB | xargs -L1 dirname)
            if [ -e "$RESTFRAMES_DIR/setup_RestFrames.sh" ]; then
                echo "auto-reloading RestFrames package..."
                source $RESTFRAMES_DIR/setup_RestFrames.sh
            else
                echo "auto-reload of RestFrames failed"
            fi
        fi
        if [[ $RESTFRAMESSYS ]]; then
            echo "RestFrames is up and running"
        fi
    else
        echo "previous build was compiled *without* RestFrames support"
    fi
fi

if [[ -z $bash_source && $PLATFORM == "etp" ]]; then
    echo "loading module system..."
    source /etc/profile.d/modules.sh
fi

echo "loading root module..."
if [[ $PLATFORM == 'etp' ]]; then
    module unload root
    module load root/6.10.08
elif [[ $PLATFORM == 'lxe' ]]; then
    echo "lxe setup:"
    source /home/grid/lcg/sw/setup_atlas_cvmfs.sh
    lsetup root
fi
echo "using root: $(which root)"

if [[ $PLATFORM == 'lxe' ]]; then
    echo "lxe setup:"
    module load git
    export LESS=-R # should fix the git color escapes
fi

echo "loading slurm module..."
if [[ $PLATFORM == 'etp' ]]; then
    module load slurm/16.05.7
    echo "using slurm: $(which sbatch)"
fi

cd python; source init_python.sh; cd ../

# now make can be executed also from project root dir
my_make() {
    if [[ "$(pwd)" = "$DSALOOP_DIR" ]]; then
        cd build; pwd
        make "$@"
        cd ../; pwd
    else
        make "$@"
    fi
}
alias make="my_make"

echo "done!"
