import os

tree_variations = [
    'Nominal',
    'TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up',
    'TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down',
    ]

input_path = '/r04/atlas/domjones/DirectStau/RECAST_TestInputs/FullXampNtuples/'
output_path = '/r04/atlas/domjones/DirectStau/RECAST_TestInputs/ProcessedXampNtuples/'

lumi_dict = "\'{\"MC16a\" : 36.20766, \"MC16d\" : 44.3074 , \"MC16e\" : 58.4501}\'"

for variation in tree_variations: 
    cmd = 'dsaloop -o {}/RECAST_signal_skimmed_ntuple_'.format(output_path) + variation + '.root '
    cmd += ' '.join(['{}/Prod_V4_Syst16{}.RECAST_signal_XAMPP/*.root'.format(input_path, x) for x in ['a', 'd', 'e']])
    cmd += ' -l ' + lumi_dict
    cmd += ' --overrideMetadata ./productions/NewMetaData.txt -p RECAST_signal --nameTree {}'.format(variation)
    print(cmd)
    os.system(cmd)
