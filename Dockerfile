FROM gitlab-registry.cern.ch/atlas/athena/analysisbase:21.2.15
ADD . /directstauxamppprocessing/
USER root
WORKDIR /directstauxamppprocessing/
RUN sudo chown -R atlas /directstauxamppprocessing
RUN bash -c 'source /home/atlas/release_setup.sh && \
    cd python && \
    function SourcePython() { source ./init_python.sh ; } ; SourcePython && \
    cd ../ && \
    export DSALOOP_DIR=$(pwd) && \
    mkdir build && \
    cd build && \
    cmake ../ && \
    make && \
    cd ../'

RUN sudo usermod -aG root atlas     
USER atlas  
