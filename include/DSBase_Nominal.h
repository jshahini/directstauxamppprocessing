//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Dec 14 15:11:23 2017 by ROOT version 6.10/04
// from TTree Staus_Nominal/SmallTree for fast analysis
// found on file: ../XAMPPstau/run/mc_test/AnalysisOutput.root
//////////////////////////////////////////////////////////

#ifndef DSBase_Nominal_h
#define DSBase_Nominal_h

#include "DirectStau_Analysis.h"
// #include "DirectStau_Store.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

class DSBase_Nominal : public Analysis {
public :
     TTree                  *fChain;     //!pointer to the analyzed TTree or TChain
     Int_t                   fCurrent; //!current Tree number in a TChain
     
     TTree      *tree;
 
     // List of branches
    TBranch                *b_mcChannelNumber;     //!
    TBranch                *b_GenWeight;       //!
    TBranch                *b_EleWeight;       //!
    TBranch                *b_EleWeightId;     //!
    TBranch                *b_EleWeightIso;     //!
    TBranch                *b_EleWeightReco;       //!
    TBranch                *b_JetWeight;       //!
    TBranch                *b_JetWeightBTag;       //!
    TBranch                *b_JetWeightJVT;     //!
    TBranch                *b_MuoWeight;       //!
    TBranch                *b_MuoWeightIsol;       //!
    TBranch                *b_MuoWeightReco;       //!
    TBranch                *b_MuoWeightTTVA;       //!
    TBranch                *b_TauWeight;       //!
    TBranch                *b_TauWeightEleOREle;       //!
    TBranch                *b_TauWeightEleORHad;       //!
    TBranch                *b_TauWeightId;     //!
    TBranch                *b_TauWeightReco;       //!
    TBranch                *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60;
    TBranch                *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12;
    TBranch                *b_TauWeightTrigHLT_tau35_medium1_tracktwo;
    TBranch                *b_TauWeightTrigHLT_tau25_medium1_tracktwo;
    TBranch                *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF;
    TBranch                *b_TauWeightTrigHLT_tau60_medium1_tracktwoEF;
    TBranch                *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo;
    TBranch                *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;

    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down;
    TBranch     *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up;
    
    TBranch     *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
    TBranch     *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
    TBranch     *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
    TBranch     *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up; 
    
    
    
    
    
    
    
    
    
    TBranch                *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
    TBranch                *b_muWeight;     //!
    TBranch                *b_EffPt_TauEle;     //!
    TBranch                *b_EffPt_TauMuo;     //!
    TBranch                *b_Ht_Jet;   //!
    TBranch                *b_Ht_Lep;   //!
    TBranch                *b_Ht_Tau;   //!
    TBranch                *b_LepMT2_max;   //!
    TBranch                *b_LepMT2_min;   //!
    TBranch                *b_MT2_lead2;       //!
    TBranch                *b_MT2_max;     //!
    TBranch                *b_MT2_min;     //!
    TBranch                *b_Meff;     //!
    TBranch                *b_Meff_TauTau;     //!
    TBranch                *b_MetTST_EleCentral;       //!
    TBranch                *b_MetTST_EleCosMinDeltaPhi;     //!
    TBranch                *b_MetTST_EleSumCosDeltaPhi;     //!
    TBranch                *b_MetTST_MuoCentral;       //!
    TBranch                *b_MetTST_Significance;     //!
    TBranch                *b_MetTST_MuoCosMinDeltaPhi;     //!
    TBranch                *b_MetTST_MuoSumCosDeltaPhi;     //!
    TBranch                *b_RelPt_TauEle;     //!
    TBranch                *b_RelPt_TauMuo;     //!
    TBranch                *b_actualInteractionsPerCrossing;       //!
    TBranch                *b_averageInteractionsPerCrossing;   //!
    TBranch                *b_corr_avgIntPerX;     //!
    TBranch                *b_SUSYFinalState;   //!
    TBranch                *b_Vtx_n;       //!
    TBranch                *b_mu_density;       //!
    TBranch                *b_n_BJets;     //!
    TBranch                *b_n_BaseJets;   //!
    TBranch                *b_n_BaseTau;       //!
    TBranch                *b_n_SignalElec;     //!
    TBranch                *b_n_BaseElec;   //!
    TBranch                *b_n_SignalJets;     //!
    TBranch                *b_n_SignalMuon;     //!
    TBranch                *b_n_BaseMuon;   //!
    TBranch                *b_n_SignalTau;     //!
    TBranch                *b_OS_BaseTauBaseEle;       //!
    TBranch                *b_OS_BaseTauBaseMuo;       //!
    TBranch                *b_OS_BaseTauEle;       //!
    TBranch                *b_OS_BaseTauMuo;       //!
    TBranch                *b_OS_EleEle;       //!
    TBranch                *b_OS_MuoMuo;       //!
    TBranch                *b_OS_TauEle;       //!
    TBranch                *b_OS_TauMuo;       //!
    TBranch                *b_OS_TauTau;       //!

    TBranch                *b_MetTST_EleBiSect;     //!
    TBranch                *b_MetTST_MuoBiSect;     //!
    TBranch                *b_RandomLumiBlockNumber;       //!
    TBranch                *b_RandomRunNumber;     //!
    TBranch                *b_backgroundFlags;     //!
    TBranch                *b_bcid;     //!
    TBranch                *b_coreFlags;       //!
    TBranch                *b_forwardDetFlags;     //!
    TBranch                *b_larFlags;     //!
    TBranch                *b_lumiBlock;       //!
    TBranch                *b_lumiFlags;       //!
    TBranch                *b_muonFlags;       //!
    TBranch                *b_pixelFlags;   //!
    TBranch                *b_runNumber;       //!
    TBranch                *b_sctFlags;     //!
    TBranch                *b_tileFlags;       //!
    TBranch                *b_trtFlags;     //!
    TBranch                *b_eventNumber;     //!
    TBranch                *b_MetCST_met;   //!
    TBranch                *b_MetCST_phi;   //!
    TBranch                *b_MetCST_sumet;     //!
    TBranch                *b_MetTST_met;   //!
    TBranch                *b_MetTST_phi;   //!
    TBranch                *b_MetTST_sumet;     //!
    TBranch                *b_TruthMET_met;     //!
    TBranch                *b_TruthMET_phi;     //!
    TBranch                *b_TruthMET_sumet;   //!
    TBranch                *b_n_TruthJets;     //!
    TBranch                *b_dilepton_pt;     //!
    TBranch                *b_dilepton_eta;     //!
    TBranch                *b_dilepton_phi;     //!
    TBranch                *b_dilepton_m;   //!
    TBranch                *b_dilepton_charge;     //!
    TBranch                *b_dilepton_pdgId;   //!
    TBranch                *b_electrons_pt;     //!
    TBranch                *b_electrons_eta;       //!
    TBranch                *b_electrons_phi;       //!
    TBranch                *b_electrons_e;     //!
    TBranch                *b_electrons_MT;     //!
    TBranch                *b_electrons_charge;     //!
    TBranch                *b_electrons_d0sig;     //!
    TBranch                *b_electrons_isol;   //!
    TBranch                *b_electrons_signal;     //!
    TBranch                *b_electrons_truthOrigin;       //!
    TBranch                *b_electrons_truthType;     //!
    TBranch                *b_electrons_z0sinTheta;     //!
    TBranch                *b_jets_pt;     //!
    TBranch                *b_jets_eta;     //!
    TBranch                *b_jets_phi;     //!
    TBranch                *b_jets_m;   //!
    TBranch                *b_jets_Jvt;     //!
    TBranch                *b_jets_LooseOR;     //!
    TBranch                *b_jets_MV2c10;     //!
    TBranch                *b_jets_NTrks;   //!
    TBranch                *b_jets_bjet;       //!
    TBranch                *b_jets_signal;     //!
    TBranch                *b_muons_pt;     //!
    TBranch                *b_muons_eta;       //!
    TBranch                *b_muons_phi;       //!
    TBranch                *b_muons_e;     //!
    TBranch                *b_muons_MT;     //!
    TBranch                *b_muons_charge;     //!
    TBranch                *b_muons_d0sig;     //!
    TBranch                *b_muons_isol;   //!
    TBranch                *b_muons_signal;     //!
    TBranch                *b_muons_truthOrigin;       //!
    TBranch                *b_muons_truthType;     //!
    TBranch                *b_muons_z0sinTheta;     //!
    TBranch                *b_taus_pt;     //!
    TBranch                *b_taus_eta;     //!
    TBranch                *b_taus_phi;     //!
    TBranch                *b_taus_e;   //!
    TBranch                *b_taus_BDTEleScore;     //!
    TBranch                *b_taus_BDTJetScore;     //!
    TBranch                *b_taus_ConeTruthLabelID;       //!
    TBranch                *b_taus_MT;     //!
    TBranch                *b_taus_NTrks;   //!
    TBranch                *b_taus_NTrksJet;       //!
    TBranch                *b_taus_PartonTruthLabelID;     //!
    TBranch                *b_taus_Quality;     //!
    TBranch                *b_taus_Width;   //!
    TBranch                *b_taus_charge;     //!
    TBranch                *b_taus_jetAssoc;       //!
    TBranch                *b_taus_signalID;       //!
    TBranch                *b_taus_truthOrigin;     //!
    TBranch                *b_taus_truthType;   //!
    TBranch                *b_trigger_tau_pt;
    TBranch                *b_trigger_tau_eta;
    TBranch                *b_trigger_tau_phi;
    TBranch                *b_trigger_tau_quality;
    TBranch                *b_trigger_tau_nChargedTracks;
    TBranch                *b_trigger_ele_LHVLoose;
    TBranch                *b_trigger_ele_pt;
    TBranch                *b_trigger_ele_eta;
    TBranch                *b_trigger_ele_phi;
    TBranch                *b_trigger_ele_quality;
    TBranch                *b_truth_taus_numCharged;
    TBranch                *b_truth_taus_pt;
    TBranch                *b_truth_taus_eta;
    TBranch                *b_truth_taus_phi;
    TBranch                *b_truth_taus_e;
    TBranch                *b_truth_electrons_pt;
    TBranch                *b_truth_electrons_eta;
    TBranch                *b_truth_electrons_phi;
    TBranch                *b_truth_electrons_e;
    TBranch                *b_truth_muons_pt;
    TBranch                *b_truth_muons_eta;
    TBranch                *b_truth_muons_phi;
    TBranch                *b_truth_muons_e;
    TBranch                *b_truth_jets_pt;
    TBranch                *b_truth_jets_eta;
    TBranch                *b_truth_jets_phi;
    TBranch                *b_truth_jets_e;
    TBranch                *b_trigtau1_asymmditau_pt;
    TBranch                *b_trigtau1_asymmditau_phi;
    TBranch                *b_trigtau1_asymmditau_eta;
    TBranch                *b_trigtau1_asymmditau_quality;
    TBranch                *b_trigtau2_asymmditau_pt;
    TBranch                *b_trigtau2_asymmditau_phi;
    TBranch                *b_trigtau2_asymmditau_eta;
    TBranch                *b_trigtau2_asymmditau_quality;
    TBranch                *b_trigtau1_ditaumet_pt;
    TBranch                *b_trigtau1_ditaumet_phi;
    TBranch                *b_trigtau1_ditaumet_eta;
    TBranch                *b_trigtau1_ditaumet_quality;
    TBranch                *b_trigtau2_ditaumet_pt;
    TBranch                *b_trigtau2_ditaumet_phi;
    TBranch                *b_trigtau2_ditaumet_eta;
    TBranch                *b_trigtau2_ditaumet_quality;
     
    TBranch                *b_jetTrigger_weight;       //!

    TBranch     *b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
    TBranch     *b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
    TBranch     *b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
    TBranch     *b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
    TBranch     *b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
    TBranch     *b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;

    TBranch     *b_MuoWeight_MUON_EFF_ISO_STAT__1down;
    TBranch     *b_MuoWeight_MUON_EFF_ISO_STAT__1up;
    TBranch     *b_MuoWeight_MUON_EFF_ISO_SYS__1down;
    TBranch     *b_MuoWeight_MUON_EFF_ISO_SYS__1up;
    TBranch     *b_MuoWeight_MUON_EFF_RECO_STAT__1down;
    TBranch     *b_MuoWeight_MUON_EFF_RECO_STAT__1up;
    TBranch     *b_MuoWeight_MUON_EFF_RECO_SYS__1down;
    TBranch     *b_MuoWeight_MUON_EFF_RECO_SYS__1up;
    TBranch     *b_MuoWeight_MUON_EFF_TTVA_STAT__1down;
    TBranch     *b_MuoWeight_MUON_EFF_TTVA_STAT__1up;
    TBranch     *b_MuoWeight_MUON_EFF_TTVA_SYS__1down;
    TBranch     *b_MuoWeight_MUON_EFF_TTVA_SYS__1up;

    TBranch     *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down;
    TBranch     *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up;
    TBranch     *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down;
    TBranch     *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
    TBranch     *b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;

    TBranch     *b_JetWeight_FT_EFF_B_systematics__1down;
    TBranch     *b_JetWeight_FT_EFF_B_systematics__1up;
    TBranch     *b_JetWeight_FT_EFF_C_systematics__1down;
    TBranch     *b_JetWeight_FT_EFF_C_systematics__1up;
    TBranch     *b_JetWeight_FT_EFF_Light_systematics__1down;
    TBranch     *b_JetWeight_FT_EFF_Light_systematics__1up;
    TBranch     *b_JetWeight_FT_EFF_extrapolation__1down;
    TBranch     *b_JetWeight_FT_EFF_extrapolation__1up;
    TBranch     *b_JetWeight_FT_EFF_extrapolation_from_charm__1down;
    TBranch     *b_JetWeight_FT_EFF_extrapolation_from_charm__1up;
    TBranch     *b_JetWeight_JET_JvtEfficiency__1down;
    TBranch     *b_JetWeight_JET_JvtEfficiency__1up;

    TBranch     *b_muWeight_PRW_DATASF__1down;
    TBranch     *b_muWeight_PRW_DATASF__1up;

    TBranch     *b_GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p20000E_01;
    TBranch     *b_GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p50000E_00;
    TBranch     *b_GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p10000E_01;
    TBranch     *b_GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p20000E_01;
    TBranch     *b_GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p50000E_00;
    TBranch     *b_GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p10000E_01;
    TBranch     *b_GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p20000E_01;
    TBranch     *b_GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p50000E_00;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260001;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260002;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260003;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260004;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260005;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260006;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260007;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260008;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260009;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260010;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260011;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260012;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260013;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260014;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260015;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260016;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260017;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260018;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260019;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260020;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260021;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260022;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260023;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260024;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260025;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260026;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260027;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260028;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260029;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260030;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260031;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260032;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260033;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260034;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260035;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260036;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260037;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260038;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260039;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260040;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260041;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260042;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260043;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260044;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260045;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260046;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260047;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260048;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260049;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260050;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260051;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260052;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260053;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260054;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260055;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260056;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260057;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260058;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260059;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260060;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260061;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260062;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260063;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260064;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260065;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260066;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260067;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260068;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260069;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260070;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260071;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260072;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260073;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260074;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260075;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260076;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260077;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260078;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260079;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260080;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260081;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260082;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260083;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260084;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260085;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260086;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260087;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260088;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260089;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260090;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260091;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260092;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260093;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260094;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260095;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260096;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260097;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260098;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260099;
    TBranch     *b_GenWeight_LHE_pdfset_eq_260100;
    TBranch     *b_GenWeight_LHE_mur_eq_0p5_muf_eq_0p5;
    TBranch     *b_GenWeight_LHE_mur_eq_0p5_muf_eq_1;
    TBranch     *b_GenWeight_LHE_mur_eq_0p5_muf_eq_2;
    TBranch     *b_GenWeight_LHE_mur_eq_1_muf_eq_0p5;
    TBranch     *b_GenWeight_LHE_mur_eq_1_muf_eq_2;
    TBranch     *b_GenWeight_LHE_mur_eq_2_muf_eq_0p5;
    TBranch     *b_GenWeight_LHE_mur_eq_2_muf_eq_1;
    TBranch     *b_GenWeight_LHE_mur_eq_2_muf_eq_2;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_CT14;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_MMHT;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_CT14;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_MMHT;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_0p5muF_CT14;
    TBranch     *b_GenWeight_LHE_0p5muF_MMHT;
    TBranch     *b_GenWeight_LHE_0p5muF_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_0p5muR_CT14;
    TBranch     *b_GenWeight_LHE_0p5muR_MMHT;
    TBranch     *b_GenWeight_LHE_0p5muR_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_CT14;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_MMHT;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_2muF_2muR_CT14;
    TBranch     *b_GenWeight_LHE_2muF_2muR_MMHT;
    TBranch     *b_GenWeight_LHE_2muF_2muR_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_2muF_CT14;
    TBranch     *b_GenWeight_LHE_2muF_MMHT;
    TBranch     *b_GenWeight_LHE_2muF_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_2muR_CT14;
    TBranch     *b_GenWeight_LHE_2muR_MMHT;
    TBranch     *b_GenWeight_LHE_2muR_PDF4LHC15;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__13165;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__25200;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260001;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260002;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260003;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260004;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260005;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260006;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260007;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260008;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260009;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260010;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260011;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260012;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260013;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260014;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260015;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260016;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260017;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260018;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260019;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260020;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260021;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260022;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260023;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260024;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260025;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260026;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260027;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260028;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260029;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260030;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260031;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260032;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260033;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260034;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260035;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260036;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260037;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260038;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260039;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260040;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260041;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260042;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260043;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260044;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260045;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260046;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260047;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260048;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260049;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260050;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260051;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260052;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260053;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260054;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260055;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260056;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260057;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260058;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260059;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260060;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260061;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260062;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260063;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260064;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260065;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260066;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260067;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260068;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260069;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260070;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260071;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260072;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260073;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260074;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260075;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260076;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260077;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260078;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260079;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260080;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260081;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260082;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260083;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260084;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260085;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260086;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260087;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260088;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260089;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260090;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260091;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260092;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260093;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260094;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260095;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260096;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260097;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260098;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260099;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260100;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90900;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90901;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90902;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90903;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90904;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90905;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90906;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90907;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90908;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90909;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90910;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90911;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90912;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90913;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90914;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90915;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90916;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90917;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90918;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90919;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90920;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90921;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90922;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90923;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90924;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90925;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90926;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90927;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90928;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90929;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90930;
    TBranch     *b_GenWeight_LHE_Var3cDown;
    TBranch     *b_GenWeight_LHE_Var3cUp;
    TBranch     *b_GenWeight_LHE_hardHi;
    TBranch     *b_GenWeight_LHE_hardLo;
    TBranch     *b_GenWeight_LHE_isrPDFminus;
    TBranch     *b_GenWeight_LHE_isrPDFplus;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_0p5;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_2p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_0p625_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_0p75_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_0p875_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p5;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p625;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p75;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p875;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p25;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p5;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p75;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_2p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p25_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p5_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_1p75_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_0p5;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_1p0;
    TBranch     *b_GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_2p0;
    TBranch     *b_GenWeight_LHE_muR__eq__0p50_muF__eq__0p50;
    TBranch     *b_GenWeight_LHE_muR__eq__0p50_muF__eq__1p00;
    TBranch     *b_GenWeight_LHE_muR__eq__0p50_muF__eq__2p00;
    TBranch     *b_GenWeight_LHE_muR__eq__1p00_muF__eq__0p50;
    TBranch     *b_GenWeight_LHE_muR__eq__1p00_muF__eq__2p00;
    TBranch     *b_GenWeight_LHE_muR__eq__2p00_muF__eq__0p50;
    TBranch     *b_GenWeight_LHE_muR__eq__2p00_muF__eq__1p00;
    TBranch     *b_GenWeight_LHE_muR__eq__2p00_muF__eq__2p00;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_0p5muF_2muR_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_0p5muF_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_0p5muF_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_0p5muF_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_0p5muF_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_0p5muR_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_0p5muR_NNPDF_0117;
    TBranch     *b_GenWeight_LHE_0p5muR_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_0p5muR_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_2muF_0p5muR_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_2muF_2muR_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_2muF_2muR_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_2muF_2muR_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_2muF_2muR_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_2muF_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_2muF_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_2muF_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_2muF_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_2muR_NNPDF31_NLO_0118;
    TBranch     *b_GenWeight_LHE_2muR_NNPDF_NLO_0117;
    TBranch     *b_GenWeight_LHE_2muR_NNPDF_NLO_0119;
    TBranch     *b_GenWeight_LHE_2muR_PDF4LHC15_NLO_30;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__265000;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__266000;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__303400;
    TBranch     *b_GenWeight_LHE_muR__eq__0p5_muF__eq__0p5;
    TBranch     *b_GenWeight_LHE_muR__eq__0p5_muF__eq__1p0;
    TBranch     *b_GenWeight_LHE_muR__eq__0p5_muF__eq__2p0;
    TBranch     *b_GenWeight_LHE_muR__eq__1p0_muF__eq__0p5;
    TBranch     *b_GenWeight_LHE_muR__eq__1p0_muF__eq__2p0;
    TBranch     *b_GenWeight_LHE_muR__eq__2p0_muF__eq__0p5;
    TBranch     *b_GenWeight_LHE_muR__eq__2p0_muF__eq__1p0;
    TBranch     *b_GenWeight_LHE_muR__eq__2p0_muF__eq__2p0;
    TBranch     *b_GenWeight_LHE_MEWeight;
    TBranch     *b_GenWeight_LHE_MUR0p5_MUF0p5_PDF261000;
    TBranch     *b_GenWeight_LHE_MUR0p5_MUF1_PDF261000;
    TBranch     *b_GenWeight_LHE_MUR1_MUF0p5_PDF261000;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF13000;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF25300;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261000;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261001;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261002;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261003;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261004;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261005;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261006;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261007;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261008;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261009;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261010;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261011;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261012;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261013;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261014;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261015;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261016;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261017;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261018;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261019;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261020;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261021;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261022;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261023;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261024;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261025;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261026;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261027;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261028;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261029;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261030;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261031;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261032;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261033;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261034;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261035;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261036;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261037;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261038;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261039;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261040;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261041;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261042;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261043;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261044;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261045;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261046;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261047;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261048;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261049;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261050;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261051;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261052;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261053;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261054;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261055;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261056;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261057;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261058;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261059;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261060;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261061;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261062;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261063;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261064;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261065;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261066;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261067;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261068;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261069;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261070;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261071;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261072;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261073;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261074;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261075;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261076;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261077;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261078;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261079;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261080;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261081;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261082;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261083;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261084;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261085;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261086;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261087;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261088;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261089;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261090;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261091;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261092;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261093;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261094;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261095;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261096;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261097;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261098;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261099;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF261100;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF269000;
    TBranch     *b_GenWeight_LHE_MUR1_MUF1_PDF270000;
    TBranch     *b_GenWeight_LHE_MUR1_MUF2_PDF261000;
    TBranch     *b_GenWeight_LHE_MUR2_MUF1_PDF261000;
    TBranch     *b_GenWeight_LHE_MUR2_MUF2_PDF261000;
    TBranch     *b_GenWeight_LHE_NTrials;
    TBranch     *b_GenWeight_LHE_WeightNormalisation;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__11068;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90400;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90401;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90402;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90403;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90404;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90405;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90406;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90407;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90408;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90409;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90410;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90411;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90412;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90413;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90414;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90415;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90416;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90417;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90418;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90419;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90420;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90421;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90422;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90423;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90424;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90425;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90426;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90427;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90428;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90429;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90430;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90431;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__90432;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__25300;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__11000;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__13100;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260000;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__91400;
    TBranch     *b_GenWeight_LHE_mtinf;
    TBranch     *b_GenWeight_LHE_mtmb;
    TBranch     *b_GenWeight_LHE_mtmb_bminlo;
    TBranch     *b_GenWeight_LHE_nnlops_bminlo;
    TBranch     *b_GenWeight_LHE_nnlops_mtinf;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgDnDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgDnNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgDnUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgNomDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgNomNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgNomUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgUpDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgUpNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloDn_pwgUpUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgDnDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgDnNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgDnUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgNomDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgNomUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgUpDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgUpNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloNom_pwgUpUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgDnDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgDnNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgDnUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgNomDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgNomNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgNomUp;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgUpDn;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgUpNom;
    TBranch     *b_GenWeight_LHE_nnlops_nnloUp_pwgUpUp;
    TBranch     *b_GenWeight_LHE_nnlops_nominal;
    TBranch     *b_GenWeight_LHE_nnlops_nominal_pdflhc;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__13191;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__25410;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260401;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260402;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260403;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260404;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260405;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260406;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260407;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260408;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260409;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260410;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260411;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260412;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260413;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260414;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260415;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260416;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260417;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260418;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260419;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260420;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260421;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260422;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260423;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260424;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260425;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260426;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260427;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260428;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260429;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260430;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260431;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260432;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260433;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260434;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260435;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260436;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260437;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260438;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260439;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260440;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260441;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260442;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260443;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260444;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260445;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260446;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260447;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260448;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260449;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260450;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260451;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260452;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260453;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260454;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260455;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260456;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260457;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260458;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260459;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260460;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260461;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260462;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260463;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260464;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260465;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260466;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260467;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260468;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260469;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260470;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260471;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260472;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260473;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260474;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260475;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260476;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260477;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260478;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260479;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260480;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260481;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260482;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260483;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260484;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260485;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260486;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260487;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260488;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260489;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260490;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260491;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260492;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260493;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260494;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260495;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260496;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260497;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260498;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260499;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__260500;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92000;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92001;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92002;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92003;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92004;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92005;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92006;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92007;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92008;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92009;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92010;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92011;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92012;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92013;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92014;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92015;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92016;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92017;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92018;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92019;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92020;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92021;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92022;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92023;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92024;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92025;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92026;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92027;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92028;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92029;
    TBranch     *b_GenWeight_LHE_PDF_set__eq__92030;

   

     DSBase_Nominal(TTree *tree=0);
     virtual ~DSBase_Nominal();
     virtual Int_t      Cut(Long64_t entry);
     virtual Int_t      GetEntry(Long64_t entry);
     virtual Long64_t LoadTree(Long64_t entry);
     virtual void           Init(TTree *tree);
     virtual void           Loop();
     virtual Bool_t     Notify();
     virtual void           Show(Long64_t entry = -1);

     virtual void           HandleDeprecatedBranches();

     template<typename T>
     void InitBranch(const char* name, T* varexp, TBranch** branch) {
             if (fChain->GetBranchStatus(name)) {
                     fChain->SetBranchAddress(name, varexp, branch);
             } else {
                     branch = nullptr;
                     varexp = nullptr;
             }
     };

     template<typename T>
     void InitBranch(const char* name, T* varexp) {
             if (fChain->GetBranchStatus(name)) {
                     fChain->SetBranchAddress(name, varexp);
             } else {
                     varexp = nullptr;
             }
     };
};

#endif 

// #ifdef DSBase_Nominal_cxx
// #endif // #ifdef DSBase_Nominal_cxx
