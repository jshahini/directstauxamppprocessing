#ifndef DIRECTSTAU_H
#define DIRECTSTAU_H

#include "DirectStau_Store.h"
#include "TriggerManager.h"
#include "GRLFilter.h"


#include <iostream>
#include <cmath>
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TList.h>
#include <TH1F.h>

class Analysis: public Store {
public :

  Analysis(){};

    virtual ~Analysis(){};

    TTree * tree;

    void Initialize();
    void Execute();
    void CalculateVariables();

    void CalculateTauVariables();
    void CalculateJetVariables();

    void CalculateTrigEventWeight();

    void LoadTriggerSets();
    void EvaluateTriggers();
    bool PassTriggers();
    bool PassPreselection();
    void FillNtuple();
    void WriteNtuple();
    void Finalize();

    template<typename T, typename U>
    double GetMt2(const T* obj1, const U* obj2, double m_invis1=0, double m_invis2=0);

    Trigger::Set* AsymDitauTrigset;
    Trigger::Set* DitauMETTrigset;
    Trigger::Set* SingleMuTrigset;
    Trigger::Set* SingleMu_HLT50_Trigset;

private :

    std::vector<const AnalysisObject::Tau*> mytaus; // correspond to Store::taus if selectAntiTaus == False, else it corresponds to the merged (pT-soreted) container of Store::taus + Store::antitaus
    std::vector<const AnalysisObject::Muon*> mymuons;

    GRLFilter* m_GRL_data17;

    int m_nevts_passed = 0;
    bool m_useNewDitauMETTrigger = false;

};

#endif //DIRECTSTAU_H
    
