#ifndef Monitoring_h
#define Monitoring_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1.h>
#include <TCanvas.h>

#include<string>
#include<vector>
#include<map>
#include<iostream>
#include<initializer_list>
#include<boost/algorithm/string/predicate.hpp>

class Monitoring {
public:
    virtual ~Monitoring() {};

    static void Initialize();

    static void AddHistogram(TH1D histo, std::string name);
    static void InitializeCutflow(std::initializer_list<std::string> cutnames);
    static void InitializeCut(const std::string cutname);
    static bool PassesCut(const bool cutexpression, const std::string cutname);
    static void WriteToOutput(TFile* outfile);

private:
    static TFile* m_destination;
    static std::vector<TH1D> m_histos;
    static std::vector<std::string> m_cutflow;
    static std::map<std::string, int> m_cutflow_raw;
    static std::map<std::string, float> m_cutflow_weighted;

};
#endif
