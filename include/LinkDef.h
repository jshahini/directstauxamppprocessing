#ifndef DSANALYSISLOOP_LINKDEF_H
#define DSANALYSISLOOP_LINKDEF_H

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class std::set<unsigned int>+;
#endif
#endif
