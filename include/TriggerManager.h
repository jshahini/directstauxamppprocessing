#ifndef TriggerManager_h
#define TriggerManager_h

#include <string>
#include <cstdarg>
#include <vector>
#include <iostream>
#include <functional>

#include "DirectStau_Store.h"

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"

namespace Trigger {

class Set {
public:
    virtual ~Set() {};
    Set(std::string name) {
        m_trigname = name;
        if (Store::Trig.find(m_trigname) == Store::Trig.end())
            LOG(ERROR) << "No trigger decision branch available for " << m_trigname << "!";
        else
            m_passedtrigdec = &Store::Trig.at(m_trigname);
        if (Store::TrigMatch.find(m_trigname) == Store::TrigMatch.end())
            LOG(ERROR) << "No trigger matching branch info available for " << m_trigname << "!";
        else
            m_istrigmatched = &Store::TrigMatch.at(m_trigname);
    };

    void EvaluateTrigMatch(bool trigmatched) { m_istrigmatched = new char(trigmatched); }; // if not called value set in the constructor will be used
    void EvaluatePlateauCuts(bool plateaucuts) { m_isinplateau = plateaucuts; }; // needs to be called for every event and each set

    std::string GetName() const { return m_trigname; };

    bool PassedTrigDec() const { return *m_passedtrigdec == 1; };
    bool IsTrigMatched() const { return *m_istrigmatched == 1; };
    bool IsInPlateau() const { return m_isinplateau; };

private:
    std::string m_trigname;
    char* m_passedtrigdec;
    char* m_istrigmatched;
    bool  m_isinplateau;
};

class Manager {
public:
    virtual ~Manager() {};

    static void Initialize(std::initializer_list<Set *> sets); // right now the logical OR of all sets is taken

    static bool PassedTrigDec();
    static bool IsTrigMatched();
    static bool IsInPlateau();

private:
    static std::vector<Set*> m_trigsets;
    static bool m_passthrough;
};
}
#endif
