#ifndef WeightCalculator_h
#define WeightCalculator_h

#include <set>

#include "DirectStau_Store.h"

class WeightCalculator: public Store {
public:
    virtual ~WeightCalculator() {};

    static void Initialize(TChain* fChain);
    static void Execute();
    static void ComputeSystematicWeights();
    static void Finalize();

    static bool RemoveDatasetOverlap();
    static void ReadMetaInfoFromTxtFile(std::initializer_list<std::string> filenames);

private:
    static std::map<int,std::map<int,std::vector<int>>> m_runNmbrDSIDProcessIDsMap; // track unique processids
    static std::vector<int> m_DSIDs; // track unique dsids (runmbr/mcchannelnmbr)

    static std::map<int,std::map<int,std::vector<int>>> m_runNmbrDSIDProcessedLumiBlocksMap;
    static std::map<int,std::map<int,std::vector<int>>> m_runNmbrDSIDTotalLumiBlocksMap;

    static std::map<int,std::map<int, std::map<int,double>>> m_runNmbrDSIDFSXSecMap;
    static std::map<int,std::map<int, std::map<int,double>>> m_runNmbrDSIDFSFilterEffMap;
    static std::map<int,std::map<int, std::map<int,double>>> m_runNmbrDSIDFSkFactorMap;
    static std::map<int,std::map<int, std::map<int,int>>>    m_runNmbrDSIDFSTotalEvtsMap;
    static std::map<int,std::map<int, std::map<int,int>>>    m_runNmbrDSIDFSProcessedEvtsMap;
    static std::map<int,std::map<int, std::map<int,double>>> m_runNmbrDSIDFSSumWMap;
    static std::map<int,std::map<int, std::map<int,double>>> m_runNmbrDSIDFSSumW2Map;

    // temporary variables (to be associated with corresponding dsid)
    static Bool_t   m_isData;
    static UInt_t   m_mcChannelNumber;
    static UInt_t   m_runNumber;
    static UInt_t   m_DSID;
    static UInt_t   m_ProcessID;
    static Double_t m_xSection;
    static Double_t m_kFactor;
    static Double_t m_FilterEfficiency;
    static Long64_t m_TotalEvents;
    static Double_t m_TotalSumW;
    static Double_t m_TotalSumW2;
    static Long64_t m_ProcessedEvents;

    static std::set<unsigned int> *m_ProcessedLumiBlocks;
    static std::set<unsigned int> *m_TotalLumiBlocks;

    static std::vector<int> m_SherpaDSIDs;
    static std::vector<int> m_DijetDSIDs;

};

// Some helper functions:
bool is_number(const std::string& s);

#endif
