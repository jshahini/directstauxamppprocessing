#ifndef RJigsaw_h
#define RJigsaw_h

#include "DirectStau_Store.h"

// Check preprocessor symbol first (see CMakeList.txt)
#if RESTFRAMES_MISSING == 0
#include "RestFrames/RestFrames.hh"

#include<string>

using namespace RestFrames;

class RJigsaw: public Store {
public:
    virtual ~RJigsaw();

    static void Initialize();
    static void WriteRFPlots(const std::string outputfilename);
    static void InitOutputBranches(TTree* outputtree);
    static void Reset();
    static void Execute();

private:
    static LabRecoFrame*         LAB;
    static DecayRecoFrame*       StauStau; // PP-frame
    static DecayRecoFrame*       Staua; // Pa-frame
    static DecayRecoFrame*       Staub; // Pb-frame

    static VisibleRecoFrame*     taua;
    static VisibleRecoFrame*     taub;
    static InvisibleRecoFrame*   X1a;
    static InvisibleRecoFrame*   X1b;

    static InvisibleGroup*       INV;

    static SetMassInvJigsaw*     X1_mass;
    static SetRapidityInvJigsaw* X1_eta;
    static ContraBoostInvJigsaw* X1X1_contra;

    static TreePlot*             treePlot; // RestFrame monitoring plots

};
#else
// Create a shallow class (which does absolutely nothing but keep everything else up and running)
class RJigsaw: public Store {
public:
    virtual ~RJigsaw();

    static void Initialize();
    static void WriteRFPlots(const std::string outputfilename) {};
    static void InitOutputBranches(TTree* outputtree) {};
    static void Reset() {};
    static void Execute() {};

};
#endif
#endif
