#ifndef FakeFactorManager_h
#define FakeFactorManager_h

#include "DirectStau_Store.h"

#include <TChain.h>
#include <TH2D.h>

#include <string>
#include <map>

class FakeFactorManager: public Store {
public:
    virtual ~FakeFactorManager() {};

    static void Initialize(TChain* fChain);
    static void Calculate();

    static bool VetoEvent() { return m_vetoevent; }; 
    static void LoadFakeFactors(std::string path);
    static void LoadFFSysSources(TFile *tdir);
    static double GetFakeFactor(std::string level, std::string promstep, std::string nonpromtauqual, int nprong, double pt, double eta);

    static double ComputeFFWeight_Default(std::string level="Nominal");
    static double ComputeFFWeight_AtLeastOneTightTau(std::string level="Nominal");
    static double ComputeFFWeight_AtLeastTwoTightTaus(std::string level="Nominal");
    static double ComputeFFWeight_AtLeastTwoTightTaus_TightID(std::string level="Nominal"); // more anti-ID tau statisitics!?

private:
    static int m_nevts_R1;   // tau1 = antiID, tau2 = antiID
    static int m_nevts_R2;   // tau1 = antiID, tau2 = ID
    static int m_nevts_R3;   // tau1 = ID,     tau2 = antiID
    static int m_nevts_R123; // SUM

    static bool m_vetoevent;
    static int m_currentregion;

    static std::map<std::string, std::map<std::string, std::map<std::string, std::map<int,TH2D>>>> m_ffhisto;
};

#endif
