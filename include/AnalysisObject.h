#include <map>
#include <vector>

#include <TLorentzVector.h>
#include <TVector3.h>

namespace AnalysisObject
{
  class Base  : public TLorentzVector {
  public:
      virtual ~Base() {};

      void SetMt(double mt) { m_mt = mt; };
      void SetTrigMatch(std::string hltname, bool hastrigmatch) { m_trigmatchmap[hltname] = hastrigmatch; };
      void SetTruthMatch(bool istruthmatched) { m_fake = !istruthmatched; };
      void SetWeight(double weight) { m_weight = weight; };
      void SetSignal(bool issignal) { m_signal = issignal; };
      void SetTruthType(int truthtype) { m_truthtype = truthtype; };
      void SetTruthOrigin(int truthorigin) { m_truthorigin = truthorigin; };

      double Mt() const { return m_mt; };
      bool Fake() const { return m_fake; };
      bool TrigMatch(std::string hltname) const { return m_trigmatchmap.at(hltname); };
      double Weight() const { return m_weight; };
      bool Signal() const { return m_signal; };
      int TruthType() const { return m_truthtype; };
      int TruthOrigin() const { return m_truthorigin; };

      Base(){};
      Base(const Base& obj) : TLorentzVector(obj),
          m_mt(obj.m_mt),
          m_fake(obj.m_fake),
          m_trigmatchmap(obj.m_trigmatchmap),
          m_weight(obj.m_weight),
          m_signal(obj.m_signal),
          m_truthtype(obj.m_truthtype),
          m_truthorigin(obj.m_truthorigin) {};

  private:
      double m_mt = -1.0;
      bool m_fake = false;
      std::map<std::string, bool> m_trigmatchmap;
      double m_weight = 1.0;
      bool m_signal = false;
      int m_truthtype = -1;
      int m_truthorigin = -1;

  };

  class Electron : public Base {
  public:
      void SetCharge(double charge) { m_charge = charge; };

      double Charge() const { return m_charge; };

      Electron(){};
      Electron(const Electron& obj) : Base(obj),
          m_charge(obj.m_charge) {};

  private:
      double m_charge;
  };

  class Muon : public Base {
  public:
      void SetCharge(double charge) { m_charge = charge; };

      double Charge() const { return m_charge; };

      Muon(){};
      Muon(const Muon& obj) : Base(obj),
          m_charge(obj.m_charge) {};

  private:
      double m_charge;
  };

  class Tau: public Base {
  public:
      void SetNProng(int nprong) { m_nprong = nprong; };
      void SetCharge(double charge) { m_charge = charge; };
      void SetQuality(int quality) { m_quality = quality; };
      void SetWidth(double width) { m_width = width; };
      void SetBDTJetScore(double bdtjetscore) { m_bdtjetscore = bdtjetscore; };
      void SetBDTEleScore(double bdtelescore) { m_bdtelescore = bdtelescore; };
      void SetBDTJetScoreSigTrans(double bdtjetscoresigtrans) { m_bdtjetscoresigtrans = bdtjetscoresigtrans; };
      void SetBDTEleScoreSigTrans(double bdtelescoresigtrans) { m_bdtelescoresigtrans = bdtelescoresigtrans; };
      void SetPartonTruthLabelID(int partontruthlabelid) { m_partontruthlabelid = partontruthlabelid; };
      void SetConeTruthLabelID(int conetruthlabelid) { m_conetruthlabelid = conetruthlabelid; };

      int NProng() const { return m_nprong; };
      double Charge() const { return m_charge; };
      int Quality() const { return m_quality; };
      double Width() const { return m_width; };
      double BDTJetScore() const { return m_bdtjetscore; };
      double BDTEleScore() const { return m_bdtelescore; };
      double BDTJetScoreSigTrans() const { return m_bdtjetscoresigtrans; };
      double BDTEleScoreSigTrans() const { return m_bdtelescoresigtrans; };
      int PartonTruthLabelID() const { return m_partontruthlabelid; };
      int ConeTruthLabelID() const { return m_conetruthlabelid; };

      Tau(){};
      Tau(const Tau& obj) : Base(obj),
          m_charge(obj.m_charge),
          m_quality(obj.m_quality),
          m_nprong(obj.m_nprong),
          m_width(obj.m_width),
          m_bdtjetscore(obj.m_bdtjetscore),
          m_bdtelescore(obj.m_bdtelescore),
          m_bdtjetscoresigtrans(obj.m_bdtjetscoresigtrans),
          m_bdtelescoresigtrans(obj.m_bdtelescoresigtrans),
          m_partontruthlabelid(obj.m_partontruthlabelid),
          m_conetruthlabelid(obj.m_conetruthlabelid) {};

  private:
      double m_charge;
      int m_quality;
      int m_nprong;
      double m_width;
      double m_bdtjetscore;
      double m_bdtelescore;
      double m_bdtjetscoresigtrans;
      double m_bdtelescoresigtrans;
      int m_partontruthlabelid = -1;
      int m_conetruthlabelid = -1;
  };

  class Jet : public Base {
  public:
      void SetBtag(double btag) { m_btag = btag; };
      void SetNTracks(double ntracks) { m_ntracks = ntracks; };
      void SetJVT(double jvt) { m_jvt = jvt; };

      bool Btag() const { return m_btag; };
      int NTracks() const { return m_ntracks; };
      double JVT() const { return m_jvt; };

      Jet(){};
      Jet(const Jet& obj) : Base(obj),
          m_btag(obj.m_btag),
          m_ntracks(obj.m_ntracks),
          m_jvt(obj.m_jvt) {};

  private:
      double m_btag;
      int m_ntracks;
      double m_jvt;
  };

  class ETmiss : public TVector3 {
  public:
      virtual ~ETmiss() {};

      void SetSumet(double etmiss_sumet) { m_etmiss_sumet = etmiss_sumet; };

      double Sumet() const { return m_etmiss_sumet; };

      ETmiss() {};
      ETmiss(const ETmiss& obj) : TVector3(obj) {};

  private:
      double m_etmiss_sumet;
  };

  template <typename T>
  void BasicSorter(std::vector<T>& container){
      std::sort(container.begin(), container.end(), [ ]( const T& lhs, const T& rhs ){return lhs.Pt() > rhs.Pt();} ) ;
  }

  template <typename T>
  void BasicSorter(std::vector<T*>& container){
      std::sort(container.begin(), container.end(), [ ]( const T* lhs, const T* rhs ){return lhs->Pt() > rhs->Pt();} ) ;
  }

  void FillElectronContainer();
  void FillMuonContainer();
  void FillTauContainer();
  void FillJetContainer();

  void GetETmiss();


  void FillContainers();
  void RemoveOverlapOfContainers(); // right now only for antitau container
  void ClearContainers();

  template<typename T, typename U>
  void RemoveOverlap(std::vector<T>& slim_container, const std::vector<U>& check_container, const double dR = 0.2) {
      // Remove items in slim_container if they geometrically overlap (within dR) with any item in check_container
      if (slim_container.empty() || check_container.empty()) return;
      for(U check_obj : check_container) {
          RemoveOverlap(slim_container, check_obj, dR);
      }
  }

  template<typename T, typename U>
  void RemoveOverlap(std::vector<T>& slim_container, const U& check_obj, const double dR = 0.2) {
      // Remove items in slim_container if they geometrically overlap (within dR) with check_obj
      if (slim_container.empty()) return;
      slim_container.erase(std::remove_if(slim_container.begin(),
                                          slim_container.end(),
                                          [&](T slim_obj) {return slim_obj.DeltaR(check_obj) < dR;}),
                           slim_container.end());
  }

  template<typename T, typename U>
  bool CheckOverlap(const std::vector<T>& container1, const std::vector<U>& container2, const double dR = 0.2) {
      if (container1.empty() || container2.empty()) return false;
      for(U obj2 : container2) {
          return CheckOverlap(container1, obj2, dR);
      }
  }

  template<typename T, typename U>
  bool CheckOverlap(const std::vector<T>& container1, const U& obj2, const double dR = 0.2) {
      if (container1.empty()) return false;
      return std::find_if(container1.begin(),
                          container1.end(),
                          [&](T obj1) {return CheckOverlap(obj1, obj2, dR);})
              != container1.end();
  }

  template<typename T, typename U>
  bool CheckOverlap(const T& obj1, const U& obj2, const double dR = 0.2) {
      return obj1.DeltaR(obj2) < dR;
  }


} // end of namespace AnalysisObject

// TODO:
//  * Truth object class (pdgID/type -> enum, source,...)
//  * Truth matching function
//  * Trigger object class
//  * Trigger matching


