#ifndef GRLFilter_h
#define GRLFilter_h

#include "../include/pugixml.hpp"

#include <string>

class GRLFilter {
public:
    virtual ~GRLFilter() {};

    GRLFilter(){};
    GRLFilter(const std::string xmlpath) : m_xmlpath(xmlpath) {this->LoadXMLFile();};

    bool Contains(const int &runnumber, const int &lumiblock) const;

private:
    void LoadXMLFile();
    std::string m_xmlpath;
    pugi::xml_document m_xmldoc;
    pugi::xpath_node_set m_nodes;
};
#endif
