# DSAnalysisLoop

Simple loop for [XAMPPstau](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau) n-tuples.
Copy of code from this repo, which may now be inaccessible (https://gitlab.cern.ch/fkrieter/DSAnalysisLoop)

## Setup

```
source current_setup.sh
```

## Compile

First time compile:
```
cd build
cmake3 ../
make 
cd ../
```

Subsequent compiles: 
``` 
cd build
make 
cd ../
```


## Example Running
```
dsaloop -o outputfile.root inputfile.root -l '{"MC16a":36.20766, "MC16d":44.3074, "MC16e" :58.4501}' --overrideMetadata ./productions/NewMetaData.txt -p Zll --nameTree Nominal
```
Above command will run over `inputfile.root` and produce an output ntuple `outputfile.root` with one tree `Zll_Nominal`.  
Need to run command for each tree-based systematic, replacing `Nominal` with name of the systematic variation  e.g.
`TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up`
See `my_recast_run.py` script to for an example of how to loop over.
Note this code will remove `_AFII` suffixes from the output treenames. 

To run over multiple input files, can either
* replace `inputfile.root` with a directory path, in which case will grab all `.root` files in that directory
* replace `inputfile.root` with a space separated list of input files
* For all three mc16 periods, best way is to do a space separated list of the different input directories with `.root` at the end

Note if want to override info in MetaData tree in input file (e.g. for cross-section), can add details to `productions/NewMetaData.txt` file.  


## RECAST Running
For running on the RECAST signal, do: 
```
dsaloop -o outputpath/RECAST_signal_skimmed_ntuple_Nominal.root path_to_inputs/Prod_V4_Syst16a.RECAST_signal_XAMPP/*.root path_to_inputs/Prod_V4_Syst16d.RECAST_signal_XAMPP/*.root path_to_inputs/Prod_V4_Syst16e.RECAST_signal_XAMPP/*.root -l '{"MC16a":36.20766, "MC16d":44.3074, "MC16e" :58.4501}' --overrideMetadata ./productions/NewMetaData.txt -p RECAST_signal --nameTree Nominal
```
for each systematic variation.
Then hadd together the output files for all the systematic variations.
A full list of the experimental systematic variations can be found here
https://gitlab.cern.ch/dojones/directstaucodeforrecast/-/blob/master/config/treeNamesdict.py 
under `treenamesdict['FullExpSysts']['sig']` assuming the signal has been processed with AFII.
