#include <iostream>
#include <cmath>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TList.h>
#include <TH1F.h>
#include <TH1.h>

#include "../include/DirectStau_Analysis.h"
#include "../include/lester_mt2_bisect.h"
#include "../include/Monitoring.h"
#include "../include/RJigsaw.h"
#include "../include/FakeFactorManager.h"
#include "../include/WeightCalculator.h"

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#       define ELPP_DEFAULT_LOGGER "default"
#endif
#include "easylogging++.h"


void Analysis::Initialize(){

    outputFile->cd();
    gFile = outputFile;

    asymm_mt2_lester_bisect::disableCopyrightMessage();

    // Bertrand Martin Dit Latour on 2019-02-09 1:06 PM
    // Use dedicated data17 GRL for HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
    // where all LBs with inactive L1 item are dropped.
    // To recover the missing 3.3 fb-1 one needs to (see Analysis::LoadTriggerSets):
    //      - Check if the event passes this new GRL, and if so, require that
    //          HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 fires (and apply corresponding plateau cuts)
    //      - If it does not pass the new GRL, check if the event passes the official GRL and require that
    //          HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 fires (and apply corresponding plateau cuts)
    //m_GRL_data17 = new GRLFilter("/project/etp1/fkrieter/Code/DSAnalysisLoop/productions/newGRL.xml");
    m_GRL_data17 = new GRLFilter("productions/newGRL.xml");

    //tree = new TTree("DirectStau", "DirectStau");
    tree = new TTree(processName + "_" + treeStem, processName + "_" + treeStem);
    LOG(INFO) << "Initializing output tree '" << std::string(tree->GetName()) << "'...";

    //LHE weights
    if (treeStem == "Nominal" && isData == false) { 
        tree->Branch("GenWeight_LHE_muR_eq_0p10000E+01_muF_eq_0p20000E+01", &GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p20000E_01);
        tree->Branch("GenWeight_LHE_muR_eq_0p10000E+01_muF_eq_0p50000E+00", &GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p50000E_00);
        tree->Branch("GenWeight_LHE_muR_eq_0p20000E+01_muF_eq_0p10000E+01", &GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p10000E_01);
        tree->Branch("GenWeight_LHE_muR_eq_0p20000E+01_muF_eq_0p20000E+01", &GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p20000E_01);
        tree->Branch("GenWeight_LHE_muR_eq_0p20000E+01_muF_eq_0p50000E+00", &GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p50000E_00);
        tree->Branch("GenWeight_LHE_muR_eq_0p50000E+00_muF_eq_0p10000E+01", &GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p10000E_01);
        tree->Branch("GenWeight_LHE_muR_eq_0p50000E+00_muF_eq_0p20000E+01", &GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p20000E_01);
        tree->Branch("GenWeight_LHE_muR_eq_0p50000E+00_muF_eq_0p50000E+00", &GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p50000E_00);
        tree->Branch("GenWeight_LHE_pdfset_eq_260001", &GenWeight_LHE_pdfset_eq_260001);
        tree->Branch("GenWeight_LHE_pdfset_eq_260002", &GenWeight_LHE_pdfset_eq_260002);
        tree->Branch("GenWeight_LHE_pdfset_eq_260003", &GenWeight_LHE_pdfset_eq_260003);
        tree->Branch("GenWeight_LHE_pdfset_eq_260004", &GenWeight_LHE_pdfset_eq_260004);
        tree->Branch("GenWeight_LHE_pdfset_eq_260005", &GenWeight_LHE_pdfset_eq_260005);
        tree->Branch("GenWeight_LHE_pdfset_eq_260006", &GenWeight_LHE_pdfset_eq_260006);
        tree->Branch("GenWeight_LHE_pdfset_eq_260007", &GenWeight_LHE_pdfset_eq_260007);
        tree->Branch("GenWeight_LHE_pdfset_eq_260008", &GenWeight_LHE_pdfset_eq_260008);
        tree->Branch("GenWeight_LHE_pdfset_eq_260009", &GenWeight_LHE_pdfset_eq_260009);
        tree->Branch("GenWeight_LHE_pdfset_eq_260010", &GenWeight_LHE_pdfset_eq_260010);
        tree->Branch("GenWeight_LHE_pdfset_eq_260011", &GenWeight_LHE_pdfset_eq_260011);
        tree->Branch("GenWeight_LHE_pdfset_eq_260012", &GenWeight_LHE_pdfset_eq_260012);
        tree->Branch("GenWeight_LHE_pdfset_eq_260013", &GenWeight_LHE_pdfset_eq_260013);
        tree->Branch("GenWeight_LHE_pdfset_eq_260014", &GenWeight_LHE_pdfset_eq_260014);
        tree->Branch("GenWeight_LHE_pdfset_eq_260015", &GenWeight_LHE_pdfset_eq_260015);
        tree->Branch("GenWeight_LHE_pdfset_eq_260016", &GenWeight_LHE_pdfset_eq_260016);
        tree->Branch("GenWeight_LHE_pdfset_eq_260017", &GenWeight_LHE_pdfset_eq_260017);
        tree->Branch("GenWeight_LHE_pdfset_eq_260018", &GenWeight_LHE_pdfset_eq_260018);
        tree->Branch("GenWeight_LHE_pdfset_eq_260019", &GenWeight_LHE_pdfset_eq_260019);
        tree->Branch("GenWeight_LHE_pdfset_eq_260020", &GenWeight_LHE_pdfset_eq_260020);
        tree->Branch("GenWeight_LHE_pdfset_eq_260021", &GenWeight_LHE_pdfset_eq_260021);
        tree->Branch("GenWeight_LHE_pdfset_eq_260022", &GenWeight_LHE_pdfset_eq_260022);
        tree->Branch("GenWeight_LHE_pdfset_eq_260023", &GenWeight_LHE_pdfset_eq_260023);
        tree->Branch("GenWeight_LHE_pdfset_eq_260024", &GenWeight_LHE_pdfset_eq_260024);
        tree->Branch("GenWeight_LHE_pdfset_eq_260025", &GenWeight_LHE_pdfset_eq_260025);
        tree->Branch("GenWeight_LHE_pdfset_eq_260026", &GenWeight_LHE_pdfset_eq_260026);
        tree->Branch("GenWeight_LHE_pdfset_eq_260027", &GenWeight_LHE_pdfset_eq_260027);
        tree->Branch("GenWeight_LHE_pdfset_eq_260028", &GenWeight_LHE_pdfset_eq_260028);
        tree->Branch("GenWeight_LHE_pdfset_eq_260029", &GenWeight_LHE_pdfset_eq_260029);
        tree->Branch("GenWeight_LHE_pdfset_eq_260030", &GenWeight_LHE_pdfset_eq_260030);
        tree->Branch("GenWeight_LHE_pdfset_eq_260031", &GenWeight_LHE_pdfset_eq_260031);
        tree->Branch("GenWeight_LHE_pdfset_eq_260032", &GenWeight_LHE_pdfset_eq_260032);
        tree->Branch("GenWeight_LHE_pdfset_eq_260033", &GenWeight_LHE_pdfset_eq_260033);
        tree->Branch("GenWeight_LHE_pdfset_eq_260034", &GenWeight_LHE_pdfset_eq_260034);
        tree->Branch("GenWeight_LHE_pdfset_eq_260035", &GenWeight_LHE_pdfset_eq_260035);
        tree->Branch("GenWeight_LHE_pdfset_eq_260036", &GenWeight_LHE_pdfset_eq_260036);
        tree->Branch("GenWeight_LHE_pdfset_eq_260037", &GenWeight_LHE_pdfset_eq_260037);
        tree->Branch("GenWeight_LHE_pdfset_eq_260038", &GenWeight_LHE_pdfset_eq_260038);
        tree->Branch("GenWeight_LHE_pdfset_eq_260039", &GenWeight_LHE_pdfset_eq_260039);
        tree->Branch("GenWeight_LHE_pdfset_eq_260040", &GenWeight_LHE_pdfset_eq_260040);
        tree->Branch("GenWeight_LHE_pdfset_eq_260041", &GenWeight_LHE_pdfset_eq_260041);
        tree->Branch("GenWeight_LHE_pdfset_eq_260042", &GenWeight_LHE_pdfset_eq_260042);
        tree->Branch("GenWeight_LHE_pdfset_eq_260043", &GenWeight_LHE_pdfset_eq_260043);
        tree->Branch("GenWeight_LHE_pdfset_eq_260044", &GenWeight_LHE_pdfset_eq_260044);
        tree->Branch("GenWeight_LHE_pdfset_eq_260045", &GenWeight_LHE_pdfset_eq_260045);
        tree->Branch("GenWeight_LHE_pdfset_eq_260046", &GenWeight_LHE_pdfset_eq_260046);
        tree->Branch("GenWeight_LHE_pdfset_eq_260047", &GenWeight_LHE_pdfset_eq_260047);
        tree->Branch("GenWeight_LHE_pdfset_eq_260048", &GenWeight_LHE_pdfset_eq_260048);
        tree->Branch("GenWeight_LHE_pdfset_eq_260049", &GenWeight_LHE_pdfset_eq_260049);
        tree->Branch("GenWeight_LHE_pdfset_eq_260050", &GenWeight_LHE_pdfset_eq_260050);
        tree->Branch("GenWeight_LHE_pdfset_eq_260051", &GenWeight_LHE_pdfset_eq_260051);
        tree->Branch("GenWeight_LHE_pdfset_eq_260052", &GenWeight_LHE_pdfset_eq_260052);
        tree->Branch("GenWeight_LHE_pdfset_eq_260053", &GenWeight_LHE_pdfset_eq_260053);
        tree->Branch("GenWeight_LHE_pdfset_eq_260054", &GenWeight_LHE_pdfset_eq_260054);
        tree->Branch("GenWeight_LHE_pdfset_eq_260055", &GenWeight_LHE_pdfset_eq_260055);
        tree->Branch("GenWeight_LHE_pdfset_eq_260056", &GenWeight_LHE_pdfset_eq_260056);
        tree->Branch("GenWeight_LHE_pdfset_eq_260057", &GenWeight_LHE_pdfset_eq_260057);
        tree->Branch("GenWeight_LHE_pdfset_eq_260058", &GenWeight_LHE_pdfset_eq_260058);
        tree->Branch("GenWeight_LHE_pdfset_eq_260059", &GenWeight_LHE_pdfset_eq_260059);
        tree->Branch("GenWeight_LHE_pdfset_eq_260060", &GenWeight_LHE_pdfset_eq_260060);
        tree->Branch("GenWeight_LHE_pdfset_eq_260061", &GenWeight_LHE_pdfset_eq_260061);
        tree->Branch("GenWeight_LHE_pdfset_eq_260062", &GenWeight_LHE_pdfset_eq_260062);
        tree->Branch("GenWeight_LHE_pdfset_eq_260063", &GenWeight_LHE_pdfset_eq_260063);
        tree->Branch("GenWeight_LHE_pdfset_eq_260064", &GenWeight_LHE_pdfset_eq_260064);
        tree->Branch("GenWeight_LHE_pdfset_eq_260065", &GenWeight_LHE_pdfset_eq_260065);
        tree->Branch("GenWeight_LHE_pdfset_eq_260066", &GenWeight_LHE_pdfset_eq_260066);
        tree->Branch("GenWeight_LHE_pdfset_eq_260067", &GenWeight_LHE_pdfset_eq_260067);
        tree->Branch("GenWeight_LHE_pdfset_eq_260068", &GenWeight_LHE_pdfset_eq_260068);
        tree->Branch("GenWeight_LHE_pdfset_eq_260069", &GenWeight_LHE_pdfset_eq_260069);
        tree->Branch("GenWeight_LHE_pdfset_eq_260070", &GenWeight_LHE_pdfset_eq_260070);
        tree->Branch("GenWeight_LHE_pdfset_eq_260071", &GenWeight_LHE_pdfset_eq_260071);
        tree->Branch("GenWeight_LHE_pdfset_eq_260072", &GenWeight_LHE_pdfset_eq_260072);
        tree->Branch("GenWeight_LHE_pdfset_eq_260073", &GenWeight_LHE_pdfset_eq_260073);
        tree->Branch("GenWeight_LHE_pdfset_eq_260074", &GenWeight_LHE_pdfset_eq_260074);
        tree->Branch("GenWeight_LHE_pdfset_eq_260075", &GenWeight_LHE_pdfset_eq_260075);
        tree->Branch("GenWeight_LHE_pdfset_eq_260076", &GenWeight_LHE_pdfset_eq_260076);
        tree->Branch("GenWeight_LHE_pdfset_eq_260077", &GenWeight_LHE_pdfset_eq_260077);
        tree->Branch("GenWeight_LHE_pdfset_eq_260078", &GenWeight_LHE_pdfset_eq_260078);
        tree->Branch("GenWeight_LHE_pdfset_eq_260079", &GenWeight_LHE_pdfset_eq_260079);
        tree->Branch("GenWeight_LHE_pdfset_eq_260080", &GenWeight_LHE_pdfset_eq_260080);
        tree->Branch("GenWeight_LHE_pdfset_eq_260081", &GenWeight_LHE_pdfset_eq_260081);
        tree->Branch("GenWeight_LHE_pdfset_eq_260082", &GenWeight_LHE_pdfset_eq_260082);
        tree->Branch("GenWeight_LHE_pdfset_eq_260083", &GenWeight_LHE_pdfset_eq_260083);
        tree->Branch("GenWeight_LHE_pdfset_eq_260084", &GenWeight_LHE_pdfset_eq_260084);
        tree->Branch("GenWeight_LHE_pdfset_eq_260085", &GenWeight_LHE_pdfset_eq_260085);
        tree->Branch("GenWeight_LHE_pdfset_eq_260086", &GenWeight_LHE_pdfset_eq_260086);
        tree->Branch("GenWeight_LHE_pdfset_eq_260087", &GenWeight_LHE_pdfset_eq_260087);
        tree->Branch("GenWeight_LHE_pdfset_eq_260088", &GenWeight_LHE_pdfset_eq_260088);
        tree->Branch("GenWeight_LHE_pdfset_eq_260089", &GenWeight_LHE_pdfset_eq_260089);
        tree->Branch("GenWeight_LHE_pdfset_eq_260090", &GenWeight_LHE_pdfset_eq_260090);
        tree->Branch("GenWeight_LHE_pdfset_eq_260091", &GenWeight_LHE_pdfset_eq_260091);
        tree->Branch("GenWeight_LHE_pdfset_eq_260092", &GenWeight_LHE_pdfset_eq_260092);
        tree->Branch("GenWeight_LHE_pdfset_eq_260093", &GenWeight_LHE_pdfset_eq_260093);
        tree->Branch("GenWeight_LHE_pdfset_eq_260094", &GenWeight_LHE_pdfset_eq_260094);
        tree->Branch("GenWeight_LHE_pdfset_eq_260095", &GenWeight_LHE_pdfset_eq_260095);
        tree->Branch("GenWeight_LHE_pdfset_eq_260096", &GenWeight_LHE_pdfset_eq_260096);
        tree->Branch("GenWeight_LHE_pdfset_eq_260097", &GenWeight_LHE_pdfset_eq_260097);
        tree->Branch("GenWeight_LHE_pdfset_eq_260098", &GenWeight_LHE_pdfset_eq_260098);
        tree->Branch("GenWeight_LHE_pdfset_eq_260099", &GenWeight_LHE_pdfset_eq_260099);
        tree->Branch("GenWeight_LHE_pdfset_eq_260100", &GenWeight_LHE_pdfset_eq_260100);
        tree->Branch("GenWeight_LHE_mur_eq_0p5_muf_eq_0p5", &GenWeight_LHE_mur_eq_0p5_muf_eq_0p5);
        tree->Branch("GenWeight_LHE_mur_eq_0p5_muf_eq_1", &GenWeight_LHE_mur_eq_0p5_muf_eq_1);
        tree->Branch("GenWeight_LHE_mur_eq_0p5_muf_eq_2", &GenWeight_LHE_mur_eq_0p5_muf_eq_2);
        tree->Branch("GenWeight_LHE_mur_eq_1_muf_eq_0p5", &GenWeight_LHE_mur_eq_1_muf_eq_0p5);
        tree->Branch("GenWeight_LHE_mur_eq_1_muf_eq_2", &GenWeight_LHE_mur_eq_1_muf_eq_2);
        tree->Branch("GenWeight_LHE_mur_eq_2_muf_eq_0p5", &GenWeight_LHE_mur_eq_2_muf_eq_0p5);
        tree->Branch("GenWeight_LHE_mur_eq_2_muf_eq_1", &GenWeight_LHE_mur_eq_2_muf_eq_1);
        tree->Branch("GenWeight_LHE_mur_eq_2_muf_eq_2", &GenWeight_LHE_mur_eq_2_muf_eq_2);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_CT14", &GenWeight_LHE_0p5muF_0p5muR_CT14);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_MMHT", &GenWeight_LHE_0p5muF_0p5muR_MMHT);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15", &GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_CT14", &GenWeight_LHE_0p5muF_2muR_CT14);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_MMHT", &GenWeight_LHE_0p5muF_2muR_MMHT);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_PDF4LHC15", &GenWeight_LHE_0p5muF_2muR_PDF4LHC15);
        tree->Branch("GenWeight_LHE_0p5muF_CT14", &GenWeight_LHE_0p5muF_CT14);
        tree->Branch("GenWeight_LHE_0p5muF_MMHT", &GenWeight_LHE_0p5muF_MMHT);
        tree->Branch("GenWeight_LHE_0p5muF_PDF4LHC15", &GenWeight_LHE_0p5muF_PDF4LHC15);
        tree->Branch("GenWeight_LHE_0p5muR_CT14", &GenWeight_LHE_0p5muR_CT14);
        tree->Branch("GenWeight_LHE_0p5muR_MMHT", &GenWeight_LHE_0p5muR_MMHT);
        tree->Branch("GenWeight_LHE_0p5muR_PDF4LHC15", &GenWeight_LHE_0p5muR_PDF4LHC15);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_CT14", &GenWeight_LHE_2muF_0p5muR_CT14);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_MMHT", &GenWeight_LHE_2muF_0p5muR_MMHT);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_PDF4LHC15", &GenWeight_LHE_2muF_0p5muR_PDF4LHC15);
        tree->Branch("GenWeight_LHE_2muF_2muR_CT14", &GenWeight_LHE_2muF_2muR_CT14);
        tree->Branch("GenWeight_LHE_2muF_2muR_MMHT", &GenWeight_LHE_2muF_2muR_MMHT);
        tree->Branch("GenWeight_LHE_2muF_2muR_PDF4LHC15", &GenWeight_LHE_2muF_2muR_PDF4LHC15);
        tree->Branch("GenWeight_LHE_2muF_CT14", &GenWeight_LHE_2muF_CT14);
        tree->Branch("GenWeight_LHE_2muF_MMHT", &GenWeight_LHE_2muF_MMHT);
        tree->Branch("GenWeight_LHE_2muF_PDF4LHC15", &GenWeight_LHE_2muF_PDF4LHC15);
        tree->Branch("GenWeight_LHE_2muR_CT14", &GenWeight_LHE_2muR_CT14);
        tree->Branch("GenWeight_LHE_2muR_MMHT", &GenWeight_LHE_2muR_MMHT);
        tree->Branch("GenWeight_LHE_2muR_PDF4LHC15", &GenWeight_LHE_2muR_PDF4LHC15);
        tree->Branch("GenWeight_LHE_PDF_set__eq__13165", &GenWeight_LHE_PDF_set__eq__13165);
        tree->Branch("GenWeight_LHE_PDF_set__eq__25200", &GenWeight_LHE_PDF_set__eq__25200);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260001", &GenWeight_LHE_PDF_set__eq__260001);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260002", &GenWeight_LHE_PDF_set__eq__260002);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260003", &GenWeight_LHE_PDF_set__eq__260003);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260004", &GenWeight_LHE_PDF_set__eq__260004);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260005", &GenWeight_LHE_PDF_set__eq__260005);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260006", &GenWeight_LHE_PDF_set__eq__260006);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260007", &GenWeight_LHE_PDF_set__eq__260007);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260008", &GenWeight_LHE_PDF_set__eq__260008);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260009", &GenWeight_LHE_PDF_set__eq__260009);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260010", &GenWeight_LHE_PDF_set__eq__260010);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260011", &GenWeight_LHE_PDF_set__eq__260011);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260012", &GenWeight_LHE_PDF_set__eq__260012);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260013", &GenWeight_LHE_PDF_set__eq__260013);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260014", &GenWeight_LHE_PDF_set__eq__260014);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260015", &GenWeight_LHE_PDF_set__eq__260015);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260016", &GenWeight_LHE_PDF_set__eq__260016);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260017", &GenWeight_LHE_PDF_set__eq__260017);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260018", &GenWeight_LHE_PDF_set__eq__260018);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260019", &GenWeight_LHE_PDF_set__eq__260019);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260020", &GenWeight_LHE_PDF_set__eq__260020);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260021", &GenWeight_LHE_PDF_set__eq__260021);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260022", &GenWeight_LHE_PDF_set__eq__260022);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260023", &GenWeight_LHE_PDF_set__eq__260023);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260024", &GenWeight_LHE_PDF_set__eq__260024);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260025", &GenWeight_LHE_PDF_set__eq__260025);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260026", &GenWeight_LHE_PDF_set__eq__260026);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260027", &GenWeight_LHE_PDF_set__eq__260027);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260028", &GenWeight_LHE_PDF_set__eq__260028);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260029", &GenWeight_LHE_PDF_set__eq__260029);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260030", &GenWeight_LHE_PDF_set__eq__260030);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260031", &GenWeight_LHE_PDF_set__eq__260031);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260032", &GenWeight_LHE_PDF_set__eq__260032);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260033", &GenWeight_LHE_PDF_set__eq__260033);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260034", &GenWeight_LHE_PDF_set__eq__260034);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260035", &GenWeight_LHE_PDF_set__eq__260035);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260036", &GenWeight_LHE_PDF_set__eq__260036);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260037", &GenWeight_LHE_PDF_set__eq__260037);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260038", &GenWeight_LHE_PDF_set__eq__260038);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260039", &GenWeight_LHE_PDF_set__eq__260039);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260040", &GenWeight_LHE_PDF_set__eq__260040);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260041", &GenWeight_LHE_PDF_set__eq__260041);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260042", &GenWeight_LHE_PDF_set__eq__260042);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260043", &GenWeight_LHE_PDF_set__eq__260043);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260044", &GenWeight_LHE_PDF_set__eq__260044);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260045", &GenWeight_LHE_PDF_set__eq__260045);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260046", &GenWeight_LHE_PDF_set__eq__260046);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260047", &GenWeight_LHE_PDF_set__eq__260047);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260048", &GenWeight_LHE_PDF_set__eq__260048);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260049", &GenWeight_LHE_PDF_set__eq__260049);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260050", &GenWeight_LHE_PDF_set__eq__260050);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260051", &GenWeight_LHE_PDF_set__eq__260051);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260052", &GenWeight_LHE_PDF_set__eq__260052);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260053", &GenWeight_LHE_PDF_set__eq__260053);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260054", &GenWeight_LHE_PDF_set__eq__260054);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260055", &GenWeight_LHE_PDF_set__eq__260055);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260056", &GenWeight_LHE_PDF_set__eq__260056);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260057", &GenWeight_LHE_PDF_set__eq__260057);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260058", &GenWeight_LHE_PDF_set__eq__260058);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260059", &GenWeight_LHE_PDF_set__eq__260059);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260060", &GenWeight_LHE_PDF_set__eq__260060);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260061", &GenWeight_LHE_PDF_set__eq__260061);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260062", &GenWeight_LHE_PDF_set__eq__260062);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260063", &GenWeight_LHE_PDF_set__eq__260063);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260064", &GenWeight_LHE_PDF_set__eq__260064);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260065", &GenWeight_LHE_PDF_set__eq__260065);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260066", &GenWeight_LHE_PDF_set__eq__260066);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260067", &GenWeight_LHE_PDF_set__eq__260067);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260068", &GenWeight_LHE_PDF_set__eq__260068);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260069", &GenWeight_LHE_PDF_set__eq__260069);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260070", &GenWeight_LHE_PDF_set__eq__260070);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260071", &GenWeight_LHE_PDF_set__eq__260071);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260072", &GenWeight_LHE_PDF_set__eq__260072);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260073", &GenWeight_LHE_PDF_set__eq__260073);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260074", &GenWeight_LHE_PDF_set__eq__260074);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260075", &GenWeight_LHE_PDF_set__eq__260075);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260076", &GenWeight_LHE_PDF_set__eq__260076);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260077", &GenWeight_LHE_PDF_set__eq__260077);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260078", &GenWeight_LHE_PDF_set__eq__260078);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260079", &GenWeight_LHE_PDF_set__eq__260079);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260080", &GenWeight_LHE_PDF_set__eq__260080);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260081", &GenWeight_LHE_PDF_set__eq__260081);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260082", &GenWeight_LHE_PDF_set__eq__260082);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260083", &GenWeight_LHE_PDF_set__eq__260083);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260084", &GenWeight_LHE_PDF_set__eq__260084);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260085", &GenWeight_LHE_PDF_set__eq__260085);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260086", &GenWeight_LHE_PDF_set__eq__260086);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260087", &GenWeight_LHE_PDF_set__eq__260087);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260088", &GenWeight_LHE_PDF_set__eq__260088);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260089", &GenWeight_LHE_PDF_set__eq__260089);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260090", &GenWeight_LHE_PDF_set__eq__260090);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260091", &GenWeight_LHE_PDF_set__eq__260091);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260092", &GenWeight_LHE_PDF_set__eq__260092);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260093", &GenWeight_LHE_PDF_set__eq__260093);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260094", &GenWeight_LHE_PDF_set__eq__260094);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260095", &GenWeight_LHE_PDF_set__eq__260095);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260096", &GenWeight_LHE_PDF_set__eq__260096);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260097", &GenWeight_LHE_PDF_set__eq__260097);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260098", &GenWeight_LHE_PDF_set__eq__260098);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260099", &GenWeight_LHE_PDF_set__eq__260099);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260100", &GenWeight_LHE_PDF_set__eq__260100);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90900", &GenWeight_LHE_PDF_set__eq__90900);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90901", &GenWeight_LHE_PDF_set__eq__90901);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90902", &GenWeight_LHE_PDF_set__eq__90902);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90903", &GenWeight_LHE_PDF_set__eq__90903);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90904", &GenWeight_LHE_PDF_set__eq__90904);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90905", &GenWeight_LHE_PDF_set__eq__90905);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90906", &GenWeight_LHE_PDF_set__eq__90906);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90907", &GenWeight_LHE_PDF_set__eq__90907);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90908", &GenWeight_LHE_PDF_set__eq__90908);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90909", &GenWeight_LHE_PDF_set__eq__90909);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90910", &GenWeight_LHE_PDF_set__eq__90910);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90911", &GenWeight_LHE_PDF_set__eq__90911);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90912", &GenWeight_LHE_PDF_set__eq__90912);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90913", &GenWeight_LHE_PDF_set__eq__90913);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90914", &GenWeight_LHE_PDF_set__eq__90914);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90915", &GenWeight_LHE_PDF_set__eq__90915);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90916", &GenWeight_LHE_PDF_set__eq__90916);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90917", &GenWeight_LHE_PDF_set__eq__90917);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90918", &GenWeight_LHE_PDF_set__eq__90918);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90919", &GenWeight_LHE_PDF_set__eq__90919);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90920", &GenWeight_LHE_PDF_set__eq__90920);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90921", &GenWeight_LHE_PDF_set__eq__90921);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90922", &GenWeight_LHE_PDF_set__eq__90922);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90923", &GenWeight_LHE_PDF_set__eq__90923);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90924", &GenWeight_LHE_PDF_set__eq__90924);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90925", &GenWeight_LHE_PDF_set__eq__90925);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90926", &GenWeight_LHE_PDF_set__eq__90926);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90927", &GenWeight_LHE_PDF_set__eq__90927);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90928", &GenWeight_LHE_PDF_set__eq__90928);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90929", &GenWeight_LHE_PDF_set__eq__90929);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90930", &GenWeight_LHE_PDF_set__eq__90930);
        tree->Branch("GenWeight_LHE_Var3cDown", &GenWeight_LHE_Var3cDown);
        tree->Branch("GenWeight_LHE_Var3cUp", &GenWeight_LHE_Var3cUp);
        tree->Branch("GenWeight_LHE_hardHi", &GenWeight_LHE_hardHi);
        tree->Branch("GenWeight_LHE_hardLo", &GenWeight_LHE_hardLo);
        tree->Branch("GenWeight_LHE_isrPDFminus", &GenWeight_LHE_isrPDFminus);
        tree->Branch("GenWeight_LHE_isrPDFplus", &GenWeight_LHE_isrPDFplus);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_0p5", &GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_0p5);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_2p0", &GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_2p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_0p625_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p625_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_0p75_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p75_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_0p875_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p875_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p5", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p5);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p625", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p625);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p75", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p75);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p875", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p875);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p25", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p25);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p5", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p5);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p75", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p75);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_2p0", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_2p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p25_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_1p25_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p5_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_1p5_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_1p75_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_1p75_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_0p5", &GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_0p5);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_1p0);
        tree->Branch("GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_2p0", &GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_2p0);
        tree->Branch("GenWeight_LHE_muR__eq__0p50_muF__eq__0p50", &GenWeight_LHE_muR__eq__0p50_muF__eq__0p50);
        tree->Branch("GenWeight_LHE_muR__eq__0p50_muF__eq__1p00", &GenWeight_LHE_muR__eq__0p50_muF__eq__1p00);
        tree->Branch("GenWeight_LHE_muR__eq__0p50_muF__eq__2p00", &GenWeight_LHE_muR__eq__0p50_muF__eq__2p00);
        tree->Branch("GenWeight_LHE_muR__eq__1p00_muF__eq__0p50", &GenWeight_LHE_muR__eq__1p00_muF__eq__0p50);
        tree->Branch("GenWeight_LHE_muR__eq__1p00_muF__eq__2p00", &GenWeight_LHE_muR__eq__1p00_muF__eq__2p00);
        tree->Branch("GenWeight_LHE_muR__eq__2p00_muF__eq__0p50", &GenWeight_LHE_muR__eq__2p00_muF__eq__0p50);
        tree->Branch("GenWeight_LHE_muR__eq__2p00_muF__eq__1p00", &GenWeight_LHE_muR__eq__2p00_muF__eq__1p00);
        tree->Branch("GenWeight_LHE_muR__eq__2p00_muF__eq__2p00", &GenWeight_LHE_muR__eq__2p00_muF__eq__2p00);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muF_0p5muR_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0117", &GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0119", &GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muF_2muR_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0117", &GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0119", &GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_0p5muF_2muR_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muF_2muR_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_0p5muF_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muF_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_0p5muF_NNPDF_NLO_0117", &GenWeight_LHE_0p5muF_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_0p5muF_NNPDF_NLO_0119", &GenWeight_LHE_0p5muF_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_0p5muF_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muF_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_0p5muR_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muR_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_0p5muR_NNPDF_0117", &GenWeight_LHE_0p5muR_NNPDF_0117);
        tree->Branch("GenWeight_LHE_0p5muR_NNPDF_NLO_0119", &GenWeight_LHE_0p5muR_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_0p5muR_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muR_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_NNPDF31_NLO_0118", &GenWeight_LHE_2muF_0p5muR_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0117", &GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0119", &GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_2muF_0p5muR_PDF4LHC15_NLO_30", &GenWeight_LHE_2muF_0p5muR_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_2muF_2muR_NNPDF31_NLO_0118", &GenWeight_LHE_2muF_2muR_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_2muF_2muR_NNPDF_NLO_0117", &GenWeight_LHE_2muF_2muR_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_2muF_2muR_NNPDF_NLO_0119", &GenWeight_LHE_2muF_2muR_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_2muF_2muR_PDF4LHC15_NLO_30", &GenWeight_LHE_2muF_2muR_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_2muF_NNPDF31_NLO_0118", &GenWeight_LHE_2muF_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_2muF_NNPDF_NLO_0117", &GenWeight_LHE_2muF_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_2muF_NNPDF_NLO_0119", &GenWeight_LHE_2muF_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_2muF_PDF4LHC15_NLO_30", &GenWeight_LHE_2muF_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_2muR_NNPDF31_NLO_0118", &GenWeight_LHE_2muR_NNPDF31_NLO_0118);
        tree->Branch("GenWeight_LHE_2muR_NNPDF_NLO_0117", &GenWeight_LHE_2muR_NNPDF_NLO_0117);
        tree->Branch("GenWeight_LHE_2muR_NNPDF_NLO_0119", &GenWeight_LHE_2muR_NNPDF_NLO_0119);
        tree->Branch("GenWeight_LHE_2muR_PDF4LHC15_NLO_30", &GenWeight_LHE_2muR_PDF4LHC15_NLO_30);
        tree->Branch("GenWeight_LHE_PDF_set__eq__265000", &GenWeight_LHE_PDF_set__eq__265000);
        tree->Branch("GenWeight_LHE_PDF_set__eq__266000", &GenWeight_LHE_PDF_set__eq__266000);
        tree->Branch("GenWeight_LHE_PDF_set__eq__303400", &GenWeight_LHE_PDF_set__eq__303400);
        tree->Branch("GenWeight_LHE_muR__eq__0p5_muF__eq__0p5", &GenWeight_LHE_muR__eq__0p5_muF__eq__0p5);
        tree->Branch("GenWeight_LHE_muR__eq__0p5_muF__eq__1p0", &GenWeight_LHE_muR__eq__0p5_muF__eq__1p0);
        tree->Branch("GenWeight_LHE_muR__eq__0p5_muF__eq__2p0", &GenWeight_LHE_muR__eq__0p5_muF__eq__2p0);
        tree->Branch("GenWeight_LHE_muR__eq__1p0_muF__eq__0p5", &GenWeight_LHE_muR__eq__1p0_muF__eq__0p5);
        tree->Branch("GenWeight_LHE_muR__eq__1p0_muF__eq__2p0", &GenWeight_LHE_muR__eq__1p0_muF__eq__2p0);
        tree->Branch("GenWeight_LHE_muR__eq__2p0_muF__eq__0p5", &GenWeight_LHE_muR__eq__2p0_muF__eq__0p5);
        tree->Branch("GenWeight_LHE_muR__eq__2p0_muF__eq__1p0", &GenWeight_LHE_muR__eq__2p0_muF__eq__1p0);
        tree->Branch("GenWeight_LHE_muR__eq__2p0_muF__eq__2p0", &GenWeight_LHE_muR__eq__2p0_muF__eq__2p0);
        tree->Branch("GenWeight_LHE_MEWeight", &GenWeight_LHE_MEWeight);
        tree->Branch("GenWeight_LHE_MUR0p5_MUF0p5_PDF261000", &GenWeight_LHE_MUR0p5_MUF0p5_PDF261000);
        tree->Branch("GenWeight_LHE_MUR0p5_MUF1_PDF261000", &GenWeight_LHE_MUR0p5_MUF1_PDF261000);
        tree->Branch("GenWeight_LHE_MUR1_MUF0p5_PDF261000", &GenWeight_LHE_MUR1_MUF0p5_PDF261000);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF13000", &GenWeight_LHE_MUR1_MUF1_PDF13000);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF25300", &GenWeight_LHE_MUR1_MUF1_PDF25300);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261000", &GenWeight_LHE_MUR1_MUF1_PDF261000);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261001", &GenWeight_LHE_MUR1_MUF1_PDF261001);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261002", &GenWeight_LHE_MUR1_MUF1_PDF261002);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261003", &GenWeight_LHE_MUR1_MUF1_PDF261003);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261004", &GenWeight_LHE_MUR1_MUF1_PDF261004);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261005", &GenWeight_LHE_MUR1_MUF1_PDF261005);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261006", &GenWeight_LHE_MUR1_MUF1_PDF261006);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261007", &GenWeight_LHE_MUR1_MUF1_PDF261007);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261008", &GenWeight_LHE_MUR1_MUF1_PDF261008);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261009", &GenWeight_LHE_MUR1_MUF1_PDF261009);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261010", &GenWeight_LHE_MUR1_MUF1_PDF261010);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261011", &GenWeight_LHE_MUR1_MUF1_PDF261011);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261012", &GenWeight_LHE_MUR1_MUF1_PDF261012);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261013", &GenWeight_LHE_MUR1_MUF1_PDF261013);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261014", &GenWeight_LHE_MUR1_MUF1_PDF261014);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261015", &GenWeight_LHE_MUR1_MUF1_PDF261015);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261016", &GenWeight_LHE_MUR1_MUF1_PDF261016);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261017", &GenWeight_LHE_MUR1_MUF1_PDF261017);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261018", &GenWeight_LHE_MUR1_MUF1_PDF261018);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261019", &GenWeight_LHE_MUR1_MUF1_PDF261019);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261020", &GenWeight_LHE_MUR1_MUF1_PDF261020);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261021", &GenWeight_LHE_MUR1_MUF1_PDF261021);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261022", &GenWeight_LHE_MUR1_MUF1_PDF261022);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261023", &GenWeight_LHE_MUR1_MUF1_PDF261023);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261024", &GenWeight_LHE_MUR1_MUF1_PDF261024);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261025", &GenWeight_LHE_MUR1_MUF1_PDF261025);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261026", &GenWeight_LHE_MUR1_MUF1_PDF261026);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261027", &GenWeight_LHE_MUR1_MUF1_PDF261027);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261028", &GenWeight_LHE_MUR1_MUF1_PDF261028);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261029", &GenWeight_LHE_MUR1_MUF1_PDF261029);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261030", &GenWeight_LHE_MUR1_MUF1_PDF261030);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261031", &GenWeight_LHE_MUR1_MUF1_PDF261031);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261032", &GenWeight_LHE_MUR1_MUF1_PDF261032);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261033", &GenWeight_LHE_MUR1_MUF1_PDF261033);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261034", &GenWeight_LHE_MUR1_MUF1_PDF261034);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261035", &GenWeight_LHE_MUR1_MUF1_PDF261035);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261036", &GenWeight_LHE_MUR1_MUF1_PDF261036);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261037", &GenWeight_LHE_MUR1_MUF1_PDF261037);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261038", &GenWeight_LHE_MUR1_MUF1_PDF261038);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261039", &GenWeight_LHE_MUR1_MUF1_PDF261039);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261040", &GenWeight_LHE_MUR1_MUF1_PDF261040);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261041", &GenWeight_LHE_MUR1_MUF1_PDF261041);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261042", &GenWeight_LHE_MUR1_MUF1_PDF261042);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261043", &GenWeight_LHE_MUR1_MUF1_PDF261043);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261044", &GenWeight_LHE_MUR1_MUF1_PDF261044);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261045", &GenWeight_LHE_MUR1_MUF1_PDF261045);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261046", &GenWeight_LHE_MUR1_MUF1_PDF261046);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261047", &GenWeight_LHE_MUR1_MUF1_PDF261047);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261048", &GenWeight_LHE_MUR1_MUF1_PDF261048);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261049", &GenWeight_LHE_MUR1_MUF1_PDF261049);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261050", &GenWeight_LHE_MUR1_MUF1_PDF261050);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261051", &GenWeight_LHE_MUR1_MUF1_PDF261051);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261052", &GenWeight_LHE_MUR1_MUF1_PDF261052);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261053", &GenWeight_LHE_MUR1_MUF1_PDF261053);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261054", &GenWeight_LHE_MUR1_MUF1_PDF261054);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261055", &GenWeight_LHE_MUR1_MUF1_PDF261055);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261056", &GenWeight_LHE_MUR1_MUF1_PDF261056);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261057", &GenWeight_LHE_MUR1_MUF1_PDF261057);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261058", &GenWeight_LHE_MUR1_MUF1_PDF261058);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261059", &GenWeight_LHE_MUR1_MUF1_PDF261059);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261060", &GenWeight_LHE_MUR1_MUF1_PDF261060);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261061", &GenWeight_LHE_MUR1_MUF1_PDF261061);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261062", &GenWeight_LHE_MUR1_MUF1_PDF261062);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261063", &GenWeight_LHE_MUR1_MUF1_PDF261063);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261064", &GenWeight_LHE_MUR1_MUF1_PDF261064);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261065", &GenWeight_LHE_MUR1_MUF1_PDF261065);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261066", &GenWeight_LHE_MUR1_MUF1_PDF261066);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261067", &GenWeight_LHE_MUR1_MUF1_PDF261067);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261068", &GenWeight_LHE_MUR1_MUF1_PDF261068);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261069", &GenWeight_LHE_MUR1_MUF1_PDF261069);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261070", &GenWeight_LHE_MUR1_MUF1_PDF261070);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261071", &GenWeight_LHE_MUR1_MUF1_PDF261071);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261072", &GenWeight_LHE_MUR1_MUF1_PDF261072);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261073", &GenWeight_LHE_MUR1_MUF1_PDF261073);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261074", &GenWeight_LHE_MUR1_MUF1_PDF261074);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261075", &GenWeight_LHE_MUR1_MUF1_PDF261075);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261076", &GenWeight_LHE_MUR1_MUF1_PDF261076);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261077", &GenWeight_LHE_MUR1_MUF1_PDF261077);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261078", &GenWeight_LHE_MUR1_MUF1_PDF261078);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261079", &GenWeight_LHE_MUR1_MUF1_PDF261079);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261080", &GenWeight_LHE_MUR1_MUF1_PDF261080);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261081", &GenWeight_LHE_MUR1_MUF1_PDF261081);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261082", &GenWeight_LHE_MUR1_MUF1_PDF261082);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261083", &GenWeight_LHE_MUR1_MUF1_PDF261083);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261084", &GenWeight_LHE_MUR1_MUF1_PDF261084);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261085", &GenWeight_LHE_MUR1_MUF1_PDF261085);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261086", &GenWeight_LHE_MUR1_MUF1_PDF261086);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261087", &GenWeight_LHE_MUR1_MUF1_PDF261087);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261088", &GenWeight_LHE_MUR1_MUF1_PDF261088);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261089", &GenWeight_LHE_MUR1_MUF1_PDF261089);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261090", &GenWeight_LHE_MUR1_MUF1_PDF261090);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261091", &GenWeight_LHE_MUR1_MUF1_PDF261091);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261092", &GenWeight_LHE_MUR1_MUF1_PDF261092);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261093", &GenWeight_LHE_MUR1_MUF1_PDF261093);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261094", &GenWeight_LHE_MUR1_MUF1_PDF261094);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261095", &GenWeight_LHE_MUR1_MUF1_PDF261095);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261096", &GenWeight_LHE_MUR1_MUF1_PDF261096);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261097", &GenWeight_LHE_MUR1_MUF1_PDF261097);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261098", &GenWeight_LHE_MUR1_MUF1_PDF261098);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261099", &GenWeight_LHE_MUR1_MUF1_PDF261099);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF261100", &GenWeight_LHE_MUR1_MUF1_PDF261100);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF269000", &GenWeight_LHE_MUR1_MUF1_PDF269000);
        tree->Branch("GenWeight_LHE_MUR1_MUF1_PDF270000", &GenWeight_LHE_MUR1_MUF1_PDF270000);
        tree->Branch("GenWeight_LHE_MUR1_MUF2_PDF261000", &GenWeight_LHE_MUR1_MUF2_PDF261000);
        tree->Branch("GenWeight_LHE_MUR2_MUF1_PDF261000", &GenWeight_LHE_MUR2_MUF1_PDF261000);
        tree->Branch("GenWeight_LHE_MUR2_MUF2_PDF261000", &GenWeight_LHE_MUR2_MUF2_PDF261000);
        tree->Branch("GenWeight_LHE_NTrials", &GenWeight_LHE_NTrials);
        tree->Branch("GenWeight_LHE_WeightNormalisation", &GenWeight_LHE_WeightNormalisation);
        tree->Branch("GenWeight_LHE_PDF_set__eq__11068", &GenWeight_LHE_PDF_set__eq__11068);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90400", &GenWeight_LHE_PDF_set__eq__90400);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90401", &GenWeight_LHE_PDF_set__eq__90401);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90402", &GenWeight_LHE_PDF_set__eq__90402);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90403", &GenWeight_LHE_PDF_set__eq__90403);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90404", &GenWeight_LHE_PDF_set__eq__90404);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90405", &GenWeight_LHE_PDF_set__eq__90405);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90406", &GenWeight_LHE_PDF_set__eq__90406);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90407", &GenWeight_LHE_PDF_set__eq__90407);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90408", &GenWeight_LHE_PDF_set__eq__90408);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90409", &GenWeight_LHE_PDF_set__eq__90409);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90410", &GenWeight_LHE_PDF_set__eq__90410);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90411", &GenWeight_LHE_PDF_set__eq__90411);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90412", &GenWeight_LHE_PDF_set__eq__90412);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90413", &GenWeight_LHE_PDF_set__eq__90413);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90414", &GenWeight_LHE_PDF_set__eq__90414);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90415", &GenWeight_LHE_PDF_set__eq__90415);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90416", &GenWeight_LHE_PDF_set__eq__90416);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90417", &GenWeight_LHE_PDF_set__eq__90417);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90418", &GenWeight_LHE_PDF_set__eq__90418);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90419", &GenWeight_LHE_PDF_set__eq__90419);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90420", &GenWeight_LHE_PDF_set__eq__90420);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90421", &GenWeight_LHE_PDF_set__eq__90421);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90422", &GenWeight_LHE_PDF_set__eq__90422);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90423", &GenWeight_LHE_PDF_set__eq__90423);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90424", &GenWeight_LHE_PDF_set__eq__90424);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90425", &GenWeight_LHE_PDF_set__eq__90425);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90426", &GenWeight_LHE_PDF_set__eq__90426);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90427", &GenWeight_LHE_PDF_set__eq__90427);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90428", &GenWeight_LHE_PDF_set__eq__90428);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90429", &GenWeight_LHE_PDF_set__eq__90429);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90430", &GenWeight_LHE_PDF_set__eq__90430);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90431", &GenWeight_LHE_PDF_set__eq__90431);
        tree->Branch("GenWeight_LHE_PDF_set__eq__90432", &GenWeight_LHE_PDF_set__eq__90432);
        tree->Branch("GenWeight_LHE_PDF_set__eq__25300", &GenWeight_LHE_PDF_set__eq__25300);
        tree->Branch("GenWeight_LHE_PDF_set__eq__11000", &GenWeight_LHE_PDF_set__eq__11000);
        tree->Branch("GenWeight_LHE_PDF_set__eq__13100", &GenWeight_LHE_PDF_set__eq__13100);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260000", &GenWeight_LHE_PDF_set__eq__260000);
        tree->Branch("GenWeight_LHE_PDF_set__eq__91400", &GenWeight_LHE_PDF_set__eq__91400);
        tree->Branch("GenWeight_LHE_mtinf", &GenWeight_LHE_mtinf);
        tree->Branch("GenWeight_LHE_mtmb", &GenWeight_LHE_mtmb);
        tree->Branch("GenWeight_LHE_mtmb-bminlo", &GenWeight_LHE_mtmb_bminlo);
        tree->Branch("GenWeight_LHE_nnlops-bminlo", &GenWeight_LHE_nnlops_bminlo);
        tree->Branch("GenWeight_LHE_nnlops-mtinf", &GenWeight_LHE_nnlops_mtinf);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgDnDn", &GenWeight_LHE_nnlops_nnloDn_pwgDnDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgDnNom", &GenWeight_LHE_nnlops_nnloDn_pwgDnNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgDnUp", &GenWeight_LHE_nnlops_nnloDn_pwgDnUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgNomDn", &GenWeight_LHE_nnlops_nnloDn_pwgNomDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgNomNom", &GenWeight_LHE_nnlops_nnloDn_pwgNomNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgNomUp", &GenWeight_LHE_nnlops_nnloDn_pwgNomUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgUpDn", &GenWeight_LHE_nnlops_nnloDn_pwgUpDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgUpNom", &GenWeight_LHE_nnlops_nnloDn_pwgUpNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloDn-pwgUpUp", &GenWeight_LHE_nnlops_nnloDn_pwgUpUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgDnDn", &GenWeight_LHE_nnlops_nnloNom_pwgDnDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgDnNom", &GenWeight_LHE_nnlops_nnloNom_pwgDnNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgDnUp", &GenWeight_LHE_nnlops_nnloNom_pwgDnUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgNomDn", &GenWeight_LHE_nnlops_nnloNom_pwgNomDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgNomUp", &GenWeight_LHE_nnlops_nnloNom_pwgNomUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgUpDn", &GenWeight_LHE_nnlops_nnloNom_pwgUpDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgUpNom", &GenWeight_LHE_nnlops_nnloNom_pwgUpNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloNom-pwgUpUp", &GenWeight_LHE_nnlops_nnloNom_pwgUpUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgDnDn", &GenWeight_LHE_nnlops_nnloUp_pwgDnDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgDnNom", &GenWeight_LHE_nnlops_nnloUp_pwgDnNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgDnUp", &GenWeight_LHE_nnlops_nnloUp_pwgDnUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgNomDn", &GenWeight_LHE_nnlops_nnloUp_pwgNomDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgNomNom", &GenWeight_LHE_nnlops_nnloUp_pwgNomNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgNomUp", &GenWeight_LHE_nnlops_nnloUp_pwgNomUp);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgUpDn", &GenWeight_LHE_nnlops_nnloUp_pwgUpDn);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgUpNom", &GenWeight_LHE_nnlops_nnloUp_pwgUpNom);
        tree->Branch("GenWeight_LHE_nnlops-nnloUp-pwgUpUp", &GenWeight_LHE_nnlops_nnloUp_pwgUpUp);
        tree->Branch("GenWeight_LHE_nnlops-nominal", &GenWeight_LHE_nnlops_nominal);
        tree->Branch("GenWeight_LHE_nnlops-nominal-pdflhc", &GenWeight_LHE_nnlops_nominal_pdflhc);
        tree->Branch("GenWeight_LHE_PDF_set__eq__13191", &GenWeight_LHE_PDF_set__eq__13191);
        tree->Branch("GenWeight_LHE_PDF_set__eq__25410", &GenWeight_LHE_PDF_set__eq__25410);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260401", &GenWeight_LHE_PDF_set__eq__260401);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260402", &GenWeight_LHE_PDF_set__eq__260402);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260403", &GenWeight_LHE_PDF_set__eq__260403);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260404", &GenWeight_LHE_PDF_set__eq__260404);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260405", &GenWeight_LHE_PDF_set__eq__260405);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260406", &GenWeight_LHE_PDF_set__eq__260406);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260407", &GenWeight_LHE_PDF_set__eq__260407);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260408", &GenWeight_LHE_PDF_set__eq__260408);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260409", &GenWeight_LHE_PDF_set__eq__260409);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260410", &GenWeight_LHE_PDF_set__eq__260410);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260411", &GenWeight_LHE_PDF_set__eq__260411);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260412", &GenWeight_LHE_PDF_set__eq__260412);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260413", &GenWeight_LHE_PDF_set__eq__260413);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260414", &GenWeight_LHE_PDF_set__eq__260414);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260415", &GenWeight_LHE_PDF_set__eq__260415);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260416", &GenWeight_LHE_PDF_set__eq__260416);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260417", &GenWeight_LHE_PDF_set__eq__260417);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260418", &GenWeight_LHE_PDF_set__eq__260418);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260419", &GenWeight_LHE_PDF_set__eq__260419);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260420", &GenWeight_LHE_PDF_set__eq__260420);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260421", &GenWeight_LHE_PDF_set__eq__260421);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260422", &GenWeight_LHE_PDF_set__eq__260422);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260423", &GenWeight_LHE_PDF_set__eq__260423);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260424", &GenWeight_LHE_PDF_set__eq__260424);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260425", &GenWeight_LHE_PDF_set__eq__260425);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260426", &GenWeight_LHE_PDF_set__eq__260426);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260427", &GenWeight_LHE_PDF_set__eq__260427);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260428", &GenWeight_LHE_PDF_set__eq__260428);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260429", &GenWeight_LHE_PDF_set__eq__260429);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260430", &GenWeight_LHE_PDF_set__eq__260430);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260431", &GenWeight_LHE_PDF_set__eq__260431);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260432", &GenWeight_LHE_PDF_set__eq__260432);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260433", &GenWeight_LHE_PDF_set__eq__260433);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260434", &GenWeight_LHE_PDF_set__eq__260434);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260435", &GenWeight_LHE_PDF_set__eq__260435);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260436", &GenWeight_LHE_PDF_set__eq__260436);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260437", &GenWeight_LHE_PDF_set__eq__260437);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260438", &GenWeight_LHE_PDF_set__eq__260438);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260439", &GenWeight_LHE_PDF_set__eq__260439);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260440", &GenWeight_LHE_PDF_set__eq__260440);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260441", &GenWeight_LHE_PDF_set__eq__260441);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260442", &GenWeight_LHE_PDF_set__eq__260442);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260443", &GenWeight_LHE_PDF_set__eq__260443);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260444", &GenWeight_LHE_PDF_set__eq__260444);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260445", &GenWeight_LHE_PDF_set__eq__260445);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260446", &GenWeight_LHE_PDF_set__eq__260446);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260447", &GenWeight_LHE_PDF_set__eq__260447);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260448", &GenWeight_LHE_PDF_set__eq__260448);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260449", &GenWeight_LHE_PDF_set__eq__260449);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260450", &GenWeight_LHE_PDF_set__eq__260450);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260451", &GenWeight_LHE_PDF_set__eq__260451);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260452", &GenWeight_LHE_PDF_set__eq__260452);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260453", &GenWeight_LHE_PDF_set__eq__260453);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260454", &GenWeight_LHE_PDF_set__eq__260454);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260455", &GenWeight_LHE_PDF_set__eq__260455);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260456", &GenWeight_LHE_PDF_set__eq__260456);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260457", &GenWeight_LHE_PDF_set__eq__260457);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260458", &GenWeight_LHE_PDF_set__eq__260458);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260459", &GenWeight_LHE_PDF_set__eq__260459);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260460", &GenWeight_LHE_PDF_set__eq__260460);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260461", &GenWeight_LHE_PDF_set__eq__260461);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260462", &GenWeight_LHE_PDF_set__eq__260462);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260463", &GenWeight_LHE_PDF_set__eq__260463);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260464", &GenWeight_LHE_PDF_set__eq__260464);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260465", &GenWeight_LHE_PDF_set__eq__260465);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260466", &GenWeight_LHE_PDF_set__eq__260466);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260467", &GenWeight_LHE_PDF_set__eq__260467);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260468", &GenWeight_LHE_PDF_set__eq__260468);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260469", &GenWeight_LHE_PDF_set__eq__260469);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260470", &GenWeight_LHE_PDF_set__eq__260470);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260471", &GenWeight_LHE_PDF_set__eq__260471);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260472", &GenWeight_LHE_PDF_set__eq__260472);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260473", &GenWeight_LHE_PDF_set__eq__260473);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260474", &GenWeight_LHE_PDF_set__eq__260474);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260475", &GenWeight_LHE_PDF_set__eq__260475);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260476", &GenWeight_LHE_PDF_set__eq__260476);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260477", &GenWeight_LHE_PDF_set__eq__260477);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260478", &GenWeight_LHE_PDF_set__eq__260478);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260479", &GenWeight_LHE_PDF_set__eq__260479);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260480", &GenWeight_LHE_PDF_set__eq__260480);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260481", &GenWeight_LHE_PDF_set__eq__260481);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260482", &GenWeight_LHE_PDF_set__eq__260482);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260483", &GenWeight_LHE_PDF_set__eq__260483);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260484", &GenWeight_LHE_PDF_set__eq__260484);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260485", &GenWeight_LHE_PDF_set__eq__260485);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260486", &GenWeight_LHE_PDF_set__eq__260486);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260487", &GenWeight_LHE_PDF_set__eq__260487);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260488", &GenWeight_LHE_PDF_set__eq__260488);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260489", &GenWeight_LHE_PDF_set__eq__260489);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260490", &GenWeight_LHE_PDF_set__eq__260490);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260491", &GenWeight_LHE_PDF_set__eq__260491);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260492", &GenWeight_LHE_PDF_set__eq__260492);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260493", &GenWeight_LHE_PDF_set__eq__260493);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260494", &GenWeight_LHE_PDF_set__eq__260494);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260495", &GenWeight_LHE_PDF_set__eq__260495);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260496", &GenWeight_LHE_PDF_set__eq__260496);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260497", &GenWeight_LHE_PDF_set__eq__260497);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260498", &GenWeight_LHE_PDF_set__eq__260498);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260499", &GenWeight_LHE_PDF_set__eq__260499);
        tree->Branch("GenWeight_LHE_PDF_set__eq__260500", &GenWeight_LHE_PDF_set__eq__260500);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92000", &GenWeight_LHE_PDF_set__eq__92000);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92001", &GenWeight_LHE_PDF_set__eq__92001);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92002", &GenWeight_LHE_PDF_set__eq__92002);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92003", &GenWeight_LHE_PDF_set__eq__92003);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92004", &GenWeight_LHE_PDF_set__eq__92004);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92005", &GenWeight_LHE_PDF_set__eq__92005);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92006", &GenWeight_LHE_PDF_set__eq__92006);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92007", &GenWeight_LHE_PDF_set__eq__92007);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92008", &GenWeight_LHE_PDF_set__eq__92008);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92009", &GenWeight_LHE_PDF_set__eq__92009);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92010", &GenWeight_LHE_PDF_set__eq__92010);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92011", &GenWeight_LHE_PDF_set__eq__92011);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92012", &GenWeight_LHE_PDF_set__eq__92012);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92013", &GenWeight_LHE_PDF_set__eq__92013);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92014", &GenWeight_LHE_PDF_set__eq__92014);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92015", &GenWeight_LHE_PDF_set__eq__92015);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92016", &GenWeight_LHE_PDF_set__eq__92016);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92017", &GenWeight_LHE_PDF_set__eq__92017);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92018", &GenWeight_LHE_PDF_set__eq__92018);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92019", &GenWeight_LHE_PDF_set__eq__92019);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92020", &GenWeight_LHE_PDF_set__eq__92020);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92021", &GenWeight_LHE_PDF_set__eq__92021);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92022", &GenWeight_LHE_PDF_set__eq__92022);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92023", &GenWeight_LHE_PDF_set__eq__92023);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92024", &GenWeight_LHE_PDF_set__eq__92024);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92025", &GenWeight_LHE_PDF_set__eq__92025);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92026", &GenWeight_LHE_PDF_set__eq__92026);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92027", &GenWeight_LHE_PDF_set__eq__92027);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92028", &GenWeight_LHE_PDF_set__eq__92028);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92029", &GenWeight_LHE_PDF_set__eq__92029);
        tree->Branch("GenWeight_LHE_PDF_set__eq__92030", &GenWeight_LHE_PDF_set__eq__92030);

        // systematic weights 
        
        tree->Branch("evt_weight_EleSys_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &evt_weight_EleSys_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        tree->Branch("evt_weight_EleSys_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &evt_weight_EleSys_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        tree->Branch("evt_weight_EleSys_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &evt_weight_EleSys_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        tree->Branch("evt_weight_EleSys_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &evt_weight_EleSys_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        tree->Branch("evt_weight_EleSys_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &evt_weight_EleSys_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        tree->Branch("evt_weight_EleSys_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &evt_weight_EleSys_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        
        tree->Branch("evt_weight_MuoSys_MUON_EFF_ISO_STAT__1down", &evt_weight_MuoSys_MUON_EFF_ISO_STAT__1down);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_ISO_STAT__1up", &evt_weight_MuoSys_MUON_EFF_ISO_STAT__1up);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_ISO_SYS__1down", &evt_weight_MuoSys_MUON_EFF_ISO_SYS__1down);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_ISO_SYS__1up", &evt_weight_MuoSys_MUON_EFF_ISO_SYS__1up);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_RECO_STAT__1down", &evt_weight_MuoSys_MUON_EFF_RECO_STAT__1down);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_RECO_STAT__1up", &evt_weight_MuoSys_MUON_EFF_RECO_STAT__1up);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_RECO_SYS__1down", &evt_weight_MuoSys_MUON_EFF_RECO_SYS__1down);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_RECO_SYS__1up", &evt_weight_MuoSys_MUON_EFF_RECO_SYS__1up);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_TTVA_STAT__1down", &evt_weight_MuoSys_MUON_EFF_TTVA_STAT__1down);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_TTVA_STAT__1up", &evt_weight_MuoSys_MUON_EFF_TTVA_STAT__1up);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_TTVA_SYS__1down", &evt_weight_MuoSys_MUON_EFF_TTVA_SYS__1down);
        tree->Branch("evt_weight_MuoSys_MUON_EFF_TTVA_SYS__1up", &evt_weight_MuoSys_MUON_EFF_TTVA_SYS__1up);


        tree->Branch("evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down", &evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up", &evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down", &evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up", &evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down);
        tree->Branch("evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up", &evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up);


        tree->Branch("evt_weight_JetSys_FT_EFF_B_systematics__1down", &evt_weight_JetSys_FT_EFF_B_systematics__1down);
        tree->Branch("evt_weight_JetSys_FT_EFF_B_systematics__1up", &evt_weight_JetSys_FT_EFF_B_systematics__1up);
        tree->Branch("evt_weight_JetSys_FT_EFF_C_systematics__1down", &evt_weight_JetSys_FT_EFF_C_systematics__1down);
        tree->Branch("evt_weight_JetSys_FT_EFF_C_systematics__1up", &evt_weight_JetSys_FT_EFF_C_systematics__1up);
        tree->Branch("evt_weight_JetSys_FT_EFF_Light_systematics__1down", &evt_weight_JetSys_FT_EFF_Light_systematics__1down);
        tree->Branch("evt_weight_JetSys_FT_EFF_Light_systematics__1up", &evt_weight_JetSys_FT_EFF_Light_systematics__1up);
        tree->Branch("evt_weight_JetSys_FT_EFF_extrapolation__1down", &evt_weight_JetSys_FT_EFF_extrapolation__1down);
        tree->Branch("evt_weight_JetSys_FT_EFF_extrapolation__1up", &evt_weight_JetSys_FT_EFF_extrapolation__1up);
        tree->Branch("evt_weight_JetSys_FT_EFF_extrapolation_from_charm__1down", &evt_weight_JetSys_FT_EFF_extrapolation_from_charm__1down);
        tree->Branch("evt_weight_JetSys_FT_EFF_extrapolation_from_charm__1up", &evt_weight_JetSys_FT_EFF_extrapolation_from_charm__1up);
        tree->Branch("evt_weight_JetSys_JET_JvtEfficiency__1down", &evt_weight_JetSys_JET_JvtEfficiency__1down);
        tree->Branch("evt_weight_JetSys_JET_JvtEfficiency__1up", &evt_weight_JetSys_JET_JvtEfficiency__1up);

        tree->Branch("evt_weight_muSys_PRW_DATASF__1down", &evt_weight_muSys_PRW_DATASF__1down);
        tree->Branch("evt_weight_muSys_PRW_DATASF__1up", &evt_weight_muSys_PRW_DATASF__1up);

        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
        tree->Branch("evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up", &evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);

        tree->Branch("evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
        tree->Branch("evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
        tree->Branch("evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
        tree->Branch("evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);




    }


    tree->Branch("mcChannelNumber", &mcChannelNumber);
    tree->Branch("runNumber", &runNumber);
    tree->Branch("Vtx_n", &Vtx_n);
    tree->Branch("mu_density", &mu_density);
    tree->Branch("evt_weight", &evt_weight);
    tree->Branch("evt_weight_noTrigWeight", &evt_weight_noTrigWeight);
    tree->Branch("evt_weight_noPRW", &evt_weight_noPRW);
    tree->Branch("evt_weight_noFF", &evt_weight_noFF);
    tree->Branch("evt_weight_ditauMET", &evt_weight_ditauMET);
    tree->Branch("evt_weight_asymditau", &evt_weight_asymditau);
    tree->Branch("evt_weight_singleMU", &evt_weight_singleMU);

    
    
    
    tree->Branch("ff_weight", &ff_weight);
    tree->Branch("ff_weight_sysUP", &ff_weight_sysUP);
    tree->Branch("ff_weight_sysDOWN", &ff_weight_sysDOWN);

    tree->Branch("ff_weight_sysVarTau1Pt",   &ff_weight_sysVarTau1Pt);
    tree->Branch("ff_weight_sysVarTau2Pt",   &ff_weight_sysVarTau2Pt);
    tree->Branch("ff_weight_sysVarOS",           &ff_weight_sysVarOS);
    tree->Branch("ff_weight_sysVarStatUP",   &ff_weight_sysVarStatUP);
    tree->Branch("ff_weight_sysVarStatDOWN", &ff_weight_sysVarStatDOWN);

    tree->Branch("total_sum_weights", &total_sum_weights);
    tree->Branch("eventNumber", &eventNumber);

    tree->Branch("MC16SubCampaign", &MC16SubCampaign);
    tree->Branch("luminosity",          &luminosity         );
    tree->Branch("xsec",                        &xsec                       );
    tree->Branch("filtereff",               &filtereff          );
    tree->Branch("kfactor",                 &kfactor                );
    tree->Branch("processedevts",       &processedevts  );
    tree->Branch("sumw",                        &sumw                       );
    tree->Branch("sumw2",                       &sumw2                  );

    tree->Branch("actualInteractionsPerCrossing", &actualInteractionsPerCrossing);
    tree->Branch("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
    tree->Branch("corr_avgIntPerX", &corr_avgIntPerX);
    
    tree->Branch("SUSYFinalState", &SUSYFinalState);

    tree->Branch("n_signaltaus", &n_signaltaus);
    tree->Branch("n_BaseTau", &n_BaseTau);

    tree->Branch("n_antitaus", &n_antitaus);
    tree->Branch("atLeastTwoMediumTaus", &atLeastTwoMediumTaus);
    tree->Branch("atLeastOneTightTau", &atLeastOneTightTau);
    tree->Branch("atLeastTwoTightTaus", &atLeastTwoTightTaus);

    tree->Branch("tau1Pt", &tau1Pt);
    tree->Branch("tau2Pt", &tau2Pt);
    tree->Branch("tau1Phi", &tau1Phi);
    tree->Branch("tau2Phi", &tau2Phi);
    tree->Branch("tau1Eta", &tau1Eta);
    tree->Branch("tau2Eta", &tau2Eta);
    tree->Branch("tau1Signal", &tau1Signal);
    tree->Branch("tau2Signal", &tau2Signal);
    tree->Branch("tau1Charge", &tau1Charge);
    tree->Branch("tau2Charge", &tau2Charge);
    tree->Branch("tau1Mt", &tau1Mt);
    tree->Branch("tau2Mt", &tau2Mt);
    tree->Branch("tau1Quality", &tau1Quality);
    tree->Branch("tau2Quality", &tau2Quality);
    tree->Branch("tau1Width", &tau1Width);
    tree->Branch("tau2Width", &tau2Width);
    //tree->Branch("tau1BDTJetScore", &tau1BDTJetScore);
    //tree->Branch("tau2BDTJetScore", &tau2BDTJetScore);
    //tree->Branch("tau1BDTJetScoreSigTrans", &tau1BDTJetScoreSigTrans);
    //tree->Branch("tau2BDTJetScoreSigTrans", &tau2BDTJetScoreSigTrans);
    //tree->Branch("tau1BDTEleScore", &tau1BDTEleScore);
    //tree->Branch("tau2BDTEleScore", &tau2BDTEleScore);
    //tree->Branch("tau1BDTEleScoreSigTrans", &tau1BDTEleScoreSigTrans);
    //tree->Branch("tau2BDTEleScoreSigTrans", &tau2BDTEleScoreSigTrans);
    //tree->Branch("tau1NProng", &tau1NProng);
    //tree->Branch("tau2NProng", &tau2NProng);
    tree->Branch("tau1TruthMatch", &tau1TruthMatch);
    tree->Branch("tau2TruthMatch", &tau2TruthMatch);
    tree->Branch("tau1TruthType", &tau1TruthType);
    tree->Branch("tau2TruthType", &tau2TruthType);
    tree->Branch("tau1TruthOrigin", &tau1TruthOrigin);
    tree->Branch("tau2TruthOrigin", &tau2TruthOrigin);
    tree->Branch("tau1PartonTruthLabelID", &tau1PartonTruthLabelID);
    tree->Branch("tau2PartonTruthLabelID", &tau2PartonTruthLabelID);
    tree->Branch("tau1ConeTruthLabelID", &tau1ConeTruthLabelID);
    tree->Branch("tau2ConeTruthLabelID", &tau2ConeTruthLabelID);
    tree->Branch("mt2tautau", &mt2tautau);
    tree->Branch("mt2tautau_1", &mt2tautau_1);
    tree->Branch("mt2tautau_20", &mt2tautau_20);
    tree->Branch("mt2tautau_40", &mt2tautau_40);
    tree->Branch("mtsumtautau", &mtsumtautau);
    tree->Branch("minvtautau", &minvtautau);
    tree->Branch("meff", &meff);
    tree->Branch("met_over_sumtautaupt", &met_over_sumtautaupt);
    tree->Branch("dPhitau1MET", &dPhitau1MET);
    tree->Branch("dPhitau2MET", &dPhitau2MET);
    tree->Branch("centrality", &centrality);
    tree->Branch("sumCosDPhi_tauMET", &sumCosDPhi_tauMET);
    tree->Branch("cmt", &cmt);
    //tree->Branch("tauPtSum", &tauPtSum);

    tree->Branch("OS", &OS);

    tree->Branch("n_jets", &n_jets);
    tree->Branch("n_btags", &n_btags);
    tree->Branch("n_elec", &n_SignalElec);
    tree->Branch("n_BaseEle", &n_BaseElec);
    tree->Branch("n_muon", &n_SignalMuon);
    tree->Branch("n_BaseMuo", &n_BaseMuon);

    tree->Branch("jet1Pt", &jet1Pt);
    tree->Branch("jet2Pt", &jet2Pt);
    //tree->Branch("jet1Mt", &jet1Mt);
    //tree->Branch("jet2Mt", &jet2Mt);
    //tree->Branch("dPhijet1jet2", &dPhijet1jet2);
    //tree->Branch("dPhiminjetsMET", &dPhiminjetsMET);
    //tree->Branch("HT", &HT);
    tree->Branch("METSig", &METSig);

    tree->Branch("dEtatautau", &dEtatautau);
    tree->Branch("dPhitautau", &dPhitautau);
    tree->Branch("dRtautau", &dRtautau);

    tree->Branch("MET", &MET_pt);
    tree->Branch("METPhi", &MET_phi);
    tree->Branch("MetTST_Significance", &MetTST_Significance);
    
    tree->Branch("GenWeight", &GenWeight);
    tree->Branch("TauWeight", &TauWeight);
    tree->Branch("muWeight", &muWeight);
    tree->Branch("effXsec", &effXsec);
    
    tree->Branch("data_prescale_weight", &jetTrigger_weight);
    tree->Branch("selected_by_asym_ditau_trigger", &selected_by_asym_ditau_trigger);
    tree->Branch("selected_by_ditau_MET_trigger", &selected_by_ditau_MET_trigger);
    tree->Branch("selected_by_singleMu_trigger", &selected_by_singleMu_trigger);
    tree->Branch("selected_by_asym_ditau_trigger_woPlateau", &selected_by_asym_ditau_trigger_woPlateau);
    tree->Branch("selected_by_ditau_MET_trigger_woPlateau", &selected_by_ditau_MET_trigger_woPlateau);
    
    tree->Branch("pass_asym_ditau_trigger", &pass_asym_ditau_trigger);
    tree->Branch("passMatch_asym_ditau_trigger", &passMatch_asym_ditau_trigger);
    tree->Branch("passPlat_asym_ditau_trigger", &passPlat_asym_ditau_trigger);
    
    tree->Branch("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60);
    tree->Branch("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12);
    tree->Branch("TauWeightTrigHLT_tau35_medium1_tracktwo",                 &TauWeightTrigHLT_tau35_medium1_tracktwo);
    tree->Branch("TauWeightTrigHLT_tau25_medium1_tracktwo",                 &TauWeightTrigHLT_tau25_medium1_tracktwo);

    //tree->Branch("OS_TauMuo", &OS_TauMuo);
    tree->Branch("minvtaumuo", &minvtaumuo);
    tree->Branch("mtsumtaumu", &mtsumtaumu);
    tree->Branch("dPhitaumu", &dPhitaumu);
    tree->Branch("dEtataumu", &dEtataumu);
    tree->Branch("dRtaumu", &dRtaumu);
    tree->Branch("dPhimuMET", &dPhimuMET);
    tree->Branch("mt2taumu", &mt2taumu);
    tree->Branch("meffmu", &meffmu);
    tree->Branch("met_over_sumtaumupt", &met_over_sumtaumupt);
    tree->Branch("mu1Pt", &mu1Pt);
    tree->Branch("mu1Phi", &mu1Phi);
    tree->Branch("mu1Eta", &mu1Eta);
    tree->Branch("mu1Mt", &mu1Mt);
    tree->Branch("mu1Charge", &mu1Charge);
    tree->Branch("mu1Signal", &mu1Signal);
    tree->Branch("tauPt", &tauPt);
    tree->Branch("tau1Mt", &tau1Mt);
    tree->Branch("dPhitauMET", &dPhitauMET);
    tree->Branch("OS_taumu", &OS_taumu);
    
    tree->Branch("n_TruthJets", &n_TruthJets);

    // BDT variables (so can use Muon as effective tau in 1 Mu 1 Tau events) 
    //tree->Branch("efftau1Pt", &efftau1Pt);
    //tree->Branch("efftau2Pt", &efftau2Pt);
    //tree->Branch("dPhi_efftau1MET", &dPhi_efftau1MET);
    //tree->Branch("dPhi_efftau2MET", &dPhi_efftau2MET);

    //RJigsaw::InitOutputBranches(tree);

}

void Analysis::LoadTriggerSets() {

    AsymDitauTrigset = 0;
    DitauMETTrigset  = 0;
    SingleMuTrigset = 0;
    SingleMu_HLT50_Trigset = 0;

    if ((!isData && (MC16SubCampaign == 1 || MC16SubCampaign == 4)) || (isData && runNumber <= 340453)) {
            
        // Data 2015-2017, MC16a+d
        AsymDitauTrigset = new Trigger::Set("HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12");

        if ((isData && runNumber >= 325713 && !m_GRL_data17->Contains(runNumber, lumiBlock))
                || (!isData && MC16SubCampaign == 4 && !m_GRL_data17->Contains(RandomRunNumber, RandomLumiBlockNumber))) {
            DitauMETTrigset  = new Trigger::Set("HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50");
            m_useNewDitauMETTrigger = true;
        } else {
            DitauMETTrigset  = new Trigger::Set("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50");
            m_useNewDitauMETTrigger = false;
        }
    } else if((!isData && MC16SubCampaign == 5) || (isData && runNumber >= 348885)) {

        // Data 2018, MC16e
        AsymDitauTrigset = new Trigger::Set("HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40");

        DitauMETTrigset  = new Trigger::Set("HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50");
    }

    SingleMuTrigset = new Trigger::Set("HLT_mu26_ivarmedium");
    SingleMu_HLT50_Trigset = new Trigger::Set("HLT_mu50");

    Trigger::Manager::Initialize({AsymDitauTrigset, DitauMETTrigset, SingleMuTrigset, SingleMu_HLT50_Trigset});

}

void Analysis::CalculateVariables(){

        MET_pt = MET.Pt();
        MET_phi = MET.Phi();

        // Order matters:
        /*1*/ CalculateTauVariables();
        /*2*/ CalculateJetVariables();
        /*3*/ CalculateTrigEventWeight();


        
        //RJigsaw::Execute();


}

void Analysis::CalculateTauVariables() {
 
        // Reset variables to default values
        tau1Pt = -1.0;
        tau2Pt = -1.0;
        tau1Signal = -1;
        tau2Signal = -1;
        tau1Charge = -10;
        tau2Charge = -10;
        tau1Mt = -1.0;
        tau2Mt = -1.0;
        tau1Quality = -1;
        tau2Quality = -1;
        tau1Width = -1.0;
        tau2Width = -1.0;
        tau1BDTJetScore = -1.0;
        tau2BDTJetScore = -1.0;
        tau1BDTJetScoreSigTrans = -1.0;
        tau2BDTJetScoreSigTrans = -1.0;
        tau1BDTEleScore = -1.0;
        tau2BDTEleScore = -1.0;
        tau1BDTEleScoreSigTrans = -1.0;
        tau2BDTEleScoreSigTrans = -1.0;
        tau1NProng = -1;
        tau2NProng = -1;
        tau1TruthMatch = false;
        tau2TruthMatch = false;
        tau1TruthType = -1;
        tau2TruthType = -1;
        tau1TruthOrigin = -1;
        tau2TruthOrigin = -1;
        tau1PartonTruthLabelID = -1;
        tau2PartonTruthLabelID = -1;
        tau1ConeTruthLabelID = -1;
        tau2ConeTruthLabelID = -1;
        OS = -1;
        mt2tautau = -1.;
        mtsumtautau = -1.;
        minvtautau = -1.;
        meff = -1.;
        met_over_sumtautaupt = -1.;
        sumtau1tau2pt = -1;
        difftau1tau2pt = -1;
        dPhitau1MET = -99.;
        dPhitau2MET = -99.;
        centrality = -99.;
        sumCosDPhi_tauMET = -99.;
        tauPtSum = - 1;
        atLeastTwoMediumTaus = false;
        atLeastOneTightTau = false;
        atLeastTwoTightTaus = false;

        selected_by_asym_ditau_trigger = false;
        selected_by_ditau_MET_trigger = false;
        selected_by_singleMu_trigger = false;
        
        selected_by_asym_ditau_trigger_woPlateau = false;
        selected_by_ditau_MET_trigger_woPlateau = false;
        
        pass_asym_ditau_trigger = false;
        passMatch_asym_ditau_trigger = false;
        passPlat_asym_ditau_trigger = false;

        //DMJ: variables that weren't being set to default values...
        dRtautau = -1.;
        dPhitautau = -1.;
        dEtatautau = -1.;

        n_signaltaus = taus.size();
        n_antitaus = antitaus.size();

        mu1Pt            = -1.;
        mu1Phi       = -1.;
        mu1Eta       = -1.;
        mu1Mt            = -1.;
        mu1Charge  = -10.;
        mu1Signal       = -1;
        tauPt            = -1; 
        tauMt            = -1; 
        dPhitauMET          = -1; 
        OS_taumu     = -1;
        minvtaumuo = -1.;
        mtsumtaumu = -1.;
        dPhitaumu  = -99.;
        dEtataumu  = -99.;
        dRtaumu      = -1.;
        dPhimuMET  = -99.;
        mt2taumu     = -1.;
        meffmu       = -1.;
        met_over_sumtaumupt         = -1.;

        efftau1Pt = -1;
        efftau2Pt = -1;
        dPhi_efftau1MET = -1;
        dPhi_efftau2MET = -1;



        if(!mytaus.empty()){
                tau1Pt = mytaus[0]->Pt();
                tau1Phi = mytaus[0]->Phi();
                tau1Eta = mytaus[0]->Rapidity();
                tau1Signal = mytaus[0]->Signal();
                tau1Charge = mytaus[0]->Charge();
                tau1Mt = mytaus[0]->Mt();
                tau1Quality = mytaus[0]->Quality();
                tau1Width = mytaus[0]->Width();
                tau1BDTJetScore = mytaus[0]->BDTJetScore();
                tau1BDTJetScoreSigTrans = mytaus[0]->BDTJetScoreSigTrans();
                tau1BDTEleScore = mytaus[0]->BDTEleScore();
                tau1BDTEleScoreSigTrans = mytaus[0]->BDTEleScoreSigTrans();
                tau1NProng = mytaus[0]->NProng();
                tau1TruthMatch = !mytaus[0]->Fake();
                tau1TruthType = mytaus[0]->TruthType();
                tau1TruthOrigin = mytaus[0]->TruthOrigin();
                tau1PartonTruthLabelID = mytaus[0]->PartonTruthLabelID();
                tau1ConeTruthLabelID = mytaus[0]->ConeTruthLabelID();
                dPhitau1MET = std::abs(MET.DeltaPhi(mytaus[0]->Vect()));
                atLeastOneTightTau = ( tau1Quality == 3 );
                
                
                // 1 Tau 1 Muon events 
                if(mytaus.size() == 1 && mymuons.size() == 1){

                    mu1Pt            = mymuons[0]->Pt();
                    mu1Phi       = mymuons[0]->Phi();
                    mu1Eta       = mymuons[0]->Rapidity();
                    mu1Mt            = mymuons[0]->Mt();
                    mu1Charge  = mymuons[0]->Charge();
                    mu1Signal  = mymuons[0]->Signal();
                    OS_taumu     = (mytaus[0]->Charge()*mymuons[0]->Charge() < 0) ? 1 : 0;

                    tauPt = mytaus[0]->Pt();
                    tauMt = mytaus[0]->Mt();
                    dPhitauMET  = std::abs(MET.DeltaPhi(mytaus[0]->Vect()));
                    
                    minvtaumuo = (*mytaus[0] + *mymuons[0]).M();
                    mtsumtaumu = tau1Mt + mu1Mt;
                    dPhitaumu  = std::abs(mytaus[0]->DeltaPhi(*mymuons[0]));
                    dEtataumu  = mytaus[0]->Rapidity() - mymuons[0]->Rapidity();
                    dRtaumu      = mytaus[0]->DeltaR(*mymuons[0]);
                    dPhimuMET  = std::abs(MET.DeltaPhi(mymuons[0]->Vect()));
                    mt2taumu = GetMt2<AnalysisObject::Tau, AnalysisObject::Muon>(mytaus[0], mymuons[0], 0., 0.);
                    meffmu       = tau1Pt + mymuons[0]->Pt() + MET.Pt();
                    met_over_sumtaumupt         = MET.Pt()/(tau1Pt + mymuons[0]->Pt());
                    
                    bool pass_HLT_mu26 = false; 
                    bool pass_HLT_mu50 = false; 
                    
                    if(SingleMuTrigset->PassedTrigDec() && SingleMuTrigset->IsInPlateau() && SingleMuTrigset->IsTrigMatched()){
                        pass_HLT_mu26 = true; 
                    }
                    if(SingleMu_HLT50_Trigset->PassedTrigDec() && SingleMu_HLT50_Trigset->IsInPlateau() && SingleMu_HLT50_Trigset->IsTrigMatched()){
                        pass_HLT_mu50 = true; 
                    }

                    if (pass_HLT_mu26 || pass_HLT_mu50) {
                        selected_by_singleMu_trigger = true;
                    }

                    // For BDT training - set 2 tau dependent variables to use 1 tau and 1 muon variables
                    mt2tautau = mt2taumu;
                    meff = meffmu;
                    mtsumtautau = mtsumtaumu;
                    dEtatautau = dEtataumu;
                    dPhitautau = dPhitaumu;
                    dRtautau = dRtaumu;
                    met_over_sumtautaupt = met_over_sumtaumupt;
                    minvtautau = minvtaumuo;
                    sumtau1tau2pt = tau1Pt + mu1Pt;
                    difftau1tau2pt = std::abs(tau1Pt - mu1Pt);

                    if (tauPt >= mu1Pt) {  
                            tau1Pt = tauPt;
                            tau2Pt = mu1Pt;
                            tau1Mt = tauMt;
                            tau2Mt = mu1Mt;
                            dPhitau1MET = dPhitauMET;
                            dPhitau2MET = dPhimuMET;
                    } else {
                            tau1Pt = mu1Pt;
                            tau2Pt = tauPt;
                            tau1Mt = mu1Mt;
                            tau2Mt = tauMt;
                            dPhitau1MET = dPhimuMET;
                            dPhitau2MET = dPhitauMET;
                    }
                }

                if(mytaus.size() >= 2){
                        tau2Pt = mytaus[1]->Pt();
                        tau2Phi = mytaus[1]->Phi();
                        tau2Eta = mytaus[1]->Rapidity();
                        tau2Signal = mytaus[1]->Signal();
                        tau2Charge = mytaus[1]->Charge();
                        tau2Mt = mytaus[1]->Mt();
                        tau2Quality = mytaus[1]->Quality();
                        tau2Width = mytaus[1]->Width();
                        tau2BDTJetScore = mytaus[1]->BDTJetScore();
                        tau2BDTJetScoreSigTrans = mytaus[1]->BDTJetScoreSigTrans();
                        tau2BDTEleScore = mytaus[1]->BDTEleScore();
                        tau2BDTEleScoreSigTrans = mytaus[1]->BDTEleScoreSigTrans();
                        tau2NProng = mytaus[1]->NProng();
                        tau2TruthMatch = !mytaus[1]->Fake();
                        tau2TruthType = mytaus[1]->TruthType();
                        tau2TruthOrigin = mytaus[1]->TruthOrigin();
                        tau2PartonTruthLabelID = mytaus[1]->PartonTruthLabelID();
                        tau2ConeTruthLabelID = mytaus[1]->ConeTruthLabelID();
                        dPhitau2MET = std::abs(MET.DeltaPhi(mytaus[1]->Vect()));
                        atLeastTwoMediumTaus = ( tau1Quality >= 2 ) && ( tau2Quality >= 2 );
                        atLeastOneTightTau = ( tau1Quality == 3 ) || ( tau2Quality == 3 );
                        atLeastTwoTightTaus = ( tau1Quality == 3 ) && ( tau2Quality == 3 );

                        OS = (mytaus[0]->Charge()*mytaus[1]->Charge() < 0) ? 1 : 0;
                        dPhitautau = std::abs(mytaus[0]->DeltaPhi(*mytaus[1]));
                        //dEtatautau = mytaus[0]->Rapidity() - mytaus[1]->Rapidity();
                        //dRtautau   = std::sqrt(dEtatautau*dEtatautau + dPhitautau*dPhitautau);
                        dRtautau     = std::abs(mytaus[0]->DeltaR(*mytaus[1]));
                        dEtatautau   = std::abs(mytaus[0]->Eta() - mytaus[1]->Eta());

                        mt2tautau = GetMt2<AnalysisObject::Tau, AnalysisObject::Tau>(mytaus[0], mytaus[1], 0., 0.);
                        mt2tautau_1 = GetMt2<AnalysisObject::Tau, AnalysisObject::Tau>(mytaus[0], mytaus[1], 1., 1.);
                        mt2tautau_20 = GetMt2<AnalysisObject::Tau, AnalysisObject::Tau>(mytaus[0], mytaus[1], 20., 20.);
                        mt2tautau_40 = GetMt2<AnalysisObject::Tau, AnalysisObject::Tau>(mytaus[0], mytaus[1], 40., 40.);
                        mtsumtautau = tau1Mt + tau2Mt;

                        minvtautau = (*mytaus[0] + *mytaus[1]).M();
                        meff = tau1Pt + tau2Pt + MET.Pt();
                        met_over_sumtautaupt = MET.Pt()/(tau1Pt + tau2Pt);
                        
                        sumtau1tau2pt = tau1Pt + tau2Pt;
                        difftau1tau2pt = tau1Pt - tau2Pt;
                        
                        centrality = (tau1Pt + tau2Pt)/(mytaus[0]->E() + mytaus[1]->E());
                        sumCosDPhi_tauMET = std::cos(dPhitau1MET) + std::cos(dPhitau2MET);
                        cmt = std::sqrt(2.0*tau1Pt*tau2Pt*(1.0 + std::cos(dPhitautau)));

                        efftau1Pt = tau1Pt;
                        dPhi_efftau1MET = dPhitau1MET;
                        efftau2Pt = tau2Pt;
                        dPhi_efftau2MET = dPhitau2MET;
                        
                        if(AsymDitauTrigset->PassedTrigDec() && AsymDitauTrigset->IsInPlateau() && AsymDitauTrigset->IsTrigMatched()){
                            selected_by_asym_ditau_trigger = true;
                        }
                        
                        if(AsymDitauTrigset->PassedTrigDec() && AsymDitauTrigset->IsTrigMatched()){
                            selected_by_asym_ditau_trigger_woPlateau = true;
                        }
                        
                        if(AsymDitauTrigset->PassedTrigDec()){
                            pass_asym_ditau_trigger = true;

                            if (AsymDitauTrigset->IsTrigMatched()) {
                                passMatch_asym_ditau_trigger = true;
                            }
                            if (AsymDitauTrigset->IsInPlateau()) {
                                passPlat_asym_ditau_trigger = true;
                            }
                        }

                        if(DitauMETTrigset->PassedTrigDec() && DitauMETTrigset->IsInPlateau() && DitauMETTrigset->IsTrigMatched()){
                            selected_by_ditau_MET_trigger = true;
                        }
                        if(DitauMETTrigset->PassedTrigDec() && DitauMETTrigset->IsTrigMatched()){
                            selected_by_ditau_MET_trigger_woPlateau = true;
                        }

                }
        }
        
        for (const AnalysisObject::Tau* tau : mytaus) {
                tauPtSum += tau->Pt();
        }

}


void Analysis::CalculateTrigEventWeight() { 

    TrigWeight = 1.0;
    //evt_weight_withTrigWeight = 1.0;
    //if (selected_by_asym_ditau_trigger) {
    if ( AsymDitauTrigset->PassedTrigDec() && !DitauMETTrigset->PassedTrigDec()) {
        TrigWeight = TrigWeight_asymditau;
    }
    //else if (selected_by_ditau_MET_trigger) {
    else if (DitauMETTrigset->PassedTrigDec() && ! AsymDitauTrigset->PassedTrigDec()) {
        TrigWeight = TrigWeight_ditauMET;
    }
    else if (AsymDitauTrigset->PassedTrigDec() && DitauMETTrigset->PassedTrigDec()) {
        if (MET_pt >= 150) {
            TrigWeight = TrigWeight_ditauMET;
        }   else  {
            TrigWeight = TrigWeight_asymditau;
        }
    }
    //else if (selected_by_singleMu_trigger) {
    else if (SingleMuTrigset->PassedTrigDec() || SingleMu_HLT50_Trigset->PassedTrigDec()) {
        TrigWeight = TrigWeight_singleMU;
    }
    else {
        TrigWeight = 0.0; 
    }
    evt_weight *= TrigWeight;

}

void Analysis::CalculateJetVariables() {

        // Reset variables to default values
        jet1Pt = -1.0;
        jet2Pt = -1.0;
        jet1Mt = -1.0;
        jet2Mt = -1.0;

        dPhijet1jet2 = -10.0;
        dPhiminjetsMET = -10.0;
        HT = tauPtSum;
        METSig = -1.0;

        n_jets = jets.size();
        n_btags = std::count_if(jets.begin(), jets.end(), [&](const AnalysisObject::Jet j) { return j.Btag(); });
        
//       int bcount = 0;
//       std::vector<double> pts_of_bjets;
//       
//       for(int i=0; i<jets_bjet->size(); i++){
//          
//           if(jets_signal->at(i)==1 &&/*jets_pt->at(i)>25000 &&*/ fabs(jets_eta->at(i))<2.5 && jets_bjet->at(i)==1){
//               bcount++;
//               pts_of_bjets.push_back(jets[i].Pt());
//           }
//           
//       }
//       if(n_btags>0){
//           LOG(INFO) << "nBtags DSALoop: " << n_btags << " eta cut: " << bcount << " XAMPP: " << n_BJets;
//           if(pts_of_bjets.size()>0) LOG(INFO) << "pt of leading b-tagged jet: " << pts_of_bjets[0];
//       }

        if(!jets.empty()) {
                jet1Pt = jets[0].Pt();
                jet1Mt = jets[0].Mt();
                dPhiminjetsMET = MET.DeltaPhi((*std::min_element(jets.begin(), jets.end(), [&](const AnalysisObject::Jet j1, const AnalysisObject::Jet j2) { return std::abs(MET.DeltaPhi(j1.Vect())) < std::abs(MET.DeltaPhi(j2.Vect())); })).Vect());
                if (jets.size() >= 2) {
                        jet2Pt = jets[1].Pt();
                        jet2Mt = jets[1].Mt();
                        dPhijet1jet2 = jets[0].DeltaPhi(jets[1]);
                }
        }

        for (AnalysisObject::Jet& jet : jets) {
                HT += jet.Pt();
        }
        if (HT > 0) {
                METSig = MET.Pt() / std::sqrt(HT);
        }

}


void Analysis::EvaluateTriggers(){

    // Trigger preselection (noTrigger==True -> pass through)
    //// Passes triggers?
    mytaus.clear();
    for (const AnalysisObject::Tau& tp : taus) {
        mytaus.push_back(&tp);
    }

    mymuons.clear();
    for (const AnalysisObject::Muon& mu : muons) {
        //       if(mu.Signal() == 1){
        mymuons.push_back(&mu);
        //       }
    }


    LoadTriggerSets();

    //if (!Monitoring::PassesCut(Trigger::Manager::PassedTrigDec(), "trigdec"));
    //return pass_preselection;
    //// Passes trigger matching?

    AsymDitauTrigset->EvaluateTrigMatch( mytaus.size()>=2
                    && mytaus[0]->TrigMatch(AsymDitauTrigset->GetName())
                    && mytaus[1]->TrigMatch(AsymDitauTrigset->GetName()) );
    DitauMETTrigset->EvaluateTrigMatch(  mytaus.size()>=2
                    && mytaus[0]->TrigMatch(DitauMETTrigset->GetName())
                    && mytaus[1]->TrigMatch(DitauMETTrigset->GetName()) );
    SingleMuTrigset->EvaluateTrigMatch(mymuons.size()>=1 && TrigMatch.at(SingleMuTrigset->GetName()));
    SingleMu_HLT50_Trigset->EvaluateTrigMatch(mymuons.size()>=1 && TrigMatch.at(SingleMu_HLT50_Trigset->GetName()));

    if ((!isData && (MC16SubCampaign == 1 || MC16SubCampaign == 4)) || (isData && runNumber < 348885)) {
            // Data 2015-2017, MC16a+d
            AsymDitauTrigset->EvaluatePlateauCuts( mytaus.size()>=2 && mytaus[0]->Pt()>95. && mytaus[1]->Pt()>60. );
            if (!m_useNewDitauMETTrigger)
                    DitauMETTrigset->EvaluatePlateauCuts(  mytaus.size()>=2 && mytaus[0]->Pt()>50. && mytaus[1]->Pt()>40. && MET.Pt()>150. );
            else
                    DitauMETTrigset->EvaluatePlateauCuts(  mytaus.size()>=2 && mytaus[0]->Pt()>75. && mytaus[1]->Pt()>40. && MET.Pt()>150. );
    
    } else if((!isData && MC16SubCampaign == 5) || (isData && runNumber >= 348885)) {
            // Data 2018, MC16e
            AsymDitauTrigset->EvaluatePlateauCuts( mytaus.size()>=2 && mytaus[0]->Pt()>95. && mytaus[1]->Pt()>75. );
            DitauMETTrigset->EvaluatePlateauCuts(  mytaus.size()>=2 && mytaus[0]->Pt()>75. && mytaus[1]->Pt()>40. && MET.Pt()>150. );
    }

    SingleMuTrigset->EvaluatePlateauCuts(mymuons.size()>=1 && mymuons[0]->Pt()>28.);
    SingleMu_HLT50_Trigset->EvaluatePlateauCuts(mymuons.size()>=1 && mymuons[0]->Pt()>52.5);


}


bool Analysis::PassTriggers() {
    bool passTriggers = false; 
    
    // Selected, matched and passes plateau cuts
    if ((Monitoring::PassesCut(Trigger::Manager::PassedTrigDec(), "trigdec")) && 
        (Monitoring::PassesCut(Trigger::Manager::IsTrigMatched(), "trigmatch"))) 
        // && (Monitoring::PassesCut(Trigger::Manager::IsInPlateau(), "plateaucut")))
     {
         passTriggers = true;
    }
    return passTriggers;
}

bool Analysis::PassPreselection(){

    //return true; //DMJ let everyting pass the selection
    bool pass_preselection = false;

    // Cutflow (in order):
    if (!noTrigger) {
        Monitoring::InitializeCut("trigdec");
        Monitoring::InitializeCut("trigmatch");
        Monitoring::InitializeCut("plateaucut");
    }
    if(!MuSelection){
        Monitoring::InitializeCut(">=2taus");
        Monitoring::InitializeCut("llveto");
        Monitoring::InitializeCut("bveto");
    }else{
        Monitoring::InitializeCut("1mu-1tau");
        Monitoring::InitializeCut("bveto");
    }
//   Monitoring::InitializeCut("Zveto");
    if (!fakefactorfile.empty()) Monitoring::InitializeCut("FFMveto");

    
    /*
    // First decide which tau container to use (here an in Analysis::CalculateVariables later on)
    mytaus.clear();
    for (const AnalysisObject::Tau& tp : taus) {
            mytaus.push_back(&tp);
    }

    mymuons.clear();
    for (const AnalysisObject::Muon& mu : muons) {
//       if(mu.Signal() == 1){
            mymuons.push_back(&mu);
//       }
    }

    if (selectAntiTaus) {
            for (const AnalysisObject::Tau& tp : antitaus) {
                    mytaus.push_back(&tp);
            }
            AnalysisObject::BasicSorter(mytaus);
    }
    */


    // Skimming
    if ((atLeastTwoMediumTaus == 1 && OS == 1 && mt2tautau > 30) || (n_BaseTau == 1 && n_BaseMuon == 1 && OS_taumu == 1 && mt2taumu > 30)) {
        pass_preselection = true;
    }
    
    // If no skimming
    // pass_preselection = true;

    return pass_preselection;

}

void Analysis::Execute(){

     //CalculateVariables();
     //WeightCalculator::Execute();
     
     if (treeStem == "Nominal") {
         WeightCalculator::ComputeSystematicWeights();
     }
     
     FakeFactorManager::Calculate();
     
    
     if (Monitoring::PassesCut(!FakeFactorManager::VetoEvent(), "FFMveto")) {
             m_nevts_passed ++;
             tree->Fill();
             if (m_nevts_passed % 1000 == 1) tree->AutoSave("SaveSelf");
     }
   

}

template<typename T, typename U>
double Analysis::GetMt2(const T* obj1, const U* obj2, double m_invis1, double m_invis2) {
        return asymm_mt2_lester_bisect::get_mT2( obj1->M(), obj1->Px(), obj1->Py(),
                                                                                         obj2->M(), obj2->Px(), obj2->Py(),
                                                                                         MET.Px(), MET.Py(),
                                                                                         m_invis1, m_invis2,
                                                                                         0);
}

void Analysis::WriteNtuple(){

    tree->Write(0, TObject::kOverwrite);
    LOG(INFO) << "Output tree '" << std::string(tree->GetName()) << "' has been written to '"
                        << std::string(tree->GetCurrentFile()->GetName()) << "'";
    
}

void Analysis::Finalize(){

    WriteNtuple();

}

template double Analysis::GetMt2<AnalysisObject::Tau, AnalysisObject::Tau>(const AnalysisObject::Tau* obj1, const AnalysisObject::Tau* obj2, double m_invis1=0, double m_invis2=0);

