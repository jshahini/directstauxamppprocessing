#include "../include/GRLFilter.h"

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"

#include <algorithm>


void GRLFilter::LoadXMLFile() {

    LOG(INFO) << "Loading GRL file '" << m_xmlpath << "'";

    if (!m_xmldoc.load_file(m_xmlpath.c_str())) {
        LOG(ERROR) << "Failed to load GRL file '" << m_xmlpath << "'";
        return;
    }

    m_nodes = m_xmldoc.select_nodes("/LumiRangeCollection/NamedLumiRange/LumiBlockCollection");

}

bool GRLFilter::Contains(const int& runnumber, const int& lumiblock) const {

    bool pass = false;

    for(const pugi::xpath_node& node : m_nodes) {
        if (runnumber != std::stoi(std::string(node.node().child_value("Run"))))
            continue;
        for (const pugi::xml_node& child : node.node().children("LBRange")) {
            if (lumiblock >= child.attribute("Start").as_int() && lumiblock <= child.attribute("End").as_int())
                pass = true;
        }
    }

    return pass;

}
