//#include "../include/AnalysisObject.h"
#include "../include/DirectStau_Store.h"

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "easylogging++.h"

void AnalysisObject::FillElectronContainer() {
    for(int iele=0; iele<Store::electrons_pt->size(); ++iele) {
        AnalysisObject::Electron thisele;
        thisele.SetPtEtaPhiE(Store::electrons_pt->at(iele)/1000.,
                             Store::electrons_eta->at(iele),
                             Store::electrons_phi->at(iele),
                             Store::electrons_e->at(iele)/1000.);
        thisele.SetMt(Store::electrons_MT->at(iele)/1000.);
//         for (const std::pair<std::string,std::vector<char>*> tm : Store::electrons_TrigMatch) {
//             thisele.SetTrigMatch(tm.first, tm.second->at(iele) == 1);
//         }
        if (!Store::isData){
            thisele.SetTruthMatch(Store::electrons_truthType->at(iele) == 2 || Store::electrons_truthType->at(iele) == 3);
            thisele.SetTruthType(Store::electrons_truthType->at(iele));
            thisele.SetTruthOrigin(Store::electrons_truthOrigin->at(iele));
        } else { thisele.SetTruthMatch(true); }
//        thisele.SetWeight(1.0);
        thisele.SetCharge(Store::electrons_charge->at(iele));
        thisele.SetSignal(Store::electrons_signal->at(iele) == 1);
        Store::electrons.push_back(thisele);
    }
}

void AnalysisObject::FillMuonContainer() {
    for(int imuo=0; imuo<Store::muons_pt->size(); ++imuo) {
        AnalysisObject::Muon thismuo;
        thismuo.SetPtEtaPhiE(Store::muons_pt->at(imuo)/1000.,
                           Store::muons_eta->at(imuo),
                           Store::muons_phi->at(imuo),
                           Store::muons_e->at(imuo)/1000.);
        thismuo.SetMt(Store::muons_MT->at(imuo)/1000.);
//         for (const std::pair<std::string,std::vector<char>*> tm : Store::muons_TrigMatch) {
//             thismuo.SetTrigMatch(tm.first, tm.second->at(imuo) == 1);
//         }
        if (!Store::isData){
            thismuo.SetTruthMatch(Store::muons_truthType->at(imuo) ==  6|| Store::muons_truthType->at(imuo) == 7);
            thismuo.SetTruthType(Store::muons_truthType->at(imuo));
            thismuo.SetTruthOrigin(Store::muons_truthOrigin->at(imuo));
        } else { thismuo.SetTruthMatch(true); }
//        thismuo.SetWeight(1.0);
        thismuo.SetCharge(Store::muons_charge->at(imuo));
        thismuo.SetSignal(Store::muons_signal->at(imuo) == 1);
        Store::muons.push_back(thismuo);
    }
}

void AnalysisObject::FillTauContainer() {
    for(int itau=0; itau<std::min(Store::n_BaseTau,2); ++itau) {
        if (Store::n_BaseTau == 0) break;  
        AnalysisObject::Tau thistau;
        thistau.SetPtEtaPhiE(Store::taus_pt->at(itau)/1000.,
                             Store::taus_eta->at(itau),
                             Store::taus_phi->at(itau),
                             Store::taus_e->at(itau)/1000.);
        thistau.SetMt(Store::taus_MT->at(itau)/1000.);
        
        
            
        for (const std::pair<std::string,std::vector<char>*> tm : Store::taus_TrigMatch) {
        //for (const auto tm : Store::taus_TrigMatch) {
            //std::cout << "tm->at(itau).second " << tm->at(itau).second << std::endl;
            //std::cout << "tm.second->at(itau) " << tm.second->at(itau) << std::endl;
            thistau.SetTrigMatch(tm.first, tm.second->at(itau) == 1);
            //thistau.SetTrigMatch(tm.first, 0);
        }
        
        if (!Store::isData) {
            thistau.SetTruthMatch(Store::taus_truthType->at(itau) == 10 || Store::taus_truthType->at(itau) == 11);
            thistau.SetTruthType(Store::taus_truthType->at(itau));
            thistau.SetTruthOrigin(Store::taus_truthOrigin->at(itau));
            thistau.SetPartonTruthLabelID(Store::taus_PartonTruthLabelID->at(itau));
            thistau.SetConeTruthLabelID(Store::taus_ConeTruthLabelID->at(itau));
        } else { thistau.SetTruthMatch(true); }
//        thistau.SetWeight(1.0);
        thistau.SetNProng(Store::taus_NTrks->at(itau));
        thistau.SetCharge(Store::taus_charge->at(itau));
        thistau.SetQuality(Store::taus_Quality->at(itau));
        thistau.SetWidth(Store::taus_Width->at(itau));
        
        /*
        thistau.SetBDTJetScore(Store::taus_BDTJetScore->at(itau));
        thistau.SetBDTEleScore(Store::taus_BDTEleScore->at(itau));
        thistau.SetBDTJetScoreSigTrans(Store::taus_BDTJetScoreSigTrans->at(itau));
        thistau.SetBDTEleScoreSigTrans(Store::taus_BDTEleScoreSigTrans->at(itau));
        */
        
        if(Store::signalTauID < 0) {
            thistau.SetSignal(Store::taus_signalID->at(itau) == 1);
        } else {
            thistau.SetSignal(Store::taus_Quality->at(itau) >= Store::signalTauID);
        }
        if(thistau.Signal()) {
            Store::taus.push_back(thistau);
        } else {
            if (Store::selectAntiTaus) {
                Store::antitaus.push_back(thistau);
            }
        }
    }
}

void AnalysisObject::FillJetContainer() {
    
    // DMJ: Was a mis-match between n_BaseJets and Jet vector sizes...
    
    //for(int ijet=0; ijet<Store::n_BaseJets; ++ijet) {
    for(int ijet=0; ijet<Store::jets_pt->size(); ++ijet) {
        
        AnalysisObject::Jet thisjet;
        thisjet.SetPtEtaPhiM(Store::jets_pt->at(ijet)/1000.,
                             Store::jets_eta->at(ijet),
                             Store::jets_phi->at(ijet),
                             Store::jets_m->at(ijet)/1000.);
        
        thisjet.SetMt(std::sqrt(2.*Store::MetTST_met*Store::jets_pt->at(ijet)*1.0e-6*(1. - std::cos(Store::jets_phi->at(ijet) - Store::MetTST_phi))));
//        thisjet.SetTruthMatch(true);
//        thisjet.SetWeight(1.0);
        thisjet.SetBtag(Store::jets_bjet->at(ijet) && Store::jets_signal->at(ijet)); // i.e. pass b-tag and JVT cut
        thisjet.SetNTracks(Store::jets_NTrks->at(ijet));
        thisjet.SetJVT(Store::jets_Jvt->at(ijet));
        
        if ((Store::selectAntiTaus && Store::jets_LooseOR->at(ijet) == 1) || !Store::selectAntiTaus) { // LooseOR flag isn't available in some XAMPP ntuples (fallback value = 1, see DSBase_Nominal::HandleDeprecatedBranches())
            thisjet.SetSignal(Store::jets_signal->at(ijet) == 1);
            Store::jets.push_back(thisjet);
        }
    }
}

void AnalysisObject::GetETmiss() {
    Store::MET.SetPtEtaPhi(Store::MetTST_met*1.0e-3, 0., Store::MetTST_phi);
    Store::MET.SetSumet(Store::MetTST_sumet*1.0e-3);
}

void AnalysisObject::RemoveOverlapOfContainers() {
    // Resolve overlap with antitaus (if antitaus are requested)
    if (Store::selectAntiTaus) {
        // 1.) dR(antitau, ele) < 0.2 --> remove antitau
        // 2.) dR(antitau, muo) < 0.2 --> remove antitau
        // 3.) dR(antitau, tau) < 0.2 --> remove antitau
        // 4.) dR(antitau, jet) < 0.2 --> a) remove jet if jet is NOT b-tagged
        //                                b) remove antitau if jet is b-tagged
        RemoveOverlap(Store::antitaus, Store::electrons, 0.2);
        RemoveOverlap(Store::antitaus, Store::muons, 0.2);
        RemoveOverlap(Store::antitaus, Store::taus, 0.2);
//        RemoveOverlap(Store::jets, Store::antitaus, 0.2); // b-tag agnostic
        for(Jet jet : Store::jets) {
            if (jet.Btag()) {
                RemoveOverlap(Store::antitaus, jet, 0.2);
            } else {
                if (CheckOverlap(Store::antitaus, jet, 0.2))
                    RemoveOverlap(Store::jets, jet, 0.2);
            }
        }
    }
    
    // Overlap removal should all be handled within Xampp
    //RemoveOverlap(Store::taus, Store::electrons, 0.2);
    //RemoveOverlap(Store::taus, Store::muons, 0.2);
    //RemoveOverlap(Store::electrons, Store::muons, 0.2);
    //RemoveOverlap(Store::electrons, Store::jets, 0.4);
    //RemoveOverlap(Store::muons, Store::jets, 0.4);
    //RemoveOverlap(Store::jets, Store::taus, 0.4);

}

void AnalysisObject::FillContainers() {

    GetETmiss();
    FillElectronContainer();
    FillMuonContainer();
    FillTauContainer();
    FillJetContainer();

    RemoveOverlapOfContainers();

    // DEBUG
    //if (CheckOverlap(Store::taus, Store::jets))
        //LOG(WARNING) << "tau and jet containers overlap!";

//    if (CheckOverlap(Store::taus, Store::electrons))
//        LOG(WARNING) << "tau and electron containers overlap!";

//    if (CheckOverlap(Store::taus, Store::muons))
//        LOG(WARNING) << "tau and muon containers overlap!";

//    if (CheckOverlap(Store::electrons, Store::muons))
//        LOG(WARNING) << "electron and muon containers overlap!";

//    if (CheckOverlap(Store::electrons, Store::jets))
//        LOG(WARNING) << "electron and jets containers overlap!";

//    if (CheckOverlap(Store::muons, Store::jets))
//        LOG(WARNING) << "muons and jets containers overlap!";

//    if (Store::selectAntiTaus) {
//        if (CheckOverlap(Store::antitaus, Store::taus))
//            LOG(WARNING) << "antitau and tau containers overlap!";

//        if (CheckOverlap(Store::antitaus, Store::jets))
//            LOG(WARNING) << "antitau and jet containers overlap!";

//        if (CheckOverlap(Store::antitaus, Store::electrons))
//            LOG(WARNING) << "antitau and electron containers overlap!";

//        if (CheckOverlap(Store::antitaus, Store::muons))
//            LOG(WARNING) << "antitau and muon containers overlap!";
//    }
}

void AnalysisObject::ClearContainers() {
    Store::electrons.clear();
    Store::muons.clear();
    Store::taus.clear();
    Store::antitaus.clear();
    Store::jets.clear();
}
