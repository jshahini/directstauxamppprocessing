#include "../include/RJigsaw.h"

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"
#include <utility>
#include <iostream>

// Check preprocessor symbol first (see CMakeList.txt)
#if RESTFRAMES_MISSING == 0

LabRecoFrame*		  RJigsaw::LAB;
DecayRecoFrame*		  RJigsaw::StauStau;
DecayRecoFrame*		  RJigsaw::Staua;
DecayRecoFrame*	      RJigsaw::Staub;
VisibleRecoFrame*	  RJigsaw::taua;
VisibleRecoFrame*	  RJigsaw::taub;
InvisibleRecoFrame*   RJigsaw::X1a;
InvisibleRecoFrame*	  RJigsaw::X1b;
InvisibleGroup*		  RJigsaw::INV;
SetMassInvJigsaw*	  RJigsaw::X1_mass;
SetRapidityInvJigsaw* RJigsaw::X1_eta;
ContraBoostInvJigsaw* RJigsaw::X1X1_contra;
TreePlot*			  RJigsaw::treePlot;

void RJigsaw::Initialize() {

    LOG(INFO) << "Initializing reconstruction tree and analysis...";

    LAB      = new LabRecoFrame("LAB","LAB");
    StauStau = new DecayRecoFrame("StauStau","#tilde{#tau} #tilde{#tau}");
    Staua    = new DecayRecoFrame("Staua","#tilde{#tau}_{a}");
    Staub    = new DecayRecoFrame("Staub","#tilde{#tau}_{b}");

    taua     = new VisibleRecoFrame("taua","#it{#tau}_{a}");
    taub     = new VisibleRecoFrame("taub","#it{#tau}_{b}");
    X1a      = new InvisibleRecoFrame("X1a","#tilde{#chi}^{ 0}_{1 a}");
    X1b      = new InvisibleRecoFrame("X1b","#tilde{#chi}^{ 0}_{1 b}");

    //-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//

    LAB->SetChildFrame(*StauStau);
    StauStau->AddChildFrame(*Staua);
    StauStau->AddChildFrame(*Staub);

    Staua->AddChildFrame(*X1a);
    Staua->AddChildFrame(*taua);
    Staub->AddChildFrame(*X1b);
    Staub->AddChildFrame(*taub);

    if(LAB->InitializeTree())
        LOG(INFO) << "...Successfully initialized reconstruction tree";
    else
        LOG(ERROR) << "...Failed initializing reconstruction tree";

    // Invisible Groups
    INV = new InvisibleGroup("INV","#tilde{#chi}_{1}^{ 0} #tilde{#chi}_{1}^{ 0} Jigsaws");
    INV->AddFrames(*X1a+*X1b);

    // Set di-LSP mass to minimum Lorentz-invariant expression
    X1_mass = new SetMassInvJigsaw("X1_mass","Set M_{#tilde{#chi}_{1}^{ 0}#tilde{#chi}_{1}^{ 0}} to minimum");
    INV->AddJigsaw(*X1_mass);

    // Set di-LSP rapidity to that of visible particles
    X1_eta = new SetRapidityInvJigsaw("X1_eta","#eta_{#tilde{#chi}_{1}^{ 0}#tilde{#chi}_{1}^{ 0}} = #eta_{#tau#tau}");
    INV->AddJigsaw(*X1_eta);
    X1_eta->AddVisibleFrames(StauStau->GetListVisibleFrames());

    X1X1_contra = new ContraBoostInvJigsaw("X1X1_contra","Contraboost invariant Jigsaw");
    //MinMassDiffInvJigsaw X1X1_contra("MinMh_R","min M_{h}, M_{h}^{ a}= M_{h}^{ b}",2);
    //MinMassesSqInvJigsaw X1X1_contra("MinMWa_R","min M_{W}, M_{W}^{a,a}= M_{W}^{a,b}", 2);
    INV->AddJigsaw(*X1X1_contra);
    X1X1_contra->AddVisibleFrames(Staua->GetListVisibleFrames(), 0);
    X1X1_contra->AddVisibleFrames(Staub->GetListVisibleFrames(), 1);
    X1X1_contra->AddInvisibleFrame(*X1a, 0);
    X1X1_contra->AddInvisibleFrame(*X1b, 1);

    if(LAB->InitializeAnalysis())
        LOG(INFO) << "...Successfully initialized analysis";
    else
        LOG(ERROR) << "...Failed initializing analysis";

    // Fill RestFrames monitoring plots
    treePlot = new TreePlot("RestFrames","RestFrames");
    treePlot->SetTree(*LAB);
    treePlot->Draw("RecoTree", "Reconstruction Tree");
    treePlot->SetTree(*INV);
    treePlot->Draw("InvTree", "Invisible Jigsaws", true);

}

void RJigsaw::WriteRFPlots(const std::string outputfilename) {

    treePlot->WriteOutput(outputfilename);

}

void RJigsaw::InitOutputBranches(TTree* outputtree) {

    outputtree->Branch("EtauStaua",     &EtauStaua);
    outputtree->Branch("EtauStaub",     &EtauStaub);
    outputtree->Branch("mass_CM",       &mass_CM);
    outputtree->Branch("cosStauStau",   &cosStauStau);
    outputtree->Branch("cosStaua",      &cosStaua);
    outputtree->Branch("cosStaub",      &cosStaub);
    outputtree->Branch("H2PP",          &H2PP);
    outputtree->Branch("HT2PP",         &HT2PP);
    outputtree->Branch("H3PP",          &H3PP);
    outputtree->Branch("HT3PP",         &HT3PP);
    outputtree->Branch("H4PP",          &H4PP);
    outputtree->Branch("HT4PP",         &HT4PP);
    outputtree->Branch("H2Pa",          &H2Pa);
    outputtree->Branch("H2Pb",          &H2Pb);

}

void RJigsaw::Reset() {

    LAB->ClearEvent();

    // Reset to default values
    EtauStaua   = -999.;
    EtauStaub   = -999.;
    mass_CM     = -999.;
    cosStauStau = -999.;
    cosStaua    = -999.;
    cosStaub    = -999.;
    H2PP        = -999.;
    HT2PP       = -999.;
    H3PP        = -999.;
    HT3PP       = -999.;
    H4PP        = -999.;
    HT4PP       = -999.;

}

void RJigsaw::Execute() {

    Reset();

    // First decide which tau container to use
    std::vector<AnalysisObject::Tau*> mytaus;
    for (AnalysisObject::Tau& tp : taus) {
        mytaus.push_back(new AnalysisObject::Tau(tp));
    }
    if (selectAntiTaus) {
        for (AnalysisObject::Tau& tp : antitaus) {
            mytaus.push_back(new AnalysisObject::Tau(tp));
        }
        AnalysisObject::BasicSorter(mytaus);
    }

    taua->SetLabFrameFourVector(*mytaus[0]);
    taub->SetLabFrameFourVector(*mytaus[1]);

    INV->SetLabFrameThreeVector(MET);

    //////////////////////////////////////////////////
    //Lorentz vectors have been set, now do the boosts
    //////////////////////////////////////////////////

    LAB->AnalyzeEvent();

    // Stau energy in parent frame
    EtauStaua = taua->GetEnergy(*Staua);
    EtauStaub = taua->GetEnergy(*Staub);

    // Invariant mass of di-stau system
    mass_CM = StauStau->GetMass();

    // Stau decay angles
    cosStauStau = StauStau->GetCosDecayAngle();
    cosStaua    = Staua->GetCosDecayAngle();
    cosStaub    = Staub->GetCosDecayAngle();

    // Get 4-vectors in the di-stau frame
    TLorentzVector taua_PP_tlv = taua->GetFourVector(*StauStau);
    TLorentzVector taub_PP_tlv = taub->GetFourVector(*StauStau);
    TLorentzVector X1a_PP_tlv = X1a->GetFourVector(*StauStau);
    TLorentzVector X1b_PP_tlv = X1b->GetFourVector(*StauStau);

    // H-variables as well as the transverse projections in the di-stau frame
    // H(1,1)PP <=> H2PP, H(2,1)PP <=> H3PP, H(2,2)PP <=> H4PP
    H2PP  = (taua_PP_tlv + taub_PP_tlv).P()  + (X1a_PP_tlv + X1b_PP_tlv).P();
    HT2PP = (taua_PP_tlv + taub_PP_tlv).Pt() + (X1a_PP_tlv + X1b_PP_tlv).Pt();

    H3PP  = taua_PP_tlv.P()  + taub_PP_tlv.P()  + (X1a_PP_tlv + X1b_PP_tlv).P();
    HT3PP = taua_PP_tlv.Pt() + taub_PP_tlv.Pt() + (X1a_PP_tlv + X1b_PP_tlv).Pt();

    H4PP  = taua_PP_tlv.P()  + taub_PP_tlv.P()  + X1a_PP_tlv.P()  + X1b_PP_tlv.P();
    HT4PP = taua_PP_tlv.Pt() + taub_PP_tlv.Pt() + X1a_PP_tlv.Pt() + X1b_PP_tlv.Pt();

    // Get vectors in the individual frames
    TLorentzVector taua_Pa_tlv = taua->GetFourVector(*Staua);
    TLorentzVector X1a_Pa_tlv = X1a->GetFourVector(*Staua);
    TLorentzVector taub_Pb_tlv = taub->GetFourVector(*Staub);
    TLorentzVector X1b_Pb_tlv = X1b->GetFourVector(*Staub);

    // H-variables in individual frames
    H2Pa = taua_Pa_tlv.P() + X1a_Pa_tlv.P();
    H2Pb = taub_Pb_tlv.P() + X1b_Pb_tlv.P();

}

RJigsaw::~RJigsaw() {

    // deletion of open tree pointers
    delete LAB;
    delete StauStau;
    delete Staua;
    delete Staub;
    delete X1a;
    delete X1b;
    delete X1X1_contra;
    delete X1_mass;
    delete X1_eta;
    delete INV;

}
#else
void RJigsaw::Initialize() {
    LOG(WARNING) << "Code has been compiled without RestFrames support. Recursive Jigsaw Reconstruction is disabled!";
};
#endif
