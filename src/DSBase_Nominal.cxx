// #define DSBase_Nominal_cxx
#include "../include/DSBase_Nominal.h"
#include "../include/WeightCalculator.h"
// #include "../include/DirectStau_Analysis.h"
// #include "../include/DirectStau_Store.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

#include <iostream>

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#               define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"


void DSBase_Nominal::Loop()
{
    Analysis::Initialize();

    if (fChain == 0) return;

    Long64_t nentries = fChain->GetEntries();
    if (stopAfterNevts > 0) {
        nentries = nentries > stopAfterNevts ? stopAfterNevts : nentries;
    }

    HandleDeprecatedBranches();

    LOG(INFO) << "Starting to loop over events...";

    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {

        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);   nbytes += nb;

        AnalysisObject::FillContainers();


        // Originally was here...
        WeightCalculator::Execute();
        evt_count ++;
        evt_count_weighted += evt_weight;

        Analysis::EvaluateTriggers();

        // if pass triggers, calculate variables for more complicated skimming selection
        if (Analysis::PassTriggers()) {

            Analysis::CalculateVariables(); // calculates trigger event weights, calculates overall Trigger Event weight
        
            if (Analysis::PassPreselection() && !WeightCalculator::RemoveDatasetOverlap()) {
                
                //LOG(INFO) << "Entry: " << jentry;

                Analysis::Execute(); // This fills the trees, , calculates weight based systs - done after passing preselection... 

                // Potentially this messes up the cutflow a bit... 
                //evt_count ++;
                //evt_count_weighted += evt_weight;
                //AnalysisObject::ClearContainers();
        
            }
        }
        
        if ((jentry+1) % 1000 == 0) {
            LOG(INFO) << "Processed " << jentry+1 << " / " << nentries << " events";
        }

        AnalysisObject::ClearContainers();

    }
    Analysis::Finalize();

}

DSBase_Nominal::DSBase_Nominal(TTree *tree) : fChain(0) 
{
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    if (!tree) {
        LOG(INFO) << "There is no input tree! Instead opening default file...";
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/project/etp4/C.Leitgeb/XAMPPstau_test/run/TestFile_Trigger.root");
        //                  TFile *f = TFile::Open("/project/etp4/C.Leitgeb/XAMPPstau_test/run/TestFile_Trigger.root"); 
        if (!f || !f->IsOpen()) {
            LOG(INFO) << "Cannot open input file! Instead opening default file...";
            f = new TFile("/project/etp4/C.Leitgeb/XAMPPstau_test/run/TestFile_Trigger.root");
        }
        f->GetObject("Staus_Nominal",tree);
        //                   f->GetObject("MetaDataTree",tree);

    }
    LOG(INFO) << "Initializing input tree '" << std::string(tree->GetName()) << "'...";
    Init(tree);
}

DSBase_Nominal::~DSBase_Nominal()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t DSBase_Nominal::GetEntry(Long64_t entry)
{
    // Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t DSBase_Nominal::LoadTree(Long64_t entry)
{
    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void DSBase_Nominal::HandleDeprecatedBranches() {
    // Not every branch exists across all versions of XAMPP ntuples.
    // If possible, set those associated variables to a dummy value
    // to allow for backwards compatibility.
    if (!fChain->GetListOfBranches()->FindObject("jets_LooseOR")) {
        jets_LooseOR = new std::vector<char>();
        for (unsigned int i=0; i<100; i++) {
            jets_LooseOR->push_back(1);
        }
    }
    if (!fChain->GetListOfBranches()->FindObject("JetWeight")) {
        JetWeight = 1.;
    }
    for (const std::string trig : {"HLT_e24_lhmedium_L1EM20VH",
            "HLT_e26_lhtight_nod0_ivarloose",
            "HLT_e60_lhmedium_nod0",
            "HLT_e140_lhloose_nod0",
            "HLT_e60_lhmedium",
            "HLT_e120_lhloose",
            "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
            "HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo",
            "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",
            "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
            "HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
            "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
            "HLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
            "HLT_mu26_imedium",
            "HLT_mu26_ivarmedium",
            "HLT_mu50",
            "HLT_mu60_0eta105_msonly",
            "HLT_mu20_iloose_L1MU15",
            "HLT_mu40",
            "HLT_mu24_ivarmedium",
            "HLT_mu14_ivarloose_tau35_medium1_tracktwo",
            "HLT_mu14_ivarloose_tau25_medium1_tracktwo",
            "HLT_mu14_tau25_medium1_tracktwo_xe50",
            "HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
            "HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
            "HLT_mu14_tau25_medium1_tracktwoEF_xe50",
            "HLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50",
            "HLT_tau25_medium1_tracktwo",
            "HLT_tau35_medium1_tracktwo",
            "HLT_tau50_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo",
            "HLT_tau80_medium1_tracktwo_L1TAU60",
            "HLT_tau125_medium1_tracktwo",
            "HLT_tau160_medium1_tracktwo",
            "HLT_tau25_medium1_tracktwoEF",
            "HLT_tau25_mediumRNN_tracktwoMVA",
            "HLT_tau60_medium1_tracktwo",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
            "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
            "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
            "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
            "HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
            "HLT_xe90_pufit_L1XE50",
            "HLT_xe100_pufit_L1XE50",
            "HLT_xe100_pufit_L1XE55",
            "HLT_xe110_pufit_L1XE50",
            "HLT_xe110_pufit_L1XE55",
            "HLT_xe110_pufit_xe65_L1XE50",
            "HLT_xe110_pufit_xe70_L1XE50",
            "HLT_xe120_pufit_L1XE50",
            "HLT_xe90_mht_L1XE50",
            "HLT_xe110_mht_L1XE50",
            "HLT_xe70_mht"}) {
                if (!fChain->GetListOfBranches()->FindObject(std::string("taus_TrigMatch" + trig).c_str())) {
                    taus_TrigMatch[trig] = new std::vector<char>();
                    for (unsigned int i=0; i<20; i++) {
                        taus_TrigMatch[trig]->push_back(0);
                    }
                }
                if (!fChain->GetListOfBranches()->FindObject(std::string("Trig" + trig).c_str())) Trig[trig] = 0;
                if (!fChain->GetListOfBranches()->FindObject(std::string("Trig" + trig).c_str())) TrigMatch[trig] = 0;
            }

    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau60_medium1_tracktwoEF").c_str())) TauWeightTrigHLT_tau60_medium1_tracktwoEF = 1.0;
    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau25_medium1_tracktwoEF").c_str())) TauWeightTrigHLT_tau25_medium1_tracktwoEF = 1.0;
    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigtau80_medium1_tracktwoEF_L1TAU60").c_str())) TauWeightTrigtau80_medium1_tracktwoEF_L1TAU60 = 1.0;
    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau60_medium1_tracktwoEF_L1TAU40").c_str())) TauWeightTrigHLT_tau60_medium1_tracktwoEF_L1TAU40 = 1.0;


    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF").c_str())) TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF  = 1.0;
    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau60_medium1_tracktwoEF").c_str())) TauWeightTrigHLT_tau60_medium1_tracktwoEF  = 1.0;
    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo").c_str())) TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo = 1.0;
    if (!fChain->GetListOfBranches()->FindObject(std::string("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo").c_str())) TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo  = 1.0;



}







void DSBase_Nominal::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    GenWeight = 0;
    TauWeight = 0;
    muWeight = 0;
    dilepton_pt = 0;
    dilepton_eta = 0;
    dilepton_phi = 0;
    dilepton_m = 0;
    dilepton_charge = 0;
    dilepton_pdgId = 0;
    electrons_pt = 0;
    electrons_eta = 0;
    electrons_phi = 0;
    electrons_e = 0;
    electrons_MT = 0;
    electrons_charge = 0;
    electrons_d0sig = 0;
    electrons_isol = 0;
    electrons_signal = 0;
    electrons_truthOrigin = 0;
    electrons_truthType = 0;
    electrons_z0sinTheta = 0;
    jets_pt = 0;
    jets_eta = 0;
    jets_phi = 0;
    jets_m = 0;
    jets_Jvt = 0;
    jets_LooseOR = 0;
    jets_MV2c10 = 0;
    jets_NTrks = 0;
    jets_bjet = 0;
    jets_signal = 0;
    muons_pt = 0;
    muons_eta = 0;
    muons_phi = 0;
    muons_e = 0;
    muons_MT = 0;
    muons_charge = 0;
    muons_d0sig = 0;
    muons_isol = 0;
    muons_signal = 0;
    muons_truthOrigin = 0;
    muons_truthType = 0;
    muons_z0sinTheta = 0;
    taus_pt = 0;
    taus_eta = 0;
    taus_phi = 0;
    taus_e = 0;
    taus_BDTEleScore = 0;
    taus_BDTJetScore = 0;
    taus_BDTEleScoreSigTrans = 0;
    taus_BDTJetScoreSigTrans = 0;
    taus_ConeTruthLabelID = 0;
    taus_MT = 0;
    taus_NTrks = 0;
    taus_NTrksJet = 0;
    taus_PartonTruthLabelID = 0;
    taus_Quality = 0;
    taus_Width = 0;
    taus_charge = 0;
    taus_jetAssoc = 0;
    taus_signalID = 0;
    taus_truthOrigin = 0;
    taus_truthType = 0;
    trigger_tau_pt = 0;
    trigger_tau_eta = 0;
    trigger_tau_phi = 0;
    trigger_tau_quality = 0;
    trigger_tau_nChargedTracks = 0;
    trigger_ele_LHVLoose = 0;
    trigger_ele_pt = 0;
    trigger_ele_eta = 0;
    trigger_ele_phi = 0;
    trigger_ele_quality = 0;
    truth_taus_numCharged = 0;
    truth_taus_pt = 0;
    truth_taus_eta = 0;
    truth_taus_phi = 0;
    truth_taus_e = 0;
    truth_electrons_pt = 0;
    truth_electrons_eta = 0;
    truth_electrons_phi = 0;
    truth_electrons_e = 0;
    truth_muons_pt = 0;
    truth_muons_eta = 0;
    truth_muons_phi = 0;
    truth_muons_e = 0;
    truth_jets_pt = 0;
    truth_jets_eta = 0;
    truth_jets_phi = 0;
    truth_jets_e = 0;
    trigtau1_asymmditau_pt  = 0;
    trigtau1_asymmditau_phi = 0;
    trigtau1_asymmditau_eta = 0;
    trigtau1_asymmditau_quality = 0;
    trigtau2_asymmditau_pt  = 0;
    trigtau2_asymmditau_phi = 0;
    trigtau2_asymmditau_eta = 0;
    trigtau2_asymmditau_quality = 0;
    trigtau1_ditaumet_pt                = 0;
    trigtau1_ditaumet_phi   = 0;
    trigtau1_ditaumet_eta   = 0;
    trigtau1_ditaumet_quality = 0;
    trigtau2_ditaumet_pt                = 0;
    trigtau2_ditaumet_phi   = 0;
    trigtau2_ditaumet_eta   = 0;
    trigtau2_ditaumet_quality = 0;


    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    //   fChain->SetMakeClass(1);

    LOG(INFO) << "Setting branch adresses...";

    InitBranch("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
    InitBranch("GenWeight", &GenWeight, &b_GenWeight);
    InitBranch("EleWeight", &EleWeight, &b_EleWeight);
    InitBranch("EleWeightId", &EleWeightId, &b_EleWeightId);
    InitBranch("EleWeightIso", &EleWeightIso, &b_EleWeightIso);
    InitBranch("EleWeightReco", &EleWeightReco, &b_EleWeightReco);
    InitBranch("JetWeight", &JetWeight, &b_JetWeight);
    InitBranch("JetWeightBTag", &JetWeightBTag, &b_JetWeightBTag);
    InitBranch("JetWeightJVT", &JetWeightJVT, &b_JetWeightJVT);
    InitBranch("MuoWeight", &MuoWeight, &b_MuoWeight);
    InitBranch("MuoWeightIsol", &MuoWeightIsol, &b_MuoWeightIsol);
    InitBranch("MuoWeightReco", &MuoWeightReco, &b_MuoWeightReco);
    InitBranch("MuoWeightTTVA", &MuoWeightTTVA, &b_MuoWeightTTVA);
    InitBranch("TauWeight", &TauWeight, &b_TauWeight);
    InitBranch("TauWeightEleOREle", &TauWeightEleOREle, &b_TauWeightEleOREle);
    InitBranch("TauWeightEleORHad", &TauWeightEleORHad, &b_TauWeightEleORHad);
    InitBranch("TauWeightId", &TauWeightId, &b_TauWeightId);
    InitBranch("TauWeightReco", &TauWeightReco, &b_TauWeightReco);

    InitBranch("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60);
    InitBranch("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12);
    InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo",                           &TauWeightTrigHLT_tau35_medium1_tracktwo,                           &b_TauWeightTrigHLT_tau35_medium1_tracktwo);
    InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo",                           &TauWeightTrigHLT_tau25_medium1_tracktwo,                           &b_TauWeightTrigHLT_tau25_medium1_tracktwo);

    InitBranch("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50);
    
    InitBranch("TauWeightTrigHLT_tau60_medium1_tracktwoEF", &TauWeightTrigHLT_tau60_medium1_tracktwoEF, &b_TauWeightTrigHLT_tau60_medium1_tracktwoEF);
    InitBranch("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF", &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF, &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF);
    InitBranch("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo", &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo, &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo);
    InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo);

    InitBranch("muWeight", &muWeight, &b_muWeight);
    InitBranch("EffPt_TauEle", &EffPt_TauEle, &b_EffPt_TauEle);
    InitBranch("EffPt_TauMuo", &EffPt_TauMuo, &b_EffPt_TauMuo);
    InitBranch("Ht_Jet", &Ht_Jet, &b_Ht_Jet);
    InitBranch("Ht_Lep", &Ht_Lep, &b_Ht_Lep);
    InitBranch("Ht_Tau", &Ht_Tau, &b_Ht_Tau);
    InitBranch("LepMT2_max", &LepMT2_max, &b_LepMT2_max);
    InitBranch("LepMT2_min", &LepMT2_min, &b_LepMT2_min);
    InitBranch("MT2_lead2", &MT2_lead2, &b_MT2_lead2);
    InitBranch("MT2_max", &MT2_max, &b_MT2_max);
    InitBranch("MT2_min", &MT2_min, &b_MT2_min);
    InitBranch("Meff", &Meff, &b_Meff);
    InitBranch("Meff_TauTau", &Meff_TauTau, &b_Meff_TauTau);
    InitBranch("MetTST_EleCentral", &MetTST_EleCentral, &b_MetTST_EleCentral);
    InitBranch("MetTST_EleCosMinDeltaPhi", &MetTST_EleCosMinDeltaPhi, &b_MetTST_EleCosMinDeltaPhi);
    InitBranch("MetTST_EleSumCosDeltaPhi", &MetTST_EleSumCosDeltaPhi, &b_MetTST_EleSumCosDeltaPhi);
    InitBranch("MetTST_MuoCentral", &MetTST_MuoCentral, &b_MetTST_MuoCentral);
    InitBranch("MetTST_Significance", &MetTST_Significance, &b_MetTST_Significance);
    InitBranch("MetTST_MuoCosMinDeltaPhi", &MetTST_MuoCosMinDeltaPhi, &b_MetTST_MuoCosMinDeltaPhi);
    InitBranch("MetTST_MuoSumCosDeltaPhi", &MetTST_MuoSumCosDeltaPhi, &b_MetTST_MuoSumCosDeltaPhi);
    InitBranch("RelPt_TauEle", &RelPt_TauEle, &b_RelPt_TauEle);
    InitBranch("RelPt_TauMuo", &RelPt_TauMuo, &b_RelPt_TauMuo);
    InitBranch("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
    InitBranch("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
    InitBranch("corr_avgIntPerX", &corr_avgIntPerX, &b_corr_avgIntPerX);
    InitBranch("SUSYFinalState", &SUSYFinalState, &b_SUSYFinalState);
    InitBranch("Vtx_n", &Vtx_n, &b_Vtx_n);
    InitBranch("mu_density", &mu_density, &b_mu_density);
    InitBranch("n_BJets", &n_BJets, &b_n_BJets);
    InitBranch("n_BaseJets", &n_BaseJets, &b_n_BaseJets);
    InitBranch("n_BaseTau", &n_BaseTau, &b_n_BaseTau);
    InitBranch("n_SignalElec", &n_SignalElec, &b_n_SignalElec);
    InitBranch("n_BaseElec", &n_BaseElec, &b_n_BaseElec);
    InitBranch("n_SignalJets", &n_SignalJets, &b_n_SignalJets);
    InitBranch("n_SignalMuon", &n_SignalMuon, &b_n_SignalMuon);
    InitBranch("n_BaseMuon", &n_BaseMuon, &b_n_BaseMuon);
    InitBranch("n_SignalTau", &n_SignalTau, &b_n_SignalTau);
    InitBranch("OS_BaseTauBaseEle", &OS_BaseTauBaseEle, &b_OS_BaseTauBaseEle);
    InitBranch("OS_BaseTauBaseMuo", &OS_BaseTauBaseMuo, &b_OS_BaseTauBaseMuo);
    InitBranch("OS_BaseTauEle", &OS_BaseTauEle, &b_OS_BaseTauEle);
    InitBranch("OS_BaseTauMuo", &OS_BaseTauMuo, &b_OS_BaseTauMuo);
    InitBranch("OS_EleEle", &OS_EleEle, &b_OS_EleEle);
    InitBranch("OS_MuoMuo", &OS_MuoMuo, &b_OS_MuoMuo);
    InitBranch("OS_TauEle", &OS_TauEle, &b_OS_TauEle);
    InitBranch("OS_TauMuo", &OS_TauMuo, &b_OS_TauMuo);
    InitBranch("OS_TauTau", &OS_TauTau, &b_OS_TauTau);

    InitBranch("n_TruthJets", &n_TruthJets, &b_n_TruthJets);

    // Trigger decisions & matching
    for (const std::string trig : {"HLT_e24_lhmedium_L1EM20VH",
            "HLT_e26_lhtight_nod0_ivarloose",
            "HLT_e60_lhmedium_nod0",
            "HLT_e140_lhloose_nod0",
            "HLT_e60_lhmedium",
            "HLT_e120_lhloose",
            "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
            "HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo",
            "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",                        
            "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
            "HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
            "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
            "HLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
            "HLT_mu26_imedium",
            "HLT_mu26_ivarmedium",
            "HLT_mu50",
            "HLT_mu60_0eta105_msonly",
            "HLT_mu20_iloose_L1MU15",
            "HLT_mu40",
            "HLT_mu24_ivarmedium",
            "HLT_mu14_ivarloose_tau35_medium1_tracktwo",
            "HLT_mu14_ivarloose_tau25_medium1_tracktwo",
            "HLT_mu14_tau25_medium1_tracktwo_xe50",
            "HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
            "HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
            "HLT_mu14_tau25_medium1_tracktwoEF_xe50",
            "HLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50",
            "HLT_tau25_medium1_tracktwo",
            "HLT_tau35_medium1_tracktwo",
            "HLT_tau50_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo",
            "HLT_tau80_medium1_tracktwo_L1TAU60",
            "HLT_tau125_medium1_tracktwo",
            "HLT_tau160_medium1_tracktwo",
            "HLT_tau25_medium1_tracktwoEF",
            "HLT_tau25_mediumRNN_tracktwoMVA",
            "HLT_tau60_medium1_tracktwo",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
            "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
            "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
            "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
            "HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
            "HLT_xe90_pufit_L1XE50",
            "HLT_xe100_pufit_L1XE50",
            "HLT_xe100_pufit_L1XE55",
            "HLT_xe110_pufit_L1XE50",
            "HLT_xe110_pufit_L1XE55",
            "HLT_xe110_pufit_xe65_L1XE50",
            "HLT_xe110_pufit_xe70_L1XE50",
            "HLT_xe120_pufit_L1XE50",
            "HLT_xe90_mht_L1XE50",
            "HLT_xe110_mht_L1XE50",
            "HLT_xe70_mht"}) {
                InitBranch(std::string("Trig" + trig).c_str(), &Trig[trig]);
                InitBranch(std::string("TrigMatch" + trig).c_str(), &TrigMatch[trig]);
            }

    //Init LHE weights
    /*    
    std::string prefix = "GenWeight_LHE_";
    std::cout << "Initializing LHE weight branches" << std::endl;
    for (int i=0; i<LHEweightNames.size(); i++) {
        InitBranch((prefix + LHEweightNames[i]).c_str(), &LHEweightMap[LHEweightNames[i]]);
        
    }
    */
    
    if (treeStem == "Nominal" && isData == false) { 
        
        // LHE weights
        InitBranch("GenWeight_LHE_muR_eq_0p10000E+01_muF_eq_0p20000E+01", &GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p20000E_01, &b_GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p20000E_01);
        InitBranch("GenWeight_LHE_muR_eq_0p10000E+01_muF_eq_0p50000E+00", &GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p50000E_00, &b_GenWeight_LHE_muR_eq_0p10000E_01_muF_eq_0p50000E_00);
        InitBranch("GenWeight_LHE_muR_eq_0p20000E+01_muF_eq_0p10000E+01", &GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p10000E_01, &b_GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p10000E_01);
        InitBranch("GenWeight_LHE_muR_eq_0p20000E+01_muF_eq_0p20000E+01", &GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p20000E_01, &b_GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p20000E_01);
        InitBranch("GenWeight_LHE_muR_eq_0p20000E+01_muF_eq_0p50000E+00", &GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p50000E_00, &b_GenWeight_LHE_muR_eq_0p20000E_01_muF_eq_0p50000E_00);
        InitBranch("GenWeight_LHE_muR_eq_0p50000E+00_muF_eq_0p10000E+01", &GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p10000E_01, &b_GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p10000E_01);
        InitBranch("GenWeight_LHE_muR_eq_0p50000E+00_muF_eq_0p20000E+01", &GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p20000E_01, &b_GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p20000E_01);
        InitBranch("GenWeight_LHE_muR_eq_0p50000E+00_muF_eq_0p50000E+00", &GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p50000E_00, &b_GenWeight_LHE_muR_eq_0p50000E_00_muF_eq_0p50000E_00);
        InitBranch("GenWeight_LHE_pdfset_eq_260001", &GenWeight_LHE_pdfset_eq_260001, &b_GenWeight_LHE_pdfset_eq_260001);
        InitBranch("GenWeight_LHE_pdfset_eq_260002", &GenWeight_LHE_pdfset_eq_260002, &b_GenWeight_LHE_pdfset_eq_260002);
        InitBranch("GenWeight_LHE_pdfset_eq_260003", &GenWeight_LHE_pdfset_eq_260003, &b_GenWeight_LHE_pdfset_eq_260003);
        InitBranch("GenWeight_LHE_pdfset_eq_260004", &GenWeight_LHE_pdfset_eq_260004, &b_GenWeight_LHE_pdfset_eq_260004);
        InitBranch("GenWeight_LHE_pdfset_eq_260005", &GenWeight_LHE_pdfset_eq_260005, &b_GenWeight_LHE_pdfset_eq_260005);
        InitBranch("GenWeight_LHE_pdfset_eq_260006", &GenWeight_LHE_pdfset_eq_260006, &b_GenWeight_LHE_pdfset_eq_260006);
        InitBranch("GenWeight_LHE_pdfset_eq_260007", &GenWeight_LHE_pdfset_eq_260007, &b_GenWeight_LHE_pdfset_eq_260007);
        InitBranch("GenWeight_LHE_pdfset_eq_260008", &GenWeight_LHE_pdfset_eq_260008, &b_GenWeight_LHE_pdfset_eq_260008);
        InitBranch("GenWeight_LHE_pdfset_eq_260009", &GenWeight_LHE_pdfset_eq_260009, &b_GenWeight_LHE_pdfset_eq_260009);
        InitBranch("GenWeight_LHE_pdfset_eq_260010", &GenWeight_LHE_pdfset_eq_260010, &b_GenWeight_LHE_pdfset_eq_260010);
        InitBranch("GenWeight_LHE_pdfset_eq_260011", &GenWeight_LHE_pdfset_eq_260011, &b_GenWeight_LHE_pdfset_eq_260011);
        InitBranch("GenWeight_LHE_pdfset_eq_260012", &GenWeight_LHE_pdfset_eq_260012, &b_GenWeight_LHE_pdfset_eq_260012);
        InitBranch("GenWeight_LHE_pdfset_eq_260013", &GenWeight_LHE_pdfset_eq_260013, &b_GenWeight_LHE_pdfset_eq_260013);
        InitBranch("GenWeight_LHE_pdfset_eq_260014", &GenWeight_LHE_pdfset_eq_260014, &b_GenWeight_LHE_pdfset_eq_260014);
        InitBranch("GenWeight_LHE_pdfset_eq_260015", &GenWeight_LHE_pdfset_eq_260015, &b_GenWeight_LHE_pdfset_eq_260015);
        InitBranch("GenWeight_LHE_pdfset_eq_260016", &GenWeight_LHE_pdfset_eq_260016, &b_GenWeight_LHE_pdfset_eq_260016);
        InitBranch("GenWeight_LHE_pdfset_eq_260017", &GenWeight_LHE_pdfset_eq_260017, &b_GenWeight_LHE_pdfset_eq_260017);
        InitBranch("GenWeight_LHE_pdfset_eq_260018", &GenWeight_LHE_pdfset_eq_260018, &b_GenWeight_LHE_pdfset_eq_260018);
        InitBranch("GenWeight_LHE_pdfset_eq_260019", &GenWeight_LHE_pdfset_eq_260019, &b_GenWeight_LHE_pdfset_eq_260019);
        InitBranch("GenWeight_LHE_pdfset_eq_260020", &GenWeight_LHE_pdfset_eq_260020, &b_GenWeight_LHE_pdfset_eq_260020);
        InitBranch("GenWeight_LHE_pdfset_eq_260021", &GenWeight_LHE_pdfset_eq_260021, &b_GenWeight_LHE_pdfset_eq_260021);
        InitBranch("GenWeight_LHE_pdfset_eq_260022", &GenWeight_LHE_pdfset_eq_260022, &b_GenWeight_LHE_pdfset_eq_260022);
        InitBranch("GenWeight_LHE_pdfset_eq_260023", &GenWeight_LHE_pdfset_eq_260023, &b_GenWeight_LHE_pdfset_eq_260023);
        InitBranch("GenWeight_LHE_pdfset_eq_260024", &GenWeight_LHE_pdfset_eq_260024, &b_GenWeight_LHE_pdfset_eq_260024);
        InitBranch("GenWeight_LHE_pdfset_eq_260025", &GenWeight_LHE_pdfset_eq_260025, &b_GenWeight_LHE_pdfset_eq_260025);
        InitBranch("GenWeight_LHE_pdfset_eq_260026", &GenWeight_LHE_pdfset_eq_260026, &b_GenWeight_LHE_pdfset_eq_260026);
        InitBranch("GenWeight_LHE_pdfset_eq_260027", &GenWeight_LHE_pdfset_eq_260027, &b_GenWeight_LHE_pdfset_eq_260027);
        InitBranch("GenWeight_LHE_pdfset_eq_260028", &GenWeight_LHE_pdfset_eq_260028, &b_GenWeight_LHE_pdfset_eq_260028);
        InitBranch("GenWeight_LHE_pdfset_eq_260029", &GenWeight_LHE_pdfset_eq_260029, &b_GenWeight_LHE_pdfset_eq_260029);
        InitBranch("GenWeight_LHE_pdfset_eq_260030", &GenWeight_LHE_pdfset_eq_260030, &b_GenWeight_LHE_pdfset_eq_260030);
        InitBranch("GenWeight_LHE_pdfset_eq_260031", &GenWeight_LHE_pdfset_eq_260031, &b_GenWeight_LHE_pdfset_eq_260031);
        InitBranch("GenWeight_LHE_pdfset_eq_260032", &GenWeight_LHE_pdfset_eq_260032, &b_GenWeight_LHE_pdfset_eq_260032);
        InitBranch("GenWeight_LHE_pdfset_eq_260033", &GenWeight_LHE_pdfset_eq_260033, &b_GenWeight_LHE_pdfset_eq_260033);
        InitBranch("GenWeight_LHE_pdfset_eq_260034", &GenWeight_LHE_pdfset_eq_260034, &b_GenWeight_LHE_pdfset_eq_260034);
        InitBranch("GenWeight_LHE_pdfset_eq_260035", &GenWeight_LHE_pdfset_eq_260035, &b_GenWeight_LHE_pdfset_eq_260035);
        InitBranch("GenWeight_LHE_pdfset_eq_260036", &GenWeight_LHE_pdfset_eq_260036, &b_GenWeight_LHE_pdfset_eq_260036);
        InitBranch("GenWeight_LHE_pdfset_eq_260037", &GenWeight_LHE_pdfset_eq_260037, &b_GenWeight_LHE_pdfset_eq_260037);
        InitBranch("GenWeight_LHE_pdfset_eq_260038", &GenWeight_LHE_pdfset_eq_260038, &b_GenWeight_LHE_pdfset_eq_260038);
        InitBranch("GenWeight_LHE_pdfset_eq_260039", &GenWeight_LHE_pdfset_eq_260039, &b_GenWeight_LHE_pdfset_eq_260039);
        InitBranch("GenWeight_LHE_pdfset_eq_260040", &GenWeight_LHE_pdfset_eq_260040, &b_GenWeight_LHE_pdfset_eq_260040);
        InitBranch("GenWeight_LHE_pdfset_eq_260041", &GenWeight_LHE_pdfset_eq_260041, &b_GenWeight_LHE_pdfset_eq_260041);
        InitBranch("GenWeight_LHE_pdfset_eq_260042", &GenWeight_LHE_pdfset_eq_260042, &b_GenWeight_LHE_pdfset_eq_260042);
        InitBranch("GenWeight_LHE_pdfset_eq_260043", &GenWeight_LHE_pdfset_eq_260043, &b_GenWeight_LHE_pdfset_eq_260043);
        InitBranch("GenWeight_LHE_pdfset_eq_260044", &GenWeight_LHE_pdfset_eq_260044, &b_GenWeight_LHE_pdfset_eq_260044);
        InitBranch("GenWeight_LHE_pdfset_eq_260045", &GenWeight_LHE_pdfset_eq_260045, &b_GenWeight_LHE_pdfset_eq_260045);
        InitBranch("GenWeight_LHE_pdfset_eq_260046", &GenWeight_LHE_pdfset_eq_260046, &b_GenWeight_LHE_pdfset_eq_260046);
        InitBranch("GenWeight_LHE_pdfset_eq_260047", &GenWeight_LHE_pdfset_eq_260047, &b_GenWeight_LHE_pdfset_eq_260047);
        InitBranch("GenWeight_LHE_pdfset_eq_260048", &GenWeight_LHE_pdfset_eq_260048, &b_GenWeight_LHE_pdfset_eq_260048);
        InitBranch("GenWeight_LHE_pdfset_eq_260049", &GenWeight_LHE_pdfset_eq_260049, &b_GenWeight_LHE_pdfset_eq_260049);
        InitBranch("GenWeight_LHE_pdfset_eq_260050", &GenWeight_LHE_pdfset_eq_260050, &b_GenWeight_LHE_pdfset_eq_260050);
        InitBranch("GenWeight_LHE_pdfset_eq_260051", &GenWeight_LHE_pdfset_eq_260051, &b_GenWeight_LHE_pdfset_eq_260051);
        InitBranch("GenWeight_LHE_pdfset_eq_260052", &GenWeight_LHE_pdfset_eq_260052, &b_GenWeight_LHE_pdfset_eq_260052);
        InitBranch("GenWeight_LHE_pdfset_eq_260053", &GenWeight_LHE_pdfset_eq_260053, &b_GenWeight_LHE_pdfset_eq_260053);
        InitBranch("GenWeight_LHE_pdfset_eq_260054", &GenWeight_LHE_pdfset_eq_260054, &b_GenWeight_LHE_pdfset_eq_260054);
        InitBranch("GenWeight_LHE_pdfset_eq_260055", &GenWeight_LHE_pdfset_eq_260055, &b_GenWeight_LHE_pdfset_eq_260055);
        InitBranch("GenWeight_LHE_pdfset_eq_260056", &GenWeight_LHE_pdfset_eq_260056, &b_GenWeight_LHE_pdfset_eq_260056);
        InitBranch("GenWeight_LHE_pdfset_eq_260057", &GenWeight_LHE_pdfset_eq_260057, &b_GenWeight_LHE_pdfset_eq_260057);
        InitBranch("GenWeight_LHE_pdfset_eq_260058", &GenWeight_LHE_pdfset_eq_260058, &b_GenWeight_LHE_pdfset_eq_260058);
        InitBranch("GenWeight_LHE_pdfset_eq_260059", &GenWeight_LHE_pdfset_eq_260059, &b_GenWeight_LHE_pdfset_eq_260059);
        InitBranch("GenWeight_LHE_pdfset_eq_260060", &GenWeight_LHE_pdfset_eq_260060, &b_GenWeight_LHE_pdfset_eq_260060);
        InitBranch("GenWeight_LHE_pdfset_eq_260061", &GenWeight_LHE_pdfset_eq_260061, &b_GenWeight_LHE_pdfset_eq_260061);
        InitBranch("GenWeight_LHE_pdfset_eq_260062", &GenWeight_LHE_pdfset_eq_260062, &b_GenWeight_LHE_pdfset_eq_260062);
        InitBranch("GenWeight_LHE_pdfset_eq_260063", &GenWeight_LHE_pdfset_eq_260063, &b_GenWeight_LHE_pdfset_eq_260063);
        InitBranch("GenWeight_LHE_pdfset_eq_260064", &GenWeight_LHE_pdfset_eq_260064, &b_GenWeight_LHE_pdfset_eq_260064);
        InitBranch("GenWeight_LHE_pdfset_eq_260065", &GenWeight_LHE_pdfset_eq_260065, &b_GenWeight_LHE_pdfset_eq_260065);
        InitBranch("GenWeight_LHE_pdfset_eq_260066", &GenWeight_LHE_pdfset_eq_260066, &b_GenWeight_LHE_pdfset_eq_260066);
        InitBranch("GenWeight_LHE_pdfset_eq_260067", &GenWeight_LHE_pdfset_eq_260067, &b_GenWeight_LHE_pdfset_eq_260067);
        InitBranch("GenWeight_LHE_pdfset_eq_260068", &GenWeight_LHE_pdfset_eq_260068, &b_GenWeight_LHE_pdfset_eq_260068);
        InitBranch("GenWeight_LHE_pdfset_eq_260069", &GenWeight_LHE_pdfset_eq_260069, &b_GenWeight_LHE_pdfset_eq_260069);
        InitBranch("GenWeight_LHE_pdfset_eq_260070", &GenWeight_LHE_pdfset_eq_260070, &b_GenWeight_LHE_pdfset_eq_260070);
        InitBranch("GenWeight_LHE_pdfset_eq_260071", &GenWeight_LHE_pdfset_eq_260071, &b_GenWeight_LHE_pdfset_eq_260071);
        InitBranch("GenWeight_LHE_pdfset_eq_260072", &GenWeight_LHE_pdfset_eq_260072, &b_GenWeight_LHE_pdfset_eq_260072);
        InitBranch("GenWeight_LHE_pdfset_eq_260073", &GenWeight_LHE_pdfset_eq_260073, &b_GenWeight_LHE_pdfset_eq_260073);
        InitBranch("GenWeight_LHE_pdfset_eq_260074", &GenWeight_LHE_pdfset_eq_260074, &b_GenWeight_LHE_pdfset_eq_260074);
        InitBranch("GenWeight_LHE_pdfset_eq_260075", &GenWeight_LHE_pdfset_eq_260075, &b_GenWeight_LHE_pdfset_eq_260075);
        InitBranch("GenWeight_LHE_pdfset_eq_260076", &GenWeight_LHE_pdfset_eq_260076, &b_GenWeight_LHE_pdfset_eq_260076);
        InitBranch("GenWeight_LHE_pdfset_eq_260077", &GenWeight_LHE_pdfset_eq_260077, &b_GenWeight_LHE_pdfset_eq_260077);
        InitBranch("GenWeight_LHE_pdfset_eq_260078", &GenWeight_LHE_pdfset_eq_260078, &b_GenWeight_LHE_pdfset_eq_260078);
        InitBranch("GenWeight_LHE_pdfset_eq_260079", &GenWeight_LHE_pdfset_eq_260079, &b_GenWeight_LHE_pdfset_eq_260079);
        InitBranch("GenWeight_LHE_pdfset_eq_260080", &GenWeight_LHE_pdfset_eq_260080, &b_GenWeight_LHE_pdfset_eq_260080);
        InitBranch("GenWeight_LHE_pdfset_eq_260081", &GenWeight_LHE_pdfset_eq_260081, &b_GenWeight_LHE_pdfset_eq_260081);
        InitBranch("GenWeight_LHE_pdfset_eq_260082", &GenWeight_LHE_pdfset_eq_260082, &b_GenWeight_LHE_pdfset_eq_260082);
        InitBranch("GenWeight_LHE_pdfset_eq_260083", &GenWeight_LHE_pdfset_eq_260083, &b_GenWeight_LHE_pdfset_eq_260083);
        InitBranch("GenWeight_LHE_pdfset_eq_260084", &GenWeight_LHE_pdfset_eq_260084, &b_GenWeight_LHE_pdfset_eq_260084);
        InitBranch("GenWeight_LHE_pdfset_eq_260085", &GenWeight_LHE_pdfset_eq_260085, &b_GenWeight_LHE_pdfset_eq_260085);
        InitBranch("GenWeight_LHE_pdfset_eq_260086", &GenWeight_LHE_pdfset_eq_260086, &b_GenWeight_LHE_pdfset_eq_260086);
        InitBranch("GenWeight_LHE_pdfset_eq_260087", &GenWeight_LHE_pdfset_eq_260087, &b_GenWeight_LHE_pdfset_eq_260087);
        InitBranch("GenWeight_LHE_pdfset_eq_260088", &GenWeight_LHE_pdfset_eq_260088, &b_GenWeight_LHE_pdfset_eq_260088);
        InitBranch("GenWeight_LHE_pdfset_eq_260089", &GenWeight_LHE_pdfset_eq_260089, &b_GenWeight_LHE_pdfset_eq_260089);
        InitBranch("GenWeight_LHE_pdfset_eq_260090", &GenWeight_LHE_pdfset_eq_260090, &b_GenWeight_LHE_pdfset_eq_260090);
        InitBranch("GenWeight_LHE_pdfset_eq_260091", &GenWeight_LHE_pdfset_eq_260091, &b_GenWeight_LHE_pdfset_eq_260091);
        InitBranch("GenWeight_LHE_pdfset_eq_260092", &GenWeight_LHE_pdfset_eq_260092, &b_GenWeight_LHE_pdfset_eq_260092);
        InitBranch("GenWeight_LHE_pdfset_eq_260093", &GenWeight_LHE_pdfset_eq_260093, &b_GenWeight_LHE_pdfset_eq_260093);
        InitBranch("GenWeight_LHE_pdfset_eq_260094", &GenWeight_LHE_pdfset_eq_260094, &b_GenWeight_LHE_pdfset_eq_260094);
        InitBranch("GenWeight_LHE_pdfset_eq_260095", &GenWeight_LHE_pdfset_eq_260095, &b_GenWeight_LHE_pdfset_eq_260095);
        InitBranch("GenWeight_LHE_pdfset_eq_260096", &GenWeight_LHE_pdfset_eq_260096, &b_GenWeight_LHE_pdfset_eq_260096);
        InitBranch("GenWeight_LHE_pdfset_eq_260097", &GenWeight_LHE_pdfset_eq_260097, &b_GenWeight_LHE_pdfset_eq_260097);
        InitBranch("GenWeight_LHE_pdfset_eq_260098", &GenWeight_LHE_pdfset_eq_260098, &b_GenWeight_LHE_pdfset_eq_260098);
        InitBranch("GenWeight_LHE_pdfset_eq_260099", &GenWeight_LHE_pdfset_eq_260099, &b_GenWeight_LHE_pdfset_eq_260099);
        InitBranch("GenWeight_LHE_pdfset_eq_260100", &GenWeight_LHE_pdfset_eq_260100, &b_GenWeight_LHE_pdfset_eq_260100);
        InitBranch("GenWeight_LHE_mur_eq_0p5_muf_eq_0p5", &GenWeight_LHE_mur_eq_0p5_muf_eq_0p5, &b_GenWeight_LHE_mur_eq_0p5_muf_eq_0p5);
        InitBranch("GenWeight_LHE_mur_eq_0p5_muf_eq_1", &GenWeight_LHE_mur_eq_0p5_muf_eq_1, &b_GenWeight_LHE_mur_eq_0p5_muf_eq_1);
        InitBranch("GenWeight_LHE_mur_eq_0p5_muf_eq_2", &GenWeight_LHE_mur_eq_0p5_muf_eq_2, &b_GenWeight_LHE_mur_eq_0p5_muf_eq_2);
        InitBranch("GenWeight_LHE_mur_eq_1_muf_eq_0p5", &GenWeight_LHE_mur_eq_1_muf_eq_0p5, &b_GenWeight_LHE_mur_eq_1_muf_eq_0p5);
        InitBranch("GenWeight_LHE_mur_eq_1_muf_eq_2", &GenWeight_LHE_mur_eq_1_muf_eq_2, &b_GenWeight_LHE_mur_eq_1_muf_eq_2);
        InitBranch("GenWeight_LHE_mur_eq_2_muf_eq_0p5", &GenWeight_LHE_mur_eq_2_muf_eq_0p5, &b_GenWeight_LHE_mur_eq_2_muf_eq_0p5);
        InitBranch("GenWeight_LHE_mur_eq_2_muf_eq_1", &GenWeight_LHE_mur_eq_2_muf_eq_1, &b_GenWeight_LHE_mur_eq_2_muf_eq_1);
        InitBranch("GenWeight_LHE_mur_eq_2_muf_eq_2", &GenWeight_LHE_mur_eq_2_muf_eq_2, &b_GenWeight_LHE_mur_eq_2_muf_eq_2);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_CT14", &GenWeight_LHE_0p5muF_0p5muR_CT14, &b_GenWeight_LHE_0p5muF_0p5muR_CT14);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_MMHT", &GenWeight_LHE_0p5muF_0p5muR_MMHT, &b_GenWeight_LHE_0p5muF_0p5muR_MMHT);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15", &GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15, &b_GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15);
        InitBranch("GenWeight_LHE_0p5muF_2muR_CT14", &GenWeight_LHE_0p5muF_2muR_CT14, &b_GenWeight_LHE_0p5muF_2muR_CT14);
        InitBranch("GenWeight_LHE_0p5muF_2muR_MMHT", &GenWeight_LHE_0p5muF_2muR_MMHT, &b_GenWeight_LHE_0p5muF_2muR_MMHT);
        InitBranch("GenWeight_LHE_0p5muF_2muR_PDF4LHC15", &GenWeight_LHE_0p5muF_2muR_PDF4LHC15, &b_GenWeight_LHE_0p5muF_2muR_PDF4LHC15);
        InitBranch("GenWeight_LHE_0p5muF_CT14", &GenWeight_LHE_0p5muF_CT14, &b_GenWeight_LHE_0p5muF_CT14);
        InitBranch("GenWeight_LHE_0p5muF_MMHT", &GenWeight_LHE_0p5muF_MMHT, &b_GenWeight_LHE_0p5muF_MMHT);
        InitBranch("GenWeight_LHE_0p5muF_PDF4LHC15", &GenWeight_LHE_0p5muF_PDF4LHC15, &b_GenWeight_LHE_0p5muF_PDF4LHC15);
        InitBranch("GenWeight_LHE_0p5muR_CT14", &GenWeight_LHE_0p5muR_CT14, &b_GenWeight_LHE_0p5muR_CT14);
        InitBranch("GenWeight_LHE_0p5muR_MMHT", &GenWeight_LHE_0p5muR_MMHT, &b_GenWeight_LHE_0p5muR_MMHT);
        InitBranch("GenWeight_LHE_0p5muR_PDF4LHC15", &GenWeight_LHE_0p5muR_PDF4LHC15, &b_GenWeight_LHE_0p5muR_PDF4LHC15);
        InitBranch("GenWeight_LHE_2muF_0p5muR_CT14", &GenWeight_LHE_2muF_0p5muR_CT14, &b_GenWeight_LHE_2muF_0p5muR_CT14);
        InitBranch("GenWeight_LHE_2muF_0p5muR_MMHT", &GenWeight_LHE_2muF_0p5muR_MMHT, &b_GenWeight_LHE_2muF_0p5muR_MMHT);
        InitBranch("GenWeight_LHE_2muF_0p5muR_PDF4LHC15", &GenWeight_LHE_2muF_0p5muR_PDF4LHC15, &b_GenWeight_LHE_2muF_0p5muR_PDF4LHC15);
        InitBranch("GenWeight_LHE_2muF_2muR_CT14", &GenWeight_LHE_2muF_2muR_CT14, &b_GenWeight_LHE_2muF_2muR_CT14);
        InitBranch("GenWeight_LHE_2muF_2muR_MMHT", &GenWeight_LHE_2muF_2muR_MMHT, &b_GenWeight_LHE_2muF_2muR_MMHT);
        InitBranch("GenWeight_LHE_2muF_2muR_PDF4LHC15", &GenWeight_LHE_2muF_2muR_PDF4LHC15, &b_GenWeight_LHE_2muF_2muR_PDF4LHC15);
        InitBranch("GenWeight_LHE_2muF_CT14", &GenWeight_LHE_2muF_CT14, &b_GenWeight_LHE_2muF_CT14);
        InitBranch("GenWeight_LHE_2muF_MMHT", &GenWeight_LHE_2muF_MMHT, &b_GenWeight_LHE_2muF_MMHT);
        InitBranch("GenWeight_LHE_2muF_PDF4LHC15", &GenWeight_LHE_2muF_PDF4LHC15, &b_GenWeight_LHE_2muF_PDF4LHC15);
        InitBranch("GenWeight_LHE_2muR_CT14", &GenWeight_LHE_2muR_CT14, &b_GenWeight_LHE_2muR_CT14);
        InitBranch("GenWeight_LHE_2muR_MMHT", &GenWeight_LHE_2muR_MMHT, &b_GenWeight_LHE_2muR_MMHT);
        InitBranch("GenWeight_LHE_2muR_PDF4LHC15", &GenWeight_LHE_2muR_PDF4LHC15, &b_GenWeight_LHE_2muR_PDF4LHC15);
        InitBranch("GenWeight_LHE_PDF_set__eq__13165", &GenWeight_LHE_PDF_set__eq__13165, &b_GenWeight_LHE_PDF_set__eq__13165);
        InitBranch("GenWeight_LHE_PDF_set__eq__25200", &GenWeight_LHE_PDF_set__eq__25200, &b_GenWeight_LHE_PDF_set__eq__25200);
        InitBranch("GenWeight_LHE_PDF_set__eq__260001", &GenWeight_LHE_PDF_set__eq__260001, &b_GenWeight_LHE_PDF_set__eq__260001);
        InitBranch("GenWeight_LHE_PDF_set__eq__260002", &GenWeight_LHE_PDF_set__eq__260002, &b_GenWeight_LHE_PDF_set__eq__260002);
        InitBranch("GenWeight_LHE_PDF_set__eq__260003", &GenWeight_LHE_PDF_set__eq__260003, &b_GenWeight_LHE_PDF_set__eq__260003);
        InitBranch("GenWeight_LHE_PDF_set__eq__260004", &GenWeight_LHE_PDF_set__eq__260004, &b_GenWeight_LHE_PDF_set__eq__260004);
        InitBranch("GenWeight_LHE_PDF_set__eq__260005", &GenWeight_LHE_PDF_set__eq__260005, &b_GenWeight_LHE_PDF_set__eq__260005);
        InitBranch("GenWeight_LHE_PDF_set__eq__260006", &GenWeight_LHE_PDF_set__eq__260006, &b_GenWeight_LHE_PDF_set__eq__260006);
        InitBranch("GenWeight_LHE_PDF_set__eq__260007", &GenWeight_LHE_PDF_set__eq__260007, &b_GenWeight_LHE_PDF_set__eq__260007);
        InitBranch("GenWeight_LHE_PDF_set__eq__260008", &GenWeight_LHE_PDF_set__eq__260008, &b_GenWeight_LHE_PDF_set__eq__260008);
        InitBranch("GenWeight_LHE_PDF_set__eq__260009", &GenWeight_LHE_PDF_set__eq__260009, &b_GenWeight_LHE_PDF_set__eq__260009);
        InitBranch("GenWeight_LHE_PDF_set__eq__260010", &GenWeight_LHE_PDF_set__eq__260010, &b_GenWeight_LHE_PDF_set__eq__260010);
        InitBranch("GenWeight_LHE_PDF_set__eq__260011", &GenWeight_LHE_PDF_set__eq__260011, &b_GenWeight_LHE_PDF_set__eq__260011);
        InitBranch("GenWeight_LHE_PDF_set__eq__260012", &GenWeight_LHE_PDF_set__eq__260012, &b_GenWeight_LHE_PDF_set__eq__260012);
        InitBranch("GenWeight_LHE_PDF_set__eq__260013", &GenWeight_LHE_PDF_set__eq__260013, &b_GenWeight_LHE_PDF_set__eq__260013);
        InitBranch("GenWeight_LHE_PDF_set__eq__260014", &GenWeight_LHE_PDF_set__eq__260014, &b_GenWeight_LHE_PDF_set__eq__260014);
        InitBranch("GenWeight_LHE_PDF_set__eq__260015", &GenWeight_LHE_PDF_set__eq__260015, &b_GenWeight_LHE_PDF_set__eq__260015);
        InitBranch("GenWeight_LHE_PDF_set__eq__260016", &GenWeight_LHE_PDF_set__eq__260016, &b_GenWeight_LHE_PDF_set__eq__260016);
        InitBranch("GenWeight_LHE_PDF_set__eq__260017", &GenWeight_LHE_PDF_set__eq__260017, &b_GenWeight_LHE_PDF_set__eq__260017);
        InitBranch("GenWeight_LHE_PDF_set__eq__260018", &GenWeight_LHE_PDF_set__eq__260018, &b_GenWeight_LHE_PDF_set__eq__260018);
        InitBranch("GenWeight_LHE_PDF_set__eq__260019", &GenWeight_LHE_PDF_set__eq__260019, &b_GenWeight_LHE_PDF_set__eq__260019);
        InitBranch("GenWeight_LHE_PDF_set__eq__260020", &GenWeight_LHE_PDF_set__eq__260020, &b_GenWeight_LHE_PDF_set__eq__260020);
        InitBranch("GenWeight_LHE_PDF_set__eq__260021", &GenWeight_LHE_PDF_set__eq__260021, &b_GenWeight_LHE_PDF_set__eq__260021);
        InitBranch("GenWeight_LHE_PDF_set__eq__260022", &GenWeight_LHE_PDF_set__eq__260022, &b_GenWeight_LHE_PDF_set__eq__260022);
        InitBranch("GenWeight_LHE_PDF_set__eq__260023", &GenWeight_LHE_PDF_set__eq__260023, &b_GenWeight_LHE_PDF_set__eq__260023);
        InitBranch("GenWeight_LHE_PDF_set__eq__260024", &GenWeight_LHE_PDF_set__eq__260024, &b_GenWeight_LHE_PDF_set__eq__260024);
        InitBranch("GenWeight_LHE_PDF_set__eq__260025", &GenWeight_LHE_PDF_set__eq__260025, &b_GenWeight_LHE_PDF_set__eq__260025);
        InitBranch("GenWeight_LHE_PDF_set__eq__260026", &GenWeight_LHE_PDF_set__eq__260026, &b_GenWeight_LHE_PDF_set__eq__260026);
        InitBranch("GenWeight_LHE_PDF_set__eq__260027", &GenWeight_LHE_PDF_set__eq__260027, &b_GenWeight_LHE_PDF_set__eq__260027);
        InitBranch("GenWeight_LHE_PDF_set__eq__260028", &GenWeight_LHE_PDF_set__eq__260028, &b_GenWeight_LHE_PDF_set__eq__260028);
        InitBranch("GenWeight_LHE_PDF_set__eq__260029", &GenWeight_LHE_PDF_set__eq__260029, &b_GenWeight_LHE_PDF_set__eq__260029);
        InitBranch("GenWeight_LHE_PDF_set__eq__260030", &GenWeight_LHE_PDF_set__eq__260030, &b_GenWeight_LHE_PDF_set__eq__260030);
        InitBranch("GenWeight_LHE_PDF_set__eq__260031", &GenWeight_LHE_PDF_set__eq__260031, &b_GenWeight_LHE_PDF_set__eq__260031);
        InitBranch("GenWeight_LHE_PDF_set__eq__260032", &GenWeight_LHE_PDF_set__eq__260032, &b_GenWeight_LHE_PDF_set__eq__260032);
        InitBranch("GenWeight_LHE_PDF_set__eq__260033", &GenWeight_LHE_PDF_set__eq__260033, &b_GenWeight_LHE_PDF_set__eq__260033);
        InitBranch("GenWeight_LHE_PDF_set__eq__260034", &GenWeight_LHE_PDF_set__eq__260034, &b_GenWeight_LHE_PDF_set__eq__260034);
        InitBranch("GenWeight_LHE_PDF_set__eq__260035", &GenWeight_LHE_PDF_set__eq__260035, &b_GenWeight_LHE_PDF_set__eq__260035);
        InitBranch("GenWeight_LHE_PDF_set__eq__260036", &GenWeight_LHE_PDF_set__eq__260036, &b_GenWeight_LHE_PDF_set__eq__260036);
        InitBranch("GenWeight_LHE_PDF_set__eq__260037", &GenWeight_LHE_PDF_set__eq__260037, &b_GenWeight_LHE_PDF_set__eq__260037);
        InitBranch("GenWeight_LHE_PDF_set__eq__260038", &GenWeight_LHE_PDF_set__eq__260038, &b_GenWeight_LHE_PDF_set__eq__260038);
        InitBranch("GenWeight_LHE_PDF_set__eq__260039", &GenWeight_LHE_PDF_set__eq__260039, &b_GenWeight_LHE_PDF_set__eq__260039);
        InitBranch("GenWeight_LHE_PDF_set__eq__260040", &GenWeight_LHE_PDF_set__eq__260040, &b_GenWeight_LHE_PDF_set__eq__260040);
        InitBranch("GenWeight_LHE_PDF_set__eq__260041", &GenWeight_LHE_PDF_set__eq__260041, &b_GenWeight_LHE_PDF_set__eq__260041);
        InitBranch("GenWeight_LHE_PDF_set__eq__260042", &GenWeight_LHE_PDF_set__eq__260042, &b_GenWeight_LHE_PDF_set__eq__260042);
        InitBranch("GenWeight_LHE_PDF_set__eq__260043", &GenWeight_LHE_PDF_set__eq__260043, &b_GenWeight_LHE_PDF_set__eq__260043);
        InitBranch("GenWeight_LHE_PDF_set__eq__260044", &GenWeight_LHE_PDF_set__eq__260044, &b_GenWeight_LHE_PDF_set__eq__260044);
        InitBranch("GenWeight_LHE_PDF_set__eq__260045", &GenWeight_LHE_PDF_set__eq__260045, &b_GenWeight_LHE_PDF_set__eq__260045);
        InitBranch("GenWeight_LHE_PDF_set__eq__260046", &GenWeight_LHE_PDF_set__eq__260046, &b_GenWeight_LHE_PDF_set__eq__260046);
        InitBranch("GenWeight_LHE_PDF_set__eq__260047", &GenWeight_LHE_PDF_set__eq__260047, &b_GenWeight_LHE_PDF_set__eq__260047);
        InitBranch("GenWeight_LHE_PDF_set__eq__260048", &GenWeight_LHE_PDF_set__eq__260048, &b_GenWeight_LHE_PDF_set__eq__260048);
        InitBranch("GenWeight_LHE_PDF_set__eq__260049", &GenWeight_LHE_PDF_set__eq__260049, &b_GenWeight_LHE_PDF_set__eq__260049);
        InitBranch("GenWeight_LHE_PDF_set__eq__260050", &GenWeight_LHE_PDF_set__eq__260050, &b_GenWeight_LHE_PDF_set__eq__260050);
        InitBranch("GenWeight_LHE_PDF_set__eq__260051", &GenWeight_LHE_PDF_set__eq__260051, &b_GenWeight_LHE_PDF_set__eq__260051);
        InitBranch("GenWeight_LHE_PDF_set__eq__260052", &GenWeight_LHE_PDF_set__eq__260052, &b_GenWeight_LHE_PDF_set__eq__260052);
        InitBranch("GenWeight_LHE_PDF_set__eq__260053", &GenWeight_LHE_PDF_set__eq__260053, &b_GenWeight_LHE_PDF_set__eq__260053);
        InitBranch("GenWeight_LHE_PDF_set__eq__260054", &GenWeight_LHE_PDF_set__eq__260054, &b_GenWeight_LHE_PDF_set__eq__260054);
        InitBranch("GenWeight_LHE_PDF_set__eq__260055", &GenWeight_LHE_PDF_set__eq__260055, &b_GenWeight_LHE_PDF_set__eq__260055);
        InitBranch("GenWeight_LHE_PDF_set__eq__260056", &GenWeight_LHE_PDF_set__eq__260056, &b_GenWeight_LHE_PDF_set__eq__260056);
        InitBranch("GenWeight_LHE_PDF_set__eq__260057", &GenWeight_LHE_PDF_set__eq__260057, &b_GenWeight_LHE_PDF_set__eq__260057);
        InitBranch("GenWeight_LHE_PDF_set__eq__260058", &GenWeight_LHE_PDF_set__eq__260058, &b_GenWeight_LHE_PDF_set__eq__260058);
        InitBranch("GenWeight_LHE_PDF_set__eq__260059", &GenWeight_LHE_PDF_set__eq__260059, &b_GenWeight_LHE_PDF_set__eq__260059);
        InitBranch("GenWeight_LHE_PDF_set__eq__260060", &GenWeight_LHE_PDF_set__eq__260060, &b_GenWeight_LHE_PDF_set__eq__260060);
        InitBranch("GenWeight_LHE_PDF_set__eq__260061", &GenWeight_LHE_PDF_set__eq__260061, &b_GenWeight_LHE_PDF_set__eq__260061);
        InitBranch("GenWeight_LHE_PDF_set__eq__260062", &GenWeight_LHE_PDF_set__eq__260062, &b_GenWeight_LHE_PDF_set__eq__260062);
        InitBranch("GenWeight_LHE_PDF_set__eq__260063", &GenWeight_LHE_PDF_set__eq__260063, &b_GenWeight_LHE_PDF_set__eq__260063);
        InitBranch("GenWeight_LHE_PDF_set__eq__260064", &GenWeight_LHE_PDF_set__eq__260064, &b_GenWeight_LHE_PDF_set__eq__260064);
        InitBranch("GenWeight_LHE_PDF_set__eq__260065", &GenWeight_LHE_PDF_set__eq__260065, &b_GenWeight_LHE_PDF_set__eq__260065);
        InitBranch("GenWeight_LHE_PDF_set__eq__260066", &GenWeight_LHE_PDF_set__eq__260066, &b_GenWeight_LHE_PDF_set__eq__260066);
        InitBranch("GenWeight_LHE_PDF_set__eq__260067", &GenWeight_LHE_PDF_set__eq__260067, &b_GenWeight_LHE_PDF_set__eq__260067);
        InitBranch("GenWeight_LHE_PDF_set__eq__260068", &GenWeight_LHE_PDF_set__eq__260068, &b_GenWeight_LHE_PDF_set__eq__260068);
        InitBranch("GenWeight_LHE_PDF_set__eq__260069", &GenWeight_LHE_PDF_set__eq__260069, &b_GenWeight_LHE_PDF_set__eq__260069);
        InitBranch("GenWeight_LHE_PDF_set__eq__260070", &GenWeight_LHE_PDF_set__eq__260070, &b_GenWeight_LHE_PDF_set__eq__260070);
        InitBranch("GenWeight_LHE_PDF_set__eq__260071", &GenWeight_LHE_PDF_set__eq__260071, &b_GenWeight_LHE_PDF_set__eq__260071);
        InitBranch("GenWeight_LHE_PDF_set__eq__260072", &GenWeight_LHE_PDF_set__eq__260072, &b_GenWeight_LHE_PDF_set__eq__260072);
        InitBranch("GenWeight_LHE_PDF_set__eq__260073", &GenWeight_LHE_PDF_set__eq__260073, &b_GenWeight_LHE_PDF_set__eq__260073);
        InitBranch("GenWeight_LHE_PDF_set__eq__260074", &GenWeight_LHE_PDF_set__eq__260074, &b_GenWeight_LHE_PDF_set__eq__260074);
        InitBranch("GenWeight_LHE_PDF_set__eq__260075", &GenWeight_LHE_PDF_set__eq__260075, &b_GenWeight_LHE_PDF_set__eq__260075);
        InitBranch("GenWeight_LHE_PDF_set__eq__260076", &GenWeight_LHE_PDF_set__eq__260076, &b_GenWeight_LHE_PDF_set__eq__260076);
        InitBranch("GenWeight_LHE_PDF_set__eq__260077", &GenWeight_LHE_PDF_set__eq__260077, &b_GenWeight_LHE_PDF_set__eq__260077);
        InitBranch("GenWeight_LHE_PDF_set__eq__260078", &GenWeight_LHE_PDF_set__eq__260078, &b_GenWeight_LHE_PDF_set__eq__260078);
        InitBranch("GenWeight_LHE_PDF_set__eq__260079", &GenWeight_LHE_PDF_set__eq__260079, &b_GenWeight_LHE_PDF_set__eq__260079);
        InitBranch("GenWeight_LHE_PDF_set__eq__260080", &GenWeight_LHE_PDF_set__eq__260080, &b_GenWeight_LHE_PDF_set__eq__260080);
        InitBranch("GenWeight_LHE_PDF_set__eq__260081", &GenWeight_LHE_PDF_set__eq__260081, &b_GenWeight_LHE_PDF_set__eq__260081);
        InitBranch("GenWeight_LHE_PDF_set__eq__260082", &GenWeight_LHE_PDF_set__eq__260082, &b_GenWeight_LHE_PDF_set__eq__260082);
        InitBranch("GenWeight_LHE_PDF_set__eq__260083", &GenWeight_LHE_PDF_set__eq__260083, &b_GenWeight_LHE_PDF_set__eq__260083);
        InitBranch("GenWeight_LHE_PDF_set__eq__260084", &GenWeight_LHE_PDF_set__eq__260084, &b_GenWeight_LHE_PDF_set__eq__260084);
        InitBranch("GenWeight_LHE_PDF_set__eq__260085", &GenWeight_LHE_PDF_set__eq__260085, &b_GenWeight_LHE_PDF_set__eq__260085);
        InitBranch("GenWeight_LHE_PDF_set__eq__260086", &GenWeight_LHE_PDF_set__eq__260086, &b_GenWeight_LHE_PDF_set__eq__260086);
        InitBranch("GenWeight_LHE_PDF_set__eq__260087", &GenWeight_LHE_PDF_set__eq__260087, &b_GenWeight_LHE_PDF_set__eq__260087);
        InitBranch("GenWeight_LHE_PDF_set__eq__260088", &GenWeight_LHE_PDF_set__eq__260088, &b_GenWeight_LHE_PDF_set__eq__260088);
        InitBranch("GenWeight_LHE_PDF_set__eq__260089", &GenWeight_LHE_PDF_set__eq__260089, &b_GenWeight_LHE_PDF_set__eq__260089);
        InitBranch("GenWeight_LHE_PDF_set__eq__260090", &GenWeight_LHE_PDF_set__eq__260090, &b_GenWeight_LHE_PDF_set__eq__260090);
        InitBranch("GenWeight_LHE_PDF_set__eq__260091", &GenWeight_LHE_PDF_set__eq__260091, &b_GenWeight_LHE_PDF_set__eq__260091);
        InitBranch("GenWeight_LHE_PDF_set__eq__260092", &GenWeight_LHE_PDF_set__eq__260092, &b_GenWeight_LHE_PDF_set__eq__260092);
        InitBranch("GenWeight_LHE_PDF_set__eq__260093", &GenWeight_LHE_PDF_set__eq__260093, &b_GenWeight_LHE_PDF_set__eq__260093);
        InitBranch("GenWeight_LHE_PDF_set__eq__260094", &GenWeight_LHE_PDF_set__eq__260094, &b_GenWeight_LHE_PDF_set__eq__260094);
        InitBranch("GenWeight_LHE_PDF_set__eq__260095", &GenWeight_LHE_PDF_set__eq__260095, &b_GenWeight_LHE_PDF_set__eq__260095);
        InitBranch("GenWeight_LHE_PDF_set__eq__260096", &GenWeight_LHE_PDF_set__eq__260096, &b_GenWeight_LHE_PDF_set__eq__260096);
        InitBranch("GenWeight_LHE_PDF_set__eq__260097", &GenWeight_LHE_PDF_set__eq__260097, &b_GenWeight_LHE_PDF_set__eq__260097);
        InitBranch("GenWeight_LHE_PDF_set__eq__260098", &GenWeight_LHE_PDF_set__eq__260098, &b_GenWeight_LHE_PDF_set__eq__260098);
        InitBranch("GenWeight_LHE_PDF_set__eq__260099", &GenWeight_LHE_PDF_set__eq__260099, &b_GenWeight_LHE_PDF_set__eq__260099);
        InitBranch("GenWeight_LHE_PDF_set__eq__260100", &GenWeight_LHE_PDF_set__eq__260100, &b_GenWeight_LHE_PDF_set__eq__260100);
        InitBranch("GenWeight_LHE_PDF_set__eq__90900", &GenWeight_LHE_PDF_set__eq__90900, &b_GenWeight_LHE_PDF_set__eq__90900);
        InitBranch("GenWeight_LHE_PDF_set__eq__90901", &GenWeight_LHE_PDF_set__eq__90901, &b_GenWeight_LHE_PDF_set__eq__90901);
        InitBranch("GenWeight_LHE_PDF_set__eq__90902", &GenWeight_LHE_PDF_set__eq__90902, &b_GenWeight_LHE_PDF_set__eq__90902);
        InitBranch("GenWeight_LHE_PDF_set__eq__90903", &GenWeight_LHE_PDF_set__eq__90903, &b_GenWeight_LHE_PDF_set__eq__90903);
        InitBranch("GenWeight_LHE_PDF_set__eq__90904", &GenWeight_LHE_PDF_set__eq__90904, &b_GenWeight_LHE_PDF_set__eq__90904);
        InitBranch("GenWeight_LHE_PDF_set__eq__90905", &GenWeight_LHE_PDF_set__eq__90905, &b_GenWeight_LHE_PDF_set__eq__90905);
        InitBranch("GenWeight_LHE_PDF_set__eq__90906", &GenWeight_LHE_PDF_set__eq__90906, &b_GenWeight_LHE_PDF_set__eq__90906);
        InitBranch("GenWeight_LHE_PDF_set__eq__90907", &GenWeight_LHE_PDF_set__eq__90907, &b_GenWeight_LHE_PDF_set__eq__90907);
        InitBranch("GenWeight_LHE_PDF_set__eq__90908", &GenWeight_LHE_PDF_set__eq__90908, &b_GenWeight_LHE_PDF_set__eq__90908);
        InitBranch("GenWeight_LHE_PDF_set__eq__90909", &GenWeight_LHE_PDF_set__eq__90909, &b_GenWeight_LHE_PDF_set__eq__90909);
        InitBranch("GenWeight_LHE_PDF_set__eq__90910", &GenWeight_LHE_PDF_set__eq__90910, &b_GenWeight_LHE_PDF_set__eq__90910);
        InitBranch("GenWeight_LHE_PDF_set__eq__90911", &GenWeight_LHE_PDF_set__eq__90911, &b_GenWeight_LHE_PDF_set__eq__90911);
        InitBranch("GenWeight_LHE_PDF_set__eq__90912", &GenWeight_LHE_PDF_set__eq__90912, &b_GenWeight_LHE_PDF_set__eq__90912);
        InitBranch("GenWeight_LHE_PDF_set__eq__90913", &GenWeight_LHE_PDF_set__eq__90913, &b_GenWeight_LHE_PDF_set__eq__90913);
        InitBranch("GenWeight_LHE_PDF_set__eq__90914", &GenWeight_LHE_PDF_set__eq__90914, &b_GenWeight_LHE_PDF_set__eq__90914);
        InitBranch("GenWeight_LHE_PDF_set__eq__90915", &GenWeight_LHE_PDF_set__eq__90915, &b_GenWeight_LHE_PDF_set__eq__90915);
        InitBranch("GenWeight_LHE_PDF_set__eq__90916", &GenWeight_LHE_PDF_set__eq__90916, &b_GenWeight_LHE_PDF_set__eq__90916);
        InitBranch("GenWeight_LHE_PDF_set__eq__90917", &GenWeight_LHE_PDF_set__eq__90917, &b_GenWeight_LHE_PDF_set__eq__90917);
        InitBranch("GenWeight_LHE_PDF_set__eq__90918", &GenWeight_LHE_PDF_set__eq__90918, &b_GenWeight_LHE_PDF_set__eq__90918);
        InitBranch("GenWeight_LHE_PDF_set__eq__90919", &GenWeight_LHE_PDF_set__eq__90919, &b_GenWeight_LHE_PDF_set__eq__90919);
        InitBranch("GenWeight_LHE_PDF_set__eq__90920", &GenWeight_LHE_PDF_set__eq__90920, &b_GenWeight_LHE_PDF_set__eq__90920);
        InitBranch("GenWeight_LHE_PDF_set__eq__90921", &GenWeight_LHE_PDF_set__eq__90921, &b_GenWeight_LHE_PDF_set__eq__90921);
        InitBranch("GenWeight_LHE_PDF_set__eq__90922", &GenWeight_LHE_PDF_set__eq__90922, &b_GenWeight_LHE_PDF_set__eq__90922);
        InitBranch("GenWeight_LHE_PDF_set__eq__90923", &GenWeight_LHE_PDF_set__eq__90923, &b_GenWeight_LHE_PDF_set__eq__90923);
        InitBranch("GenWeight_LHE_PDF_set__eq__90924", &GenWeight_LHE_PDF_set__eq__90924, &b_GenWeight_LHE_PDF_set__eq__90924);
        InitBranch("GenWeight_LHE_PDF_set__eq__90925", &GenWeight_LHE_PDF_set__eq__90925, &b_GenWeight_LHE_PDF_set__eq__90925);
        InitBranch("GenWeight_LHE_PDF_set__eq__90926", &GenWeight_LHE_PDF_set__eq__90926, &b_GenWeight_LHE_PDF_set__eq__90926);
        InitBranch("GenWeight_LHE_PDF_set__eq__90927", &GenWeight_LHE_PDF_set__eq__90927, &b_GenWeight_LHE_PDF_set__eq__90927);
        InitBranch("GenWeight_LHE_PDF_set__eq__90928", &GenWeight_LHE_PDF_set__eq__90928, &b_GenWeight_LHE_PDF_set__eq__90928);
        InitBranch("GenWeight_LHE_PDF_set__eq__90929", &GenWeight_LHE_PDF_set__eq__90929, &b_GenWeight_LHE_PDF_set__eq__90929);
        InitBranch("GenWeight_LHE_PDF_set__eq__90930", &GenWeight_LHE_PDF_set__eq__90930, &b_GenWeight_LHE_PDF_set__eq__90930);
        InitBranch("GenWeight_LHE_Var3cDown", &GenWeight_LHE_Var3cDown, &b_GenWeight_LHE_Var3cDown);
        InitBranch("GenWeight_LHE_Var3cUp", &GenWeight_LHE_Var3cUp, &b_GenWeight_LHE_Var3cUp);
        InitBranch("GenWeight_LHE_hardHi", &GenWeight_LHE_hardHi, &b_GenWeight_LHE_hardHi);
        InitBranch("GenWeight_LHE_hardLo", &GenWeight_LHE_hardLo, &b_GenWeight_LHE_hardLo);
        InitBranch("GenWeight_LHE_isrPDFminus", &GenWeight_LHE_isrPDFminus, &b_GenWeight_LHE_isrPDFminus);
        InitBranch("GenWeight_LHE_isrPDFplus", &GenWeight_LHE_isrPDFplus, &b_GenWeight_LHE_isrPDFplus);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_0p5", &GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_0p5, &b_GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_0p5);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_2p0", &GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_2p0, &b_GenWeight_LHE_isrmuRfac_eq_0p5_fsrmuRfac_eq_2p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_0p625_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p625_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_0p625_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_0p75_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p75_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_0p75_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_0p875_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_0p875_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_0p875_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p5", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p5, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p5);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p625", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p625, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p625);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p75", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p75, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p75);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p875", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p875, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_0p875);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p25", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p25, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p25);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p5", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p5, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p5);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p75", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p75, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_1p75);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_2p0", &GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_2p0, &b_GenWeight_LHE_isrmuRfac_eq_1p0_fsrmuRfac_eq_2p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p25_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_1p25_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_1p25_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p5_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_1p5_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_1p5_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_1p75_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_1p75_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_1p75_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_0p5", &GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_0p5, &b_GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_0p5);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_1p0", &GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_1p0, &b_GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_1p0);
        InitBranch("GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_2p0", &GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_2p0, &b_GenWeight_LHE_isrmuRfac_eq_2p0_fsrmuRfac_eq_2p0);
        InitBranch("GenWeight_LHE_muR__eq__0p50_muF__eq__0p50", &GenWeight_LHE_muR__eq__0p50_muF__eq__0p50, &b_GenWeight_LHE_muR__eq__0p50_muF__eq__0p50);
        InitBranch("GenWeight_LHE_muR__eq__0p50_muF__eq__1p00", &GenWeight_LHE_muR__eq__0p50_muF__eq__1p00, &b_GenWeight_LHE_muR__eq__0p50_muF__eq__1p00);
        InitBranch("GenWeight_LHE_muR__eq__0p50_muF__eq__2p00", &GenWeight_LHE_muR__eq__0p50_muF__eq__2p00, &b_GenWeight_LHE_muR__eq__0p50_muF__eq__2p00);
        InitBranch("GenWeight_LHE_muR__eq__1p00_muF__eq__0p50", &GenWeight_LHE_muR__eq__1p00_muF__eq__0p50, &b_GenWeight_LHE_muR__eq__1p00_muF__eq__0p50);
        InitBranch("GenWeight_LHE_muR__eq__1p00_muF__eq__2p00", &GenWeight_LHE_muR__eq__1p00_muF__eq__2p00, &b_GenWeight_LHE_muR__eq__1p00_muF__eq__2p00);
        InitBranch("GenWeight_LHE_muR__eq__2p00_muF__eq__0p50", &GenWeight_LHE_muR__eq__2p00_muF__eq__0p50, &b_GenWeight_LHE_muR__eq__2p00_muF__eq__0p50);
        InitBranch("GenWeight_LHE_muR__eq__2p00_muF__eq__1p00", &GenWeight_LHE_muR__eq__2p00_muF__eq__1p00, &b_GenWeight_LHE_muR__eq__2p00_muF__eq__1p00);
        InitBranch("GenWeight_LHE_muR__eq__2p00_muF__eq__2p00", &GenWeight_LHE_muR__eq__2p00_muF__eq__2p00, &b_GenWeight_LHE_muR__eq__2p00_muF__eq__2p00);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muF_0p5muR_NNPDF31_NLO_0118, &b_GenWeight_LHE_0p5muF_0p5muR_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0117", &GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0117, &b_GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0119", &GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0119, &b_GenWeight_LHE_0p5muF_0p5muR_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15_NLO_30, &b_GenWeight_LHE_0p5muF_0p5muR_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_0p5muF_2muR_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muF_2muR_NNPDF31_NLO_0118, &b_GenWeight_LHE_0p5muF_2muR_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0117", &GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0117, &b_GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0119", &GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0119, &b_GenWeight_LHE_0p5muF_2muR_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_0p5muF_2muR_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muF_2muR_PDF4LHC15_NLO_30, &b_GenWeight_LHE_0p5muF_2muR_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_0p5muF_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muF_NNPDF31_NLO_0118, &b_GenWeight_LHE_0p5muF_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_0p5muF_NNPDF_NLO_0117", &GenWeight_LHE_0p5muF_NNPDF_NLO_0117, &b_GenWeight_LHE_0p5muF_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_0p5muF_NNPDF_NLO_0119", &GenWeight_LHE_0p5muF_NNPDF_NLO_0119, &b_GenWeight_LHE_0p5muF_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_0p5muF_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muF_PDF4LHC15_NLO_30, &b_GenWeight_LHE_0p5muF_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_0p5muR_NNPDF31_NLO_0118", &GenWeight_LHE_0p5muR_NNPDF31_NLO_0118, &b_GenWeight_LHE_0p5muR_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_0p5muR_NNPDF_0117", &GenWeight_LHE_0p5muR_NNPDF_0117, &b_GenWeight_LHE_0p5muR_NNPDF_0117);
        InitBranch("GenWeight_LHE_0p5muR_NNPDF_NLO_0119", &GenWeight_LHE_0p5muR_NNPDF_NLO_0119, &b_GenWeight_LHE_0p5muR_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_0p5muR_PDF4LHC15_NLO_30", &GenWeight_LHE_0p5muR_PDF4LHC15_NLO_30, &b_GenWeight_LHE_0p5muR_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_2muF_0p5muR_NNPDF31_NLO_0118", &GenWeight_LHE_2muF_0p5muR_NNPDF31_NLO_0118, &b_GenWeight_LHE_2muF_0p5muR_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0117", &GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0117, &b_GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0119", &GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0119, &b_GenWeight_LHE_2muF_0p5muR_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_2muF_0p5muR_PDF4LHC15_NLO_30", &GenWeight_LHE_2muF_0p5muR_PDF4LHC15_NLO_30, &b_GenWeight_LHE_2muF_0p5muR_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_2muF_2muR_NNPDF31_NLO_0118", &GenWeight_LHE_2muF_2muR_NNPDF31_NLO_0118, &b_GenWeight_LHE_2muF_2muR_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_2muF_2muR_NNPDF_NLO_0117", &GenWeight_LHE_2muF_2muR_NNPDF_NLO_0117, &b_GenWeight_LHE_2muF_2muR_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_2muF_2muR_NNPDF_NLO_0119", &GenWeight_LHE_2muF_2muR_NNPDF_NLO_0119, &b_GenWeight_LHE_2muF_2muR_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_2muF_2muR_PDF4LHC15_NLO_30", &GenWeight_LHE_2muF_2muR_PDF4LHC15_NLO_30, &b_GenWeight_LHE_2muF_2muR_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_2muF_NNPDF31_NLO_0118", &GenWeight_LHE_2muF_NNPDF31_NLO_0118, &b_GenWeight_LHE_2muF_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_2muF_NNPDF_NLO_0117", &GenWeight_LHE_2muF_NNPDF_NLO_0117, &b_GenWeight_LHE_2muF_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_2muF_NNPDF_NLO_0119", &GenWeight_LHE_2muF_NNPDF_NLO_0119, &b_GenWeight_LHE_2muF_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_2muF_PDF4LHC15_NLO_30", &GenWeight_LHE_2muF_PDF4LHC15_NLO_30, &b_GenWeight_LHE_2muF_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_2muR_NNPDF31_NLO_0118", &GenWeight_LHE_2muR_NNPDF31_NLO_0118, &b_GenWeight_LHE_2muR_NNPDF31_NLO_0118);
        InitBranch("GenWeight_LHE_2muR_NNPDF_NLO_0117", &GenWeight_LHE_2muR_NNPDF_NLO_0117, &b_GenWeight_LHE_2muR_NNPDF_NLO_0117);
        InitBranch("GenWeight_LHE_2muR_NNPDF_NLO_0119", &GenWeight_LHE_2muR_NNPDF_NLO_0119, &b_GenWeight_LHE_2muR_NNPDF_NLO_0119);
        InitBranch("GenWeight_LHE_2muR_PDF4LHC15_NLO_30", &GenWeight_LHE_2muR_PDF4LHC15_NLO_30, &b_GenWeight_LHE_2muR_PDF4LHC15_NLO_30);
        InitBranch("GenWeight_LHE_PDF_set__eq__265000", &GenWeight_LHE_PDF_set__eq__265000, &b_GenWeight_LHE_PDF_set__eq__265000);
        InitBranch("GenWeight_LHE_PDF_set__eq__266000", &GenWeight_LHE_PDF_set__eq__266000, &b_GenWeight_LHE_PDF_set__eq__266000);
        InitBranch("GenWeight_LHE_PDF_set__eq__303400", &GenWeight_LHE_PDF_set__eq__303400, &b_GenWeight_LHE_PDF_set__eq__303400);
        InitBranch("GenWeight_LHE_muR__eq__0p5_muF__eq__0p5", &GenWeight_LHE_muR__eq__0p5_muF__eq__0p5, &b_GenWeight_LHE_muR__eq__0p5_muF__eq__0p5);
        InitBranch("GenWeight_LHE_muR__eq__0p5_muF__eq__1p0", &GenWeight_LHE_muR__eq__0p5_muF__eq__1p0, &b_GenWeight_LHE_muR__eq__0p5_muF__eq__1p0);
        InitBranch("GenWeight_LHE_muR__eq__0p5_muF__eq__2p0", &GenWeight_LHE_muR__eq__0p5_muF__eq__2p0, &b_GenWeight_LHE_muR__eq__0p5_muF__eq__2p0);
        InitBranch("GenWeight_LHE_muR__eq__1p0_muF__eq__0p5", &GenWeight_LHE_muR__eq__1p0_muF__eq__0p5, &b_GenWeight_LHE_muR__eq__1p0_muF__eq__0p5);
        InitBranch("GenWeight_LHE_muR__eq__1p0_muF__eq__2p0", &GenWeight_LHE_muR__eq__1p0_muF__eq__2p0, &b_GenWeight_LHE_muR__eq__1p0_muF__eq__2p0);
        InitBranch("GenWeight_LHE_muR__eq__2p0_muF__eq__0p5", &GenWeight_LHE_muR__eq__2p0_muF__eq__0p5, &b_GenWeight_LHE_muR__eq__2p0_muF__eq__0p5);
        InitBranch("GenWeight_LHE_muR__eq__2p0_muF__eq__1p0", &GenWeight_LHE_muR__eq__2p0_muF__eq__1p0, &b_GenWeight_LHE_muR__eq__2p0_muF__eq__1p0);
        InitBranch("GenWeight_LHE_muR__eq__2p0_muF__eq__2p0", &GenWeight_LHE_muR__eq__2p0_muF__eq__2p0, &b_GenWeight_LHE_muR__eq__2p0_muF__eq__2p0);
        InitBranch("GenWeight_LHE_MEWeight", &GenWeight_LHE_MEWeight, &b_GenWeight_LHE_MEWeight);
        InitBranch("GenWeight_LHE_MUR0p5_MUF0p5_PDF261000", &GenWeight_LHE_MUR0p5_MUF0p5_PDF261000, &b_GenWeight_LHE_MUR0p5_MUF0p5_PDF261000);
        InitBranch("GenWeight_LHE_MUR0p5_MUF1_PDF261000", &GenWeight_LHE_MUR0p5_MUF1_PDF261000, &b_GenWeight_LHE_MUR0p5_MUF1_PDF261000);
        InitBranch("GenWeight_LHE_MUR1_MUF0p5_PDF261000", &GenWeight_LHE_MUR1_MUF0p5_PDF261000, &b_GenWeight_LHE_MUR1_MUF0p5_PDF261000);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF13000", &GenWeight_LHE_MUR1_MUF1_PDF13000, &b_GenWeight_LHE_MUR1_MUF1_PDF13000);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF25300", &GenWeight_LHE_MUR1_MUF1_PDF25300, &b_GenWeight_LHE_MUR1_MUF1_PDF25300);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261000", &GenWeight_LHE_MUR1_MUF1_PDF261000, &b_GenWeight_LHE_MUR1_MUF1_PDF261000);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261001", &GenWeight_LHE_MUR1_MUF1_PDF261001, &b_GenWeight_LHE_MUR1_MUF1_PDF261001);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261002", &GenWeight_LHE_MUR1_MUF1_PDF261002, &b_GenWeight_LHE_MUR1_MUF1_PDF261002);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261003", &GenWeight_LHE_MUR1_MUF1_PDF261003, &b_GenWeight_LHE_MUR1_MUF1_PDF261003);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261004", &GenWeight_LHE_MUR1_MUF1_PDF261004, &b_GenWeight_LHE_MUR1_MUF1_PDF261004);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261005", &GenWeight_LHE_MUR1_MUF1_PDF261005, &b_GenWeight_LHE_MUR1_MUF1_PDF261005);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261006", &GenWeight_LHE_MUR1_MUF1_PDF261006, &b_GenWeight_LHE_MUR1_MUF1_PDF261006);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261007", &GenWeight_LHE_MUR1_MUF1_PDF261007, &b_GenWeight_LHE_MUR1_MUF1_PDF261007);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261008", &GenWeight_LHE_MUR1_MUF1_PDF261008, &b_GenWeight_LHE_MUR1_MUF1_PDF261008);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261009", &GenWeight_LHE_MUR1_MUF1_PDF261009, &b_GenWeight_LHE_MUR1_MUF1_PDF261009);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261010", &GenWeight_LHE_MUR1_MUF1_PDF261010, &b_GenWeight_LHE_MUR1_MUF1_PDF261010);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261011", &GenWeight_LHE_MUR1_MUF1_PDF261011, &b_GenWeight_LHE_MUR1_MUF1_PDF261011);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261012", &GenWeight_LHE_MUR1_MUF1_PDF261012, &b_GenWeight_LHE_MUR1_MUF1_PDF261012);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261013", &GenWeight_LHE_MUR1_MUF1_PDF261013, &b_GenWeight_LHE_MUR1_MUF1_PDF261013);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261014", &GenWeight_LHE_MUR1_MUF1_PDF261014, &b_GenWeight_LHE_MUR1_MUF1_PDF261014);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261015", &GenWeight_LHE_MUR1_MUF1_PDF261015, &b_GenWeight_LHE_MUR1_MUF1_PDF261015);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261016", &GenWeight_LHE_MUR1_MUF1_PDF261016, &b_GenWeight_LHE_MUR1_MUF1_PDF261016);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261017", &GenWeight_LHE_MUR1_MUF1_PDF261017, &b_GenWeight_LHE_MUR1_MUF1_PDF261017);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261018", &GenWeight_LHE_MUR1_MUF1_PDF261018, &b_GenWeight_LHE_MUR1_MUF1_PDF261018);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261019", &GenWeight_LHE_MUR1_MUF1_PDF261019, &b_GenWeight_LHE_MUR1_MUF1_PDF261019);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261020", &GenWeight_LHE_MUR1_MUF1_PDF261020, &b_GenWeight_LHE_MUR1_MUF1_PDF261020);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261021", &GenWeight_LHE_MUR1_MUF1_PDF261021, &b_GenWeight_LHE_MUR1_MUF1_PDF261021);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261022", &GenWeight_LHE_MUR1_MUF1_PDF261022, &b_GenWeight_LHE_MUR1_MUF1_PDF261022);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261023", &GenWeight_LHE_MUR1_MUF1_PDF261023, &b_GenWeight_LHE_MUR1_MUF1_PDF261023);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261024", &GenWeight_LHE_MUR1_MUF1_PDF261024, &b_GenWeight_LHE_MUR1_MUF1_PDF261024);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261025", &GenWeight_LHE_MUR1_MUF1_PDF261025, &b_GenWeight_LHE_MUR1_MUF1_PDF261025);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261026", &GenWeight_LHE_MUR1_MUF1_PDF261026, &b_GenWeight_LHE_MUR1_MUF1_PDF261026);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261027", &GenWeight_LHE_MUR1_MUF1_PDF261027, &b_GenWeight_LHE_MUR1_MUF1_PDF261027);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261028", &GenWeight_LHE_MUR1_MUF1_PDF261028, &b_GenWeight_LHE_MUR1_MUF1_PDF261028);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261029", &GenWeight_LHE_MUR1_MUF1_PDF261029, &b_GenWeight_LHE_MUR1_MUF1_PDF261029);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261030", &GenWeight_LHE_MUR1_MUF1_PDF261030, &b_GenWeight_LHE_MUR1_MUF1_PDF261030);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261031", &GenWeight_LHE_MUR1_MUF1_PDF261031, &b_GenWeight_LHE_MUR1_MUF1_PDF261031);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261032", &GenWeight_LHE_MUR1_MUF1_PDF261032, &b_GenWeight_LHE_MUR1_MUF1_PDF261032);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261033", &GenWeight_LHE_MUR1_MUF1_PDF261033, &b_GenWeight_LHE_MUR1_MUF1_PDF261033);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261034", &GenWeight_LHE_MUR1_MUF1_PDF261034, &b_GenWeight_LHE_MUR1_MUF1_PDF261034);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261035", &GenWeight_LHE_MUR1_MUF1_PDF261035, &b_GenWeight_LHE_MUR1_MUF1_PDF261035);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261036", &GenWeight_LHE_MUR1_MUF1_PDF261036, &b_GenWeight_LHE_MUR1_MUF1_PDF261036);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261037", &GenWeight_LHE_MUR1_MUF1_PDF261037, &b_GenWeight_LHE_MUR1_MUF1_PDF261037);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261038", &GenWeight_LHE_MUR1_MUF1_PDF261038, &b_GenWeight_LHE_MUR1_MUF1_PDF261038);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261039", &GenWeight_LHE_MUR1_MUF1_PDF261039, &b_GenWeight_LHE_MUR1_MUF1_PDF261039);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261040", &GenWeight_LHE_MUR1_MUF1_PDF261040, &b_GenWeight_LHE_MUR1_MUF1_PDF261040);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261041", &GenWeight_LHE_MUR1_MUF1_PDF261041, &b_GenWeight_LHE_MUR1_MUF1_PDF261041);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261042", &GenWeight_LHE_MUR1_MUF1_PDF261042, &b_GenWeight_LHE_MUR1_MUF1_PDF261042);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261043", &GenWeight_LHE_MUR1_MUF1_PDF261043, &b_GenWeight_LHE_MUR1_MUF1_PDF261043);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261044", &GenWeight_LHE_MUR1_MUF1_PDF261044, &b_GenWeight_LHE_MUR1_MUF1_PDF261044);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261045", &GenWeight_LHE_MUR1_MUF1_PDF261045, &b_GenWeight_LHE_MUR1_MUF1_PDF261045);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261046", &GenWeight_LHE_MUR1_MUF1_PDF261046, &b_GenWeight_LHE_MUR1_MUF1_PDF261046);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261047", &GenWeight_LHE_MUR1_MUF1_PDF261047, &b_GenWeight_LHE_MUR1_MUF1_PDF261047);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261048", &GenWeight_LHE_MUR1_MUF1_PDF261048, &b_GenWeight_LHE_MUR1_MUF1_PDF261048);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261049", &GenWeight_LHE_MUR1_MUF1_PDF261049, &b_GenWeight_LHE_MUR1_MUF1_PDF261049);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261050", &GenWeight_LHE_MUR1_MUF1_PDF261050, &b_GenWeight_LHE_MUR1_MUF1_PDF261050);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261051", &GenWeight_LHE_MUR1_MUF1_PDF261051, &b_GenWeight_LHE_MUR1_MUF1_PDF261051);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261052", &GenWeight_LHE_MUR1_MUF1_PDF261052, &b_GenWeight_LHE_MUR1_MUF1_PDF261052);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261053", &GenWeight_LHE_MUR1_MUF1_PDF261053, &b_GenWeight_LHE_MUR1_MUF1_PDF261053);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261054", &GenWeight_LHE_MUR1_MUF1_PDF261054, &b_GenWeight_LHE_MUR1_MUF1_PDF261054);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261055", &GenWeight_LHE_MUR1_MUF1_PDF261055, &b_GenWeight_LHE_MUR1_MUF1_PDF261055);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261056", &GenWeight_LHE_MUR1_MUF1_PDF261056, &b_GenWeight_LHE_MUR1_MUF1_PDF261056);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261057", &GenWeight_LHE_MUR1_MUF1_PDF261057, &b_GenWeight_LHE_MUR1_MUF1_PDF261057);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261058", &GenWeight_LHE_MUR1_MUF1_PDF261058, &b_GenWeight_LHE_MUR1_MUF1_PDF261058);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261059", &GenWeight_LHE_MUR1_MUF1_PDF261059, &b_GenWeight_LHE_MUR1_MUF1_PDF261059);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261060", &GenWeight_LHE_MUR1_MUF1_PDF261060, &b_GenWeight_LHE_MUR1_MUF1_PDF261060);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261061", &GenWeight_LHE_MUR1_MUF1_PDF261061, &b_GenWeight_LHE_MUR1_MUF1_PDF261061);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261062", &GenWeight_LHE_MUR1_MUF1_PDF261062, &b_GenWeight_LHE_MUR1_MUF1_PDF261062);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261063", &GenWeight_LHE_MUR1_MUF1_PDF261063, &b_GenWeight_LHE_MUR1_MUF1_PDF261063);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261064", &GenWeight_LHE_MUR1_MUF1_PDF261064, &b_GenWeight_LHE_MUR1_MUF1_PDF261064);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261065", &GenWeight_LHE_MUR1_MUF1_PDF261065, &b_GenWeight_LHE_MUR1_MUF1_PDF261065);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261066", &GenWeight_LHE_MUR1_MUF1_PDF261066, &b_GenWeight_LHE_MUR1_MUF1_PDF261066);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261067", &GenWeight_LHE_MUR1_MUF1_PDF261067, &b_GenWeight_LHE_MUR1_MUF1_PDF261067);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261068", &GenWeight_LHE_MUR1_MUF1_PDF261068, &b_GenWeight_LHE_MUR1_MUF1_PDF261068);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261069", &GenWeight_LHE_MUR1_MUF1_PDF261069, &b_GenWeight_LHE_MUR1_MUF1_PDF261069);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261070", &GenWeight_LHE_MUR1_MUF1_PDF261070, &b_GenWeight_LHE_MUR1_MUF1_PDF261070);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261071", &GenWeight_LHE_MUR1_MUF1_PDF261071, &b_GenWeight_LHE_MUR1_MUF1_PDF261071);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261072", &GenWeight_LHE_MUR1_MUF1_PDF261072, &b_GenWeight_LHE_MUR1_MUF1_PDF261072);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261073", &GenWeight_LHE_MUR1_MUF1_PDF261073, &b_GenWeight_LHE_MUR1_MUF1_PDF261073);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261074", &GenWeight_LHE_MUR1_MUF1_PDF261074, &b_GenWeight_LHE_MUR1_MUF1_PDF261074);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261075", &GenWeight_LHE_MUR1_MUF1_PDF261075, &b_GenWeight_LHE_MUR1_MUF1_PDF261075);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261076", &GenWeight_LHE_MUR1_MUF1_PDF261076, &b_GenWeight_LHE_MUR1_MUF1_PDF261076);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261077", &GenWeight_LHE_MUR1_MUF1_PDF261077, &b_GenWeight_LHE_MUR1_MUF1_PDF261077);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261078", &GenWeight_LHE_MUR1_MUF1_PDF261078, &b_GenWeight_LHE_MUR1_MUF1_PDF261078);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261079", &GenWeight_LHE_MUR1_MUF1_PDF261079, &b_GenWeight_LHE_MUR1_MUF1_PDF261079);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261080", &GenWeight_LHE_MUR1_MUF1_PDF261080, &b_GenWeight_LHE_MUR1_MUF1_PDF261080);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261081", &GenWeight_LHE_MUR1_MUF1_PDF261081, &b_GenWeight_LHE_MUR1_MUF1_PDF261081);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261082", &GenWeight_LHE_MUR1_MUF1_PDF261082, &b_GenWeight_LHE_MUR1_MUF1_PDF261082);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261083", &GenWeight_LHE_MUR1_MUF1_PDF261083, &b_GenWeight_LHE_MUR1_MUF1_PDF261083);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261084", &GenWeight_LHE_MUR1_MUF1_PDF261084, &b_GenWeight_LHE_MUR1_MUF1_PDF261084);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261085", &GenWeight_LHE_MUR1_MUF1_PDF261085, &b_GenWeight_LHE_MUR1_MUF1_PDF261085);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261086", &GenWeight_LHE_MUR1_MUF1_PDF261086, &b_GenWeight_LHE_MUR1_MUF1_PDF261086);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261087", &GenWeight_LHE_MUR1_MUF1_PDF261087, &b_GenWeight_LHE_MUR1_MUF1_PDF261087);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261088", &GenWeight_LHE_MUR1_MUF1_PDF261088, &b_GenWeight_LHE_MUR1_MUF1_PDF261088);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261089", &GenWeight_LHE_MUR1_MUF1_PDF261089, &b_GenWeight_LHE_MUR1_MUF1_PDF261089);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261090", &GenWeight_LHE_MUR1_MUF1_PDF261090, &b_GenWeight_LHE_MUR1_MUF1_PDF261090);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261091", &GenWeight_LHE_MUR1_MUF1_PDF261091, &b_GenWeight_LHE_MUR1_MUF1_PDF261091);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261092", &GenWeight_LHE_MUR1_MUF1_PDF261092, &b_GenWeight_LHE_MUR1_MUF1_PDF261092);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261093", &GenWeight_LHE_MUR1_MUF1_PDF261093, &b_GenWeight_LHE_MUR1_MUF1_PDF261093);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261094", &GenWeight_LHE_MUR1_MUF1_PDF261094, &b_GenWeight_LHE_MUR1_MUF1_PDF261094);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261095", &GenWeight_LHE_MUR1_MUF1_PDF261095, &b_GenWeight_LHE_MUR1_MUF1_PDF261095);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261096", &GenWeight_LHE_MUR1_MUF1_PDF261096, &b_GenWeight_LHE_MUR1_MUF1_PDF261096);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261097", &GenWeight_LHE_MUR1_MUF1_PDF261097, &b_GenWeight_LHE_MUR1_MUF1_PDF261097);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261098", &GenWeight_LHE_MUR1_MUF1_PDF261098, &b_GenWeight_LHE_MUR1_MUF1_PDF261098);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261099", &GenWeight_LHE_MUR1_MUF1_PDF261099, &b_GenWeight_LHE_MUR1_MUF1_PDF261099);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF261100", &GenWeight_LHE_MUR1_MUF1_PDF261100, &b_GenWeight_LHE_MUR1_MUF1_PDF261100);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF269000", &GenWeight_LHE_MUR1_MUF1_PDF269000, &b_GenWeight_LHE_MUR1_MUF1_PDF269000);
        InitBranch("GenWeight_LHE_MUR1_MUF1_PDF270000", &GenWeight_LHE_MUR1_MUF1_PDF270000, &b_GenWeight_LHE_MUR1_MUF1_PDF270000);
        InitBranch("GenWeight_LHE_MUR1_MUF2_PDF261000", &GenWeight_LHE_MUR1_MUF2_PDF261000, &b_GenWeight_LHE_MUR1_MUF2_PDF261000);
        InitBranch("GenWeight_LHE_MUR2_MUF1_PDF261000", &GenWeight_LHE_MUR2_MUF1_PDF261000, &b_GenWeight_LHE_MUR2_MUF1_PDF261000);
        InitBranch("GenWeight_LHE_MUR2_MUF2_PDF261000", &GenWeight_LHE_MUR2_MUF2_PDF261000, &b_GenWeight_LHE_MUR2_MUF2_PDF261000);
        InitBranch("GenWeight_LHE_NTrials", &GenWeight_LHE_NTrials, &b_GenWeight_LHE_NTrials);
        InitBranch("GenWeight_LHE_WeightNormalisation", &GenWeight_LHE_WeightNormalisation, &b_GenWeight_LHE_WeightNormalisation);
        InitBranch("GenWeight_LHE_PDF_set__eq__11068", &GenWeight_LHE_PDF_set__eq__11068, &b_GenWeight_LHE_PDF_set__eq__11068);
        InitBranch("GenWeight_LHE_PDF_set__eq__90400", &GenWeight_LHE_PDF_set__eq__90400, &b_GenWeight_LHE_PDF_set__eq__90400);
        InitBranch("GenWeight_LHE_PDF_set__eq__90401", &GenWeight_LHE_PDF_set__eq__90401, &b_GenWeight_LHE_PDF_set__eq__90401);
        InitBranch("GenWeight_LHE_PDF_set__eq__90402", &GenWeight_LHE_PDF_set__eq__90402, &b_GenWeight_LHE_PDF_set__eq__90402);
        InitBranch("GenWeight_LHE_PDF_set__eq__90403", &GenWeight_LHE_PDF_set__eq__90403, &b_GenWeight_LHE_PDF_set__eq__90403);
        InitBranch("GenWeight_LHE_PDF_set__eq__90404", &GenWeight_LHE_PDF_set__eq__90404, &b_GenWeight_LHE_PDF_set__eq__90404);
        InitBranch("GenWeight_LHE_PDF_set__eq__90405", &GenWeight_LHE_PDF_set__eq__90405, &b_GenWeight_LHE_PDF_set__eq__90405);
        InitBranch("GenWeight_LHE_PDF_set__eq__90406", &GenWeight_LHE_PDF_set__eq__90406, &b_GenWeight_LHE_PDF_set__eq__90406);
        InitBranch("GenWeight_LHE_PDF_set__eq__90407", &GenWeight_LHE_PDF_set__eq__90407, &b_GenWeight_LHE_PDF_set__eq__90407);
        InitBranch("GenWeight_LHE_PDF_set__eq__90408", &GenWeight_LHE_PDF_set__eq__90408, &b_GenWeight_LHE_PDF_set__eq__90408);
        InitBranch("GenWeight_LHE_PDF_set__eq__90409", &GenWeight_LHE_PDF_set__eq__90409, &b_GenWeight_LHE_PDF_set__eq__90409);
        InitBranch("GenWeight_LHE_PDF_set__eq__90410", &GenWeight_LHE_PDF_set__eq__90410, &b_GenWeight_LHE_PDF_set__eq__90410);
        InitBranch("GenWeight_LHE_PDF_set__eq__90411", &GenWeight_LHE_PDF_set__eq__90411, &b_GenWeight_LHE_PDF_set__eq__90411);
        InitBranch("GenWeight_LHE_PDF_set__eq__90412", &GenWeight_LHE_PDF_set__eq__90412, &b_GenWeight_LHE_PDF_set__eq__90412);
        InitBranch("GenWeight_LHE_PDF_set__eq__90413", &GenWeight_LHE_PDF_set__eq__90413, &b_GenWeight_LHE_PDF_set__eq__90413);
        InitBranch("GenWeight_LHE_PDF_set__eq__90414", &GenWeight_LHE_PDF_set__eq__90414, &b_GenWeight_LHE_PDF_set__eq__90414);
        InitBranch("GenWeight_LHE_PDF_set__eq__90415", &GenWeight_LHE_PDF_set__eq__90415, &b_GenWeight_LHE_PDF_set__eq__90415);
        InitBranch("GenWeight_LHE_PDF_set__eq__90416", &GenWeight_LHE_PDF_set__eq__90416, &b_GenWeight_LHE_PDF_set__eq__90416);
        InitBranch("GenWeight_LHE_PDF_set__eq__90417", &GenWeight_LHE_PDF_set__eq__90417, &b_GenWeight_LHE_PDF_set__eq__90417);
        InitBranch("GenWeight_LHE_PDF_set__eq__90418", &GenWeight_LHE_PDF_set__eq__90418, &b_GenWeight_LHE_PDF_set__eq__90418);
        InitBranch("GenWeight_LHE_PDF_set__eq__90419", &GenWeight_LHE_PDF_set__eq__90419, &b_GenWeight_LHE_PDF_set__eq__90419);
        InitBranch("GenWeight_LHE_PDF_set__eq__90420", &GenWeight_LHE_PDF_set__eq__90420, &b_GenWeight_LHE_PDF_set__eq__90420);
        InitBranch("GenWeight_LHE_PDF_set__eq__90421", &GenWeight_LHE_PDF_set__eq__90421, &b_GenWeight_LHE_PDF_set__eq__90421);
        InitBranch("GenWeight_LHE_PDF_set__eq__90422", &GenWeight_LHE_PDF_set__eq__90422, &b_GenWeight_LHE_PDF_set__eq__90422);
        InitBranch("GenWeight_LHE_PDF_set__eq__90423", &GenWeight_LHE_PDF_set__eq__90423, &b_GenWeight_LHE_PDF_set__eq__90423);
        InitBranch("GenWeight_LHE_PDF_set__eq__90424", &GenWeight_LHE_PDF_set__eq__90424, &b_GenWeight_LHE_PDF_set__eq__90424);
        InitBranch("GenWeight_LHE_PDF_set__eq__90425", &GenWeight_LHE_PDF_set__eq__90425, &b_GenWeight_LHE_PDF_set__eq__90425);
        InitBranch("GenWeight_LHE_PDF_set__eq__90426", &GenWeight_LHE_PDF_set__eq__90426, &b_GenWeight_LHE_PDF_set__eq__90426);
        InitBranch("GenWeight_LHE_PDF_set__eq__90427", &GenWeight_LHE_PDF_set__eq__90427, &b_GenWeight_LHE_PDF_set__eq__90427);
        InitBranch("GenWeight_LHE_PDF_set__eq__90428", &GenWeight_LHE_PDF_set__eq__90428, &b_GenWeight_LHE_PDF_set__eq__90428);
        InitBranch("GenWeight_LHE_PDF_set__eq__90429", &GenWeight_LHE_PDF_set__eq__90429, &b_GenWeight_LHE_PDF_set__eq__90429);
        InitBranch("GenWeight_LHE_PDF_set__eq__90430", &GenWeight_LHE_PDF_set__eq__90430, &b_GenWeight_LHE_PDF_set__eq__90430);
        InitBranch("GenWeight_LHE_PDF_set__eq__90431", &GenWeight_LHE_PDF_set__eq__90431, &b_GenWeight_LHE_PDF_set__eq__90431);
        InitBranch("GenWeight_LHE_PDF_set__eq__90432", &GenWeight_LHE_PDF_set__eq__90432, &b_GenWeight_LHE_PDF_set__eq__90432);
        InitBranch("GenWeight_LHE_PDF_set__eq__25300", &GenWeight_LHE_PDF_set__eq__25300, &b_GenWeight_LHE_PDF_set__eq__25300);
        InitBranch("GenWeight_LHE_PDF_set__eq__11000", &GenWeight_LHE_PDF_set__eq__11000, &b_GenWeight_LHE_PDF_set__eq__11000);
        InitBranch("GenWeight_LHE_PDF_set__eq__13100", &GenWeight_LHE_PDF_set__eq__13100, &b_GenWeight_LHE_PDF_set__eq__13100);
        InitBranch("GenWeight_LHE_PDF_set__eq__260000", &GenWeight_LHE_PDF_set__eq__260000, &b_GenWeight_LHE_PDF_set__eq__260000);
        InitBranch("GenWeight_LHE_PDF_set__eq__91400", &GenWeight_LHE_PDF_set__eq__91400, &b_GenWeight_LHE_PDF_set__eq__91400);
        InitBranch("GenWeight_LHE_mtinf", &GenWeight_LHE_mtinf, &b_GenWeight_LHE_mtinf);
        InitBranch("GenWeight_LHE_mtmb", &GenWeight_LHE_mtmb, &b_GenWeight_LHE_mtmb);
        InitBranch("GenWeight_LHE_mtmb-bminlo", &GenWeight_LHE_mtmb_bminlo, &b_GenWeight_LHE_mtmb_bminlo);
        InitBranch("GenWeight_LHE_nnlops-bminlo", &GenWeight_LHE_nnlops_bminlo, &b_GenWeight_LHE_nnlops_bminlo);
        InitBranch("GenWeight_LHE_nnlops-mtinf", &GenWeight_LHE_nnlops_mtinf, &b_GenWeight_LHE_nnlops_mtinf);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgDnDn", &GenWeight_LHE_nnlops_nnloDn_pwgDnDn, &b_GenWeight_LHE_nnlops_nnloDn_pwgDnDn);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgDnNom", &GenWeight_LHE_nnlops_nnloDn_pwgDnNom, &b_GenWeight_LHE_nnlops_nnloDn_pwgDnNom);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgDnUp", &GenWeight_LHE_nnlops_nnloDn_pwgDnUp, &b_GenWeight_LHE_nnlops_nnloDn_pwgDnUp);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgNomDn", &GenWeight_LHE_nnlops_nnloDn_pwgNomDn, &b_GenWeight_LHE_nnlops_nnloDn_pwgNomDn);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgNomNom", &GenWeight_LHE_nnlops_nnloDn_pwgNomNom, &b_GenWeight_LHE_nnlops_nnloDn_pwgNomNom);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgNomUp", &GenWeight_LHE_nnlops_nnloDn_pwgNomUp, &b_GenWeight_LHE_nnlops_nnloDn_pwgNomUp);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgUpDn", &GenWeight_LHE_nnlops_nnloDn_pwgUpDn, &b_GenWeight_LHE_nnlops_nnloDn_pwgUpDn);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgUpNom", &GenWeight_LHE_nnlops_nnloDn_pwgUpNom, &b_GenWeight_LHE_nnlops_nnloDn_pwgUpNom);
        InitBranch("GenWeight_LHE_nnlops-nnloDn-pwgUpUp", &GenWeight_LHE_nnlops_nnloDn_pwgUpUp, &b_GenWeight_LHE_nnlops_nnloDn_pwgUpUp);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgDnDn", &GenWeight_LHE_nnlops_nnloNom_pwgDnDn, &b_GenWeight_LHE_nnlops_nnloNom_pwgDnDn);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgDnNom", &GenWeight_LHE_nnlops_nnloNom_pwgDnNom, &b_GenWeight_LHE_nnlops_nnloNom_pwgDnNom);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgDnUp", &GenWeight_LHE_nnlops_nnloNom_pwgDnUp, &b_GenWeight_LHE_nnlops_nnloNom_pwgDnUp);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgNomDn", &GenWeight_LHE_nnlops_nnloNom_pwgNomDn, &b_GenWeight_LHE_nnlops_nnloNom_pwgNomDn);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgNomUp", &GenWeight_LHE_nnlops_nnloNom_pwgNomUp, &b_GenWeight_LHE_nnlops_nnloNom_pwgNomUp);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgUpDn", &GenWeight_LHE_nnlops_nnloNom_pwgUpDn, &b_GenWeight_LHE_nnlops_nnloNom_pwgUpDn);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgUpNom", &GenWeight_LHE_nnlops_nnloNom_pwgUpNom, &b_GenWeight_LHE_nnlops_nnloNom_pwgUpNom);
        InitBranch("GenWeight_LHE_nnlops-nnloNom-pwgUpUp", &GenWeight_LHE_nnlops_nnloNom_pwgUpUp, &b_GenWeight_LHE_nnlops_nnloNom_pwgUpUp);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgDnDn", &GenWeight_LHE_nnlops_nnloUp_pwgDnDn, &b_GenWeight_LHE_nnlops_nnloUp_pwgDnDn);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgDnNom", &GenWeight_LHE_nnlops_nnloUp_pwgDnNom, &b_GenWeight_LHE_nnlops_nnloUp_pwgDnNom);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgDnUp", &GenWeight_LHE_nnlops_nnloUp_pwgDnUp, &b_GenWeight_LHE_nnlops_nnloUp_pwgDnUp);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgNomDn", &GenWeight_LHE_nnlops_nnloUp_pwgNomDn, &b_GenWeight_LHE_nnlops_nnloUp_pwgNomDn);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgNomNom", &GenWeight_LHE_nnlops_nnloUp_pwgNomNom, &b_GenWeight_LHE_nnlops_nnloUp_pwgNomNom);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgNomUp", &GenWeight_LHE_nnlops_nnloUp_pwgNomUp, &b_GenWeight_LHE_nnlops_nnloUp_pwgNomUp);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgUpDn", &GenWeight_LHE_nnlops_nnloUp_pwgUpDn, &b_GenWeight_LHE_nnlops_nnloUp_pwgUpDn);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgUpNom", &GenWeight_LHE_nnlops_nnloUp_pwgUpNom, &b_GenWeight_LHE_nnlops_nnloUp_pwgUpNom);
        InitBranch("GenWeight_LHE_nnlops-nnloUp-pwgUpUp", &GenWeight_LHE_nnlops_nnloUp_pwgUpUp, &b_GenWeight_LHE_nnlops_nnloUp_pwgUpUp);
        InitBranch("GenWeight_LHE_nnlops-nominal", &GenWeight_LHE_nnlops_nominal, &b_GenWeight_LHE_nnlops_nominal);
        InitBranch("GenWeight_LHE_nnlops-nominal-pdflhc", &GenWeight_LHE_nnlops_nominal_pdflhc, &b_GenWeight_LHE_nnlops_nominal_pdflhc);
        InitBranch("GenWeight_LHE_PDF_set__eq__13191", &GenWeight_LHE_PDF_set__eq__13191, &b_GenWeight_LHE_PDF_set__eq__13191);
        InitBranch("GenWeight_LHE_PDF_set__eq__25410", &GenWeight_LHE_PDF_set__eq__25410, &b_GenWeight_LHE_PDF_set__eq__25410);
        InitBranch("GenWeight_LHE_PDF_set__eq__260401", &GenWeight_LHE_PDF_set__eq__260401, &b_GenWeight_LHE_PDF_set__eq__260401);
        InitBranch("GenWeight_LHE_PDF_set__eq__260402", &GenWeight_LHE_PDF_set__eq__260402, &b_GenWeight_LHE_PDF_set__eq__260402);
        InitBranch("GenWeight_LHE_PDF_set__eq__260403", &GenWeight_LHE_PDF_set__eq__260403, &b_GenWeight_LHE_PDF_set__eq__260403);
        InitBranch("GenWeight_LHE_PDF_set__eq__260404", &GenWeight_LHE_PDF_set__eq__260404, &b_GenWeight_LHE_PDF_set__eq__260404);
        InitBranch("GenWeight_LHE_PDF_set__eq__260405", &GenWeight_LHE_PDF_set__eq__260405, &b_GenWeight_LHE_PDF_set__eq__260405);
        InitBranch("GenWeight_LHE_PDF_set__eq__260406", &GenWeight_LHE_PDF_set__eq__260406, &b_GenWeight_LHE_PDF_set__eq__260406);
        InitBranch("GenWeight_LHE_PDF_set__eq__260407", &GenWeight_LHE_PDF_set__eq__260407, &b_GenWeight_LHE_PDF_set__eq__260407);
        InitBranch("GenWeight_LHE_PDF_set__eq__260408", &GenWeight_LHE_PDF_set__eq__260408, &b_GenWeight_LHE_PDF_set__eq__260408);
        InitBranch("GenWeight_LHE_PDF_set__eq__260409", &GenWeight_LHE_PDF_set__eq__260409, &b_GenWeight_LHE_PDF_set__eq__260409);
        InitBranch("GenWeight_LHE_PDF_set__eq__260410", &GenWeight_LHE_PDF_set__eq__260410, &b_GenWeight_LHE_PDF_set__eq__260410);
        InitBranch("GenWeight_LHE_PDF_set__eq__260411", &GenWeight_LHE_PDF_set__eq__260411, &b_GenWeight_LHE_PDF_set__eq__260411);
        InitBranch("GenWeight_LHE_PDF_set__eq__260412", &GenWeight_LHE_PDF_set__eq__260412, &b_GenWeight_LHE_PDF_set__eq__260412);
        InitBranch("GenWeight_LHE_PDF_set__eq__260413", &GenWeight_LHE_PDF_set__eq__260413, &b_GenWeight_LHE_PDF_set__eq__260413);
        InitBranch("GenWeight_LHE_PDF_set__eq__260414", &GenWeight_LHE_PDF_set__eq__260414, &b_GenWeight_LHE_PDF_set__eq__260414);
        InitBranch("GenWeight_LHE_PDF_set__eq__260415", &GenWeight_LHE_PDF_set__eq__260415, &b_GenWeight_LHE_PDF_set__eq__260415);
        InitBranch("GenWeight_LHE_PDF_set__eq__260416", &GenWeight_LHE_PDF_set__eq__260416, &b_GenWeight_LHE_PDF_set__eq__260416);
        InitBranch("GenWeight_LHE_PDF_set__eq__260417", &GenWeight_LHE_PDF_set__eq__260417, &b_GenWeight_LHE_PDF_set__eq__260417);
        InitBranch("GenWeight_LHE_PDF_set__eq__260418", &GenWeight_LHE_PDF_set__eq__260418, &b_GenWeight_LHE_PDF_set__eq__260418);
        InitBranch("GenWeight_LHE_PDF_set__eq__260419", &GenWeight_LHE_PDF_set__eq__260419, &b_GenWeight_LHE_PDF_set__eq__260419);
        InitBranch("GenWeight_LHE_PDF_set__eq__260420", &GenWeight_LHE_PDF_set__eq__260420, &b_GenWeight_LHE_PDF_set__eq__260420);
        InitBranch("GenWeight_LHE_PDF_set__eq__260421", &GenWeight_LHE_PDF_set__eq__260421, &b_GenWeight_LHE_PDF_set__eq__260421);
        InitBranch("GenWeight_LHE_PDF_set__eq__260422", &GenWeight_LHE_PDF_set__eq__260422, &b_GenWeight_LHE_PDF_set__eq__260422);
        InitBranch("GenWeight_LHE_PDF_set__eq__260423", &GenWeight_LHE_PDF_set__eq__260423, &b_GenWeight_LHE_PDF_set__eq__260423);
        InitBranch("GenWeight_LHE_PDF_set__eq__260424", &GenWeight_LHE_PDF_set__eq__260424, &b_GenWeight_LHE_PDF_set__eq__260424);
        InitBranch("GenWeight_LHE_PDF_set__eq__260425", &GenWeight_LHE_PDF_set__eq__260425, &b_GenWeight_LHE_PDF_set__eq__260425);
        InitBranch("GenWeight_LHE_PDF_set__eq__260426", &GenWeight_LHE_PDF_set__eq__260426, &b_GenWeight_LHE_PDF_set__eq__260426);
        InitBranch("GenWeight_LHE_PDF_set__eq__260427", &GenWeight_LHE_PDF_set__eq__260427, &b_GenWeight_LHE_PDF_set__eq__260427);
        InitBranch("GenWeight_LHE_PDF_set__eq__260428", &GenWeight_LHE_PDF_set__eq__260428, &b_GenWeight_LHE_PDF_set__eq__260428);
        InitBranch("GenWeight_LHE_PDF_set__eq__260429", &GenWeight_LHE_PDF_set__eq__260429, &b_GenWeight_LHE_PDF_set__eq__260429);
        InitBranch("GenWeight_LHE_PDF_set__eq__260430", &GenWeight_LHE_PDF_set__eq__260430, &b_GenWeight_LHE_PDF_set__eq__260430);
        InitBranch("GenWeight_LHE_PDF_set__eq__260431", &GenWeight_LHE_PDF_set__eq__260431, &b_GenWeight_LHE_PDF_set__eq__260431);
        InitBranch("GenWeight_LHE_PDF_set__eq__260432", &GenWeight_LHE_PDF_set__eq__260432, &b_GenWeight_LHE_PDF_set__eq__260432);
        InitBranch("GenWeight_LHE_PDF_set__eq__260433", &GenWeight_LHE_PDF_set__eq__260433, &b_GenWeight_LHE_PDF_set__eq__260433);
        InitBranch("GenWeight_LHE_PDF_set__eq__260434", &GenWeight_LHE_PDF_set__eq__260434, &b_GenWeight_LHE_PDF_set__eq__260434);
        InitBranch("GenWeight_LHE_PDF_set__eq__260435", &GenWeight_LHE_PDF_set__eq__260435, &b_GenWeight_LHE_PDF_set__eq__260435);
        InitBranch("GenWeight_LHE_PDF_set__eq__260436", &GenWeight_LHE_PDF_set__eq__260436, &b_GenWeight_LHE_PDF_set__eq__260436);
        InitBranch("GenWeight_LHE_PDF_set__eq__260437", &GenWeight_LHE_PDF_set__eq__260437, &b_GenWeight_LHE_PDF_set__eq__260437);
        InitBranch("GenWeight_LHE_PDF_set__eq__260438", &GenWeight_LHE_PDF_set__eq__260438, &b_GenWeight_LHE_PDF_set__eq__260438);
        InitBranch("GenWeight_LHE_PDF_set__eq__260439", &GenWeight_LHE_PDF_set__eq__260439, &b_GenWeight_LHE_PDF_set__eq__260439);
        InitBranch("GenWeight_LHE_PDF_set__eq__260440", &GenWeight_LHE_PDF_set__eq__260440, &b_GenWeight_LHE_PDF_set__eq__260440);
        InitBranch("GenWeight_LHE_PDF_set__eq__260441", &GenWeight_LHE_PDF_set__eq__260441, &b_GenWeight_LHE_PDF_set__eq__260441);
        InitBranch("GenWeight_LHE_PDF_set__eq__260442", &GenWeight_LHE_PDF_set__eq__260442, &b_GenWeight_LHE_PDF_set__eq__260442);
        InitBranch("GenWeight_LHE_PDF_set__eq__260443", &GenWeight_LHE_PDF_set__eq__260443, &b_GenWeight_LHE_PDF_set__eq__260443);
        InitBranch("GenWeight_LHE_PDF_set__eq__260444", &GenWeight_LHE_PDF_set__eq__260444, &b_GenWeight_LHE_PDF_set__eq__260444);
        InitBranch("GenWeight_LHE_PDF_set__eq__260445", &GenWeight_LHE_PDF_set__eq__260445, &b_GenWeight_LHE_PDF_set__eq__260445);
        InitBranch("GenWeight_LHE_PDF_set__eq__260446", &GenWeight_LHE_PDF_set__eq__260446, &b_GenWeight_LHE_PDF_set__eq__260446);
        InitBranch("GenWeight_LHE_PDF_set__eq__260447", &GenWeight_LHE_PDF_set__eq__260447, &b_GenWeight_LHE_PDF_set__eq__260447);
        InitBranch("GenWeight_LHE_PDF_set__eq__260448", &GenWeight_LHE_PDF_set__eq__260448, &b_GenWeight_LHE_PDF_set__eq__260448);
        InitBranch("GenWeight_LHE_PDF_set__eq__260449", &GenWeight_LHE_PDF_set__eq__260449, &b_GenWeight_LHE_PDF_set__eq__260449);
        InitBranch("GenWeight_LHE_PDF_set__eq__260450", &GenWeight_LHE_PDF_set__eq__260450, &b_GenWeight_LHE_PDF_set__eq__260450);
        InitBranch("GenWeight_LHE_PDF_set__eq__260451", &GenWeight_LHE_PDF_set__eq__260451, &b_GenWeight_LHE_PDF_set__eq__260451);
        InitBranch("GenWeight_LHE_PDF_set__eq__260452", &GenWeight_LHE_PDF_set__eq__260452, &b_GenWeight_LHE_PDF_set__eq__260452);
        InitBranch("GenWeight_LHE_PDF_set__eq__260453", &GenWeight_LHE_PDF_set__eq__260453, &b_GenWeight_LHE_PDF_set__eq__260453);
        InitBranch("GenWeight_LHE_PDF_set__eq__260454", &GenWeight_LHE_PDF_set__eq__260454, &b_GenWeight_LHE_PDF_set__eq__260454);
        InitBranch("GenWeight_LHE_PDF_set__eq__260455", &GenWeight_LHE_PDF_set__eq__260455, &b_GenWeight_LHE_PDF_set__eq__260455);
        InitBranch("GenWeight_LHE_PDF_set__eq__260456", &GenWeight_LHE_PDF_set__eq__260456, &b_GenWeight_LHE_PDF_set__eq__260456);
        InitBranch("GenWeight_LHE_PDF_set__eq__260457", &GenWeight_LHE_PDF_set__eq__260457, &b_GenWeight_LHE_PDF_set__eq__260457);
        InitBranch("GenWeight_LHE_PDF_set__eq__260458", &GenWeight_LHE_PDF_set__eq__260458, &b_GenWeight_LHE_PDF_set__eq__260458);
        InitBranch("GenWeight_LHE_PDF_set__eq__260459", &GenWeight_LHE_PDF_set__eq__260459, &b_GenWeight_LHE_PDF_set__eq__260459);
        InitBranch("GenWeight_LHE_PDF_set__eq__260460", &GenWeight_LHE_PDF_set__eq__260460, &b_GenWeight_LHE_PDF_set__eq__260460);
        InitBranch("GenWeight_LHE_PDF_set__eq__260461", &GenWeight_LHE_PDF_set__eq__260461, &b_GenWeight_LHE_PDF_set__eq__260461);
        InitBranch("GenWeight_LHE_PDF_set__eq__260462", &GenWeight_LHE_PDF_set__eq__260462, &b_GenWeight_LHE_PDF_set__eq__260462);
        InitBranch("GenWeight_LHE_PDF_set__eq__260463", &GenWeight_LHE_PDF_set__eq__260463, &b_GenWeight_LHE_PDF_set__eq__260463);
        InitBranch("GenWeight_LHE_PDF_set__eq__260464", &GenWeight_LHE_PDF_set__eq__260464, &b_GenWeight_LHE_PDF_set__eq__260464);
        InitBranch("GenWeight_LHE_PDF_set__eq__260465", &GenWeight_LHE_PDF_set__eq__260465, &b_GenWeight_LHE_PDF_set__eq__260465);
        InitBranch("GenWeight_LHE_PDF_set__eq__260466", &GenWeight_LHE_PDF_set__eq__260466, &b_GenWeight_LHE_PDF_set__eq__260466);
        InitBranch("GenWeight_LHE_PDF_set__eq__260467", &GenWeight_LHE_PDF_set__eq__260467, &b_GenWeight_LHE_PDF_set__eq__260467);
        InitBranch("GenWeight_LHE_PDF_set__eq__260468", &GenWeight_LHE_PDF_set__eq__260468, &b_GenWeight_LHE_PDF_set__eq__260468);
        InitBranch("GenWeight_LHE_PDF_set__eq__260469", &GenWeight_LHE_PDF_set__eq__260469, &b_GenWeight_LHE_PDF_set__eq__260469);
        InitBranch("GenWeight_LHE_PDF_set__eq__260470", &GenWeight_LHE_PDF_set__eq__260470, &b_GenWeight_LHE_PDF_set__eq__260470);
        InitBranch("GenWeight_LHE_PDF_set__eq__260471", &GenWeight_LHE_PDF_set__eq__260471, &b_GenWeight_LHE_PDF_set__eq__260471);
        InitBranch("GenWeight_LHE_PDF_set__eq__260472", &GenWeight_LHE_PDF_set__eq__260472, &b_GenWeight_LHE_PDF_set__eq__260472);
        InitBranch("GenWeight_LHE_PDF_set__eq__260473", &GenWeight_LHE_PDF_set__eq__260473, &b_GenWeight_LHE_PDF_set__eq__260473);
        InitBranch("GenWeight_LHE_PDF_set__eq__260474", &GenWeight_LHE_PDF_set__eq__260474, &b_GenWeight_LHE_PDF_set__eq__260474);
        InitBranch("GenWeight_LHE_PDF_set__eq__260475", &GenWeight_LHE_PDF_set__eq__260475, &b_GenWeight_LHE_PDF_set__eq__260475);
        InitBranch("GenWeight_LHE_PDF_set__eq__260476", &GenWeight_LHE_PDF_set__eq__260476, &b_GenWeight_LHE_PDF_set__eq__260476);
        InitBranch("GenWeight_LHE_PDF_set__eq__260477", &GenWeight_LHE_PDF_set__eq__260477, &b_GenWeight_LHE_PDF_set__eq__260477);
        InitBranch("GenWeight_LHE_PDF_set__eq__260478", &GenWeight_LHE_PDF_set__eq__260478, &b_GenWeight_LHE_PDF_set__eq__260478);
        InitBranch("GenWeight_LHE_PDF_set__eq__260479", &GenWeight_LHE_PDF_set__eq__260479, &b_GenWeight_LHE_PDF_set__eq__260479);
        InitBranch("GenWeight_LHE_PDF_set__eq__260480", &GenWeight_LHE_PDF_set__eq__260480, &b_GenWeight_LHE_PDF_set__eq__260480);
        InitBranch("GenWeight_LHE_PDF_set__eq__260481", &GenWeight_LHE_PDF_set__eq__260481, &b_GenWeight_LHE_PDF_set__eq__260481);
        InitBranch("GenWeight_LHE_PDF_set__eq__260482", &GenWeight_LHE_PDF_set__eq__260482, &b_GenWeight_LHE_PDF_set__eq__260482);
        InitBranch("GenWeight_LHE_PDF_set__eq__260483", &GenWeight_LHE_PDF_set__eq__260483, &b_GenWeight_LHE_PDF_set__eq__260483);
        InitBranch("GenWeight_LHE_PDF_set__eq__260484", &GenWeight_LHE_PDF_set__eq__260484, &b_GenWeight_LHE_PDF_set__eq__260484);
        InitBranch("GenWeight_LHE_PDF_set__eq__260485", &GenWeight_LHE_PDF_set__eq__260485, &b_GenWeight_LHE_PDF_set__eq__260485);
        InitBranch("GenWeight_LHE_PDF_set__eq__260486", &GenWeight_LHE_PDF_set__eq__260486, &b_GenWeight_LHE_PDF_set__eq__260486);
        InitBranch("GenWeight_LHE_PDF_set__eq__260487", &GenWeight_LHE_PDF_set__eq__260487, &b_GenWeight_LHE_PDF_set__eq__260487);
        InitBranch("GenWeight_LHE_PDF_set__eq__260488", &GenWeight_LHE_PDF_set__eq__260488, &b_GenWeight_LHE_PDF_set__eq__260488);
        InitBranch("GenWeight_LHE_PDF_set__eq__260489", &GenWeight_LHE_PDF_set__eq__260489, &b_GenWeight_LHE_PDF_set__eq__260489);
        InitBranch("GenWeight_LHE_PDF_set__eq__260490", &GenWeight_LHE_PDF_set__eq__260490, &b_GenWeight_LHE_PDF_set__eq__260490);
        InitBranch("GenWeight_LHE_PDF_set__eq__260491", &GenWeight_LHE_PDF_set__eq__260491, &b_GenWeight_LHE_PDF_set__eq__260491);
        InitBranch("GenWeight_LHE_PDF_set__eq__260492", &GenWeight_LHE_PDF_set__eq__260492, &b_GenWeight_LHE_PDF_set__eq__260492);
        InitBranch("GenWeight_LHE_PDF_set__eq__260493", &GenWeight_LHE_PDF_set__eq__260493, &b_GenWeight_LHE_PDF_set__eq__260493);
        InitBranch("GenWeight_LHE_PDF_set__eq__260494", &GenWeight_LHE_PDF_set__eq__260494, &b_GenWeight_LHE_PDF_set__eq__260494);
        InitBranch("GenWeight_LHE_PDF_set__eq__260495", &GenWeight_LHE_PDF_set__eq__260495, &b_GenWeight_LHE_PDF_set__eq__260495);
        InitBranch("GenWeight_LHE_PDF_set__eq__260496", &GenWeight_LHE_PDF_set__eq__260496, &b_GenWeight_LHE_PDF_set__eq__260496);
        InitBranch("GenWeight_LHE_PDF_set__eq__260497", &GenWeight_LHE_PDF_set__eq__260497, &b_GenWeight_LHE_PDF_set__eq__260497);
        InitBranch("GenWeight_LHE_PDF_set__eq__260498", &GenWeight_LHE_PDF_set__eq__260498, &b_GenWeight_LHE_PDF_set__eq__260498);
        InitBranch("GenWeight_LHE_PDF_set__eq__260499", &GenWeight_LHE_PDF_set__eq__260499, &b_GenWeight_LHE_PDF_set__eq__260499);
        InitBranch("GenWeight_LHE_PDF_set__eq__260500", &GenWeight_LHE_PDF_set__eq__260500, &b_GenWeight_LHE_PDF_set__eq__260500);
        InitBranch("GenWeight_LHE_PDF_set__eq__92000", &GenWeight_LHE_PDF_set__eq__92000, &b_GenWeight_LHE_PDF_set__eq__92000);
        InitBranch("GenWeight_LHE_PDF_set__eq__92001", &GenWeight_LHE_PDF_set__eq__92001, &b_GenWeight_LHE_PDF_set__eq__92001);
        InitBranch("GenWeight_LHE_PDF_set__eq__92002", &GenWeight_LHE_PDF_set__eq__92002, &b_GenWeight_LHE_PDF_set__eq__92002);
        InitBranch("GenWeight_LHE_PDF_set__eq__92003", &GenWeight_LHE_PDF_set__eq__92003, &b_GenWeight_LHE_PDF_set__eq__92003);
        InitBranch("GenWeight_LHE_PDF_set__eq__92004", &GenWeight_LHE_PDF_set__eq__92004, &b_GenWeight_LHE_PDF_set__eq__92004);
        InitBranch("GenWeight_LHE_PDF_set__eq__92005", &GenWeight_LHE_PDF_set__eq__92005, &b_GenWeight_LHE_PDF_set__eq__92005);
        InitBranch("GenWeight_LHE_PDF_set__eq__92006", &GenWeight_LHE_PDF_set__eq__92006, &b_GenWeight_LHE_PDF_set__eq__92006);
        InitBranch("GenWeight_LHE_PDF_set__eq__92007", &GenWeight_LHE_PDF_set__eq__92007, &b_GenWeight_LHE_PDF_set__eq__92007);
        InitBranch("GenWeight_LHE_PDF_set__eq__92008", &GenWeight_LHE_PDF_set__eq__92008, &b_GenWeight_LHE_PDF_set__eq__92008);
        InitBranch("GenWeight_LHE_PDF_set__eq__92009", &GenWeight_LHE_PDF_set__eq__92009, &b_GenWeight_LHE_PDF_set__eq__92009);
        InitBranch("GenWeight_LHE_PDF_set__eq__92010", &GenWeight_LHE_PDF_set__eq__92010, &b_GenWeight_LHE_PDF_set__eq__92010);
        InitBranch("GenWeight_LHE_PDF_set__eq__92011", &GenWeight_LHE_PDF_set__eq__92011, &b_GenWeight_LHE_PDF_set__eq__92011);
        InitBranch("GenWeight_LHE_PDF_set__eq__92012", &GenWeight_LHE_PDF_set__eq__92012, &b_GenWeight_LHE_PDF_set__eq__92012);
        InitBranch("GenWeight_LHE_PDF_set__eq__92013", &GenWeight_LHE_PDF_set__eq__92013, &b_GenWeight_LHE_PDF_set__eq__92013);
        InitBranch("GenWeight_LHE_PDF_set__eq__92014", &GenWeight_LHE_PDF_set__eq__92014, &b_GenWeight_LHE_PDF_set__eq__92014);
        InitBranch("GenWeight_LHE_PDF_set__eq__92015", &GenWeight_LHE_PDF_set__eq__92015, &b_GenWeight_LHE_PDF_set__eq__92015);
        InitBranch("GenWeight_LHE_PDF_set__eq__92016", &GenWeight_LHE_PDF_set__eq__92016, &b_GenWeight_LHE_PDF_set__eq__92016);
        InitBranch("GenWeight_LHE_PDF_set__eq__92017", &GenWeight_LHE_PDF_set__eq__92017, &b_GenWeight_LHE_PDF_set__eq__92017);
        InitBranch("GenWeight_LHE_PDF_set__eq__92018", &GenWeight_LHE_PDF_set__eq__92018, &b_GenWeight_LHE_PDF_set__eq__92018);
        InitBranch("GenWeight_LHE_PDF_set__eq__92019", &GenWeight_LHE_PDF_set__eq__92019, &b_GenWeight_LHE_PDF_set__eq__92019);
        InitBranch("GenWeight_LHE_PDF_set__eq__92020", &GenWeight_LHE_PDF_set__eq__92020, &b_GenWeight_LHE_PDF_set__eq__92020);
        InitBranch("GenWeight_LHE_PDF_set__eq__92021", &GenWeight_LHE_PDF_set__eq__92021, &b_GenWeight_LHE_PDF_set__eq__92021);
        InitBranch("GenWeight_LHE_PDF_set__eq__92022", &GenWeight_LHE_PDF_set__eq__92022, &b_GenWeight_LHE_PDF_set__eq__92022);
        InitBranch("GenWeight_LHE_PDF_set__eq__92023", &GenWeight_LHE_PDF_set__eq__92023, &b_GenWeight_LHE_PDF_set__eq__92023);
        InitBranch("GenWeight_LHE_PDF_set__eq__92024", &GenWeight_LHE_PDF_set__eq__92024, &b_GenWeight_LHE_PDF_set__eq__92024);
        InitBranch("GenWeight_LHE_PDF_set__eq__92025", &GenWeight_LHE_PDF_set__eq__92025, &b_GenWeight_LHE_PDF_set__eq__92025);
        InitBranch("GenWeight_LHE_PDF_set__eq__92026", &GenWeight_LHE_PDF_set__eq__92026, &b_GenWeight_LHE_PDF_set__eq__92026);
        InitBranch("GenWeight_LHE_PDF_set__eq__92027", &GenWeight_LHE_PDF_set__eq__92027, &b_GenWeight_LHE_PDF_set__eq__92027);
        InitBranch("GenWeight_LHE_PDF_set__eq__92028", &GenWeight_LHE_PDF_set__eq__92028, &b_GenWeight_LHE_PDF_set__eq__92028);
        InitBranch("GenWeight_LHE_PDF_set__eq__92029", &GenWeight_LHE_PDF_set__eq__92029, &b_GenWeight_LHE_PDF_set__eq__92029);
        InitBranch("GenWeight_LHE_PDF_set__eq__92030", &GenWeight_LHE_PDF_set__eq__92030, &b_GenWeight_LHE_PDF_set__eq__92030);
    
        // Systematic weights
        InitBranch("EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        InitBranch("EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        InitBranch("EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        InitBranch("EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        InitBranch("EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        InitBranch("EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up); 
        
        InitBranch("MuoWeight_MUON_EFF_ISO_STAT__1down", &MuoWeight_MUON_EFF_ISO_STAT__1down, &b_MuoWeight_MUON_EFF_ISO_STAT__1down) ;
        InitBranch("MuoWeight_MUON_EFF_ISO_STAT__1up", &MuoWeight_MUON_EFF_ISO_STAT__1up, &b_MuoWeight_MUON_EFF_ISO_STAT__1up) ;
        InitBranch("MuoWeight_MUON_EFF_ISO_SYS__1down", &MuoWeight_MUON_EFF_ISO_SYS__1down, &b_MuoWeight_MUON_EFF_ISO_SYS__1down) ;
        InitBranch("MuoWeight_MUON_EFF_ISO_SYS__1up", &MuoWeight_MUON_EFF_ISO_SYS__1up, &b_MuoWeight_MUON_EFF_ISO_SYS__1up) ;
        InitBranch("MuoWeight_MUON_EFF_RECO_STAT__1down", &MuoWeight_MUON_EFF_RECO_STAT__1down, &b_MuoWeight_MUON_EFF_RECO_STAT__1down) ;
        InitBranch("MuoWeight_MUON_EFF_RECO_STAT__1up", &MuoWeight_MUON_EFF_RECO_STAT__1up, &b_MuoWeight_MUON_EFF_RECO_STAT__1up) ;
        InitBranch("MuoWeight_MUON_EFF_RECO_SYS__1down", &MuoWeight_MUON_EFF_RECO_SYS__1down, &b_MuoWeight_MUON_EFF_RECO_SYS__1down) ;
        InitBranch("MuoWeight_MUON_EFF_RECO_SYS__1up", &MuoWeight_MUON_EFF_RECO_SYS__1up, &b_MuoWeight_MUON_EFF_RECO_SYS__1up) ;
        InitBranch("MuoWeight_MUON_EFF_TTVA_STAT__1down", &MuoWeight_MUON_EFF_TTVA_STAT__1down, &b_MuoWeight_MUON_EFF_TTVA_STAT__1down) ;
        InitBranch("MuoWeight_MUON_EFF_TTVA_STAT__1up", &MuoWeight_MUON_EFF_TTVA_STAT__1up, &b_MuoWeight_MUON_EFF_TTVA_STAT__1up) ;
        InitBranch("MuoWeight_MUON_EFF_TTVA_SYS__1down", &MuoWeight_MUON_EFF_TTVA_SYS__1down, &b_MuoWeight_MUON_EFF_TTVA_SYS__1down) ;
        InitBranch("MuoWeight_MUON_EFF_TTVA_SYS__1up", &MuoWeight_MUON_EFF_TTVA_SYS__1up, &b_MuoWeight_MUON_EFF_TTVA_SYS__1up) ;


        InitBranch("TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down) ;
        InitBranch("TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up) ;
        InitBranch("TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down) ;
        InitBranch("TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down) ;
        InitBranch("TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up) ;


        InitBranch("JetWeight_FT_EFF_B_systematics__1down", &JetWeight_FT_EFF_B_systematics__1down, &b_JetWeight_FT_EFF_B_systematics__1down) ;
        InitBranch("JetWeight_FT_EFF_B_systematics__1up", &JetWeight_FT_EFF_B_systematics__1up, &b_JetWeight_FT_EFF_B_systematics__1up) ;
        InitBranch("JetWeight_FT_EFF_C_systematics__1down", &JetWeight_FT_EFF_C_systematics__1down, &b_JetWeight_FT_EFF_C_systematics__1down) ;
        InitBranch("JetWeight_FT_EFF_C_systematics__1up", &JetWeight_FT_EFF_C_systematics__1up, &b_JetWeight_FT_EFF_C_systematics__1up) ;
        InitBranch("JetWeight_FT_EFF_Light_systematics__1down", &JetWeight_FT_EFF_Light_systematics__1down, &b_JetWeight_FT_EFF_Light_systematics__1down) ;
        InitBranch("JetWeight_FT_EFF_Light_systematics__1up", &JetWeight_FT_EFF_Light_systematics__1up, &b_JetWeight_FT_EFF_Light_systematics__1up) ;
        InitBranch("JetWeight_FT_EFF_extrapolation__1down", &JetWeight_FT_EFF_extrapolation__1down, &b_JetWeight_FT_EFF_extrapolation__1down) ;
        InitBranch("JetWeight_FT_EFF_extrapolation__1up", &JetWeight_FT_EFF_extrapolation__1up, &b_JetWeight_FT_EFF_extrapolation__1up) ;
        InitBranch("JetWeight_FT_EFF_extrapolation_from_charm__1down", &JetWeight_FT_EFF_extrapolation_from_charm__1down, &b_JetWeight_FT_EFF_extrapolation_from_charm__1down) ;
        InitBranch("JetWeight_FT_EFF_extrapolation_from_charm__1up", &JetWeight_FT_EFF_extrapolation_from_charm__1up, &b_JetWeight_FT_EFF_extrapolation_from_charm__1up) ;
        InitBranch("JetWeight_JET_JvtEfficiency__1down", &JetWeight_JET_JvtEfficiency__1down, &b_JetWeight_JET_JvtEfficiency__1down) ;
        InitBranch("JetWeight_JET_JvtEfficiency__1up", &JetWeight_JET_JvtEfficiency__1up, &b_JetWeight_JET_JvtEfficiency__1up) ;


        InitBranch("muWeight_PRW_DATASF__1down", &muWeight_PRW_DATASF__1down, &b_muWeight_PRW_DATASF__1down) ;
        InitBranch("muWeight_PRW_DATASF__1up", &muWeight_PRW_DATASF__1up, &b_muWeight_PRW_DATASF__1up) ;

        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
        InitBranch("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
        InitBranch("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
        InitBranch("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up, &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);

        InitBranch("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
        InitBranch("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
        InitBranch("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
        InitBranch("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);


    }



    InitBranch("MetTST_EleBiSect", &MetTST_EleBiSect, &b_MetTST_EleBiSect);
    InitBranch("MetTST_MuoBiSect", &MetTST_MuoBiSect, &b_MetTST_MuoBiSect);
    InitBranch("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
    InitBranch("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
    InitBranch("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
    InitBranch("bcid", &bcid, &b_bcid);
    InitBranch("coreFlags", &coreFlags, &b_coreFlags);
    InitBranch("forwardDetFlags", &forwardDetFlags, &b_forwardDetFlags);
    InitBranch("larFlags", &larFlags, &b_larFlags);
    InitBranch("lumiBlock", &lumiBlock, &b_lumiBlock);
    InitBranch("lumiFlags", &lumiFlags, &b_lumiFlags);
    InitBranch("muonFlags", &muonFlags, &b_muonFlags);
    InitBranch("pixelFlags", &pixelFlags, &b_pixelFlags);
    InitBranch("runNumber", &runNumber, &b_runNumber);
    InitBranch("sctFlags", &sctFlags, &b_sctFlags);
    InitBranch("tileFlags", &tileFlags, &b_tileFlags);
    InitBranch("trtFlags", &trtFlags, &b_trtFlags);
    InitBranch("eventNumber", &eventNumber, &b_eventNumber);

    InitBranch("MetCST_met", &MetCST_met, &b_MetCST_met);
    InitBranch("MetCST_phi", &MetCST_phi, &b_MetCST_phi);
    InitBranch("MetCST_sumet", &MetCST_sumet, &b_MetCST_sumet);
    InitBranch("MetTST_met", &MetTST_met, &b_MetTST_met);
    InitBranch("MetTST_phi", &MetTST_phi, &b_MetTST_phi);
    InitBranch("MetTST_sumet", &MetTST_sumet, &b_MetTST_sumet);
    InitBranch("TruthMET_met", &TruthMET_met, &b_TruthMET_met);
    InitBranch("TruthMET_phi", &TruthMET_phi, &b_TruthMET_phi);
    InitBranch("TruthMET_sumet", &TruthMET_sumet, &b_TruthMET_sumet);

    InitBranch("dilepton_pt", &dilepton_pt, &b_dilepton_pt);
    InitBranch("dilepton_eta", &dilepton_eta, &b_dilepton_eta);
    InitBranch("dilepton_phi", &dilepton_phi, &b_dilepton_phi);
    InitBranch("dilepton_m", &dilepton_m, &b_dilepton_m);
    InitBranch("dilepton_charge", &dilepton_charge, &b_dilepton_charge);
    InitBranch("dilepton_pdgId", &dilepton_pdgId, &b_dilepton_pdgId);

    InitBranch("electrons_pt", &electrons_pt, &b_electrons_pt);
    InitBranch("electrons_eta", &electrons_eta, &b_electrons_eta);
    InitBranch("electrons_phi", &electrons_phi, &b_electrons_phi);
    InitBranch("electrons_e", &electrons_e, &b_electrons_e);
    InitBranch("electrons_MT", &electrons_MT, &b_electrons_MT);
    InitBranch("electrons_charge", &electrons_charge, &b_electrons_charge);
    InitBranch("electrons_d0sig", &electrons_d0sig, &b_electrons_d0sig);
    InitBranch("electrons_isol", &electrons_isol, &b_electrons_isol);
    InitBranch("electrons_signal", &electrons_signal, &b_electrons_signal);
    InitBranch("electrons_truthOrigin", &electrons_truthOrigin, &b_electrons_truthOrigin);
    InitBranch("electrons_truthType", &electrons_truthType, &b_electrons_truthType);
    InitBranch("electrons_z0sinTheta", &electrons_z0sinTheta, &b_electrons_z0sinTheta);

    //          for (const std::string trig : {"HLT_e24_lhmedium_L1EM20VH",
    //                                                                                                                                   "HLT_e26_lhtight_nod0_ivarloose",
    //                                                                                                                                   "HLT_e60_lhmedium_nod0",
    //                                                                                                                                   "HLT_e140_lhloose_nod0",
    //                                                                                                                                   "HLT_e60_lhmedium",
    //                                                                                                                                   "HLT_e120_lhloose",
    //                                                                                                                                   "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
    //                                                                                                                                   "HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo",
    //                                                                                                                                   "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",                        
    //                                                                                                                                   "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
    //                                                                                                                                   "HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
    //                                                                                                                                   "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
    //                                                                                                                                   "HLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50"}) {
    //                          InitBranch(std::string("electrons_TrigMatch" + trig).c_str(), &electrons_TrigMatch[trig]);
    //          }

    InitBranch("jets_pt", &jets_pt, &b_jets_pt);
    InitBranch("jets_eta", &jets_eta, &b_jets_eta);
    InitBranch("jets_phi", &jets_phi, &b_jets_phi);
    InitBranch("jets_m", &jets_m, &b_jets_m);
    InitBranch("jets_Jvt", &jets_Jvt, &b_jets_Jvt);
    InitBranch("jets_LooseOR", &jets_LooseOR, &b_jets_LooseOR);
    InitBranch("jets_MV2c10", &jets_MV2c10, &b_jets_MV2c10);
    InitBranch("jets_NTrks", &jets_NTrks, &b_jets_NTrks);
    InitBranch("jets_bjet", &jets_bjet, &b_jets_bjet);
    InitBranch("jets_signal", &jets_signal, &b_jets_signal);

    InitBranch("muons_pt", &muons_pt, &b_muons_pt);
    InitBranch("muons_eta", &muons_eta, &b_muons_eta);
    InitBranch("muons_phi", &muons_phi, &b_muons_phi);
    InitBranch("muons_e", &muons_e, &b_muons_e);
    InitBranch("muons_MT", &muons_MT, &b_muons_MT);
    InitBranch("muons_charge", &muons_charge, &b_muons_charge);
    InitBranch("muons_d0sig", &muons_d0sig, &b_muons_d0sig);
    InitBranch("muons_isol", &muons_isol, &b_muons_isol);
    InitBranch("muons_signal", &muons_signal, &b_muons_signal);
    InitBranch("muons_truthOrigin", &muons_truthOrigin, &b_muons_truthOrigin);
    InitBranch("muons_truthType", &muons_truthType, &b_muons_truthType);
    InitBranch("muons_z0sinTheta", &muons_z0sinTheta, &b_muons_z0sinTheta);

    //          for (const std::string trig : {"HLT_mu26_imedium",
    //                                                                                                                                   "HLT_mu26_ivarmedium",
    //                                                                                                                                   "HLT_mu50",
    //                                                                                                                                   "HLT_mu60_0eta105_msonly",
    //                                                                                                                                   "HLT_mu20_iloose_L1MU15",
    //                                                                                                                                   "HLT_mu40",
    //                                                                                                                                   "HLT_mu24_ivarmedium",
    //                                                                                                                                   "HLT_mu14_ivarloose_tau35_medium1_tracktwo",
    //                                                                                                                                   "HLT_mu14_ivarloose_tau25_medium1_tracktwo",
    //                                                                                                                                   "HLT_mu14_tau25_medium1_tracktwo_xe50",
    //                                                                                                                                   "HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
    //                                                                                                                                   "HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
    //                                                                                                                                   "HLT_mu14_tau25_medium1_tracktwoEF_xe50",
    //                                                                                                                                   "HLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50"}) {
    //                          InitBranch(std::string("muons_TrigMatch" + trig).c_str(), &muons_TrigMatch[trig]);
    //          }

    InitBranch("taus_pt", &taus_pt, &b_taus_pt);
    InitBranch("taus_eta", &taus_eta, &b_taus_eta);
    InitBranch("taus_phi", &taus_phi, &b_taus_phi);
    InitBranch("taus_e", &taus_e, &b_taus_e);
    InitBranch("taus_BDTEleScore", &taus_BDTEleScore, &b_taus_BDTEleScore);
    InitBranch("taus_BDTJetScore", &taus_BDTJetScore, &b_taus_BDTJetScore);
    InitBranch("taus_BDTEleScoreSigTrans", &taus_BDTEleScoreSigTrans);
    InitBranch("taus_BDTJetScoreSigTrans", &taus_BDTJetScoreSigTrans);
    InitBranch("taus_ConeTruthLabelID", &taus_ConeTruthLabelID, &b_taus_ConeTruthLabelID);
    InitBranch("taus_MT", &taus_MT, &b_taus_MT);
    InitBranch("taus_NTrks", &taus_NTrks, &b_taus_NTrks);
    InitBranch("taus_NTrksJet", &taus_NTrksJet, &b_taus_NTrksJet);
    InitBranch("taus_PartonTruthLabelID", &taus_PartonTruthLabelID, &b_taus_PartonTruthLabelID);
    InitBranch("taus_Quality", &taus_Quality, &b_taus_Quality);
    InitBranch("taus_Width", &taus_Width, &b_taus_Width);
    InitBranch("taus_charge", &taus_charge, &b_taus_charge);
    InitBranch("taus_jetAssoc", &taus_jetAssoc, &b_taus_jetAssoc);
    InitBranch("taus_signalID", &taus_signalID, &b_taus_signalID);
    InitBranch("taus_truthOrigin", &taus_truthOrigin, &b_taus_truthOrigin);
    InitBranch("taus_truthType", &taus_truthType, &b_taus_truthType);

    for (const std::string trig : {"HLT_tau25_medium1_tracktwo",
            "HLT_tau35_medium1_tracktwo",
            "HLT_tau50_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo",
            "HLT_tau80_medium1_tracktwo_L1TAU60",
            "HLT_tau125_medium1_tracktwo",
            "HLT_tau160_medium1_tracktwo",
            "HLT_tau25_medium1_tracktwoEF",
            "HLT_tau25_mediumRNN_tracktwoMVA",
            "HLT_tau60_medium1_tracktwo",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
            "HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
            "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
            "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
            "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
            "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
            "HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
            "HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50"}) {
                InitBranch(std::string("taus_TrigMatch" + trig).c_str(), &taus_TrigMatch[trig]);
            }

    InitBranch("jetTrigger_weight", &jetTrigger_weight, &b_jetTrigger_weight);

    Notify();

}

Bool_t DSBase_Nominal::Notify()
{
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.

    return kTRUE;
}

void DSBase_Nominal::Show(Long64_t entry)
{
    // Print contents of entry.
    // If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}
Int_t DSBase_Nominal::Cut(Long64_t entry)
{
    // This function may be called from Loop.
    // returns  1 if entry is accepted.
    // returns -1 otherwise.
    return 1;
}

