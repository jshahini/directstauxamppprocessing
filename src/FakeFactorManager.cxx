#include "../include/FakeFactorManager.h"


// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"

#include <cmath>
#include <iostream>
#include <algorithm>


int FakeFactorManager::m_nevts_R1(0);
int FakeFactorManager::m_nevts_R2(0);
int FakeFactorManager::m_nevts_R3(0);
int FakeFactorManager::m_nevts_R123(0);
bool FakeFactorManager::m_vetoevent(false);
int FakeFactorManager::m_currentregion(0);
std::map<std::string, std::map<std::string, std::map<std::string, std::map<int,TH2D>>>> FakeFactorManager::m_ffhisto({});

void FakeFactorManager::Initialize(TChain *fChain) {

    if (fakefactorfile.empty()) return;

    LoadFakeFactors(fakefactorfile);

}

void FakeFactorManager::Calculate() {

    if (fakefactorfile.empty()) return;

    m_vetoevent = false;
    m_currentregion = 0;

    switch (tau1Signal) {
        case false: switch (tau2Signal) {
            case false:
                m_currentregion = 1;
                break;
            case true: 
                m_currentregion = 2;
                break;
        } break;
        case true: switch (tau2Signal) {
            case false:
                m_currentregion = 3;
                break;
            case true:
                m_currentregion = 4;
                break;
        } break;
    }

    if (m_currentregion == 4) {
        m_vetoevent = true;
        return;
    }

    ff_weight                = 1.0;
    ff_weight_sysUP          = 1.0;
    ff_weight_sysDOWN        = 1.0;
    ff_weight_sysVarTau1Pt   = 1.0;
    ff_weight_sysVarTau2Pt   = 1.0;
    ff_weight_sysVarOS       = 1.0;
    ff_weight_sysVarStatUP   = 1.0;
    ff_weight_sysVarStatDOWN = 1.0;

    if (FFMTargetSelection == "Default") {
        ff_weight                = ComputeFFWeight_Default();
        ff_weight_sysUP          = ComputeFFWeight_Default("SysUP");
        ff_weight_sysDOWN        = ComputeFFWeight_Default("SysDOWN");
        ff_weight_sysVarTau1Pt   = ComputeFFWeight_Default("SysVarTau1Pt");
        ff_weight_sysVarTau2Pt   = ComputeFFWeight_Default("SysVarTau2Pt");
        ff_weight_sysVarOS       = ComputeFFWeight_Default("SysVarOS");
        ff_weight_sysVarStatUP   = ComputeFFWeight_Default("SysVarStatUP");
        ff_weight_sysVarStatDOWN = ComputeFFWeight_Default("SysVarStatDOWN");
    } else if (FFMTargetSelection == "AtLeastOneTightTau") {
        ff_weight                = ComputeFFWeight_AtLeastOneTightTau();
        ff_weight_sysUP          = ComputeFFWeight_AtLeastOneTightTau("SysUP");
        ff_weight_sysDOWN        = ComputeFFWeight_AtLeastOneTightTau("SysDOWN");
        ff_weight_sysVarTau1Pt   = ComputeFFWeight_AtLeastOneTightTau("SysVarTau1Pt");
        ff_weight_sysVarTau2Pt   = ComputeFFWeight_AtLeastOneTightTau("SysVarTau2Pt");
        ff_weight_sysVarOS       = ComputeFFWeight_AtLeastOneTightTau("SysVarOS");
        ff_weight_sysVarStatUP   = ComputeFFWeight_AtLeastOneTightTau("SysVarStatUP");
        ff_weight_sysVarStatDOWN = ComputeFFWeight_AtLeastOneTightTau("SysVarStatDOWN");
        atLeastOneTightTau = true;
    } else if (FFMTargetSelection == "AtLeastTwoTightTaus") {
        if ((m_currentregion == 2 && tau2Quality == 2) || (m_currentregion == 3 && tau1Quality == 2)) {
            m_vetoevent = true;
            return;
        }
        if (signalTauID == 3) {
            ff_weight                = ComputeFFWeight_AtLeastTwoTightTaus_TightID();
            ff_weight_sysUP          = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysUP");
            ff_weight_sysDOWN        = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysDOWN");
            ff_weight_sysVarTau1Pt   = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysVarTau1Pt");
            ff_weight_sysVarTau2Pt   = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysVarTau2Pt");
            ff_weight_sysVarOS       = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysVarOS");
            ff_weight_sysVarStatUP   = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysVarStatUP");
            ff_weight_sysVarStatDOWN = ComputeFFWeight_AtLeastTwoTightTaus_TightID("SysVarStatDOWN");
        }
        else {
            ff_weight                = ComputeFFWeight_AtLeastTwoTightTaus();
            ff_weight_sysUP          = ComputeFFWeight_AtLeastTwoTightTaus("SysUP");
            ff_weight_sysDOWN        = ComputeFFWeight_AtLeastTwoTightTaus("SysDOWN");
            ff_weight_sysVarTau1Pt   = ComputeFFWeight_AtLeastTwoTightTaus("SysVarTau1Pt");
            ff_weight_sysVarTau2Pt   = ComputeFFWeight_AtLeastTwoTightTaus("SysVarTau2Pt");
            ff_weight_sysVarOS       = ComputeFFWeight_AtLeastTwoTightTaus("SysVarOS");
            ff_weight_sysVarStatUP   = ComputeFFWeight_AtLeastTwoTightTaus("SysVarStatUP");
            ff_weight_sysVarStatDOWN = ComputeFFWeight_AtLeastTwoTightTaus("SysVarStatDOWN");
        }
        atLeastOneTightTau = true;
        atLeastTwoTightTaus = true;
    } else {
        LOG(ERROR) << "Unknown FFM target selection: '" << FFMTargetSelection << "' "
                   << "-> Will use 'Default' instead...";
    }

    // N_qcd = N_data - N_MC
    if (!isData) {
        ff_weight                *= -1.0;
        ff_weight_sysUP          *= -1.0;
        ff_weight_sysDOWN        *= -1.0;
        ff_weight_sysVarTau1Pt   *= -1.0;
        ff_weight_sysVarTau2Pt   *= -1.0;
        ff_weight_sysVarOS       *= -1.0;
        ff_weight_sysVarStatUP   *= -1.0;
        ff_weight_sysVarStatDOWN *= -1.0;
    }

    tau1Signal = true;
    tau2Signal = true;

}

void FakeFactorManager::LoadFakeFactors(std::string filepath) {

    LOG(INFO) << "Loading fake factors from file '" << filepath << "'";

    TFile* tfile = TFile::Open(filepath.c_str());

    std::vector<std::string> levels({"Nominal", "SysUP", "SysDOWN"});
    std::vector<std::string> promSteps({"VL+L+M=>T", "VL+L=>M+T", "VL+L=>T"});
    std::vector<std::string> nonPromTauQuals({"VL+L+M", "VL+L", "M+T", "T"});
    std::vector<int> decaymodes({1, 3});
    for (std::string level : levels) {
        for (std::string promStep : promSteps) {
            for (std::string nonPromTauQual : nonPromTauQuals) {
                for (int decaymode : decaymodes) {
                    m_ffhisto[level][promStep][nonPromTauQual][decaymode] = *(TH2D*)tfile->Get(std::string("FF_" + level + "/"
                        + promStep + "/" + nonPromTauQual + "/" + std::to_string(decaymode) + "-prong/FF_SS_" + promStep
                        + "_[" + nonPromTauQual + "]_" + std::to_string(decaymode) + "prong_combined_" + level).c_str());
                    m_ffhisto[level][promStep][nonPromTauQual][decaymode].SetDirectory(0);
                }
            }
        }
    }
    LoadFFSysSources(tfile);
    tfile->Close();

}

void FakeFactorManager::LoadFFSysSources(TFile* tfile) {

    std::vector<std::string> sources({"OS", "Tau1Pt", "Tau2Pt", "StatUP", "StatDOWN"});
    std::vector<std::string> promSteps({"VL+L+M=>T", "VL+L=>M+T", "VL+L=>T"});
    std::vector<std::string> nonPromTauQuals({"VL+L+M", "VL+L", "M+T", "T"});
    std::vector<int> decaymodes({1, 3});
    for (std::string source : sources) {
        for (std::string promStep : promSteps) {
            for (std::string nonPromTauQual : nonPromTauQuals) {
                for (int decaymode : decaymodes) {
                    std::string key = "SysVar" + source;
                    tfile->cd(std::string("SysVarDiffHistos/" + source + "/" + promStep + "/" + nonPromTauQual
                        + "/" + std::to_string(decaymode) + "-prong").c_str());
                    m_ffhisto[key][promStep][nonPromTauQual][decaymode] = *(TH2D*)gDirectory->Get(std::string("FF_SS_"
                        + promStep + "_[" + nonPromTauQual + "]_" + std::to_string(decaymode)
                        + "prong_combined_Nominal_" + key).c_str());
                    m_ffhisto[key][promStep][nonPromTauQual][decaymode].SetDirectory(0);
                    m_ffhisto[key][promStep][nonPromTauQual][decaymode].Add(dynamic_cast<TH2D*>(&m_ffhisto["Nominal"][promStep][nonPromTauQual][decaymode]));
                }
            }
        }
    }

}

double FakeFactorManager::GetFakeFactor(std::string level, std::string promstep, std::string nonpromtauqual, int nprong, double pt, double eta) {

    int binpt  = m_ffhisto[level][promstep][nonpromtauqual][nprong].GetXaxis()->FindBin(pt);
    int bineta = m_ffhisto[level][promstep][nonpromtauqual][nprong].GetYaxis()->FindBin(fabs(eta));
    int nbinspt  = m_ffhisto[level][promstep][nonpromtauqual][nprong].GetXaxis()->GetNbins();
    int nbinseta = m_ffhisto[level][promstep][nonpromtauqual][nprong].GetYaxis()->GetNbins();

    if (binpt > nbinspt) {
        binpt = nbinspt;
    } else if (binpt < 1) {
        LOG(WARNING) << "No fake factor available for a " << nprong << "-prong tau with pT = " << pt << "GeV";
        binpt = 1;
    }
    if (bineta > nbinseta) {
        bineta = nbinseta;
    } else if (bineta < 1) {
        LOG(WARNING) << "No fake factor available for a " << nprong << "-prong tau with |eta| = " << fabs(eta);
        bineta = 1;
    }
    int bin = m_ffhisto[level][promstep][nonpromtauqual][nprong].GetBin(binpt, bineta);
    return m_ffhisto[level][promstep][nonpromtauqual][nprong].GetBinContent(bin);

}

double FakeFactorManager::ComputeFFWeight_Default(std::string level) {

    // Compute and return the FF weight for a target selection with the two leading taus fulfilling
    // signal identification (medium).
    // For XAMPPstau-HadHad ntuples this should be baseline.

    double ff = 1.0;
    if (m_currentregion == 1) {
        ff *= ( GetFakeFactor(level, "VL+L=>M+T", "VL+L", tau1NProng, tau1Pt, tau1Eta)
              * GetFakeFactor(level, "VL+L=>M+T", "M+T",  tau2NProng, tau2Pt, tau2Eta) );
        ff += ( GetFakeFactor(level, "VL+L=>M+T", "VL+L", tau2NProng, tau2Pt, tau2Eta)
              * GetFakeFactor(level, "VL+L=>M+T", "M+T",  tau1NProng, tau1Pt, tau1Eta) );
        ff *= -0.5;
    } else if(m_currentregion == 2) {
        ff *= GetFakeFactor(level, "VL+L=>M+T", "M+T", tau1NProng, tau1Pt, tau1Eta);
    } else if(m_currentregion == 3) {
        ff *= GetFakeFactor(level, "VL+L=>M+T", "M+T", tau2NProng, tau2Pt, tau2Eta);
    }
    return ff;

}

double FakeFactorManager::ComputeFFWeight_AtLeastOneTightTau(std::string level) {

    // Compute and return the FF weight for a target selection with the two leading taus fulfilling
    // signal identification (medium) *AND* with at least one of them also fulfilling tight identification.

    double ff = 1.0;
    if (m_currentregion == 1) {
        ff *= ( GetFakeFactor(level, "VL+L=>T", "VL+L", tau1NProng, tau1Pt, tau1Eta)
              * GetFakeFactor(level, "VL+L=>T", "T",    tau2NProng, tau2Pt, tau2Eta) );
        ff += ( GetFakeFactor(level, "VL+L=>T", "VL+L", tau2NProng, tau2Pt, tau2Eta)
              * GetFakeFactor(level, "VL+L=>T", "T",    tau1NProng, tau1Pt, tau1Eta) );
        ff *= -0.5;
    } else if(m_currentregion == 2) {
        ff *= GetFakeFactor(level, "VL+L=>T", "M+T", tau1NProng, tau1Pt, tau1Eta);
    } else if(m_currentregion == 3) {
        ff *= GetFakeFactor(level, "VL+L=>T", "M+T", tau2NProng, tau2Pt, tau2Eta);
    }
    return ff;

}

double FakeFactorManager::ComputeFFWeight_AtLeastTwoTightTaus(std::string level) {

    // Compute and return the FF weight for a target selection with the two leading taus fulfilling
    // signal identification (medium) *AND* tight identification.

    double ff = 1.0;
    if (m_currentregion == 1) {
        ff *= ( GetFakeFactor(level, "VL+L=>T", "VL+L", tau1NProng, tau1Pt, tau1Eta)
              * GetFakeFactor(level, "VL+L=>T", "T",    tau2NProng, tau2Pt, tau2Eta) );
        ff += ( GetFakeFactor(level, "VL+L=>T", "VL+L", tau2NProng, tau2Pt, tau2Eta)
              * GetFakeFactor(level, "VL+L=>T", "T",    tau1NProng, tau1Pt, tau1Eta) );
        ff *= -0.5;
    } else if(m_currentregion == 2) {
        ff *= GetFakeFactor(level, "VL+L=>T", "T", tau1NProng, tau1Pt, tau1Eta);
    } else if(m_currentregion == 3) {
        ff *= GetFakeFactor(level, "VL+L=>T", "T", tau2NProng, tau2Pt, tau2Eta);
    }
    return ff;

}

double FakeFactorManager::ComputeFFWeight_AtLeastTwoTightTaus_TightID(std::string level) {

    // Compute and return the FF weight for a target selection with the two leading taus fulfilling
    // signal identification (medium) *AND* tight identification.
    // Medium taus are included into anti-ID tau definition!

    double ff = 1.0;
    if (m_currentregion == 1) {
        ff *= ( GetFakeFactor(level, "VL+L+M=>T", "VL+L+M", tau1NProng, tau1Pt, tau1Eta)
              * GetFakeFactor(level, "VL+L+M=>T", "T",    tau2NProng, tau2Pt, tau2Eta) );
        ff += ( GetFakeFactor(level, "VL+L+M=>T", "VL+L+M", tau2NProng, tau2Pt, tau2Eta)
              * GetFakeFactor(level, "VL+L+M=>T", "T",    tau1NProng, tau1Pt, tau1Eta) );
        ff *= -0.5;
    } else if(m_currentregion == 2) {
        ff *= GetFakeFactor(level, "VL+L+M=>T", "T", tau1NProng, tau1Pt, tau1Eta);
    } else if(m_currentregion == 3) {
        ff *= GetFakeFactor(level, "VL+L+M=>T", "T", tau2NProng, tau2Pt, tau2Eta);
    }
    return ff;

}
