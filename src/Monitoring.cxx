#include "../include/Monitoring.h"
#include "../include/DirectStau_Store.h"


// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"
#include <utility>
#include <iostream>


TFile* Monitoring::m_destination;
std::vector<TH1D> Monitoring::m_histos;
std::vector<std::string> Monitoring::m_cutflow;
std::map<std::string, int> Monitoring::m_cutflow_raw;
std::map<std::string, float> Monitoring::m_cutflow_weighted;


void Monitoring::Initialize() {
    m_cutflow.push_back("initial");
    m_cutflow_raw.insert( std::pair<std::string, int>("initial", 0) );
    m_cutflow_weighted.insert( std::pair<std::string, float>("initial", 0.) );
}

void Monitoring::AddHistogram(TH1D histo, std::string name) {
    std::vector<TH1D>::iterator it = std::find_if(m_histos.begin(),
                                                  m_histos.end(),
                                                  [&](const TH1D h)
    {return boost::algorithm::ends_with(h.GetName(), name);});
    if (it != m_histos.end())
        m_histos[std::distance(m_histos.begin(), it)].Add(dynamic_cast<TH1D*>(histo.Clone(name.c_str())));
    else
        m_histos.push_back(*dynamic_cast<TH1D*>(histo.Clone(name.c_str())));
}

void Monitoring::InitializeCutflow(std::initializer_list<std::string> cutnames) {
    if (!m_cutflow.size() > 1) {
        for (std::string cutname : cutnames) {
            if (std::find(m_cutflow.begin(), m_cutflow.end(), cutname) == m_cutflow.end()) {
                m_cutflow.push_back(cutname);
                m_cutflow_raw.insert( std::pair<std::string, int>(cutname, 0) );
                m_cutflow_weighted.insert( std::pair<std::string, float>(cutname, 0.) );
            }
        }
    } else {
        LOG(ERROR) << "Cutflow already initialized!";
    }
}

void Monitoring::InitializeCut(const std::string cutname) {
    if (std::find(m_cutflow.begin(), m_cutflow.end(), cutname) == m_cutflow.end()) {
        m_cutflow.push_back(cutname);
        m_cutflow_raw.insert( std::pair<std::string, int>(cutname, 0) );
        m_cutflow_weighted.insert( std::pair<std::string, float>(cutname, 0.) );
    }
}

bool Monitoring::PassesCut(const bool cutexpression, const std::string cutname) {
    if (cutexpression) {
        m_cutflow_raw[cutname] += 1;
        m_cutflow_weighted[cutname] += Store::evt_weight;
    }
    return cutexpression;
}

void Monitoring::WriteToOutput(TFile* outfile) {
    int ncuts = m_cutflow.size();
    m_cutflow_raw["initial"] = Store::evt_count;
    m_cutflow_weighted["initial"] = Store::evt_count_weighted;

    std::string h_raw_name = "DSALoop_CutFlow";
    std::string h_weighted_name = "DSALoop_CutFlow_weighted";
    TH1D* h_raw = new TH1D(h_raw_name.c_str(), "", ncuts, -0.5, ncuts-0.5);
    TH1D* h_weighted = new TH1D(h_weighted_name.c_str(), "", ncuts, -0.5, ncuts-0.5);
    h_raw->Sumw2();
    h_weighted->Sumw2();

    int bin(1);
    for(std::string cutname : m_cutflow) {
        h_raw->SetBinContent(bin, m_cutflow_raw[cutname]);
        h_weighted->SetBinContent(bin, m_cutflow_weighted[cutname]);
        h_raw->GetXaxis()->SetBinLabel(bin, cutname.c_str());
        h_weighted->GetXaxis()->SetBinLabel(bin, cutname.c_str());
        bin++;
    }
    m_histos.push_back(*h_raw);
    if(!Store::isData)
        m_histos.push_back(*h_weighted);

    outfile->mkdir("Monitoring");
    outfile->cd("Monitoring");
    for (auto h : m_histos) {
        h.Write();
    }
}




