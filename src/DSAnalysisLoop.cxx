// C++ includes
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <iomanip>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

#include "../include/DSBase_Nominal.h"
#include "../include/DirectStau_Analysis.h"
#include "../include/DirectStau_Store.h"
#include "../include/WeightCalculator.h"
#include "../include/Monitoring.h"
#include "../include/RJigsaw.h"
#include "../include/FakeFactorManager.h"


// ROOT includes
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TList.h>
#include <TH1F.h>

#define _ELPP_NO_DEFAULT_LOG_FILE
#include "easylogging++.h"
INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv) {

    START_EASYLOGGINGPP(argc, argv);
    el::Configurations defaultConf;
    defaultConf.setToDefault();
    defaultConf.setGlobally(el::ConfigurationType::Format, "%fbase :: %level :: %msg");
    el::Loggers::reconfigureLogger("default", defaultConf);

    char hostname[HOST_NAME_MAX+1];
    gethostname(hostname, HOST_NAME_MAX+1);
    auto now_c = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    LOG(INFO) << std::put_time(std::localtime(&now_c), "%c") << " (" << getenv("USER") << "@" << hostname << ")";

    // Default run options:
    std::string inputFileListString;
    std::string outputFileName("DSALoop.output.root");
    std::string processName("DirectStau");
    std::string metadataTreeName("MetaDataTree");
    std::string treeName("Nominal");
    Store::LumiDef = {{0, 1.0}};

    // Lumi map keys:
    // 0 = unknown
    // 1 = mc16a (data15+16, true pileup profile)
    // 2 = mc16b (no match)
    // 3 = mc16c (data17, prelim. pileup profile)
    // 4 = mc16d (data17, true pileup profile)
    // 5 = mc16e
    // 6 = mc16f

    Store::stopAfterNevts -1; // process everything
    Store::noTrigger = false;
    Store::MuSelection = false;
    Store::signalTauID = -1; // equals XAMPP signal tau WP
    Store::selectAntiTaus = false;
    Store::fakefactorfile;
    Store::FFMTargetSelection = "Default"; // at least two signal taus
    Store::overrideMetadata;
    Store::ttbarMETSlicing = false;

    for (int i=1; i<argc; i++) {
        if (strncmp(argv[i], "-ifiles", 7) == 0) inputFileListString = std::string(argv[i]).erase(0, 8);
        if (strncmp(argv[i], "-ofile", 6) == 0) outputFileName = std::string(argv[i]).erase(0, 7);
        if (strncmp(argv[i], "-process", 8) == 0) processName = std::string(argv[i]).erase(0, 9);
        if (strncmp(argv[i], "-mtree", 6) == 0) metadataTreeName = std::string(argv[i]).erase(0, 7);
        if (strncmp(argv[i], "-ntree", 6) == 0) treeName = std::string(argv[i]).erase(0, 7);
        if (strncmp(argv[i], "-lumiMC16a", 10) == 0) Store::LumiDef[1] = std::stod(std::string(argv[i]).erase(0, 11));
        if (strncmp(argv[i], "-lumiMC16d", 10) == 0) Store::LumiDef[4] = std::stod(std::string(argv[i]).erase(0, 11));
        if (strncmp(argv[i], "-lumiMC16e", 10) == 0) Store::LumiDef[5] = std::stod(std::string(argv[i]).erase(0, 11));
        if (strncmp(argv[i], "--nevents", 9) == 0) Store::stopAfterNevts = std::stoi(std::string(argv[i]).erase(0, 10));
        if (strncmp(argv[i], "--noTrigger", 11) == 0) Store::noTrigger = true;
        if (strncmp(argv[i], "--MuonSelection", 15) == 0) Store::MuSelection = true;
        if (strncmp(argv[i], "--signalTauID", 13) == 0) Store::signalTauID = std::stoi(std::string(argv[i]).erase(0, 14));
        if (strncmp(argv[i], "--selectAntiTaus", 16) == 0) Store::selectAntiTaus = true;
        if (strncmp(argv[i], "--applyFakeFactors", 18) == 0) {
            Store::fakefactorfile = std::string(argv[i]).erase(0, 19);
            Store::selectAntiTaus = true;
        }
        if (strncmp(argv[i], "--FFMTargetSelection", 20) == 0) Store::FFMTargetSelection = std::string(argv[i]).erase(0, 21);
        if (strncmp(argv[i], "--overrideMetadata", 18) == 0) Store::overrideMetadata = std::string(argv[i]).erase(0, 19);
        if (strncmp(argv[i], "--ttbarMETSlicing", 17) == 0) Store::ttbarMETSlicing = true;
    }

    std::map<int,std::string> tauWPDict = {std::make_pair<int,std::string>(-1, "XAMPP definition"),
                                           std::make_pair<int,std::string>(1,  "Loose"),
                                           std::make_pair<int,std::string>(2,  "Medium"),
                                           std::make_pair<int,std::string>(3,  "Tight")};

//    if (Store::signalTauID != -1) {
//        // This messes up the FF Manager. Until I come up with a solution please stick with the XAMPP default (medium).
//        LOG(ERROR) << "I'm sorry Dave, I'm afraid I can't do that (--signalTauID option will be ignored).";
//        Store::signalTauID == -1;
//    }

    LOG(INFO) << "Metadata tree : " << metadataTreeName;
    LOG(INFO) << "Nominal tree : " << treeName;
    LOG(INFO) << "Number of events to be processed : " << Store::stopAfterNevts;
    LOG(INFO) << "Disregard trigger : " << Store::noTrigger;
    LOG(INFO) << "Apply muon CR/VR selection : " << Store::MuSelection;
    LOG(INFO) << "Signal tau ID : " << tauWPDict[Store::signalTauID];
    LOG(INFO) << "Select anti taus : " << Store::selectAntiTaus;
    LOG(INFO) << "Apply fake factors: " << !Store::fakefactorfile.empty();
    LOG(INFO) << "FFM target selection: " << Store::FFMTargetSelection;
    LOG(INFO) << "Override metadata: " << !Store::overrideMetadata.empty();
    LOG(INFO) << "Scale to luminosity : " << Store::LumiDef[1] << " fb^-1 (MC16a), " << Store::LumiDef[4] << " fb^-1 (MC16d), " << Store::LumiDef[5] << " fb-1 (MC16e)";
    if(Store::ttbarMETSlicing) LOG(INFO) << "Overlap of MET sliced ttbar samples will be removed.";

    if(inputFileListString == ""){
        LOG(ERROR) << "No input files specified! Exiting...";
        return 1;
    } else {

        LOG(INFO) << "Given input " << inputFileListString;
        std::vector<std::string> inputFileList;
        // Determine if input is a directory 
        struct stat s;
        if( stat(inputFileListString.c_str(),&s) == 0 ) //its a file or a directory
        {
            LOG(INFO) << "Think input is a single file or directory";
            if( s.st_mode & S_IFDIR )
            {
                LOG(INFO) << "Think input is a directory";
                //it's a directory
                DIR *dir;
                struct dirent *en;
                dir=opendir(inputFileListString.c_str());
                
                if (dir) {
                    while ((en = readdir(dir)) != NULL) {
                        //pdir=readdir(dir);
                        //std::cout<< pdir->d_name<<std::endl;
                        std::string file_name = en->d_name;
                        std::string root_ending = ".root";
                        LOG(INFO) << "Asessing file " << file_name;
                        bool include;
                        if (file_name.length() >= root_ending.length()) {
                            include = (0 == file_name.compare (file_name.length() - root_ending.length(), root_ending.length(), root_ending));
                        } else {
                            include =  false;
                        }
                        if (include) { 
                            LOG(INFO) << "Adding file " << file_name;
                            inputFileList.push_back(inputFileListString + file_name);
                            LOG(INFO) << "Added file " << file_name;
                        }
                    }
                    closedir(dir);
                }                
                LOG(INFO) << "Closing directory";
            }
            else if( s.st_mode & S_IFREG )
            {
                // it's just a file?
                inputFileList.push_back(inputFileListString);
            }
            else 
            {
                // Unknown error
                LOG(ERROR) << "Not sure what the input is, Exiting";
                return 1;
            }
        } else {
            
            // Could be a list of files
            std::istringstream iss(inputFileListString);
            std::copy(std::istream_iterator<std::string>(iss),
                    std::istream_iterator<std::string>(),
                    std::back_inserter(inputFileList));
        }
        
        LOG(INFO) << "Got " << inputFileList.size() <<  " input files. ";
        LOG(INFO) << "Input files : ";
        for (const std::string filepath : inputFileList) {
            LOG(INFO) << "  * " << filepath;
        }


        TChain *metadataTreeChain = new TChain(metadataTreeName.c_str());
        for (std::string filepath : inputFileList) {
            metadataTreeChain->Add(filepath.c_str());
            
            // Check meta data treename in file
            TFile *f = new TFile (filepath.c_str());
            bool tree_exists = f->GetListOfKeys()->Contains(metadataTreeName.c_str());
            if (!tree_exists) {
                LOG(ERROR) << "Meta data Tree " << metadataTreeName.c_str() << " not found in file " << filepath.c_str();
                return EXIT_FAILURE;
            }
            delete f;
        }
        std::string commonStem("Staus_");
        //TChain *treeChain = new TChain(commonStem.c_str() + treeName.c_str());
        TChain *treeChain = new TChain((commonStem + treeName).c_str());
        for (std::string filepath : inputFileList) {
            
            // Check treename in file
            TFile *f = new TFile (filepath.c_str());
            bool tree_exists = f->GetListOfKeys()->Contains((commonStem+treeName).c_str());
            if (!tree_exists) {
                LOG(ERROR) << "Systematic Tree " << (commonStem+treeName).c_str() << " not found in file " << filepath.c_str();
                return EXIT_FAILURE;
            }
            delete f;

            treeChain->Add(filepath.c_str());
        }

        WeightCalculator::Initialize(metadataTreeChain);

        FakeFactorManager::Initialize(treeChain);

        Monitoring::Initialize();

        for (const std::string filepath : inputFileList) {
            TFile* tfile = TFile::Open(filepath.c_str());
            TDirectory* tdir;
            if (tfile->GetDirectory("Histos_tautau_Nominal/InfoHistograms"))
                tdir = tfile->GetDirectory("Histos_tautau_Nominal/InfoHistograms");
            else if (tfile->GetDirectory("Histos_#tau#tau_Nominal/InfoHistograms"))
                tdir = tfile->GetDirectory("Histos_#tau#tau_Nominal/InfoHistograms");
            else continue;
            for (TObject* tobj : *(tdir->GetListOfKeys())) {
                if (gROOT->GetClass( tdir->Get(tobj->GetName())->ClassName() )->InheritsFrom(TH1D::Class())) {
                    TH1D* h = dynamic_cast<TH1D*>(tdir->Get(tobj->GetName()));
                    h->SetDirectory(0);
                    std::string hname = std::string(h->GetName());
                    hname = std::regex_replace(hname, std::regex("DSID_\\d*"), "");
                    Monitoring::AddHistogram(*h, std::string("XAMPP") + hname);
                }
            }
        }
        

        RJigsaw::Initialize();

        Store::outputFile = new TFile(outputFileName.c_str(), "RECREATE");
        Store::processName = processName;
        //Store::treeStem = treeName;

        // Replace these so can combine into a single systematic (for samples that have mixed AFII and full sim DSIDs)
        std::string treeStem = std::regex_replace(treeName, std::regex("_AFII"), "");
        treeStem = std::regex_replace(treeStem, std::regex("_MC16"), "");
        Store::treeStem = treeStem;

        LOG(INFO) << "Output file : " << outputFileName;

        DSBase_Nominal* loop_nominal = new DSBase_Nominal(treeChain);
        loop_nominal->Loop();

        Monitoring::WriteToOutput(Store::outputFile);

        Store::outputFile->Close();

        RJigsaw::WriteRFPlots(outputFileName);

    }
    
    
    return EXIT_SUCCESS;
}
