#include "../include/TriggerManager.h"

// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"

using namespace Trigger;

bool Manager::m_passthrough = false;
std::vector<Set*> Manager::m_trigsets({});

void Manager::Initialize(std::initializer_list<Set*> sets) {

    m_trigsets.clear();

    if (sets.size() > 0) {
//        LOG(INFO) << "The following triggers have been requested:";
        for (Set* set : sets) {
            if (std::find(m_trigsets.begin(), m_trigsets.end(), set) == m_trigsets.end()) {
                m_trigsets.push_back(set);
//                LOG(INFO) << "  * " << set->GetName();
            }
        }
    } else {
//        LOG(INFO) << "No triggers have been requested.";
        m_passthrough = true;
    }

}

bool Manager::PassedTrigDec() {
    return (m_passthrough || std::any_of(m_trigsets.begin(), m_trigsets.end(), [](Set* set){ return set->PassedTrigDec(); }));
}

bool Manager::IsTrigMatched() {
    return (m_passthrough || std::any_of(m_trigsets.begin(), m_trigsets.end(), [](Set* set){ return set->PassedTrigDec() && set->IsTrigMatched(); }));
}

bool Manager::IsInPlateau() {
    return (m_passthrough || std::any_of(m_trigsets.begin(), m_trigsets.end(), [](Set* set){ return set->PassedTrigDec() && set->IsTrigMatched() && set->IsInPlateau(); }));
}
