#include "../include/WeightCalculator.h"


// Get current instance of ELPP logger
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "default"
#endif
#include "../include/easylogging++.h"

#include <utility>
#include <iostream>

std::map<int,std::map<int,std::vector<int>>> WeightCalculator::m_runNmbrDSIDProcessIDsMap;
std::vector<int> WeightCalculator::m_DSIDs({});
std::map<int,std::map<int, std::map<int,double>>> WeightCalculator::m_runNmbrDSIDFSXSecMap;
std::map<int,std::map<int, std::map<int,double>>> WeightCalculator::m_runNmbrDSIDFSFilterEffMap;
std::map<int,std::map<int, std::map<int,double>>> WeightCalculator::m_runNmbrDSIDFSkFactorMap;
std::map<int,std::map<int, std::map<int,int>>>    WeightCalculator::m_runNmbrDSIDFSTotalEvtsMap;
std::map<int,std::map<int, std::map<int,int>>>    WeightCalculator::m_runNmbrDSIDFSProcessedEvtsMap;
std::map<int,std::map<int, std::map<int,double>>> WeightCalculator::m_runNmbrDSIDFSSumWMap;
std::map<int,std::map<int, std::map<int,double>>> WeightCalculator::m_runNmbrDSIDFSSumW2Map;
Bool_t   WeightCalculator::m_isData;
UInt_t   WeightCalculator::m_mcChannelNumber;
UInt_t   WeightCalculator::m_runNumber;
UInt_t   WeightCalculator::m_DSID;
UInt_t   WeightCalculator::m_ProcessID;
Double_t WeightCalculator::m_xSection;
Double_t WeightCalculator::m_kFactor;
Double_t WeightCalculator::m_FilterEfficiency;
Long64_t WeightCalculator::m_TotalEvents;
Double_t WeightCalculator::m_TotalSumW;
Double_t WeightCalculator::m_TotalSumW2;
Long64_t WeightCalculator::m_ProcessedEvents;
std::set<unsigned int> *WeightCalculator::m_ProcessedLumiBlocks;
std::set<unsigned int> *WeightCalculator::m_TotalLumiBlocks;

std::vector<int> WeightCalculator::m_SherpaDSIDs = {
    345705, 345706, 363355, 363356, 363357, 363358, 363359, 363360,
    363489, 363494, 364100, 364101, 364102, 364103, 364104, 364105,
    364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113,
    364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121,
    364122, 364123, 364124, 364125, 364126, 364127, 364128, 364129,
    364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137,
    364138, 364139, 364140, 364141, 364142, 364143, 364144, 364145,
    364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153,
    364154, 364155, 364156, 364157, 364158, 364159, 364160, 364161,
    364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169,
    364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177,
    364178, 364179, 364180, 364181, 364182, 364183, 364184, 364185,
    364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193,
    364194, 364195, 364196, 364197, 364198, 364199, 364200, 364201,
    364202, 364203, 364204, 364205, 364206, 364207, 364208, 364209,
    364210, 364211, 364212, 364213, 364214, 364215, 364242, 364243,
    364244, 364245, 364246, 364247, 364248, 364249, 364250, 364253,
    364254, 364255, 364283, 364284, 364285, 364286, 364287, 407311,
    407312, 407313, 407314, 407315
};

std::vector<int> WeightCalculator::m_DijetDSIDs = {
    364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707,
    364708, 364709, 364710, 364711, 364712
};


void WeightCalculator::Initialize(TChain *fChain){

    m_isData           = false;
    m_mcChannelNumber  = -1;
    m_runNumber        = -1;
    m_DSID             = -1;
    m_ProcessID        = -1;
    m_xSection         = -1.0;
    m_kFactor          = -1.0;
    m_FilterEfficiency = -1.0;
    m_TotalEvents      = -1;
    m_TotalSumW        = -1.0;
    m_TotalSumW2       = -1.0;
    m_ProcessedEvents  = -1;

    m_ProcessedLumiBlocks = nullptr;
    m_TotalLumiBlocks     = nullptr;


//    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("isData", &m_isData);
    fChain->GetEntry(0);
    isData = bool(m_isData);

    if (m_isData) {
        LOG(INFO) << "Input file contains real data";
        fChain->SetBranchAddress("ProcessedLumiBlocks", &m_ProcessedLumiBlocks);
        fChain->SetBranchAddress("TotalLumiBlocks",     &m_TotalLumiBlocks    );
    } else {
        LOG(INFO) << "Input file contains simulated data";
        fChain->SetBranchAddress("mcChannelNumber",  &m_mcChannelNumber );
        fChain->SetBranchAddress("ProcessID",        &m_ProcessID       );
        fChain->SetBranchAddress("xSection",         &m_xSection        );
        fChain->SetBranchAddress("kFactor",          &m_kFactor         );
        fChain->SetBranchAddress("FilterEfficiency", &m_FilterEfficiency);
        fChain->SetBranchAddress("TotalSumW",        &m_TotalSumW       );
        fChain->SetBranchAddress("TotalSumW2",       &m_TotalSumW2      );
    }
    fChain->SetBranchAddress("runNumber",       &m_runNumber      );
    fChain->SetBranchAddress("TotalEvents",     &m_TotalEvents    );
    fChain->SetBranchAddress("ProcessedEvents", &m_ProcessedEvents);

    LOG(INFO) << "Processing meta data... ";

    // loop over the metadata tree
    for (Long64_t i=0; i<fChain->GetEntries(); i++) {

        
        // DMJ Hack to get things loaded
        /*
        if (i == 0) m_ProcessID = 206; 
        else if (i == 1) m_ProcessID = 207; 
        */
        fChain->GetEntry(i);
        if(m_isData != isData) {
            LOG(FATAL) << "Found data and MC entries in meta data! Please check your input.";
        }

        if(isData) {

            m_DSID = m_runNumber;

        } else {

            m_DSID = m_mcChannelNumber;
            if (m_runNumber != 284500 && m_runNumber != 300000 && m_runNumber != 310000) {
                LOG(ERROR) << "Found unknown MC runNumber '" << m_runNumber << "' in MetaDataTree. "
                           << "Cannot associate entry to any known MC subcampaign!";
            }
            
            
            // DMJ: This doesn't work when not over-riding meta data
            //overrideMetadata = ""; // hardcoding to read meta data
            if (!overrideMetadata.empty()) {
                std::cout << "Attempting to read meta data info" << std::endl;
                ReadMetaInfoFromTxtFile({overrideMetadata});
            }
            
            if (m_xSection < 0) {

                // If no cross section is found read info from text file
                std::cout << "Didn't find xsec in overwrite file for m_mcChannelNumber " << m_mcChannelNumber  << std::endl;
                
                ReadMetaInfoFromTxtFile({"$DSALOOP_DIR/productions/mc15_13TeV.DirectStau_DSID-FS-xsec-kfac-filteff.txt",
                                         "$DSALOOP_DIR/productions/mc16_13TeV.Backgrounds_DSID-physshort-xec-kfac-filteff.txt"});
            }
            if (m_xSection < 0) {
                LOG(WARNING) << "No cross section found (xSection = "
                             << m_xSection << ", ProcessID = " << m_ProcessID << ")";
            }
            if (m_kFactor < 0) {
                LOG(WARNING) << "No k-factor found (kFactor = "
                             << m_kFactor << ", ProcessID = " << m_ProcessID << ")";
            }
            if (m_FilterEfficiency < 0) {
                LOG(WARNING) << "No filter efficiency found (FilterEfficiency = "
                             << m_FilterEfficiency << ", ProcessID = " << m_ProcessID << ")";
            }
//            std::cout << "mcChannelNumber  =" << m_mcChannelNumber  << std::endl;
//            std::cout << "ProcessID        =" << m_ProcessID        << std::endl;
//            std::cout << "xSection         =" << m_xSection         << std::endl;
//            std::cout << "kFactor          =" << m_kFactor          << std::endl;
//            std::cout << "FilterEfficiency =" << m_FilterEfficiency << std::endl;
//            std::cout << "TotalSumW        =" << m_TotalSumW        << std::endl;
//            std::cout << "TotalSumW2       =" << m_TotalSumW2       << std::endl;
        }
//        std::cout << "runNumber        =" << m_runNumber       << std::endl;
//        std::cout << "TotalEvents      =" << m_TotalEvents     << std::endl;
//        std::cout << "ProcessedEvents  =" << m_ProcessedEvents << std::endl;
//        std::cout << std::endl;

        if ( std::find(m_DSIDs.begin(), m_DSIDs.end(), m_DSID) == m_DSIDs.end() ) {
            m_DSIDs.push_back(m_DSID);
        }

        if ( !isData && std::find(m_runNmbrDSIDProcessIDsMap[m_runNumber][m_DSID].begin(), m_runNmbrDSIDProcessIDsMap[m_runNumber][m_DSID].end(), m_ProcessID) != m_runNmbrDSIDProcessIDsMap[m_runNumber][m_DSID].end() ) {

            m_runNmbrDSIDFSTotalEvtsMap[m_runNumber][m_DSID][m_ProcessID]     += m_TotalEvents;
            m_runNmbrDSIDFSProcessedEvtsMap[m_runNumber][m_DSID][m_ProcessID] += m_ProcessedEvents;
            m_runNmbrDSIDFSSumWMap[m_runNumber][m_DSID][m_ProcessID]          += m_TotalSumW;
            m_runNmbrDSIDFSSumW2Map[m_runNumber][m_DSID][m_ProcessID]         += m_TotalSumW2;

            if (m_xSection != m_runNmbrDSIDFSXSecMap[m_runNumber][m_DSID][m_ProcessID]) {
                LOG(ERROR) << "Got different values for cross section: " << m_xSection
                           << " != " << m_runNmbrDSIDFSXSecMap[m_runNumber][m_DSID][m_ProcessID]
                           << " (ProcessID = " << m_ProcessID << ")";
            }
            if (m_FilterEfficiency != m_runNmbrDSIDFSFilterEffMap[m_runNumber][m_DSID][m_ProcessID]) {
                LOG(ERROR) << "Got different values for filter efficiency: " << m_FilterEfficiency
                           << " != " << m_runNmbrDSIDFSFilterEffMap[m_runNumber][m_DSID][m_ProcessID]
                           << " (ProcessID = " << m_ProcessID << ")";
            }
            if (m_kFactor != m_runNmbrDSIDFSkFactorMap[m_runNumber][m_DSID][m_ProcessID]) {
                LOG(ERROR) << "Got different values for k-factor: " << m_kFactor
                           << " != " << m_runNmbrDSIDFSkFactorMap[m_runNumber][m_DSID][m_ProcessID]
                           << " (ProcessID = " << m_ProcessID << ")";

            }

        } else {

            m_runNmbrDSIDFSTotalEvtsMap[m_runNumber][m_DSID].insert     ( std::pair<int, int>   (m_ProcessID, m_TotalEvents) );
            m_runNmbrDSIDFSProcessedEvtsMap[m_runNumber][m_DSID].insert ( std::pair<int, int>   (m_ProcessID, m_ProcessedEvents) );
            m_runNmbrDSIDFSSumWMap[m_runNumber][m_DSID].insert          ( std::pair<int, double>(m_ProcessID, m_TotalSumW) );
            m_runNmbrDSIDFSSumW2Map[m_runNumber][m_DSID].insert         ( std::pair<int, double>(m_ProcessID, m_TotalSumW2) );

            m_runNmbrDSIDFSXSecMap[m_runNumber][m_DSID].insert          ( std::pair<int, double>(m_ProcessID, m_xSection) );
            m_runNmbrDSIDFSFilterEffMap[m_runNumber][m_DSID].insert     ( std::pair<int, double>(m_ProcessID, m_FilterEfficiency) );
            m_runNmbrDSIDFSkFactorMap[m_runNumber][m_DSID].insert       ( std::pair<int, double>(m_ProcessID, m_kFactor) );

            m_runNmbrDSIDProcessIDsMap[m_runNumber][m_DSID].push_back(m_ProcessID);

        }
        
        switch (m_runNumber) {
            case 284500: MC16SubCampaign = 1; // MC16a
                         break;
            case 300000: MC16SubCampaign = 4; // MC16d
                         break;
            case 310000: MC16SubCampaign = 5; // MC16e
                         break;
            
            default:     MC16SubCampaign = 0; // unknown
                         break;
        }


    }

//    if (m_DSIDs.size() > 1) {
//        LOG(WARNING) << "Input chain consists of multiple DSIDs!";
//    }
    

}

void WeightCalculator::Execute() {

    //std::cout << "SUSYFinalState " << SUSYFinalState << std::endl;
    evt_weight = 1.0;
    evt_weight_noTrigWeight = 1.0;
    evt_weight_NoSys = 1.0;
    evt_weight_noPRW = 1.0;
    evt_weight_noFF = 1.0;

    evt_weight_ditauMET = 1.0;
    evt_weight_asymditau = 1.0;
    evt_weight_singleMU = 1.0;

    double evt_weight_NoGen = 1.0;
    
    MC16SubCampaign = -1;

    if (isData) {

        evt_weight *= jetTrigger_weight;

    } else {

        
        switch (runNumber) {
            case 284500: MC16SubCampaign = 1; // MC16a
                         break;
            case 300000: MC16SubCampaign = 4; // MC16d
                         break;
            case 310000: MC16SubCampaign = 5; // MC16e
                         break;
            
            default:     MC16SubCampaign = 0; // unknown
                         break;
        }
       

        luminosity = LumiDef[MC16SubCampaign];

        if (std::find(m_SherpaDSIDs.begin(), m_SherpaDSIDs.end(), mcChannelNumber) != m_SherpaDSIDs.end()
            && fabs(GenWeight) > 100.) {
            // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BosonJetsFocusGroup#Treating_pathological_high_weigh
            GenWeight = 1.;
        }

        evt_weight *= GenWeight*TauWeight*JetWeight*MuoWeight*EleWeight*luminosity*1.0e3;
        
        evt_weight_NoGen *= TauWeight*JetWeight*MuoWeight*EleWeight*luminosity*1.0e3;
        
        evt_weight_NoSys *= GenWeight*luminosity*1.0e3;

        xsec          = m_runNmbrDSIDFSXSecMap[runNumber][mcChannelNumber][SUSYFinalState];
        filtereff     = m_runNmbrDSIDFSFilterEffMap[runNumber][mcChannelNumber][SUSYFinalState];
        kfactor       = m_runNmbrDSIDFSkFactorMap[runNumber][mcChannelNumber][SUSYFinalState];
        processedevts = m_runNmbrDSIDFSProcessedEvtsMap[runNumber][mcChannelNumber][SUSYFinalState];
        totalevts     = m_runNmbrDSIDFSTotalEvtsMap[runNumber][mcChannelNumber][SUSYFinalState];
        sumw          = m_runNmbrDSIDFSSumWMap[runNumber][mcChannelNumber][SUSYFinalState];
        sumw2         = m_runNmbrDSIDFSSumW2Map[runNumber][mcChannelNumber][SUSYFinalState];

        //std::cout << "xsec = " << xsec << std::endl;

        evt_weight *= kfactor*filtereff*xsec;
        evt_weight_NoGen *= kfactor*filtereff*xsec;
        evt_weight_NoSys *= kfactor*filtereff*xsec;

        if (std::find(m_DijetDSIDs.begin(), m_DijetDSIDs.end(), mcChannelNumber) != m_DijetDSIDs.end()) {
            // Special dijet MC reweighting:
            // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetStudies2012#Weighting_scheme_and_MC_quality
            // https://cds.cern.ch/record/1370089
            evt_weight /= totalevts;
            evt_weight_NoGen /= totalevts;
            evt_weight_NoSys /= totalevts;
        } else {
            evt_weight /= sumw;
            evt_weight_NoGen /= sumw;
            evt_weight_NoSys /= sumw;
        }

        evt_weight_noPRW = evt_weight;
        
        evt_weight *= muWeight;
        evt_weight_noTrigWeight = evt_weight; 

        // Pile up weight
        evt_weight_noFF = evt_weight;

    }

    //TODO need to add these in to the total event weight
    
    // MC16a+d, // Data 2015-2017
    
    std::vector<std::reference_wrapper<double>> evt_weight_mc16ad_AsymDitauTrigSysts = {
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down,
        evt_weight_TauTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up,
    };

    std::vector<double> mc16ad_AsymDitauTrigWeights = {
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down,
        TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up,
    };

    std::vector<std::reference_wrapper<double>> evt_weight_SingleMuSysts = {
        evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down,
        evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up,
        evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down,
        evt_weight_MuoTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up,
    };

    std::vector<double> SingleMuSystWeights = {
        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down,
        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up,
        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down,
        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up,
    };


    if (MC16SubCampaign == 1 || MC16SubCampaign == 4 || (isData && runNumber < 348885)) {

        // ditau + MET
        TrigWeight_ditauMET = TauWeightTrigHLT_tau35_medium1_tracktwo*TauWeightTrigHLT_tau25_medium1_tracktwo;
        evt_weight_ditauMET  = evt_weight*TrigWeight_ditauMET;

        // asym ditau
        TrigWeight_asymditau = TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo*TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
        evt_weight_asymditau = evt_weight*TrigWeight_asymditau;

        for (int i=0; i<evt_weight_mc16ad_AsymDitauTrigSysts.size(); i++) {
            evt_weight_mc16ad_AsymDitauTrigSysts[i].get() = evt_weight*TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo*mc16ad_AsymDitauTrigWeights[i];
        }
    
    // Data 2018, MC16e
    } else if ((MC16SubCampaign == 5) || (isData && runNumber >= 348885)) {

        // ditau + MET
        TrigWeight_ditauMET = TauWeightTrigHLT_tau60_medium1_tracktwoEF*TauWeightTrigHLT_tau25_medium1_tracktwoEF;
        evt_weight_ditauMET  = evt_weight*TrigWeight_ditauMET; 

        // asym ditau
        TrigWeight_asymditau = TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF*TauWeightTrigHLT_tau60_medium1_tracktwoEF;
        evt_weight_asymditau = evt_weight*TrigWeight_asymditau;
        
        // Trig not present -> don't consider uncertainty for these events
        for (int i=0; i<evt_weight_mc16ad_AsymDitauTrigSysts.size(); i++) {
            evt_weight_mc16ad_AsymDitauTrigSysts[i].get() = evt_weight_asymditau;
        }

    }
    
    // Single Muon
    TrigWeight_singleMU = MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50; 
    evt_weight_singleMU  = evt_weight*TrigWeight_singleMU;
    for (int i=0; i<evt_weight_SingleMuSysts.size(); i++) {
        evt_weight_SingleMuSysts[i].get() = evt_weight*SingleMuSystWeights[i];
    }

}

//void WeightCalculator::ComputeEvtWeightEleSys(double &evt_weight_EleSys, double EleWeight_EleSys, double evt_weight_NoEle ) {
    //evt_weight
    


void WeightCalculator::ComputeSystematicWeights() {

    std::vector<std::reference_wrapper<double>> evt_weight_EleSysts = {
        evt_weight_EleSys_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down,
        evt_weight_EleSys_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up,
        evt_weight_EleSys_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down,
        evt_weight_EleSys_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up,
        evt_weight_EleSys_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down,
        evt_weight_EleSys_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up,
    }; 

    std::vector<double> EleWeights = {
        EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down,
        EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up,
        EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down,
        EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up,
        EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down,
        EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up,
    };
        
    std::vector<std::reference_wrapper<double>> evt_weight_MuoSysts = {
        evt_weight_MuoSys_MUON_EFF_ISO_STAT__1down,
        evt_weight_MuoSys_MUON_EFF_ISO_STAT__1up,
        evt_weight_MuoSys_MUON_EFF_ISO_SYS__1down,
        evt_weight_MuoSys_MUON_EFF_ISO_SYS__1up,
        evt_weight_MuoSys_MUON_EFF_RECO_STAT__1down,
        evt_weight_MuoSys_MUON_EFF_RECO_STAT__1up,
        evt_weight_MuoSys_MUON_EFF_RECO_SYS__1down,
        evt_weight_MuoSys_MUON_EFF_RECO_SYS__1up,
        evt_weight_MuoSys_MUON_EFF_TTVA_STAT__1down,
        evt_weight_MuoSys_MUON_EFF_TTVA_STAT__1up,
        evt_weight_MuoSys_MUON_EFF_TTVA_SYS__1down,
        evt_weight_MuoSys_MUON_EFF_TTVA_SYS__1up,

    }; 
    std::vector<double> MuoWeights = {
        MuoWeight_MUON_EFF_ISO_STAT__1down,
        MuoWeight_MUON_EFF_ISO_STAT__1up,
        MuoWeight_MUON_EFF_ISO_SYS__1down,
        MuoWeight_MUON_EFF_ISO_SYS__1up,
        MuoWeight_MUON_EFF_RECO_STAT__1down,
        MuoWeight_MUON_EFF_RECO_STAT__1up,
        MuoWeight_MUON_EFF_RECO_SYS__1down,
        MuoWeight_MUON_EFF_RECO_SYS__1up,
        MuoWeight_MUON_EFF_TTVA_STAT__1down,
        MuoWeight_MUON_EFF_TTVA_STAT__1up,
        MuoWeight_MUON_EFF_TTVA_SYS__1down,
        MuoWeight_MUON_EFF_TTVA_SYS__1up,
    };
    
    std::vector<std::reference_wrapper<double>> evt_weight_TauSysts = {
        evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down,
        evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up,
        evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down,
        evt_weight_TauSys_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down,
        evt_weight_TauSys_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up,

    }; 
    std::vector<double> TauWeights = {
        TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1down,
        TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT__1up,
        TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1down,
        TauWeight_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down,
        TauWeight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up,
    };
    
    std::vector<std::reference_wrapper<double>> evt_weight_JetSysts = {
        evt_weight_JetSys_FT_EFF_B_systematics__1down,
        evt_weight_JetSys_FT_EFF_B_systematics__1up,
        evt_weight_JetSys_FT_EFF_C_systematics__1down,
        evt_weight_JetSys_FT_EFF_C_systematics__1up,
        evt_weight_JetSys_FT_EFF_Light_systematics__1down,
        evt_weight_JetSys_FT_EFF_Light_systematics__1up,
        evt_weight_JetSys_FT_EFF_extrapolation__1down,
        evt_weight_JetSys_FT_EFF_extrapolation__1up,
        evt_weight_JetSys_FT_EFF_extrapolation_from_charm__1down,
        evt_weight_JetSys_FT_EFF_extrapolation_from_charm__1up,
        evt_weight_JetSys_JET_JvtEfficiency__1down,
        evt_weight_JetSys_JET_JvtEfficiency__1up,


    }; 
    std::vector<double> JetWeights = {
        JetWeight_FT_EFF_B_systematics__1down,
        JetWeight_FT_EFF_B_systematics__1up,
        JetWeight_FT_EFF_C_systematics__1down,
        JetWeight_FT_EFF_C_systematics__1up,
        JetWeight_FT_EFF_Light_systematics__1down,
        JetWeight_FT_EFF_Light_systematics__1up,
        JetWeight_FT_EFF_extrapolation__1down,
        JetWeight_FT_EFF_extrapolation__1up,
        JetWeight_FT_EFF_extrapolation_from_charm__1down,
        JetWeight_FT_EFF_extrapolation_from_charm__1up,
        JetWeight_JET_JvtEfficiency__1down,
        JetWeight_JET_JvtEfficiency__1up,
    };
    
    std::vector<std::reference_wrapper<double>> evt_weight_muSysts = {
        evt_weight_muSys_PRW_DATASF__1down,
        evt_weight_muSys_PRW_DATASF__1up,

    }; 
    std::vector<double> muWeights = {
        muWeight_PRW_DATASF__1down,
        muWeight_PRW_DATASF__1up,
    };
    
    double evt_weight_NoEle = evt_weight_NoSys*MuoWeight*TauWeight*JetWeight*TrigWeight*muWeight; 

    double evt_weight_NoMuo = evt_weight_NoSys*EleWeight*TauWeight*JetWeight*TrigWeight*muWeight; 
    
    double evt_weight_NoTau = evt_weight_NoSys*EleWeight*MuoWeight*JetWeight*TrigWeight*muWeight; 
    
    double evt_weight_NoJet = evt_weight_NoSys*EleWeight*MuoWeight*TauWeight*TrigWeight*muWeight; 
    
    double evt_weight_Nomu = evt_weight_NoSys*EleWeight*MuoWeight*TauWeight*JetWeight*TrigWeight; 

    for (int i=0; i<evt_weight_EleSysts.size(); i++) {
        evt_weight_EleSysts[i].get() = evt_weight_NoEle * EleWeights[i];
    }
    for (int i=0; i<evt_weight_MuoSysts.size(); i++) {
        evt_weight_MuoSysts[i].get() = evt_weight_NoMuo * MuoWeights[i];
    }
    for (int i=0; i<evt_weight_TauSysts.size(); i++) {
        evt_weight_TauSysts[i].get() = evt_weight_NoTau * TauWeights[i];
    }
    for (int i=0; i<evt_weight_JetSysts.size(); i++) {
        evt_weight_JetSysts[i].get() = evt_weight_NoJet * JetWeights[i];
    }
    for (int i=0; i<evt_weight_muSysts.size(); i++) {
        //evt_weight_JetSysts[i].get() = evt_weight_Nomu * muWeights[i];
        evt_weight_muSysts[i].get() = evt_weight_Nomu * muWeights[i];
    }

}


void WeightCalculator::ReadMetaInfoFromTxtFile(std::initializer_list<std::string> filenames) {

    
    //m_ProcessID = 206; 
    //std::cout << "SUSYFinalState " << SUSYFinalState << std::endl;
    //std::cout << "m_DSID = " << m_DSID << std::endl;
    //std::cout << "m_ProcessID = " << m_ProcessID << std::endl;
    for (std::string filename : filenames) {

        if (filename[0] == '$') {
            std::string envvar = filename.substr(1, filename.find("/")-1);
            std::string expenvvar = std::getenv(envvar.c_str());
            filename = expenvvar + filename.substr(envvar.size()+1, filename.size());
        }

        std::cout << "Reading metadata from " << filename << std::endl;

        std::ifstream file(filename);
        std::string line;
        while (std::getline(file, line)) {
            if (line[0] != '#') {
                std::istringstream line_stream(line);

                int entry1 = -1; // dataset id
                line_stream >> entry1;
                //std:: cout << "entry1 " << entry1 << std::endl;
                if (entry1 != m_DSID) continue;
                
                
                std::string entry2; // 2nd entry can be physicsShort or SUSY FS depending on input
                std::string physshort;
                int fs = -1;
                line_stream >> entry2;
                std::cout << "Process ID " << m_ProcessID << std::endl;
                std::cout << "entry2 " << entry2 << std::endl;
                if (is_number(entry2)) {
                    fs = atoi(entry2.c_str());
                    if (fs != m_ProcessID) {
                        continue;
                    }
                } else {
                    physshort = entry2;
                }
                LOG(INFO) << "Metadata will be overriden by " << filename;

                line_stream >> m_xSection; // xsec in pb (like in XAMPP)
                line_stream >> m_kFactor;
                line_stream >> m_FilterEfficiency;
                std::cout << "found xsec " << m_xSection << std::endl;

            }
        }
    }

}

bool WeightCalculator::RemoveDatasetOverlap() {

    bool remove_evt = false;

    if (ttbarMETSlicing) {
        // PhPy8EG_A14_ttbar_hdamp258p75_nonallhad
        if (mcChannelNumber == 410470 && MET.Pt() > 200.0) remove_evt = true;
        // PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad
        if (mcChannelNumber == 407345 && (MET.Pt() < 200.0 || MET.Pt() > 300.0)) remove_evt = true;
        // PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad
        if (mcChannelNumber == 407346 && (MET.Pt() < 300.0 || MET.Pt() > 400.0)) remove_evt = true;
        // PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad
        if (mcChannelNumber == 407347 && MET.Pt() < 400.0) remove_evt = true;
    }

    return remove_evt;

}

bool is_number(const std::string& s) {
    return !s.empty() && std::find_if(s.begin(),
            s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}






